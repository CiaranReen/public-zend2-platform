<?php

class SearchController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new App_Model_Navigation();
        $settings           = Core_Settings_Models_Settings::getSettings();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop - Search');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';
        $this->view->languages 				= $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        /* Instantiate Models */
        $categoryModel  = new Core_Category_Models_Category();
        $productModel   = new Core_Product_Models_Product();
        $searchModel    = new Core_Search_Models_Search();
        /* Get Request Parameters */
        $search = '';
        if($this->getRequest()->getParam('search')){
            $search = (string) $this->getParam('search');
            $searchLog = new Core_Search_Models_SearchLog();
            $dateSearched = date("Y-m-d H:i:s");
            $searchLog->setSearchTerm($search)
                    ->setDateSearched($dateSearched);
            $searchLog->save();
        }
        $results = $searchModel->getSearchResults($search);
        $categoryId = 1;
        
        /* Get Data */
        $categoryProducts   = array();
        $categoryObject     = $categoryModel->findById($categoryId);
        $childCategories    = $categoryObject->getChildCategories();
        $mostPopular        = $productModel->getMostPopularProducts();
        
        if(empty($childCategories)) {
            $parentCategoryId       = $categoryObject->getParentId();
            $categoryModel2         = new Core_Category_Models_Category();
            $parentCategoryObject   = $categoryModel2->findById($parentCategoryId);
            $childCategories        = $parentCategoryObject->getChildCategories();
        }
        
        /* Set View Properties */
        $this->view->search             = $search;
        $this->view->childCategories    = $childCategories;
        $this->view->products           = $results;
        
    }


}

