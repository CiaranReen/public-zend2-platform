<?php

class AddressController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new App_Model_Navigation();
        $settings           = Core_Settings_Models_Settings::getSettings();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        else{
            $this->_redirect('/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop :: Address');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->categoryUrl            = '/category/index/id/';
        
    }

    public function indexAction()
    {
        /* Instantiate Models */
        $addressMapper  = new Core_Address_Models_AddressMapper();
        $addressModel   = new Core_Address_Models_Address();
        $userModel      = new Core_User_Models_User();
        
        /* Forms */
        $addressForm    = new Core_Address_Forms_AddressForm();
        
        /* Get Data */
        $user = $userModel->findById($userModel->getUserSession()->userId);
        if($this->getParam('type')){
            $addressType = (string) $this->getParam('type');
        }
        if($addressType === 'billing'){ }
        else{
            $addressType = 'delivery';
        }

        $addressPageUrl = '/address/index/type/'.$addressType;

        $getAddressFunction = 'get'.ucfirst($addressType).'Address'; //get address function name for user object e.g getBillingAddress()
        $setAddressIdFunction = 'set'.ucfirst($addressType).'AddressId'; //gets address function name for user object e.g setBillingId()
        $formDescription    = 'New ' . ucfirst($addressType).' Address'; //get form description e.g. Billing Address
        $addressForm->setDescription($formDescription); //set form description

        $address            = $user->$getAddressFunction(); //execute generated function e.g. $user->getBillingAddress() returns the Address model object
        if($address){
            //$addressForm->populate($address); //populate address form with address details if user has address
            $this->view->defaultAddress = $address;
        }

        $userAddresses = $addressModel->getAllUserAddresses($user->getId());

        //Select a default address
        if($this->getRequest()->getParam('select')){
            $selectId = (int) $this->getRequest()->getParam('select');
            $newAddress = $addressModel->findById($selectId);
            //check if the address is an instance of the address model and if the address belongs to the current user
            if($newAddress instanceof Core_Address_Models_Address && $newAddress->getUserId() === $user->getId()){
                $user->$setAddressIdFunction($newAddress->getId())->save();
                $this->_redirect('/checkout/');
            }
        }

        //Delete a user's previous address
        if($this->getRequest()->getParam('delete')){
            $deleteId = (int) $this->getRequest()->getParam('delete');
            $deleteAddress = $addressModel->findById($deleteId);
            //check if the address is an instance of the address model and if the address belongs to the current user
            if($deleteAddress instanceof Core_Address_Models_Address && $deleteAddress->getUserId() === $user->getId()){
                //check if the address being deleted is the users current address
                if($user->$getAddressFunction() && $user->$getAddressFunction()->getId() === $deleteAddress->getId()){
                    $this->view->errors = 'Could not delete your default address. Set a new default address to delete this.';
                }
                else{
                    $deleteAddress->setActive(0)->save();
                    $this->_redirect($addressPageUrl . '/');
                }

            }
        }

        //Get Post Data
        if(!$this->getRequest()->isPost()) {
			// Render the blank form

		} else if(!$addressForm->isValid($_POST)) {
            $this->view->formErrors = 'There were errors';

		} else {
			/*
             * Set Address model options to save. You do not need to set the address id since this was already set when it was got from the user.
             * If the user does not have an address the id would not be set and therefore it will be saved as a new entry
             */
            $newAddress = new Core_Address_Models_Address();
            $newAddress->setOptions(
                    array(
                        'lineOne'   => $addressForm->getValue('lineOne'),
                        'lineTwo'   => $addressForm->getValue('lineTwo'),
                        'city'      => $addressForm->getValue('city'),
                        'region'    => $addressForm->getValue('region'),
                        'postcode'  => $addressForm->getValue('postcode'),
                        'countryId' => $addressForm->getValue('countryId'),
                        'userId'    => $user->getId(),
                        'active'    => 1,
                    ));
            $addressMapper->save($newAddress);
            if (null === ($id = $newAddress->getId())){
                $addressId = $addressMapper->getDbTable()->getAdapter()->lastInsertId();
                $user->$setAddressIdFunction($addressId)->save();
            } 
            $this->_redirect('/checkout/'); // Redirect to admin homepage

		}

        $this->view->addressType = $addressType;
        $this->view->addressForm = $addressForm;
        $this->view->addressPageUrl = $addressPageUrl;
        $this->view->userAddresses = $userAddresses;
    }
}