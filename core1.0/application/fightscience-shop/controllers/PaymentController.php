<?php

class PaymentController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new App_Model_Navigation();
        $shippingModel      = new Core_Shipping_Models_Shipping();
        $settings           = Core_Settings_Models_Settings::getSettings();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        else{
            $this->_redirect('/login/');
        }
        
        if($basketModel->isEmpty()){
            $this->_redirect('/basket/');
        }
        $parentCategories   = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        
        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop :: Payment');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        //Instantiate Models
        $orderModel         = new Core_Order_Models_Order();
        $form               = new Core_Order_Forms_cardPaymentForm();
        $basketModel        = new Core_Basket_Models_Basket();
        $shippingModel      = new Core_Shipping_Models_Shipping();
        $userModel          = new Core_User_Models_User();
        $ugModel            = new Core_Usergroup_Models_Usergroup();
        $currencyModel      = new Core_Currency_Models_Currency();
        $currencyMapper     = new Core_Currency_Models_CurrencyMapper();
        
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        }
        else
        {
            $currency = $currencyModel->load();
        }
        
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        
        //Get data
        $delivery = new Zend_Session_Namespace('delivery');
        if($this->getRequest()->getParam('delivery')){
            $delivery->deliveryCharge = $this->getRequest()->getParam('delivery');
        }
        $payment       = new Zend_Session_Namespace('payment');
        if($this->getRequest()->getParam('payment_type')){
            $payment->paymentType = (string) $this->getRequest()->getParam('payment_type');
        }

        if(isset($payment->paymentType) && $payment->paymentType == 'invoice'){
            $payment->isConfirmed = true;
            $this->_redirect('/success/');
        }

        //Get the usergroup for displaying VAT
        $ugId = $user->getUserGroupId();
        if (isset($ugId)) {
            $usergroup = $ugModel->findById($ugId);
            $this->view->usergroup = $usergroup;
        }

        $order = $orderModel->setConversion($conversion)->generateOrder();
        
        if($this->getRequest()->isPost() && !isset($_POST['checkoutsubmit']) && !$form->isValid($this->getRequest()->getParams()))
        {
            $this->view->formErrors = '<p class="notification error">There were errors</p>';
        }
        else if($this->getRequest()->isPost() && !isset($_POST['checkoutsubmit']) && $form->isValid($this->getRequest()->getParams()))
        {
            $response = $order->processPayment($this->getRequest()->getParams());
            if($response === true)
            {
                $payment->isConfirmed = true;
                $this->_redirect('/success/'); //redirect to success page if payment goes through successfully
                exit();
            }
            else
            {
                //show errors if payment does not got through
                $this->view->formErrors = '<p class="notification error">'.$response.'</p>';
                $form->populate($this->getRequest()->getParams());
            } 
        }
        
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        $this->view->cardImageLocation = '/'.APP_PLATFORM.'/img/payment-type-images/';
        $this->view->form = $form;
        $this->view->order = $order;
        
    }

}

