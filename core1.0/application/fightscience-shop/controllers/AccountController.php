<?php

class AccountController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $navigationModel    = new App_Model_Navigation();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $settings           = Core_Settings_Models_Settings::getSettings();

        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }else{
            $this->_redirect('/login/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        /* Set Views */

        $this->view->headTitle($settings->getCompanyName() . ' Shop :: Account');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
        $this->view->navigation             = $navigationModel;
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';

    }

    public function indexAction() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $userMapper = new Core_User_Models_UserMapper();
        $currencyModel = new Core_Currency_Models_Currency();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $addressModel = new Core_Address_Models_Address();
        $addressMapper  = new Core_Address_Models_AddressMapper();

        /* Forms */


        $userId = $userModel->getUserSession()->userId;
        $user   = Core_User_Models_User::getCurrent();

        /* Forms */
        $resetPasswordForm  = new Core_User_Forms_ResetPassword();
        $resetPasswordForm->setupResetForm();
        $addressForm    = new Core_Address_Forms_AddressForm();

        /* Get Data */
        $myOrders   = $user->getOrders();
        $currency   = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        $userAddresses = $addressModel->getAllUserAddresses($user->getId());


        /* Get Request Parameters */
        $request = $this->getRequest();
        if($this->getParam('tab') !== NULL) {
            $activeTab = (int) $this->getParam('tab');
        } else {
            $activeTab = 1;
        }

        //Reset password form and save in db
        if($request->isPost() && isset($_POST['submitresetpassword']) && $resetPasswordForm->isValid($request->getParams())) {
            $currentPassword    = $resetPasswordForm->getValue('old_password');
            $newPassword        = $resetPasswordForm->getValue('pwdconf');
            $result             = $user->resetPassword($currentPassword, $newPassword);
            if($result === 1) {
                $this->view->resetPasswordFeedback = '<div class="notification success">Your password has successfully been reset</div>';
            } else {
                $this->view->resetPasswordFeedback = '<div class="notification error">There were problems saving your password. Crosscheck your current password and try again.</div>';
            }
        }
        else if($request->isPost() && isset($_POST['submitresetpassword']) && !$resetPasswordForm->isValid($request->getParams())){
            $this->view->resetPasswordFeedback = '<div class="notification error">There were errors</div>';
        }





        /* Get Data */
        $addressUrl = '/account/index';
        $formDescription    = 'New Address'; //get form description e.g. Billing Address
        $addressForm->setDescription($formDescription); //set form description

        $address            = $user->getDeliveryAddress(); //execute generated function e.g. $user->getBillingAddress() returns the Address model object
        if($address){
            //$addressForm->populate($address); //populate address form with address details if user has address
            $this->view->defaultAddress = $address;
        }

        $userAddresses = $addressModel->getAllUserAddresses($user->getId());

        //Select a default address
        if($this->getRequest()->getParam('select')){
            $selectId = (int) $this->getRequest()->getParam('select');
            $newAddress = $addressModel->findById($selectId);
            //check if the address is an instance of the address model and if the address belongs to the current user
            if($newAddress instanceof Core_Address_Models_Address && $newAddress->getUserId() === $user->getId()){
                $user->setBillingAddressId($newAddress->getId())
                    ->setDeliveryAddressId($newAddress->getId())
                    ->save();
                $this->_redirect('/account/#pane2');
            }
        }

        //Delete a user's previous address
        if($this->getRequest()->getParam('delete')){
            $deleteId = (int) $this->getRequest()->getParam('delete');
            $deleteAddress = $addressModel->findById($deleteId);
            //check if the address is an instance of the address model and if the address belongs to the current user
            if($deleteAddress instanceof Core_Address_Models_Address && $deleteAddress->getUserId() === $user->getId()){
                //check if the address being deleted is the users current address
                if($user->getDeliveryAddressId() === $deleteAddress->getId()
                    || $user->getBillingAddressId() === $deleteAddress->getId() ){
                    $this->view->errors = 'Could not delete your default address. Set a new default address to delete this.';
                }
                else{
                    $deleteAddress->setActive(0)->save();
                    $this->_redirect('/account/#pane2');
                }

            }
        }

        //Get Post Data
        if(!$this->getRequest()->isPost()) {
            // Render the blank form

        } else if(!$addressForm->isValid($_POST)) {
            $this->view->formErrors = 'There were errors';

        } else {
            /*
             * Set Address model options to save. You do not need to set the address id since this was already set when it was got from the user.
             * If the user does not have an address the id would not be set and therefore it will be saved as a new entry
             */
            $newAddress = new Core_Address_Models_Address();
            $newAddress->setOptions(
                array(
                    'lineOne'   => $addressForm->getValue('lineOne'),
                    'lineTwo'   => $addressForm->getValue('lineTwo'),
                    'city'      => $addressForm->getValue('city'),
                    'region'    => $addressForm->getValue('region'),
                    'postcode'  => $addressForm->getValue('postcode'),
                    'countryId' => $addressForm->getValue('countryId'),
                    'userId'    => $user->getId(),
                    'active'    => 1,
                ));
            $addressMapper->save($newAddress);
            $this->_redirect('/account/#pane2');; // Redirect to admin homepage

        }

        /* Set View Properties */
        $this->view->activeTab              = $activeTab;
        $this->view->currency               = $currency;
        $this->view->conversion             = $conversion;
        $this->view->myOrders               = $myOrders;
        $this->view->userAddresses          = $userAddresses;
        $this->view->addressForm = $addressForm;
        $this->view->resetForm = $resetPasswordForm;
        $this->view->addressUrl = $addressUrl;
    }

}

