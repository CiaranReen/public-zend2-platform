<?php

class CategoryController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new App_Model_Navigation();
        $settings           = Core_Settings_Models_Settings::getSettings();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        if ($this->getParam('id')) {
            $categoryId = (int) $this->getParam('id');
        } else {
            $categoryId = 1;
        }
        $parentCategories = $categoryModel->getAllParentCategories($categoryId);

        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop :: Shop');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/'.APP_PLATFORM.'/img/products/';
        $this->view->languages = $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        $userModel = new Core_User_Models_User();
        $productsModel = new App_Model_Product();
        $categoryModel = new Core_Category_Models_Category();
        $currencyModel = new Core_Currency_Models_Currency();
        $ugModel = new Core_Usergroup_Models_Usergroup();

        //Get the parent categories on the site from db
        $parentCats = $categoryModel->getAllParentCategories();
        $this->view->parentCats = $parentCats;

        //Foreach parent category get the child categories that belong to them
        $childCategories = array();
        foreach ($parentCats as $pc) {
            $childCategories[$pc->id] = $pc->getChildCategories();
        }
        $this->view->childCats = $childCategories;

        $user = Core_User_Models_User::getCurrent();
        if($user instanceof Core_User_Models_User){
            $ugId = $user->getUserGroupId();
            if (isset($ugId)) {
                $usergroup = $ugModel->findById($ugId);
                $this->view->usergroup = $usergroup;
            }
        }

        //Check the current user can view the categories
        if (isset($ugId) && $ugModel->isAssignedToCategories($ugId)) {
            $filteredCategories = array();
            foreach($childCategories as $parentId => $childCategory) {
                //echo '<pre>'; var_dump($childCategory); die();
                foreach ($childCategory as $c) {
                    $ugCheck = $ugModel->usergroupAvailability($ugId, $c->id);
                    if ($ugCheck === true) {
                        $filteredCategories[$parentId][] = $c;
                    }
                }
            }
            $this->view->childCats = $filteredCategories;
        }

        /* Get Request Parameters */
        if ($this->getParam('id')) {
            $categoryId = (int) $this->getParam('id');

            $categoryObject = $categoryModel->findById($categoryId);
            $currency = $currencyModel->load();
            $conversion = $currencyModel->setConversionRate($currency->currencyId);
            $categoryProducts = $categoryObject->getProducts();

            /* Set View Properties */
            $this->view->categoryProducts = $categoryProducts;
            $this->view->currency = $currency;
            $this->view->conversion = $conversion;
        } else {
            $categoryProducts = $productsModel->getAllProducts();
            $currency = $currencyModel->load();
            $conversion = $currencyModel->setConversionRate($currency->currencyId);

            $this->view->categoryProducts = $categoryProducts;
            $this->view->currency = $currency;
            $this->view->conversion = $conversion;
        }
    }
}
