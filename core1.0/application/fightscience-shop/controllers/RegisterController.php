<?php

class RegisterController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new App_Model_Navigation();
        $settings           = Core_Settings_Models_Settings::getSettings();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop :: Register');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->languages = $languageMapper->fetchActiveNotSelected($this->view->languageSelected->getId());
        $this->view->navigation = $navigationModel;
        $this->view->basket = $basketModel; //update basket in view
        $this->view->currency           = $currency; //update currency symbol in view
        $this->view->conversion         = $conversion; //update currency conversion in view
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/' . APP_PLATFORM . '/img/products/';
    }

    public function indexAction() {
        $userMapper      = new Core_User_Models_UserMapper();
        $userMapper     = new Core_User_Models_UserMapper();
        $mailingModel   = new Core_User_Models_MailingList();
        $addressModel   = new Core_Address_Models_Address();
        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notificationModel = new Core_Notification_Models_Notification();
        $form           = new Core_User_Forms_Register();

        $countryArray = $userMapper->getAllCountries();
        $gymArray = $userMapper->getAllGyms();
        $form->setUpRegForm($countryArray, $gymArray);
        $form->getElement('terms')->getDecorator('Label')->setOption('escape',false);

        if($this->getRequest()->isPost() && !$form->isValid($this->getRequest()->getParams())){
            $this->view->errors = '<div class="alert alert-error">There were errors</div>';
        } elseif ($this->getRequest()->isPost() && $form->isValid($this->getRequest()->getParams())) {
            // Passed input validation.
            $userData = array(
                'forename' => $_POST['forename'],
                'surname' => $_POST['surname'],
                'username' => $_POST['username'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'password' => $_POST['password'],
                'company' => $_POST['company'],
                'gym' => $_POST['gym'],
                'active' => '1'
            );

            $addressData = array(
                'line_one' => $_POST['address'],
                'line_two' => $_POST['address2'],
                'town' => $_POST['town'],
                'region' => $_POST['region'],
                'postcode' => $_POST['postcode'],
                'country' => $_POST['country'],
                'active' => '1'
            );

            $mailingData = array(
                'mailing' => $_POST['emailoffers'],
            );

            $addressId = $addressModel->saveAddress($addressData);
            $userMapper->registerUser($userData);

            $userId = $userMapper->getDbTable()->getAdapter()->lastInsertId();

            $address = $addressModel->findById($addressId);
            $address->setUserId($userId)->save();

            if ($mailingData["mailing"] == '0') {
                $mailingModel->save($userId);
            }

            $notificationModel->setEvent('New user ' . $this->getParam('forename') . ' ' . $this->getParam('surname') . ' registered')
                ->setUrl('/admin/users')
                ->setIcon('fa fa-user');
            $notificationMapper->save($notificationModel);
            $url = "/login/";
            $this->redirect($url);
        }

        $this->view->form = $form;
    }

}

