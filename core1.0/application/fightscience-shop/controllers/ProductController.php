<?php

class ProductController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new App_Model_Navigation();
        $settings           = Core_Settings_Models_Settings::getSettings();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        /* Set Views */
        $this->view->headTitle($settings->getCompanyName() . ' Shop - Product');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/'.APP_PLATFORM.'/img/products/';
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->languages = $languageMapper->fetchAll();
    }

    public function indexAction() {
        $basketruleModel = new Core_Basketrule_Models_Basketrule();
        $productId = 41;
        $quantity = 10;
        $usergroupId = 1;
        $discount = $basketruleModel->getProductDiscount($productId, $quantity, $usergroupId);
        /* Instantiate Models */
        $shippingRateMapper = new Core_Shipping_Models_ShippingRateMapper();
        $productModel = new Core_Product_Models_Product();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $navigationModel = new Core_Navigation_Models_Navigation();
        $packModel = new Core_Productpacks_Models_ProductpacksMapper();
        $currencyModel = new Core_Currency_Models_Currency();
        $basketruleModel     = new Core_Basketrule_Models_Basketrule();
        $userModel = new Core_User_Models_User();
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();

        /* Get Request Parameters */
        $productId = (int) $this->getParam('id');
        //$packId         = $packModel->findById($ppId);
        $categoryId = $navigationModel->getPreviouslyViewedCategory()->categoryId; //get the category id that was previously viewed
        $productObject = $productModel->findById($productId);

        //check if user has selected a particular colour of a colourway product
        if (!is_null($productObject->getId())) {
            if ($productObject->hasColourways()) {
                if ($this->getParam('col')) {
                    $colourwaySelected = (int) $this->getParam('col');
                } else {
                    $colourwaySelected = $productObject->getDefaultImage()->colourway_id;
                }
                $this->view->colourwaySelected = $colourwaySelected;
            }
        } else {
            $this->_redirect('/category/index/id/' . $categoryId); //redirect to category page if product cannot be found
        }

        //Get Post Data
        if ($this->getRequest()->isPost()) {
            $basketModel->addItem($_POST); //add item to basket
            $basketModel->reset(); //reset basket after adding item reset basket view
            $this->view->success = true; //set success message
            $this->view->basket = $basketModel; //update basket in view
        }

        //Get usergroup properties
        $user = Core_User_Models_User::getCurrent();
        if($user instanceof Core_User_Models_User){
            $ugId = $user->getUserGroupId();
            if (isset($ugId)) {
                $usergroup = $ugModel->findById($ugId);
                $this->view->usergroup = $usergroup;
            }
        }


        /* Get Data */
        $currency = $currencyModel->load();

        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        $productStock = $productObject->getStock();

        if($productStock)
        {
            foreach($productStock as $stock)
            {
                $stock->setConversion($conversion);
                $stock->convertPrice();
            }
        }

        //Get the parent categories on the site from db
        $parentCats = $categoryModel->getAllParentCategories();
        $this->view->parentCats = $parentCats;

        //Foreach parent category get the child categories that belong to them
        $childCategories = array();
        foreach ($parentCats as $pc) {
            $childCategories[$pc->id] = $pc->getChildCategories();
        }
        $this->view->childCats = $childCategories;
        if (isset($userModel->getCurrent()->userGroupId)) {
            $ugId = $userModel->getCurrent()->userGroupId;
        }

        $deliveryOptions = $shippingRateMapper->fetchAll();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        //Check the current user can view the categories
        if (isset($ugId) && $ugModel->isAssignedToCategories($ugId)) {
            $filteredCategories = array();
            foreach($childCategories as $parentId => $childCategory) {
                foreach ($childCategory as $c) {
                    $ugCheck = $ugModel->usergroupAvailability($ugId, $c->id);
                    if ($ugCheck === true) {
                        $filteredCategories[$parentId][] = $c;
                    }
                }
            }
            $this->view->childCats = $filteredCategories;
        }

        /* Set View Properties */
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        $this->view->product = $productObject;
        $this->view->productStock = $productStock;
        $this->view->delivery_options = $deliveryOptions;
    }

}

