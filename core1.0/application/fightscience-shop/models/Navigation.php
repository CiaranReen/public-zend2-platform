<?php

class App_Model_Navigation extends Core_Navigation_Models_Navigation
{
    public function constructBreadcrumbs(){
        $breadcrumbs        = array();
        $categoryModel      = new Core_Category_Models_Category();
        $pageLink           = $_SERVER['REQUEST_URI']; //get all page link parameters
        $linkParts          = explode('/', $pageLink); //split up into array
        $baseCategoryLink   = '/category/index/id/'; //set this link as the base link for all category breadcrumbs

        $i = 0;
        //add home page first
        $breadcrumbs[$i]['name']  = 'Shop'; //set home as the first breadcrumb link
        $breadcrumbs[$i]['url']   = '/category/';
        $i++;
        /*
         * check for direct pages eg. frontend/basket and frontend/checkout
         * add on any other pages that are direct from the homepage eg contact, about, help etc
        */
        $directPages = array('basket', 'account', 'login');
        foreach($directPages as $directPage){
            if(in_array($directPage, $linkParts)){
                if($directPage === 'payment'){
                    //if you are on the payment page make sure that the user can go back to the checkout page
                    $breadcrumbs[$i]['name']  = 'Checkout';
                    $breadcrumbs[$i]['url']   = '/checkout/';
                    $i++;
                }
                if($directPage === 'checkout'){
                    //if you are on the checkout page make sure that the user can go back to the basket page
                    $breadcrumbs[$i]['name']  = 'Basket';
                    $breadcrumbs[$i]['url']   = '/basket/';
                    $i++;
                }
                if($directPage === 'address'){
                    //if you are on the address edit page make sure that the user can go back to the checkout page
                    $breadcrumbs[$i]['name']  = 'Checkout';
                    $breadcrumbs[$i]['url']   = '/checkout/';
                    $i++;
                }
                $breadcrumbs[$i]['name']  = ucwords($directPage);
                $breadcrumbs[$i]['url']   = false;
            }
        }
        //echo '<pre>'; var_dump($breadcrumbs);

        //checks if we are on a category page or if we are on a product page and the previously viewed category before
        //reach the product page has been saved in a session so we can pull the category details for the breadcrumb
        if( in_array('category', $linkParts) || (in_array('product', $linkParts) && $this->getPreviouslyViewedCategory()->categoryId) ){
            if(in_array('category', $linkParts)){
                //executes when we are on a category page
                $categoryId     = end($linkParts); //get category id from end of pageLink
            }
            else{
                //exceutes when we are on a product page
                $categoryId     = $this->getPreviouslyViewedCategory()->categoryId;
            }
            $categoryObject     = $categoryModel->findById($categoryId);
            //var_dump($categoryObject); die();
            //check if category is a child of another
            if($categoryId && intval($categoryId) && $categoryObject instanceof Core_Category_Models_Category){
                if($categoryObject->isChild()){
                    //gets parent category details for breadcrumb
                    $parentCategoryId           = $categoryModel->getParentId();
                    $categoryModel2             = new Core_Category_Models_Category();
                    $parentCategoryObject       = $categoryModel2->findById($parentCategoryId);
                    $breadcrumbs[$i]['name']    = ucwords($parentCategoryObject->getNameByLanguage());
                    $breadcrumbs[$i]['url']     = true;
                    $i++;
                }

                $breadcrumbs[$i]['name']  = ucwords($categoryObject->getNameByLanguage());
                if(in_array('category', $linkParts)){
                    //sets breadcrumb link to false if we are on the category page
                    $breadcrumbs[$i]['url']   = false;
                }
                else{
                    //sets breadcrumb link live if we are on the product page so we can navigate back to category page
                    $breadcrumbs[$i]['url']   = $baseCategoryLink . $categoryId;
                }
                $i++;
                /* Save category page breadcrumbs in session so that when you go to product page
                 * the category page which you came from is saved in the session
                 * this is because products are assigned to multiple products so you cannot tell which category the
                 * user came from if the user goes straight to the product page or if the user
                 */
                if(in_array('category', $linkParts)){
                    $this->setPreviouslyViewedCategory($categoryId);
                }
            }
        }

        //check for product page
        if(in_array('product', $linkParts)){
            $productIdKey = array_search('id', $linkParts) + 1;
            $productId              = $linkParts[$productIdKey]; //get product id from end of pageLink
            $productModel           = new Core_Product_Models_Product();
            $productObject          = $productModel->findById($productId);
            if(!$this->getPreviouslyViewedCategory()->categoryId){
                /*
                 * gets default category details if a previously viewed category has not been set in session
                 */
                foreach($productObject->getCategories() as $category){
                    $productCategoryIds[] = $category->getId();
                }
                $categoryId             = reset($productCategoryIds); //get first category id from array of category ids
                $categoryObject         = $categoryModel->findById($categoryId);
                if($categoryObject->isChild()){
                    //gets parent category details for breadcrumb
                    $parentCategoryId       = $categoryModel->getParentId();
                    $categoryModel2         = new Core_Category_Models_Category();
                    $parentCategoryObject   = $categoryModel2->findById($parentCategoryId);
                    $breadcrumbs[$i]['name']  = ucwords($parentCategoryObject->getNameByLanguage());
                    $breadcrumbs[$i]['url']   = true;
                    $i++;
                }
                //set product's category link
                $breadcrumbs[$i]['name']  = ucwords($categoryObject->getNameByLanguage());
                $breadcrumbs[$i]['url']   = $baseCategoryLink . $categoryId;
                $i++;
            }
            //set product's name
            $breadcrumbs[$i]['name']  = ucwords($productObject->getNameByLanguage());
            $breadcrumbs[$i]['url']   = false;
            //return $breadcrumbs;
        }

        //size will be 1 when it is on the home page and since we dont want to show breadcrumbs here we return nothing
        if(sizeof($breadcrumbs) === 1){
            return ;
        }

        return $breadcrumbs;
    }

    /**
     * Constructs the breadcrumbs to be shown on the navigation page
     * not used at the moment since breadcrumbs is passed as an array to the view using the getBreadcrumbs() method
     *
     * @return string
     */
    public function showBreadCrumbs(){
        $display = '';
        if($this->getBreadcrumbs()){
            foreach($this->getBreadcrumbs() as $breadcrumb){
                if($breadcrumb['url'] === false){
                    $display .= '<li class= "active">'.$breadcrumb['name'].'</li>';
                }
                else if($breadcrumb['url'] === true){
                    $display .= '<li>'.$breadcrumb['name'].' <span class="divider">'.$this->getSeparator().'</span> </li>';
                }
                else{
                    $display .= '<li><a href = "'.$breadcrumb['url'].'">'.$breadcrumb['name'].'</a> <span class="divider">'.$this->getSeparator().'</span> </li>';
                }
            }
        }
        return $display;
    }
    
}