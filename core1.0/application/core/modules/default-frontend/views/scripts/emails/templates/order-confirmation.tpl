<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Email Notification</title>
</head>

<body bgcolor="#efefef">
    <style type="text/css">
    body {
      font: 11px "Trebuchet MS", Trebuchet, Tahoma, Arial, sans-serif;
      margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
      padding: 0;
      color: #000000;
    }
    table{
        width: 580;
    }
    table tr th {
        font-size: 11px;
        text-align: left;
    }
    table tr td {
        font-size: 11px;
        text-align: left;
    }
    h1 { color: #ce0404; font-size: 21px; }
    h1 a { color: #ce0404; }
    h2 { color: #444444; font-size: 16px; }
    h3 { color: #666666; font-size: 14px; }
    h4 { color: #666666; font-size: 12px; font-weight: bold; padding: 0px; margin: 5px 0; }
    p { font-size: 13px; }
    .header * { margin: 0; height: 83px; }
    .header { padding: 10px; }
    .header h1 { color: #000000; }
    .header h1 a { color: #000000; }
    .content { padding: 0 15px 5px 15px; }
    .disclaimer { padding: 2px 15px 5px 15px; color: #BDBDBD;}
    .footer { padding: 2px 15px 5px 15px; color: #000000;}
    </style>

    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
        <tr>
            <td align="left" bgcolor="#ffffff" color="#ffffff" class="header">
                <h1><a href="http://www.lifefitness-shop.com/"><img src="http://beta.lifefitness-shop.com/lifefitness-shop/img/logo.jpg" alt="Life Fitness Shop" border="0"/></a></h1>
            </td>
        </tr>
    </table>
    <table width="580" border="0" cellpadding="15" cellspacing="0" align="center" bgcolor="#ffffff">
        <tr>
            <td align="left" class="content">
                <h2><?php echo $this->orderHeading; ?></h2>
                
                <h4><?php echo $this->orderNumber; ?></h4>
                <h4><?php echo $this->user; ?></h4>
                <h4><?php echo $this->itemsTotal; ?></h4>
                <h4><?php echo $this->delivery; ?></h4>
                <h4><?php echo $this->VAT; ?></h4>
                <h4><?php echo $this->orderTotal; ?></h4>

                <?php echo $this->productsTable; ?>
            </td>
        </tr>
    </table>
    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
        <tr>
            <td align="left" valign="middle" class="disclaimer">
                <?php echo $this->disclaimer; ?>
            </td>
        </tr>
    </table>
    <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff">
        <tr>
            <td align="left" valign="middle" class="footer" height="105">
                <p style="color: #999;padding: 20px 0 0 0;font-size:11px;">2012 Life Fitness Shop</p>
            </td>
        </tr>
    </table>
</body>
</html>