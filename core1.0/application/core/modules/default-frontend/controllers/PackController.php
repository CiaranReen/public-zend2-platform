<?php

class PackController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new Core_Navigation_Models_Navigation();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Product');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/lifefitness-shop/img/products/';
        $this->view->languages = $languageMapper->fetchAll();
    }

    public function indexAction() {
        $packsModel = new Core_Productpacks_Models_Productpacks();
        $allPacks = $packsModel->getAllProductPacks();

        $this->view->allPacks = $allPacks;
    }

    public function viewAction() {
        $packsModel = new Core_Productpacks_Models_ProductpacksMapper();
        $productModel = new Core_Product_Models_Product();
        $basketModel = new Core_Basket_Models_Basket();

        $packId = (int) $this->getParam('id');
        $packProducts = $packsModel->loadProductsInPack($packId);
        $products = $packsModel->getAllProductsById($packId);
        //echo '<pre>'; var_dump($products); die();

        foreach ($products as $product) {
            $productIds[] = $product->product_id;
        }

        $packStock = $productModel->getPackStock($productIds);
        //echo '<pre>'; var_dump($packStock); die();

        if ($this->getRequest()->isPost()) {
            $basketModel->addItem($_POST); //add item to basket
            $basketModel->reset(); //reset basket after adding item reset basket view
            $this->view->success = true; //set success message
            $this->view->basket = $basketModel; //update basket in view
        }

        $this->view->packStock = $packStock;
        $this->view->packProducts = $packProducts;
    }

}

