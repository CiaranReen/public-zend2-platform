<?php

class LoginController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new Core_Navigation_Models_Navigation();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $this->_redirect('/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - User Login');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->languages = $languageMapper->fetchAll();
        $this->view->currency           = $currency; //update currency symbol in view
        $this->view->conversion         = $conversion; //update currency conversion in view
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/lifefitness-shop/img/products/';
    }

    public function indexAction() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();

        /* Language */
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $content = array();
        $content['formdesc'] = $languageMapper->fetchByIp()->getContent('user_login_text')->getContent();
        $content['formusername'] = $languageMapper->fetchByIp()->getContent('form_username')->getContent();
        $content['formpassword'] = $languageMapper->fetchByIp()->getContent('form_password')->getContent();
        $content['formloginbtn'] = $languageMapper->fetchByIp()->getContent('login')->getContent();

        /* Forms */
        $loginForm = new Core_User_Forms_LoginForm();
        $loginForm->setup($content);

        //Get Post Data
        $this->view->loginForm = $loginForm;
        if (!$this->getRequest()->isPost()) {
            // Render the blank form
        } else if (!$loginForm->isValid($_POST)) {
            $this->view->form_errors = 'There were errors';
        } else {

            // Passed input validation.
            // Check if this user is allowed to login to the admin
            $username = $loginForm->getValue('username');
            $password = $loginForm->getValue('password');
            $user = $userModel->processLogin($username, $password);
            if ($user->getId() !== NULL) {
                $this->_redirect('/category/index/1'); // Redirect to admin homepage
            } else {
                // Failed login
                $this->view->form_errors = 'Your username or password were not recognised';
            }
        }
    }

    public function resetAction() {
        $resetModel = new Core_PasswordReset_Models_PasswordReset();
        $resetMapper = new Core_PasswordReset_Models_PasswordResetMapper();
        $userMapper = new Core_User_Models_UserMapper();

        $resetRequest = $this->getParam('id');

        if (!$_POST && is_null($resetRequest)) {
            $form = new Core_PasswordReset_Forms_ResetPassword();
            $form->setUpResetPasswordForm();
            $this->view->form = $form;
        } elseif (is_null($resetRequest)) {
            $query = $_POST['query'];
            $queryResult = $userMapper->searchForUser($query); //Search for the user based on their search term
            //Check we have a result
            if (!is_null($queryResult)) {
                $this->view->user_result = $queryResult;
            } else {
                //Tell the user that no search results were found
                $errors = '1';
                $this->view->errors = $errors;
            }
        }
    }

    //Send Email to user to reset password
    public function continueAction() {
        $resetModel = new Core_PasswordReset_Models_PasswordReset();
        $resetMapper = new Core_PasswordReset_Models_PasswordResetMapper();
        $mailer = new Core_Emailer_Models_Emailer();

        $userId = $_GET['u'];
        $email = $_GET['e'];
        $url = $resetMapper->generateRandomString();
        $resetModel->saveRequest($userId, $url);
        $mailer->passwordResetEmail($email, $url);
        $this->_redirect('/login/newpassword/');

    }

    //Reset users password and login
    public function newpasswordAction() {
        $resetModel = new Core_PasswordReset_Models_PasswordReset();
        $resetMapper = new Core_PasswordReset_Models_PasswordResetMapper();
        $userModel = new Core_User_Models_User();
        $userMapper = new Core_User_Models_UserMapper();
        $passwordForm = new Core_PasswordReset_Forms_Password();

        if (!$_POST && !isset($_GET['code'])) {
            $form = '1';
            $this->view->cform = $form;
        }

        $request = $this->getRequest();
        if (isset($_GET['code'])) {
            $resetRequest = $_GET['code'];
            $check = $resetModel->checkExpirationTime($resetRequest);
            if(isset($check) && !$_POST) {
                $passwordForm->setUpPasswordForm();
                $this->view->pform = $passwordForm;
            } elseif ($passwordForm->isValid($request->getParams())) {
                $userId = $resetModel->getUserIdByRequestId($resetRequest);
                $password = $_POST['password'];
                $update = $userMapper->saveNewPassword($password, $userId);
                if ($update === 1) {
                    $username = $userMapper->getUserById($userId);
                    $userModel->processLogin($username->username, $password);
                    $this->_redirect('/');
                }
            }
        }

    }

}
