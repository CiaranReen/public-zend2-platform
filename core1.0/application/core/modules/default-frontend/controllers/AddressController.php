<?php

class AddressController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        else{
            $this->_redirect('/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - User Addresses');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->categoryUrl            = '/category/index/id/';
        
    }

    public function indexAction()
    {
        /* Instantiate Models */
        $addressMapper  = new Core_Address_Models_AddressMapper();
        $addressModel   = new Core_Address_Models_Address();
        $userModel      = new Core_User_Models_User();
        
        /* Forms */
        $addressForm    = new Core_Address_Forms_AddressForm();
        
        /* Get Data */
        $user = $userModel->findById($userModel->getUserSession()->userId);
        if($this->getParam('type')){
            $addressType = (string) $this->getParam('type');
        }
        if($addressType === 'billing'){ }
        else{
            $addressType = 'delivery';
        }
        $getAddressFunction = 'get'.ucfirst($addressType).'Address'; //get address function name for user object e.g getBillingAddress()
        $setAddressIdFunction = 'set'.ucfirst($addressType).'Id'; //gets address function name for user object e.g setBillingId()
        $formDescription    = ucfirst($addressType).' Address'; //get form description e.g. Billing Address
        $address            = $user->$getAddressFunction(); //execute generated function e.g. $user->getBillingAddress() returns the Address model object
        $addressForm->setDescription($formDescription); //set form description
        if($address){
            $addressForm->populate($address); //populate address form with address details if user has address
        }

        $userAddresses = $addressModel->getAllUserAddresses($user);

        
        //Get Post Data
        if(!$this->getRequest()->isPost()) {
			// Render the blank form

		} else if(!$addressForm->isValid($_POST)) {
            $this->view->formErrors = 'There were errors';

		} else {
			/*
             * Set Address model options to save. You do not need to set the address id since this was already set when it was got from the user.
             * If the user does not have an address the id would not be set and therefore it will be saved as a new entry
             */
            $address->setOptions(
                    array(
                        'lineOne'   => $addressForm->getValue('lineOne'),
                        'lineTwo'   => $addressForm->getValue('lineTwo'),
                        'city'      => $addressForm->getValue('city'),
                        'region'    => $addressForm->getValue('region'),
                        'postcode'  => $addressForm->getValue('postcode'),
                        'countryId' => $addressForm->getValue('countryId'),
                        'userId'    => $user->getId(),
                    ));
            $addressMapper->save($address);
            if (null === ($id = $address->getId())){
                $addressId = $addressMapper->getDbTable()->lastInsertId();
                $user->$setAddressIdFunction($addressId)->save();
            } 
            $this->_redirect('/checkout/'); // Redirect to admin homepage

		}

        $this->view->addressType = $addressType;
        $this->view->addressForm = $addressForm;
        $this->view->userAddresses = $userAddresses;
    }
}