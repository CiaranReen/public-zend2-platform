<?php

class IndexController extends Zend_Controller_Action
{
    public function init()
    {
         /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        //This is hard coded because the local IP address doesn't work!
        
        /* Set Views */
        $this->view->headTitle('LifeFitness E-Shop');
        $this->view->parentCategories   = $parentCategories; //for header nav
        $this->view->languageSelected 	= $languageMapper->fetchByIp();
        $this->view->languages          = $languageMapper->fetchActiveNotSelected($this->view->languageSelected->getId());
        $this->view->navigation         = $navigationModel;
        $this->view->basket             = $basketModel; //update basket in view
        $this->view->currency           = $currency; //update currency symbol in view
        $this->view->conversion         = $conversion; //update currency conversion in view
        $this->view->categoryUrl        = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';

    }

    public function indexAction()
    {
        $productsModel = new Core_Product_Models_Product();
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $productModel = new Core_Product_Models_Product();
        $currencyModel = new Core_Currency_Models_Currency();
        $ugModel = new Core_Usergroup_Models_Usergroup();

        //Get the parent categories on the site from db
        $parentCats = $categoryModel->getAllParentCategories();
        $this->view->parentCats = $parentCats;
        //Foreach parent category get the child categories that belong to them
        $childCategories = array();
        foreach ($parentCats as $pc) {
            $childCategories[$pc->id] = $pc->getChildCategories();
        }
        $this->view->childCats = $childCategories;
        //Pass them through to the view
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        //Check the current user can view the categories
        if (isset($ugId) && $ugModel->isAssignedToCategories($ugId)) {
            $filteredCategories = array();
            foreach($childCategories as $childCategory){
                $ugCheck = $ugModel->usergroupAvailability($ugId, $childCategory->getId());
                if ($ugCheck === true) {
                    $filteredCategories[] = $childCategory;
                }
            }
            $this->view->childCategories = $filteredCategories;
        }

        /* Set View Properties */
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
    }
}

