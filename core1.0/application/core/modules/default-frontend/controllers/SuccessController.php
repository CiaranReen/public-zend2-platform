<?php

class SuccessController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = Core_User_Models_User::getCurrent();
            $this->view->user = $user;
        }
        else{
            $this->_redirect('/login/');
        }
        
        if($basketModel->isEmpty()){
            $this->_redirect('/basket/');
        }
        $parentCategories   = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Payment');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        //Instantiate Models
        $userModel      = new Core_User_Models_User();
        $orderModel     = new Core_Order_Models_Order();
        $emailer        = new Core_Emailer_Models_Emailer();
        $currencyModel  = new Core_Currency_Models_Currency();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $basketModel    = new Core_Basket_Models_Basket();

        $user = $userModel->findById($userModel->getUserSession()->userId);
        $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());

        $order = $orderModel->setConversion($conversion)->generateOrder();
        if($order){
            $orderNumber = $order->confirm();
            $basketModel->emptyBasket();
            $emailer->sendOrderSuccessEmail($order); 
            $order->clearSession();
        }
        else{
            $this->_redirect('/');
        }
        $this->view->successMessage = 'Order has been successfully processed. Your Order number is '.$orderNumber.'.';
        
    }

}

