<?php

class AjaxController extends Zend_Controller_Action {

    public function init() {
        // Disable the main layout renderer and view renderer as well
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {
        
    }

    /**
     * Gets all basket items
     */
    public function getbasketAction() {
        $basketModel = new Core_Basket_Models_Basket();
        $basketItems = $basketModel->getDisplayData();
        echo json_encode($basketItems);
    }

    /**
     * updates basket item with stock id and new quantity entered
     */
    public function updatebasketitemAction() {
        $basketModel = new Core_Basket_Models_Basket();
        $stockId = (int) $this->getParam('id');
        $newQty = (int) $this->getParam('newQty');
        $basketModel->updateItemQty($stockId, $newQty);
    }

    /**
     * Gets all stock for a particular product
     */
    public function getproductstockAction() {
        $productId = (int) $this->getParam('productid');
        $productModel = new Core_Product_Models_Product();
        $productObject = $productModel->findById($productId);
        $productStock = $productObject->getStock();
        foreach ($productStock as $stock) {
            $stockObject = array(
                'id' => $stock->getId(),
                'option' => $stock->getOption(),
                'colourway_id' => $stock->getColourwayId(),
                'price' => $stock->getPrice(),
                'stock_level' => $stock->getStockLevel()
            );
            $stockObjects[] = $stockObject;
        }
        echo json_encode($stockObjects);
    }

    /**
     * Gets all colourways for a product using product id
     */
    public function getproductcolourwaysAction() {
        $productId = (int) $this->getParam('productid');
        $productModel = new Core_Product_Models_Product();
        $productObject = $productModel->findById($productId);
        $productColourways = $productObject->getColourways();
        foreach ($productColourways as $colourway) {
            $colourwayObject = array(
                'id' => $colourway->getId(),
                'name' => $colourway->getNameByLanguage(),
                'hex' => $colourway->getHex(),
                'code' => $colourway->getCode()
            );
            $colourwayObjects[] = $colourwayObject;
        }
        echo json_encode($colourwayObjects);
    }

    /**
     * Gets all colourway images for a product using the productid and colourwayid
     */
    public function getcolourwayimagesAction() {
        $colourwayImages = array();
        $productId = (int) $this->getParam('productid');
        $colourwayId = (int) $this->getParam('colourwayid');
        $productModel = new Core_Product_Models_Product();
        $productObject = $productModel->findById($productId);
        $productColourwayImages = $productObject->getColourwayImagesByColourwayId($colourwayId);
        foreach ($productColourwayImages as $image) {
            $imageObject = array(
                'id' => $image->id,
                'image_url' => $image->image_url,
                'colourway_id' => $image->colourway_id,
                'order' => $image->order,
                'is_default' => $image->is_default
            );
            $colourwayImages[] = $imageObject;
        }
        echo json_encode($colourwayImages);
    }

    public function vendorproductsAction() {
        $vpModel = new Core_Product_Models_VendorProducts();

        // Get Search
        $search_string = preg_replace("/[^A-Za-z0-9]/", " ", $this->getRequest()->getParam('query'));
        // Check Length More Than One Character
        if (strlen($search_string) >= 1 && $search_string !== ' ') {
            // Build Query
            $resultSet = $vpModel->getSearch($search_string);
            // Check If We Have Results
            if (isset($resultSet)) {
                foreach ($resultSet as $result) {

                    // Format Output Strings And Hightlight Matches
                    $name = preg_replace("/'/", '', $result['description']);
                    $display_function = preg_replace("/" . $search_string . "/i", "<p name='" . $name . "' vendor_id ='" . $result['vendor_product_id'] . "' class='vp-search'><b class='highlight'>" . $search_string . "</b></p>", $result['description']);
                    $display_name = preg_replace("/" . $search_string . "/i", "<p name='" . $name . "' vendor_id ='" . $result['vendor_product_id'] . "' class='vp-search'><b class='highlight'>" . $search_string . "</b></p>", $result['vendor_part_no']);
                    $html = '';
                    $html .= '<li class="result" name="' . $name . '">';
                    $html .= '<h3>nameString</h3>';
                    $html .= '<h4>functionString</h4>';
                    $html .= '</a>';
                    $html .= '</li>';
                    // Insert Name
                    $output = str_replace('nameString', $display_name, $html);

                    // Insert Function
                    $output = str_replace('functionString', $display_function, $output);

                    // Output
                    echo($output);
                }
            } else {

                $output = str_replace('nameString', '<b>No Results Found.</b>', $output);
                $output = str_replace('functionString', 'Sorry :(', $output);

                // Output
                echo($output);
            }
        }
    }

    public function editcontentAction() {
        $content = $_POST['content']; //get posted data
        //var_dump($content);
        $id = $_POST['id'];
        $manModel = new Core_Product_Models_VendorProducts();
        $manModel->updateContent($content, $id);
    }
    
    public function removecolourwayAction(){
        $colourwayModel = new Core_Colourway_Models_Colourway();
        $colourwayId = (int) $this->getRequest()->getParam('id');
        echo $colourwayModel->findById($colourwayId)->delete();
    }
    
    public function getTemplateDetailsAction(){
        $basketruleTemplateModel    = new Core_Basketrule_Models_BasketruleTemplate();
        $allTemplates               = $basketruleTemplateModel->getAllTemplates();
        $details                    = array();
        foreach($allTemplates as $template){
            $details[$template->getId()]['name']                = $template->getName();
            $details[$template->getId()]['condition_id']        = $template->getConditionId();
            $details[$template->getId()]['quantity']            = $template->getQuantity();
            $details[$template->getId()]['discount']            = $template->getDiscount();
            $details[$template->getId()]['discount_type_id']    = $template->getDiscountTypeId();
        }
        echo json_encode($details);
    }
    
    public function getCurrentUserDetails() {
        $currentUser = Core_User_Models_User::getCurrent();
        if(!$currentUser instanceof Core_User_Models_User){
            echo '';
        }
        $userDetails = array(
            'user_id' => $currentUser->getId(),
            'forename' => $currentUser->getForename(),
            'surname' => $currentUser->getSurname(),
            'user_group_id' => $currentUser->getUserGroupId(),
        );
        echo json_encode($userDetails);
    }

    public function getBasketruleDiscountDetailsAction() {
        $basketruleModel    = new Core_Basketrule_Models_Basketrule();
        $currentUser        = Core_User_Models_User::getCurrent();
        $productModel       = new Core_Product_Models_Product();
        $discountDetails    = array();
        $productId          = (int) $this->getParam('productid');
        $quantity           = (int) $this->getParam('quantity');
        $product            = $productModel->findById($productId);
        if($currentUser instanceof Core_User_Models_User && $product instanceof Core_Product_Models_Product){
            $price                  = (float) $product->getMinPrice();
            $totalPrice             = (float) ($price * $quantity);
            $productDiscount        = (float) $basketruleModel->getProductDiscount($productId, $quantity, $currentUser->getUserGroupId());
            $totalCost              = $totalPrice - $productDiscount;
            if($productDiscount > 0){
                $totalSaving        = $totalPrice - $totalCost;
                $discountDetails    = array(
                    'product_discount'  => $productDiscount,
                    'total_cost'        => $totalCost,
                    'total_saving'      => $productDiscount
                );
            }
        }
        echo json_encode($discountDetails);
    }
    
//    public function getBasketruleDiscountDetailsAction() {
//        $basketruleModel    = new Core_Basketrule_Models_Basketrule();
//        $currentUser        = Core_User_Models_User::getCurrent();
//        $stockMapper        = new Core_Stock_Models_StockMapper();
//        $discountDetails    = array();
//        $stockId            = (int) $this->getParam('stockid');
//        $quantity           = (int) $this->getParam('quantity');
//        $stock = $stockMapper->find($stockId, new Core_Stock_Models_Stock);
//        if($currentUser instanceof Core_User_Models_User && $stock instanceof Core_Stock_Models_Stock){
//            $price                  = (float) $stock->getPrice();
//            $totalPrice             = (float) ($price * $quantity);
//            $stockDiscount          = (float) $basketruleModel->getStockDiscount($stockId, $quantity, $currentUser->getUserGroupId());
//            $stockDiscountPerItem   = (float) $basketruleModel->getStockDiscountPerItem($stockId, $quantity, $currentUser->getUserGroupId());
//            $totalCost              = $totalPrice - $stockDiscount;
//            if($stockDiscount > 0){
//                $totalSaving        = $totalPrice - $totalCost;
//                $discountDetails    = array(
//                    'stock_discount'    => $stockDiscount,
//                    'unit_cost'         => ($price - $stockDiscountPerItem),
//                    'total_cost'        => $totalCost,
//                    'total_saving'      => $totalSaving
//                );
//            }
//        }
//        echo json_encode($discountDetails);
//    }
    
    public function getMatchingProductsAction() {
        $term = $this->getParam('term');
        $productModel   = new Core_Product_Models_Product();
        $allProducts    = $productModel->getAllProducts();
        $matches        = array();
        if($allProducts){
            foreach($allProducts as $product){
                if(stripos($product->getNameByLanguage(), $term) !== false){
                    $products['value'] = $product->getNameByLanguage();
                    //$products['label'] = "{$product->getName()}, {$product->getId()}";
                    $products['label'] = "{$product->getNameByLanguage()}";
                    $products['id'] = "{$product->getId()}";
                    $matches[] = $products;
                }
            }
        }
        echo json_encode($matches);
	}

    public function getPopularProductsAction(){
        $productModel = new Core_Product_Models_Product();
        $mostPopularProducts = $productModel->getMostPopularProducts(5);
        $result = array();
        if($mostPopularProducts){
            $result[] = array(
                'product', 'count'
            );
            foreach($mostPopularProducts as $popularProduct){
                $result[] = array(
                    $popularProduct->name,
                    $popularProduct->count
                );
            }
        }
        echo json_encode($result);
    }
    
    public function getGoogleAnalyticsAction(){
        $settings = new Core_Settings_Models_Settings();
        $results = array();
        if($settings->getGoogleAnalytics()){
            $results = $settings->getGoogleAnalytics();
        }
        echo json_encode($results);
    }

}   

