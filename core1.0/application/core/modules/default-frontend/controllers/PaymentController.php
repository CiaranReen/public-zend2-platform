<?php

class PaymentController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        $shippingModel      = new Core_Shipping_Models_Shipping();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        else{
            $this->_redirect('/login/');
        }
        
        if($basketModel->isEmpty()){
            $this->_redirect('/basket/');
        }
        $parentCategories   = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        
        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Payment');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        //Instantiate Models
        $orderModel         = new Core_Order_Models_Order();
        $form               = new Core_Order_Forms_cardPaymentForm();
        $basketModel        = new Core_Basket_Models_Basket();
        $shippingModel      = new Core_Shipping_Models_Shipping();
        $userModel          = new Core_User_Models_User();
        $currencyModel      = new Core_Currency_Models_Currency();
        $currencyMapper     = new Core_Currency_Models_CurrencyMapper();
        
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        }
        else
        {
            $currency = $currencyModel->load();
        }
        
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        
        //Get data
        if(isset($_POST['checkoutsubmit']))
        {
            try
            {
                if(isset($_POST['delivery']) && $_POST['delivery'] != '' && is_numeric($_POST['delivery']))
                {
                    /*$deliveryChoice = $_POST['delivery'];
                    $items = $basketModel->getItems();
                    $shippingModel->setBasket($items);
                    $prices = $shippingModel->calculate();
                    $basketModel->setDelivery($prices[$deliveryChoice]->getPrice());*/
                    $basketModel->setBasketDelivery($_POST['delivery']);
                }
                else
                {
                    throw new Exception(
                        'Delivery could not be added because the system
                            could not recognise your choice'
                    );
                }
            }
            catch(Exception $e)
            {
                echo $e->getMessage();
            }
        }
        $order = $orderModel->setConversion($conversion)->generateOrder();
        if($_POST['delivery']){
            $order->setDeliveryCharge($_POST['delivery']);
        }

        
        if($this->getRequest()->isPost() && !isset($_POST['checkoutsubmit']) && !$form->isValid($this->getRequest()->getParams()))
        {
            $this->view->formErrors = '<div class="alert alert-error">There were errors</div>';
        }
        else if($this->getRequest()->isPost() && !isset($_POST['checkoutsubmit']) && $form->isValid($this->getRequest()->getParams()))
        {
            $response = $order->processPayment($this->getRequest()->getParams());
            if($response === true)
            {
                //$order->save();
                $this->_redirect('/success/'); //redirect to success page if payment goes through successfully
                exit();
            }
            else
            {
                //show errors if payment does not got through
                $this->view->formErrors = '<div class="alert alert-error">'.$response.'</div>';
                $form->populate($this->getRequest()->getParams());
            } 
        }
        
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        $this->view->form = $form;
        $this->view->order = $order;
        
    }

}

