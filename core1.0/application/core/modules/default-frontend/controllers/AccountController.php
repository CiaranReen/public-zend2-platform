<?php

class AccountController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();

        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }else{
            $this->_redirect('/login/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Product');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
        $this->view->navigation             = $navigationModel;
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';

    }

    public function indexAction() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $userMapper = new Core_User_Models_UserMapper();
        $currencyModel = new Core_Currency_Models_Currency();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();

        $userId = $userModel->getUserSession()->userId;
        $user   = Core_User_Models_User::getCurrent();

        /* Forms */
        //TODO change this to match the new implementation of the reset password functionality
        $resetPasswordForm  = new Core_User_Forms_ResetPassword();
        $resetPasswordForm->setupResetForm();
        $this->view->resetForm = $resetPasswordForm;
        //Currency form set up and save in db
        if ($this->getRequest()->getPost() && $this->getParam('submitcurrency')) {
            $currency = reset($_POST);
            $save = $userMapper->saveCurrency($currency, $userId);
            if ($save === true) {
                $success = 1;
                $this->view->success = $success;
            }
        }
        $changeCurrencyForm  = new Core_Currency_Forms_ChangeUserCurrency();
        $changeCurrencyUrl   = '/account/index/tab/3';
        $activeCurrency = $user->getCurrencyId();
        $changeCurrencyForm->setAction($changeCurrencyUrl);
        $changeCurrencyForm->setupForm($currencyMapper->fetchAll(), $activeCurrency);
        $this->view->changeCurrencyForm  = $changeCurrencyForm;

        /* Get Data */

        $myOrders   = $user->getOrders();
        $currency   = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());

        /* Get Request Parameters */
        $request = $this->getRequest();
        if($this->getParam('tab') !== NULL){
            $activeTab = (int) $this->getParam('tab');
        }
        else{
            $activeTab = 1;
        }

        //TODO change this to match the new implementation of the reset password functionality
        if($request->isPost() && isset($_POST['submitresetpassword']) && $resetPasswordForm->isValid($request->getParams())) {
            $currentPassword    = $resetPasswordForm->getValue('old_password');
            $newPassword        = $resetPasswordForm->getValue('new_password');
            $result             = $user->resetPassword($currentPassword, $newPassword);
            if($result === 1) {
                $this->view->resetPasswordFeedback = '<div class="alert alert-success">Your password has successfully been reset</div>';
            } else {
                $this->view->resetPasswordFeedback = '<div class="alert alert-error">There were problems saving your password. Crosscheck your current password and try again.</div>';
            }
        }
        else if($request->isPost() && isset($_POST['submitresetpassword']) && !$resetPasswordForm->isValid($request->getParams())){
            $this->view->resetPasswordFeedback = '<div class="alert alert-error">There were errors</div>';
        }

        /* Set View Properties */
        $this->view->activeTab              = $activeTab;
        $this->view->currency               = $currency;
        $this->view->conversion             = $conversion;
        $this->view->myOrders               = $myOrders;
    }

}

