<?php

class BasketController extends Zend_Controller_Action
{

    public function init()
    {
        /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();
        
        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        
        /* Set Views */
        $this->view->headTitle('FightScience Shop - Basket');
        $this->view->parentCategories       = $parentCategories; //for header nav
        $this->view->basket                 = $basketModel;
        $this->view->currency               = $currency; //update currency symbol in view
        $this->view->conversion             = $conversion; //update currency conversion in view
        $this->view->categoryUrl            = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/'.APP_PLATFORM.'/img/products/';
        $this->view->navigation             = $navigationModel;
        $this->view->languageSelected       = $languageMapper->fetchByIp();
        $this->view->languages              = $languageMapper->fetchAll();
    }

    public function indexAction()
    {
        /* Instantiate Models */
        $basketModel = new Core_Basket_Models_Basket();
        $currencyModel = new Core_Currency_Models_Currency();
        $userModel = new Core_User_Models_User();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $currency = $currencyModel->load();
        
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        $settings = new Core_Settings_Models_Settings();
        //Get Post Data
        if ($this->getParam('id')) {
            $stockId = (int) $this->getParam('id');
            $basketModel->removeItem($stockId); //remove item from basket
            $basketModel->reset(); //reset basket after removing item reset basket view
            $this->view->success = true; //set success message
            $this->view->basket  = $basketModel; //update basket in view
        }
        
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
    }

}

