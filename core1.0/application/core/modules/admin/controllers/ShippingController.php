<?php

class Admin_ShippingController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $user = Core_User_Models_User::getCurrent();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {

    }

    public function companiesAction()
    {
        $companyMapper = new Core_Shipping_Models_CompanyMapper();

        if(isset($_POST['addcompanysubmit']))
        {
            $company = new Core_Shipping_Models_Company();
            $company->setCompany($_POST['name']);
            $companyMapper->save($company);
        }

        $form = new Core_Shipping_Forms_Choosecompany();
        $form->setupForm($companyMapper->fetchAll());
        $this->view->form = $form;
    }

    public function bandsAction() {
        $shippingBandsMapper = new Core_Shipping_Models_ShippingBandMapper();
        $bands = $shippingBandsMapper->fetchAll();
        $this->view->bands = $bands;
    }

    public function addbandAction() {
        $shippingBandMapper     = new Core_Shipping_Models_ShippingBandMapper();
        $shippingBandModel      = new Core_Shipping_Models_ShippingBand();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Addband();

        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->getPost()) {
            $form->setUpNewBandForm();
            $this->view->form = $form;
        } else {
            $shippingBandModel->setId($this->getParam('id'))
                ->setStartWeight($this->getParam('start_weight'))
                ->setEndWeight($this->getParam('end_weight'));
            $shippingBandMapper->save($shippingBandModel);
            $historyMapper->saveAction('added', 'shipping band', $adminUserId, $this->getParam('name'));
            $this->_redirect('/admin/shipping/bands');
        }
    }

    public function editbandAction() {
        $shippingBandModel      = new Core_Shipping_Models_ShippingBand();
        $shippingBandsMapper    = new Core_Shipping_Models_ShippingBandMapper();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Editband();

        $bandId = $this->getParam('id');
        $shippingObject = $shippingBandModel->findById($bandId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->getPost()) {
            $form->setUpEditBandForm($shippingObject);
            $this->view->form = $form;
        } else {
            $shippingBandModel->setId($this->getParam('id'))
                ->setStartWeight($this->getParam('start_weight'))
                ->setEndWeight($this->getParam('end_weight'));
            $shippingBandsMapper->save($shippingBandModel);
            $historyMapper->saveAction('edited', 'shipping band', $adminUserId, $this->getParam('start_weight') . '-' . $this->getParam('end_weight'));
            $this->_redirect('/admin/shipping/bands');
        }
    }

    public function deletebandAction() {
        $shippingBandMapper     = new Core_Shipping_Models_ShippingBandMapper();
        $shippingBandModel      = new Core_Shipping_Models_ShippingBand();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Deleteband();

        $bandId = $this->getParam('id');
        $shippingObject = $shippingBandModel->findById($bandId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            $form->setupDeleteBandForm($shippingObject);
            $this->view->form = $form;
        } else {
            $shippingBandMapper->delete($bandId);
            $historyMapper->saveAction('deleted', 'shipping band', $adminUserId, $this->getParam('name'));
            $this->redirect('/admin/shipping/bands');
        }
    }

    public function addcompanyAction() {
        $form = new Core_Shipping_Forms_Addcompany();
        $form->setupForm();
        $this->view->form = $form;
    }

    public function ratesAction() {
        $shippingBandRateMapper = new Core_Shipping_Models_ShippingBandRateMapper();
        $this->view->shippingRates = $shippingBandRateMapper->fetchAll();
    }


    public function optionsAction()
    {
        $optionsMapper = new Core_Shipping_Models_ShippingOptionMapper();
        $this->view->options = $optionsMapper->fetchAll();
    }

    public function addoptionAction() {
        $newOption = $_GET['newOption'];
        $option = new Core_Shipping_Models_ShippingOption();
        $optionMapper = new Core_Shipping_Models_ShippingOptionMapper();

        $option->setOption($newOption);
        $optionMapper->save($option);

        echo 'success';
    }

    public function deleteoptionAction() {
        $option = intval($_GET['deleteOption']);
        $optionMapper = new Core_Shipping_Models_ShippingOptionMapper();
        $optionMapper->delete($option);

        echo 'success';
    }

    public function typesAction()
    {
        $typesMapper = new Core_Shipping_Models_ShippingTypeMapper();
        $this->view->types = $typesMapper->fetchAll();
    }

    public function addtypeAction()
    {
        $type = new Core_Shipping_Models_ShippingType();
        $typeMapper = new Core_Shipping_Models_ShippingTypeMapper();

        $type->setName($this->getParam('name'));
        $type->setDescription($_GET['description']);
        $type->setMinimumWeight($_GET['minweight']);
        $type->setWeightLimit($_GET['weightlimit']);
        $type->setLengthLimit($_GET['lengthlimit']);
        $type->setWidthLimit($_GET['widthlimit']);
        $type->setDepthLimit($_GET['depthlimit']);

        $typeMapper->save($type);

        echo 'success';
    }

    public function deletetypeAction() {
        $type = intval($_GET['deleteType']);
        $typeMapper = new Core_Shipping_Models_ShippingTypeMapper();
        $typeMapper->delete($type);

        echo 'success';
    }

    public function addrateAction() {
        $shippingRateModel      = new Core_Shipping_Models_ShippingRate();
        $shippingRateMapper     = new Core_Shipping_Models_ShippingRateMapper();
        $shippingBandMapper     = new Core_Shipping_Models_ShippingBandMapper();
        $shippingBandRateMapper = new Core_Shipping_Models_ShippingBandRateMapper();
        $shippingBandRateModel  = new Core_Shipping_Models_ShippingBandRate();
        $companyMapper          = new Core_Shipping_Models_CompanyMapper();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Addrate();

        $id = null;
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            $companyArray = $companyMapper->fetchAll();
            $bandArray = $shippingBandMapper->fetchAll();
            $form->setUpForm($companyArray, $bandArray);
        } else {
            $shippingRateModel->setCompany($this->getParam('company'))
                ->setName($this->getParam('name'));
            $shippingRateMapper->save($id, $shippingRateModel);

            $shippingBandRateModel->setBandId($this->getParam('band'))
                ->setRateId($shippingRateMapper->getDbTable()->getAdapter()->lastInsertId())
                ->setPrice($this->getParam('price'));
            $shippingBandRateMapper->save($id, $shippingBandRateModel);
            $historyMapper->saveAction('added', 'shipping rate', $adminUserId, $this->getParam('name'));

            $this->redirect('/admin/shipping/rates');
        }
        $this->view->form = $form;
    }

    public function editrateAction() {
        $shippingBandRateModel  = new Core_Shipping_Models_ShippingBandRate();
        $shippingRateBandMapper = new Core_Shipping_Models_ShippingBandRateMapper();
        $shippingRateModel      = new Core_Shipping_Models_ShippingRate();
        $shippingRateMapper     = new Core_Shipping_Models_ShippingRateMapper();
        $companyMapper          = new Core_Shipping_Models_CompanyMapper();
        $shippingBandMapper     = new Core_Shipping_Models_ShippingBandMapper();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Editrate();

        $id = $this->getParam('id');
        $shippingObject = $shippingBandRateModel->findById($id);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->getPost()) {
            $companyArray = $companyMapper->fetchAll();
            $bandArray = $shippingBandMapper->fetchAll();
            $form->setUpEditRateForm($shippingObject, $companyArray, $bandArray);
            $this->view->form = $form;
        } else {
            $shippingRateModel->setCompany($this->getParam('company'))
                ->setName($this->getParam('name'));
            $shippingRateMapper->save($shippingObject->getRateId(), $shippingRateModel);

            $shippingBandRateModel->setPrice($this->getParam('price'))
                                    ->setBandId($this->getParam('band'));
            $shippingRateBandMapper->save($id, $shippingBandRateModel);
            $historyMapper->saveAction('edited', 'shipping rate', $adminUserId, $shippingRateModel->getName());
            $this->_redirect('/admin/shipping/rates');
        }
    }

    public function deleterateAction() {
        $shippingRateBandModel  = new Core_Shipping_Models_ShippingBandRate();
        $shippingRateBandMapper = new Core_Shipping_Models_ShippingBandRateMapper();
        $companyMapper          = new Core_Shipping_Models_CompanyMapper();
        $shippingBandMapper     = new Core_Shipping_Models_ShippingBandMapper();
        $historyMapper          = new Core_History_Models_HistoryMapper();
        $form                   = new Core_Shipping_Forms_Deleterate();

        $id = $this->getParam('id');
        $shippingObject = $shippingRateBandModel->findById($id);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->getPost()) {
            $companyArray = $companyMapper->fetchAll();
            $bandArray = $shippingBandMapper->fetchAll();
            $form->setUpDeleteRateForm($shippingObject, $companyArray, $bandArray);
            $this->view->form = $form;
        } else {
            $shippingRateBandMapper->delete($id);
            $historyMapper->saveAction('deleted', 'shipping rate', $adminUserId, $shippingObject->getName());
            $this->redirect('/admin/shipping/rates');
        }
    }
}