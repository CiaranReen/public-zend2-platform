<?php

class Admin_OrdersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {
        $orderModel = new Core_Order_Models_Order();
        $users      = array();
        
        foreach($orderModel->getAllOrders() as $order){
            $userModel              = new Core_User_Models_User();
            $userId                 = $order->getUserId();
            $users[$order->getId()] = $userModel->findById($userId);
        }

        // Set up Zend Paginator with data
        $fetchAll = $orderModel->getAllOrders();
        $paginator = Zend_Paginator::factory($fetchAll);

        //Set the page count on POST request
        $page = $this->getParam('page');

        $paginatorSession = new Zend_Session_Namespace('paginator');
        if ($this->getParam('page_count') !== null ) {
            if ($this->getParam('page_count') == 'all') {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            } elseif ($this->getParam('page_count') <= $paginator->getTotalItemCount()) {
                $paginatorSession->pageCount = (int) $this->getParam('page_count');
            } else {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            }
        }

        if(!$paginatorSession->pageCount){
            $paginatorSession->pageCount = 10;
        }

        $pageCount = $paginatorSession->pageCount;

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($pageCount);

        $currentPageItems = $page * $pageCount;

        if ($page == '1' || $page == null) {
            $numberOfItems = 'Showing ' . ($pageCount) . ' of ' . $paginator->getTotalItemCount();
        } elseif ($page < $paginator->getPages()->pageCount) {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($currentPageItems) . ' of ' .
                $paginator->getTotalItemCount();
        } else {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($paginator->getTotalItemCount()) . ' of ' .
                $paginator->getTotalItemCount();
        }
        
        $this->view->title      = 'All Orders';
        $this->view->numberOfItems = $numberOfItems;
        $this->view->allOrders  = $paginator;
        $this->view->users      = $users;
    }
    
    public function viewAction() {
        $orderModel = new Core_Order_Models_Order();
        $userModel  = new Core_User_Models_User();
        $request    = $this->getRequest();
        $order      = '';
        if($request->getParam('orderno')){
            $orderId    = (int) $request->getParam('orderno');
            $order      = $orderModel->findbyId($orderId);
            $user       = $userModel->findById($order->getUserId());
        }
        if($request->getParam('process')){
            $orderId    = (int) $request->getParam('process');
            $order      = $orderModel->findbyId($orderId);
            $user       = $userModel->findById($order->getUserId());
            $order->process();
            $this->_redirect('/admin/orders/dispatch/orderno/'.$orderId);
        }
        
        if(!$order instanceof Core_Order_Models_Order){
            $this->_redirect('/admin/orders/index');
        }
        $this->view->prodImageLocation = '/lifefitness-shop/img/products/';
        $this->view->order  = $order;
        $this->view->user   = $user;
    }
    
    public function dispatchAction(){
        $orderModel     = new Core_Order_Models_Order();
        $userModel      = new Core_User_Models_User();
        $deliveryModel  = new Core_Delivery_Models_Delivery();
        $request        = $this->getRequest();
        $form           = new Core_Order_Forms_dispatch();
        
        $order          = '';
        
        if($request->isPost() && !$form->isValid($request->getParams())){
            $this->view->formError = '<p class="notification error">There were errors</p>';
        }
        elseif($request->isPost() && $form->isValid($form->getValues())){
            $orderId    = (int) $request->getParam('orderno');
            $deliveryModel->setOrderId($orderId)
                        ->setDate(date('Y-m-d H:i:s'))
                        ->setTracking($form->getValue('consignment_number'))
                        ->setComments($form->getValue('comments'));
            $deliveryModel->save();
            $orderModel->findById($orderId)->dispatch();
        }
        
        if($request->getParam('orderno')){
            $orderId    = (int) $request->getParam('orderno');
            $order      = $orderModel->findbyId($orderId);
            $user       = $userModel->findById($order->getUserId());
        }
        
        if(!$order instanceof Core_Order_Models_Order){
            $this->_redirect('/admin/orders/index');
        }
        $this->view->prodImageLocation = '/lifefitness-shop/img/products/';
        $this->view->order  = $order;
        $this->view->user   = $user;
        $this->view->form   = $form;
    }
    
    public function generateSlipAction(){
        $orderModel = new Core_Order_Models_Order();
        $request    = $this->getRequest();
        $order      = '';
        if($request->getParam('orderno')){
            $orderId    = (int) $request->getParam('orderno');
            $order      = $orderModel->findbyId($orderId);
            $order->generatePackingSlip();
        }
        
        if(!$order instanceof Core_Order_Models_Order){
            $this->_redirect('/admin/orders/index');
        }
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function quotewerksExportAction(){
        $orderModel = new Core_Order_Models_Order();
        $quotewerksModel = new Core_Quotewerks_Models_Quotewerks();
        $request    = $this->getRequest();
        $order      = '';
        if($request->getParam('orderno')){
            $orderId    = (int) $request->getParam('orderno');
            $order      = $orderModel->findbyId($orderId);
            $quotewerksModel->export($order);
        }
        
        if(!$order instanceof Core_Order_Models_Order){
            $this->_redirect('/admin/orders/index');
        }
        
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);
    }

}
