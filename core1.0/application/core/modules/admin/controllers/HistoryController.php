<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 04/03/14
 * Time: 16:09
 */

class Admin_HistoryController extends Zend_Controller_Action {

    public function init()
    {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();
        $this->view->user = $user;
    }

    public function indexAction() {
        $historyMapper = new Core_History_Models_HistoryMapper();
        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $history = $historyMapper->fetchAll();

        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->allHistory = $history;
    }
}