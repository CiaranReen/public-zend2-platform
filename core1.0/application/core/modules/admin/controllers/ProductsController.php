<?php

class Admin_ProductsController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {
        $productModel = new Core_Product_Models_Product();

        // Set up Zend Paginator with data
        $fetchAll = $productModel->getAllProducts();
        $paginator = Zend_Paginator::factory($fetchAll);

        //Set the page count on POST request
        $page = $this->getParam('page');

        $paginatorSession = new Zend_Session_Namespace('paginator');
        if ($this->getParam('page_count') !== null ) {
            if ($this->getParam('page_count') == 'all') {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            } elseif ($this->getParam('page_count') <= $paginator->getTotalItemCount()) {
                $paginatorSession->pageCount = (int) $this->getParam('page_count');
            } else {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            }
        }

        if(!$paginatorSession->pageCount){
            $paginatorSession->pageCount = 10;
        }

        $pageCount = $paginatorSession->pageCount;

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($pageCount);

        $currentPageItems = $page * $pageCount;

        if ($page == '1' || $page == null) {
            $numberOfItems = 'Showing ' . ($pageCount) . ' of ' . $paginator->getTotalItemCount();
        } elseif ($page < $paginator->getPages()->pageCount) {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($currentPageItems) . ' of ' .
                $paginator->getTotalItemCount();
        } else {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($paginator->getTotalItemCount()) . ' of ' .
                $paginator->getTotalItemCount();
        }

        $this->view->title = 'All Products';
        $this->view->numberOfItems = $numberOfItems;
        $this->view->allProducts = $paginator;
        $this->view->iconsLocation = '/core/img/icons/';
        $this->view->productImageLocation = '/'.APP_PLATFORM.'/img/products/';

    }

    public function vendorproductsAction() {
    }

    public function addVendorProductAction() {
        $vendorProductsMapper = new Core_Product_Models_VendorProductsMapper();

        $request = $this->getRequest();
        $adminUserId = Core_User_Models_User::getCurrent()->id; // get the admin user id to save the action later

        if (!$request->isPost()) {
            $form = new Core_Product_Forms_VendorProductsForm();
            $this->view->form = $form;
        } else {
            $vendorProductsModel = new Core_Product_Models_VendorProducts();
            $historyMapper = new Core_History_Models_HistoryMapper();
            $vendorProductsModel->setId($this->getParam('vendor_product_id'))
                ->setVendorPartNo($this->getParam('vendor_part_no'))
                ->setDescription($this->getParam('description'))
                ->setManufacturerId($this->getParam('manufacturer'))
                ->setManufacturerPartNo($this->getParam('manufacturer_part_no'))
                ->setCost($this->getParam('cost'));

            $vendorProductsMapper->save($vendorProductsModel);
            $historyMapper->saveAction('added', 'vendor product', $adminUserId, $vendorProductsModel->getDescription());
            $this->redirect('/admin/products/view-vendor-products');
        }

    }

    public function viewManufacturerAction() {
        $vendorMapper = new Core_Product_Models_VendorProductsMapper();
        $manufacturerMapper = new Core_Product_Models_VendorProductManufacturersMapper();

        if ($this->getRequest()->getPost()) {
            $postArray = $this->getRequest()->getPost();
            $id = reset($postArray);
            $name = next($postArray);
            $save = $manufacturerMapper->saveManufacturer($id, $name);
            if ($save === true) {
                $success = 1;
                $this->view->success = $success;
            }
        }
        $manufacturers = $vendorMapper->fetchAllManufacturers();
        $manufacturersForms = array();

        foreach ($manufacturers as $v) {
            $form = new Core_Product_Forms_EditManufacturers();
            $manufacturersForms[]= $form->setupManufacturersForm($v);
        }
        $this->view->forms = $manufacturersForms;
    }

    public function viewVendorProductsAction() {
        $vendorProductsMapper = new Core_Product_Models_VendorProductsMapper();

        // Set up Zend Paginator with data
        $fetchAll = $vendorProductsMapper->fetchAllVendorProducts();
        $paginator = Zend_Paginator::factory($fetchAll);

        //Set the page count on POST request
        $page = $this->getParam('page');

        $paginatorSession = new Zend_Session_Namespace('paginator');
        if ($this->getParam('page_count') !== null ) {
            if ($this->getParam('page_count') == 'all') {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            } elseif ($this->getParam('page_count') <= $paginator->getTotalItemCount()) {
                $paginatorSession->pageCount = (int) $this->getParam('page_count');
            } else {
                $paginatorSession->pageCount = (int) $paginator->getTotalItemCount();
            }
        }

        if(!$paginatorSession->pageCount){
            $paginatorSession->pageCount = 10;
        }

        $pageCount = $paginatorSession->pageCount;

        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage($pageCount);

        $currentPageItems = $page * $pageCount;

        if ($page == '1' || $page == null) {
            $numberOfItems = 'Showing ' . ($pageCount) . ' of ' . $paginator->getTotalItemCount();
        } elseif ($page < $paginator->getPages()->pageCount) {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($currentPageItems) . ' of ' .
                $paginator->getTotalItemCount();
        } else {
            $numberOfItems = 'Showing ' . (($currentPageItems) - ($pageCount - 1)) . ' - ' . ($paginator->getTotalItemCount()) . ' of ' .
                $paginator->getTotalItemCount();
        }

        $this->view->numberOfItems = $numberOfItems;
        $this->view->vendor_products = $paginator;
    }

    public function editVendorProductAction() {
        $vendorProductsMapper = new Core_Product_Models_VendorProductsMapper();
        $form = new Core_Product_Forms_VendorProductsForm();

        $request = $this->getRequest();

        $vendorProductId = $this->getParam('id');
        $vendorProductObject = $vendorProductsMapper->find($vendorProductId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$request->isPost()) {
            $form->edit($vendorProductObject);
            $this->view->form = $form;
        } else {
            $vendorProductsModel = new Core_Product_Models_VendorProducts();
            $historyMapper = new Core_History_Models_HistoryMapper();
            $vendorProductsModel->setId($this->getParam('vendor_product_id'))
                ->setVendorPartNo($this->getParam('vendor_part_no'))
                ->setDescription($this->getParam('description'))
                ->setManufacturerId($this->getParam('manufacturer'))
                ->setManufacturerPartNo($this->getParam('manufacturer_part_no'))
                ->setCost($this->getParam('cost'));

            $vendorProductsMapper->save($vendorProductsModel);
            $historyMapper->saveAction('edited', 'vendor product', $adminUserId, $vendorProductsModel->getDescription());
            $this->redirect('/admin/products/view-vendor-products');
        }

    }

    public function productpacksAction() {
        $ppModel = new Core_Productpacks_Models_Productpacks();
        $packProductsModel = new Core_Productpacks_Models_ProductpacksProducts();

        $ppData = $ppModel->getAllProductPacks();
        $products = $packProductsModel->getAllProducts();

        $this->view->pp_data = $ppData;
        $this->view->products = $products;
    }

    public function editpackAction() {
        $packsModel = new Core_Productpacks_Models_Productpacks();
        $productModel = new Core_Productpacks_Models_ProductpacksProducts();
        $productMapper = new Core_Product_Models_ProductMapper();
        $userMapper = new Core_User_Models_UserMapper();
        $puModel = new Core_Productpacks_Models_ProductpacksUsers();
        $form = new Core_Productpacks_Forms_Edit();

        //Get data
        $ppId = $this->getParam('id');
        $ppObject = $packsModel->findById($ppId);

        $products = $packsModel->getAllProductsById($ppId);
        $users = $packsModel->getAllUsersById($ppId);
        $ppData = $packsModel->getAllProductPacks();

        foreach ($products as $product) {
            $productIds[] = $product->product_id;
        }

        foreach ($users as $user) {
            $userIds[] = $user->user_id;
        }
        if (!$_POST) {
            // Render the form
            $productsArray = $productMapper->fetchAll();
            $usersArray = $userMapper->fetchUsers();
            $form->setupEditForm($productsArray, $usersArray, $ppObject);
            if (isset($productIds)) {
                $form->getElement('products')->setValue($productIds);
            }
            if (isset($userIds)) {
                $form->getElement('users')->setValue($userIds);
            }
        } else {
            // Passed input validation.
            $ppData = array(
                'name' => $_POST['name'],
                'price' => $_POST['price'],
                'status' => $_POST['status'],
            );

            $ppObject->saveProductPack($ppData);
            if (isset($productIds)) {
                $productModel->saveProductsInPack($ppObject, $_POST['products']);
            }
            if (isset($userIds)) {
                $puModel->saveUsersInPack($ppObject, $_POST['users']);
            }
            $url = "/admin/products/productpacks";
            $this->redirect($url);
        }
        $this->view->users = $users;
        $this->view->pp_id = $ppId;
        $this->view->products = $products;
        $this->view->form = $form;
    }

    public function addpackAction() {
        $packsModel = new Core_Productpacks_Models_Productpacks();
        $packProductsModel = new Core_Productpacks_Models_ProductpacksProducts();
        $productPacksMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $productMapper = new Core_Product_Models_ProductMapper();
        $userMapper = new Core_User_Models_UserMapper();
        $form = new Core_Productpacks_Forms_Add();

        //$products = $packsModel->getAllProductsById();

        if (!$_POST) {
            // Render the form
            $productsArray = $productMapper->fetchAll();
            $usersArray = $userMapper->fetchUsers();
            $form->setupAddForm($productsArray, $usersArray, $packsModel);
        } else {
            //Get Product data
            foreach ($_POST['products'] as $productId) {
                $product = new Core_Product_Models_Product();
                $product = $productMapper->find($productId, $product);
                $products[] = $product;
            }

            //Get User Data
            foreach ($_POST['users'] as $userId) {
                $user = new Core_User_Models_User();
                $user = $userMapper->find($userId, $user);
                $users[] = $user;
            }
            //Set Product Data 
            $packsModel->setPackName($_POST['name'])
                ->setPrice($_POST['price'])
                ->setStatus($_POST['status'])
                ->setProducts($products)
                ->setUsers($users);
            //echo '<pre>';var_dump($packsModel->getProducts()); die();

            $productPacksMapper->save($packsModel);

            $url = "/admin/products/productpacks";
            $this->redirect($url);
        }

        //echo '<pre>';var_dump($products); die();
        $this->view->form = $form;
    }

    public function deletepackAction() {
        $packsModel = new Core_Productpacks_Models_Productpacks();
        $form = new Core_Productpacks_Forms_Delete();

        $ppId = $this->getParam('id');
        $packsObject = $packsModel->findById($ppId);

        if (!$_POST) {
            // Render the form
            $productMapper = new Core_Product_Models_ProductMapper();
            $userMapper = new Core_User_Models_UserMapper();
            $productsArray = $productMapper->fetchAll();
            $usersArray = $userMapper->fetchUsers();
            $form->setUpDeleteForm($productsArray, $usersArray, $packsObject);
        } else {
            // Passed input validation.
            $form->getValues();
            $packsModel->delete($ppId);
            $url = "/admin/products/productpacks";
            $this->redirect($url);
        }
        $this->view->form = $form;
    }

    public function addproductdetailsAction() {
        //Set Models
        $productModel = new Core_Product_Models_Product();
        $vplModel = new Core_Product_Models_VendorProductsLink();

        //Set Forms
        $form = new Core_Product_Forms_AddproductForm();

        //Form Post and Validation
        if ($this->getRequest()->isPost() && !$form->isValid($_POST)) {
            $this->view->formErrors = '<p class="notification error">There were errors</p>';
        } elseif ($this->getRequest()->isPost()) {
            $key = 'details';
            $productModel->saveTempProductInfo($_POST, $key);

            //Passed input validation.
            if ($this->getRequest()->getParam('edit')) {
                $productId = (int) $this->getParam('edit');
                $_POST['id'] = $productId;
                $productModel->saveTempProductInfo($_POST, $key);
                $productObject = $productModel->findById($productId);
                if (null !== $productObject->getId()) {
                    $productTempInfo = $productModel->getTempProductInfo();
                    $productTempInfo['variants']['options'] = $productModel->getOptions(); //populate product temp info session options with product object options
                    $productColourways = $productModel->getColourways();
                    foreach ($productColourways as $productColourway) {
                        $productColourwayImages = $productModel->getColourwayImagesByColourwayId($productColourway->getId());
                        foreach ($productColourwayImages as $colourwayImage) {
                            $index = intval($colourwayImage->order) - 1;
                            $savedColourwayImages[$productColourway->getId()][$index] = $colourwayImage->image_url;
                        }
                    }
                    $productTempInfo['variants']['colourways'] = $savedColourwayImages;
                    $key = 'variants';
                    $productModel->saveTempProductInfo($productTempInfo['variants'], $key);
                }
            }
            header("Location: /admin/products/addproductvariants");
            exit();
        }

        if ($this->getRequest()->getParam('new')) {
            $productModel->clearTempProductInfoSession(); //delete current product temp info session just in case a product form was filled but not saved
        }

        //Set All Product Temp info data if it is an edit product request with product requested data
        if ($this->getRequest()->getParam('edit')) {
            $productId = (int) $this->getParam('edit');
            $productObject = $productModel->findById($productId);
            $productIds = array();
            $productdesc = array();

            $products = $vplModel->getAllVendorProductsById($productId);
            foreach ($products as $product) {
                $productdesc[] = $product->description;
                $productIds[] = $product->vendor_product_id;
            }
            $this->view->id = $productIds;
            $this->view->description = $productdesc;
            if (null !== $productObject->getId()) {
                $form->populateUsingModel($productObject); //populate form using the generated product object
                $productModel->clearTempProductInfoSession(); //delete current product temp info session just in case a product form was filled but not saved
            }
        }

        //Populate product details form with all details saved in product temp session if there are any
        $productTempInfo = $productModel->getTempProductInfo();
        if ($productTempInfo) {
            $form->populate($productTempInfo['details']);
        }
        //echo '<pre>'; var_dump($productTempInfo); die();
        //Set Content
        $this->view->title = $form->getDescription();
        $this->view->productTempInfo = $productTempInfo;
        $this->view->form = $form;
    }

    public function addproductvariantsAction() {
        //Set Models
        $productModel = new Core_Product_Models_Product();
        $colourwayModel = new Core_Colourway_Models_Colourway();
        $productOptionModel = new Core_Product_Models_ProductOption();
        $fileTransferAdapter = new Zend_File_Transfer_Adapter_Http();
        $colourwayForm  = new Core_Colourway_Forms_AddColourwayForm();
        $nameContent        = new Core_Content_Models_Content();
        $languageModel      = new Core_Language_Models_Languages();
        $activatedLanguages = $languageModel->getActivatedLanguages();

        $colourwayForm->setAction('/admin/products/addproductvariants');

        //Instantiate variables
        $productTempInfo = array();
        $colourwaysToSave = array();

        //Get Content
        $allColourways = $colourwayModel->getAllColourways();
        $allOptions = $productOptionModel->getAllOptions();
        $productTempInfo = $productModel->getTempProductInfo(); //get temporary product information stored in session
        //to run if data has been posted by form

        $colourwayForm->getElement('submit')->setLabel('Add');
        if($this->getRequest()->getParam('name_gb')){
            if ($this->getRequest()->isPost() && $colourwayForm->isValid($this->getRequest()->getParams())) {
                $name           = strtolower($this->getRequest()->getParam('name_gb'));
                $search         = array(' ', "'");
                $replace        = array('_', '');
                $nameIdentifier = str_replace($search, $replace, $name);
                $nameContent->setIdentifier($nameIdentifier);
                foreach($activatedLanguages as $language){
                    $shortcode  = $language->getIsoShortcode();
                    $content    = $this->getRequest()->getParam('name_'.$shortcode);
                    if($content){
                        $nameContentId = $nameContent->setIsoShortcode($shortcode)
                            ->setContent($content)
                            ->saveContent();
                        if(!$nameContent->getId()){
                            $nameContent->setId($nameContentId);
                        }
                    }
                }

                $options = array(
                    'name' => $nameIdentifier,
                    'hex' => $this->getRequest()->getParam('hex'),
                    'code' => $this->getRequest()->getParam('code')
                );
                $colourwayModel->setOptions($options);
                $colourwayModel->save();

                $this->_redirect("/admin/products/addproductvariants");
                exit();
            }
            else{
                $this->view->error = '<p class="notification error">Please ensure the colourway details have been filled in properly.</p>';
            }

        }


        if ($this->getRequest()->isPost()) {
            try {
                //places all post info in data array | only contains 'options' index
                $data = $_POST;
                //checks if any files have been uploaded
                $filesUploaded = false;
                foreach ($_FILES as $key => $value) {
                    for ($i = 0; $i < sizeof($value['name']); $i++) {
                        if (isset($value['name'][$i]) && $value['name'][$i] != '') {
                            $filesUploaded = true;
                        }
                    }
                }
                //adds validators to file transfer adapter
                $fileTransferAdapter->addValidator('Count', false, array('min' => 1))
                    ->addValidator('Size', false, array('max' => 10000000))
                    ->addValidator('IsImage', false);
                //set up upload destination
                $path = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/'.APP_PLATFORM.'/img/products/';
//                die($path);
                $fileTransferAdapter->setDestination($path);
                //get all file information
                $files = $fileTransferAdapter->getFileInfo();


                foreach ($files as $fieldname => $fileinfo) {
                    $parts = explode('_', $fieldname);
                    $parts2 = explode('-', $parts[0]);
                    $index = $parts[1];
                    $colourwayId = $parts2[1]; //get colourway id from input name which is posted as eg colourway-1_0_
                    //Any images that are deleted are posted as colourway-1 where 1 represents the colourwayId
                    //This checks if there are any deleted images and then saved as an empty value of a colourway Index
                    if (isset($_POST['colourway-' . $colourwayId])) {
                        foreach ($_POST['colourway-' . $colourwayId] as $key => $value) {
                            $colourwaysToSave[$colourwayId][$key] = '';
                        }
                    }
                    //runs if files have been uploaded
                    if ($filesUploaded) {
                        //checks if a file has been selected for this specific input field before attempting to save to folder
                        if ($fileinfo['name'] != '' && $fileTransferAdapter->isUploaded($fileinfo['name']) && $fileTransferAdapter->isValid($fileinfo['name'])) {
                            $extension = substr($fileinfo['name'], strrpos($fileinfo['name'], '.') + 1); //gets files extension
                            $filename = 'prod_' . date('Ymdhs') . substr((string)microtime(), 1, 8) . '.' . $extension; //gets filename
                            $fileTransferAdapter->addFilter('Rename',array('target' => $path . $filename));
                            $fileTransferAdapter->receive($fileinfo['name']); //uploads file using file transfer object
                            $colourwaysToSave[$colourwayId][$index] = $filename; //save filename to array
                        }
                    }

                }
                //checks if the user has previously saved some product colourways in the product temporary info session and wants to edit them
                if ($productTempInfo['variants']['colourways']) {
                    $data['colourways'] = $productTempInfo['variants']['colourways']; //sets the colourways to the saved to what was in the previous temp session
                    foreach ($colourwaysToSave as $key1 => $value1) {
                        foreach ($value1 as $key2 => $value2) {
                            if ($value2) {
                                $data['colourways'][$key1][$key2] = $value2; //sets the data array index to the colourway to be saved
                            } else {
                                unset($data['colourways'][$key1][$key2]); //deletes the data array index where the colourway image was deleted
                                if (empty($data['colourways'][$key1])) {
                                    unset($data['colourways'][$key1]); //unsets the colourway parent index where all sub indexes have been unset
                                }
                            }
                        }
                    }
                } else {
                    $data['colourways'] = $colourwaysToSave; //saves all uploaded files to data array where the product temp info session is empty
                }
                //ensures both 'options' and 'colourways' have been passed to be saved in the temp product session
                if (isset($data['options']) && isset($data['colourways'])) {
                    $key = 'variants'; //sets key for saving to product temp session
                    $productModel->saveTempProductInfo($data, $key);
                    header("Location: /admin/products/addproductstock"); //redirect to next section of adding a product (add-stock page)
                }
                else{
                    $this->view->error = '<p class="notification error">Please ensure an option has been selected and at least one image has been uploaded.</p>';
                }
            }
            catch (Exception $e) {
                $errorMessage = $e->getMessage();
                $this->view->error = '<p class="notification error">' . $errorMessage . '</p>';
            }
        }
        //Set Content
        $this->view->headTitle = 'Add Product Variant &middot; Admin Panel &middot; CO3 Fight Science';
        $this->view->title = 'Add Product Variants';
        $this->view->allColourways = $allColourways;
        $this->view->allOptions = $allOptions;
        $this->view->productImageLocation = '/' . APP_PLATFORM . '/img/products/';
        $this->view->productTempInfo = $productTempInfo;
        $this->view->colourwayForm = $colourwayForm;
    }

    public function addproductstockAction() {
        //Set Models
        $productModel = new Core_Product_Models_Product();
        $historyMapper = new Core_History_Models_HistoryMapper();

        //Instantiate variables
        $generatedStock = array();
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        //Get Content
        $productTempInfo = $productModel->getTempProductInfo();
        $generatedStock = $productModel->generateStockFromVariants();
        $action = 'added';
        if (isset($productTempInfo['details']['id'])) {
            $action = 'edited';
            $editProductId = (int) $productTempInfo['details']['id'];
            $currentStock = $productModel->findById($editProductId)->getStock();
            $this->view->currentStock = $currentStock;
        }
        //checks if data has been posted
        if ($this->getRequest()->isPost()) {
            try {
                $productModel->saveProduct($_POST); //saves the product
                $historyMapper->saveAction($action, 'product', $adminUserId, $productTempInfo['details']['name_gb']); //Save the added product into history list
                $productModel->clearTempProductInfoSession(); //clears the temporary product session
                header("Location: /admin/products/index"); //redirects to the product page
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                $this->view->error = '<p class="notification error">' . $errorMessage . '</p>';
            }
        }
        //Set Content
        $this->view->headTitle = 'Add Product Stock &middot; Admin Panel &middot; CO3 Life Fitness';
        $this->view->title = 'Add Product Stock';
        $this->view->generatedStock = $generatedStock;
        $this->view->productTempInfo = $productTempInfo;
    }

    public function statsAction() {
        $productMapper = new Core_Product_Models_ProductMapper();
        $productModel = new Core_Product_Models_Product();

        if (!$this->getRequest()->isPost()) {
            $form = new Core_Product_Forms_Stats();
            $productsArray = $productMapper->fetchAll();
            $form->setUpStatForm($productsArray);
            $this->view->form = $form;
        } else {
            // Save highest rated
            $highest = array(
                'highest' => $this->getParam('highest'),
            );
            $productModel->saveHighest($highest);

            //Save Featured
            $featured = array(
                'featured' => $this->getParam('featured'),
            );
            $productModel->saveFeatured($featured);

            //Save New
            $new = array(
                'new' => $this->getParam('new'),
            );
            $productModel->saveNew($new);
            $url = '/admin/products/index';
            $this->redirect($url);
        }
    }

    public function manageColourwaysAction() {
        //Set models
        $colourwayModel     = new Core_Colourway_Models_Colourway();
        $nameContent        = new Core_Content_Models_Content();
        $languageModel      = new Core_Language_Models_Languages();
        $request            = $this->getRequest();
        $activatedLanguages = $languageModel->getActivatedLanguages();
        //Set forms
        $form = new Core_Colourway_Forms_AddColourwayForm();

        //save post data from form
        if ($request->getParam('edit')) {
            $colourwayId = (int) $this->getParam('edit');
            $colourwayObject = $colourwayModel->findById($colourwayId);
            if($colourwayObject->getNameContent()){
                $nameContent->setId($colourwayObject->getNameContent()->content_id);
            }
            $form->populate($colourwayObject);
        }
        if ($request->isPost() && !$form->isValid($request->getParams())) {
            $this->view->form_errors = '<p class="notification error">There were errors</p>';
        } else if ($request->isPost()) {
            $name           = strtolower($form->getValue('name_gb'));
            $search         = array(' ', "'");
            $replace        = array('_', '');
            $nameIdentifier = str_replace($search, $replace, $name);
            $nameContent->setIdentifier($nameIdentifier);
            foreach($activatedLanguages as $language){
                $shortcode  = $language->getIsoShortcode();
                $content    = $form->getValue('name_'.$shortcode);
                if($content){
                    $nameContentId = $nameContent->setIsoShortcode($shortcode)
                        ->setContent($content)
                        ->saveContent();
                    if(!$nameContent->getId()){
                        $nameContent->setId($nameContentId);
                    }
                }
            }

            $options = array(
                'name' => $nameIdentifier,
                'hex' => $form->getValue('hex'),
                'code' => $form->getValue('code')
            );
            if (isset($colourwayId)) {
                $options['id'] = $colourwayId;
            }
            $colourwayModel->setOptions($options);
            $colourwayModel->save();
        }

        //Get content
        $allColourways = $colourwayModel->getAllColourways();

        //Set content
        $this->view->title = 'Colourways';
        $this->view->allColourways = $allColourways;
        $this->view->form = $form;
    }

    public function manageCategoriesAction(){
        $categoryModel = new Core_Category_Models_Category();
        $allCategories = $categoryModel->getAllCategories();
        $parentCategories = $categoryModel->getAllParentCategories();

        foreach ($parentCategories as $parentCategory) {
            $childCategories[$parentCategory->getId()] = $parentCategory->getChildCategories();
        }

        $this->view->title  = 'Categories';
        $this->view->categoryImageLocation = '/'.APP_PLATFORM.'/img/categories/';
        $this->view->parentCategories = $parentCategories;
        $this->view->childCategories = $childCategories;
        $this->view->allCategories = $allCategories;
    }

    public function deleteCategoryAction(){
        $categoryModel      = new Core_Category_Models_Category();
        $categoryMapper     = new Core_Category_Models_CategoryMapper();
        $categoryForm       = new Core_Category_Forms_AddCategory();
        $languageModel      = new Core_Language_Models_Languages();
        $nameContent        = new Core_Content_Models_Content();
        $descriptionContent = new Core_Content_Models_Content();
        $historyMapper      = new Core_History_Models_HistoryMapper();

        $request            = $this->getRequest();
        $activatedLanguages = $languageModel->getActivatedLanguages();

        if(!$request->getParam('id')){
            $this->_redirect('/admin/products/manage-categories/');
        }
        $categoryId  = (int) $request->getParam('id');
        $category = $categoryModel->findById($categoryId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;
        if(!$category instanceof Core_Category_Models_Category){
            $this->_redirect('/admin/products/manage-categories/');
        }
        if($category->getNameContent()){
            $nameContent->setId($category->getNameContent()->content_id);
        }
        if($category->getDescriptionContent()){
            $descriptionContent->setId($category->getDescriptionContent()->content_id);
        }
        $categoryForm->setUpDeleteForm($category);
        $this->view->title = "Delete '".$category->getNameByLanguage()."' Category";

        if($request->isPost()){
            if($category->getChildCategories()){
                $this->view->formErrors = '<p class="notification error">Sorry. This category has subcategories and can therefore not be deleted.</p>';
            }
            elseif($category->getProducts()){
                $this->view->formErrors = '<p class="notification error">Sorry. This category is assigned to products and can therefore not be deleted.</p>';
            }
            else{
                $categoryMapper->delete($category->getId());
                $historyMapper->saveAction('deleted', 'category', $adminUserId, $category->getName());
                $this->_redirect('/admin/products/manage-categories/');
            }
        }

        $this->view->activatedLanguages = $activatedLanguages;
        $this->view->form               = $categoryForm;
    }

    public function categoryAction(){
        $categoryModel      = new Core_Category_Models_Category();
        $categoryForm       = new Core_Category_Forms_AddCategory();
        $languageModel      = new Core_Language_Models_Languages();
        $request            = $this->getRequest();
        $activatedLanguages = $languageModel->getActivatedLanguages();
        $nameContent        = new Core_Content_Models_Content();
        $descriptionContent = new Core_Content_Models_Content();

        if($request->getParam('id')){
            $categoryId     = (int) $request->getParam('id');
            $categoryModel->findById($categoryId);
            if($categoryModel->getNameContent()){
                $nameContent->setId($categoryModel->getNameContent()->content_id);
            }
            if($categoryModel->getDescriptionContent()){
                $descriptionContent->setId($categoryModel->getDescriptionContent()->content_id);
            }
            $categoryForm->populate($categoryModel);
            $this->view->title = "Edit '".$categoryModel->getNameByLanguage()."' Details";
        }
        else{
            $this->view->title = "Add New Category";
        }

        if($request->isPost() && !$categoryForm->isValid($request->getParams())){
            $this->view->formErrors = '<p class="notification error">There were errors</p>';
        }
        elseif($request->isPost() && $categoryForm->isValid($request->getParams())){
            $name           = strtolower($categoryForm->getValue('name_gb'));
            $search         = array(' ', "'");
            $replace        = array('_', '');
            $nameIdentifier = str_replace($search, $replace, $name);
            $nameContent->setIdentifier($nameIdentifier);
            foreach($activatedLanguages as $language){
                $shortcode  = $language->getIsoShortcode();
                $content    = $categoryForm->getValue('name_'.$shortcode);
                if($content){
                    $nameContentId = $nameContent->setIsoShortcode($shortcode)
                        ->setContent($content)
                        ->saveContent();
                    if(!$nameContent->getId()){
                        $nameContent->setId($nameContentId);
                    }
                }
            }

            $description            = strtolower($categoryForm->getValue('name_gb')) . '_desc';
            $descriptionIdentifier  = str_replace($search, $replace, $description);
            $descriptionContent->setIdentifier($descriptionIdentifier);
            foreach($activatedLanguages as $language){
                $shortcode  = $language->getIsoShortcode();
                $content    = $categoryForm->getValue('description_'.$shortcode);
                if($content){
                    $descriptionContentId = $descriptionContent->setIsoShortcode($shortcode)
                        ->setContent($content)
                        ->saveContent();
                    if(!$descriptionContent->getId()){
                        $descriptionContent->setId($descriptionContentId);
                    }
                }
            }

            $categoryModel->setName($nameIdentifier)
                ->setDescription($descriptionIdentifier)
                ->setParentId($categoryForm->getValue('parent'))
                ->save();
        }


        $this->view->activatedLanguages = $activatedLanguages;
        $this->view->form               = $categoryForm;
    }

    public function uploadAction() {
        $adapter = new Zend_File_Transfer_Adapter_Http();
        $manModel = new Core_Product_Models_VendorProducts();

        $adapter->setDestination('/' . APP_PLATFORM.'/csv');

        if (!$adapter->receive()) {
            $messages = $adapter->getMessages();
            echo implode("\n", $messages);
        }
        $file = $adapter->getFileName();
        $manModel->readDataFromCsv($file);
    }

    public function manageBasketrulesAction(){
        $basketruleModel        = new Core_Basketrule_Models_Basketrule();
        $allBasketrules         = $basketruleModel->getAllRules();
        $this->view->allRules   = $allBasketrules;
        $this->view->title      = 'Basket Rules';
    }

    public function addbasketruleAction(){
        $basketruleModel            = new Core_Basketrule_Models_Basketrule();
        $basketruleTemplateModel    = new Core_Basketrule_Models_BasketruleTemplate();
        $basketruleUsergroupModel   = new Core_Basketrule_Models_BasketruleUsergroup();
        $basketruleProductModel     = new Core_Basketrule_Models_BasketruleProduct();
        $usergroupModel             = new Core_Usergroup_Models_Usergroup();
        $form                       = new Core_Basketrule_Forms_Addbasketrule();
        $historyMapper              = new Core_History_Models_HistoryMapper();
        $request                    = $this->getRequest();
        $adminUserId                = Core_User_Models_User::getCurrent()->id;

        if($request->getParam('id')){
            $basketruleId = (int) $request->getParam('id');
            $basketrule = $basketruleModel->findById($basketruleId);
            $basketruleProduct = $basketruleProductModel->findByBasketRuleId($basketruleId);
            $form->populate($basketrule);
            if($basketruleProduct->getProducts()){
                foreach($basketruleProduct->getProducts() as $product){
                    $productIds[]   = $product->getId();
                    $productNames[] = $product->getNameByLanguage();
                }
                $this->view->productIds = $productIds;
                $this->view->productNames = $productNames;
            }
        }
        if($request->isPost() && !$form->isValid($this->getRequest()->getParams())){
            $this->view->formErrors = '<p class="notification error">There were errors</p>';
        }
        elseif($request->isPost() && $form->isValid($this->getRequest()->getParams())){
            $usergroups = $form->getValue('usergroup');
            $products   = $request->getParam('products');
            $startDate = date("Y-m-d", strtotime($form->getValue('start_date')));
            $endDate = date("Y-m-d", strtotime($form->getValue('end_date')));
            $basketruleModel->setName($form->getValue('name'))
                ->setConditionId($form->getValue('condition'))
                ->setQuantity($form->getValue('quantity'))
                ->setQuantityMax($form->getValue('quantity_2'))
                ->setDiscount($form->getValue('discount'))
                ->setDiscountTypeId($form->getValue('discount_type'))
                ->setStartDate($startDate)
                ->setEndDate($endDate);
            if(!$request->getParam('id')) {
                $basketruleId = $basketruleModel->save();
                $historyMapper->saveAction('added', 'basket rule', $adminUserId, $form->getValue('name'));
            }else{
                $basketruleId = (int) $request->getParam('id');
                $basketruleModel->setId($basketruleId)->save();
                $historyMapper->saveAction('edited', 'basket rule', $adminUserId, $form->getValue('name'));
            }

            if(sizeof($usergroups) == 1 && array_shift($usergroups) == 0){
                //means all usergroups were selected
                $usergroups = array_keys($usergroupModel->getPairs());
            }
            if(sizeof($products) > 0){
                $basketruleProductModel->setBasketRuleId($basketruleId)
                    ->setProductIds($products)
                    ->save();
            }
            $basketruleUsergroupModel->setBasketRuleId($basketruleId)
                ->setUsergroupIds($usergroups)
                ->save();

            $this->_redirect('/admin/products/manage-basketrules');
        }
        $this->view->title          = 'Add a Basket Rule';
        $this->view->form           = $form;
        $this->view->allTemplates   = $basketruleTemplateModel->getAllTemplates();
    }

}
