<?php

class Admin_SettingsController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {

    }

    public function ratesAction() {
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();

        if ($this->getRequest()->getPost()) {
            $postArray = $this->getRequest()->getPost();
            $cId = reset($postArray);
            $value = next($postArray);
            $save = $currencyMapper->saveValue($value, $cId);
            if ($save === true) {
                $success = 1;
                $this->view->success = $success;
            }
        }
        $currencies = $currencyMapper->getCurrencies();
        $rateForms = array();

        foreach ($currencies as $c) {
            $form = new Core_Settings_Forms_Rates();
            $rateForms[]= $form->setupRatesForm($c);
        }
        $this->view->forms = $rateForms;
    }

    public function socialmediaAction() {
        $socialMediaMapper = new Core_SocialMedia_Models_SocialMediaMapper();
        $activeSocialMedia = $socialMediaMapper->getActiveSocialMedia();
        $availableSocialMedia = $socialMediaMapper->getAvailableSocialMedia();

        $this->view->active_social_media = $activeSocialMedia;
        $this->view->available_social_media = $availableSocialMedia;
    }

    public function editsocialmediaAction() {
        $socialMediaModel = new Core_SocialMedia_Models_SocialMedia();
        $editForm = new Core_SocialMedia_Forms_Edit();

        $request = $this->getRequest();
        $smId = $this->getParam('id');
        $smObject = $socialMediaModel->findById($smId);
        $this->view->sm_edit = $smObject;

        if (!$request->isPost()) {
            $editForm->setupEditForm($smObject);
            $this->view->editForm = $editForm;
        } else {
            $smObject->setLink($this->getParam('link'))
                ->setActive($this->getParam('active'));
            $socialMediaModel->save();
            $this->redirect('/admin/settings/socialmedia');
        }
    }

    public function languagesAction() {
        $languageModel = new Core_Language_Models_Languages();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $form = new Core_Language_Forms_Activatelanguage();
        $errors = '';
        $success = '';

        if(isset($_GET['deactivate']))
        {
            $id = $_GET['deactivate'];
            try
            {
                if(!is_numeric($id))
                {
                    throw new InvalidArgumentException(
                        'Cannot deactivate language as incorrect id was provided'
                    );
                }
            }
            catch(InvalidArgumentException $e)
            {
                echo $e->getMessage();
            }

            $languageMapper->deactivateLanguage($id);
        }

        //Check to see whether the form has been submitted
        if(isset($_POST['submit']))
        {
            //If the form has been submitted
            if(!$form->isValid($_POST))
            {
                //If the form is not valid
                $errors = '<p class="notification error">There were errors</p>';
            }
            else
            {
                //If the form is valid
                $languageId = intval($_POST['language']);
                $languageModel->activateLanguage($languageId);
                $success = '<p class="notification success">The language was successfully activated for this site!</p>';
            }
        }

        $unusedLanguageData = $languageModel->getUnactivatedLanguages();
        $form->setupAddForm($unusedLanguageData);

        //$page->setTitle('Manage Language Settings &middot; Admin Panel &middot; CO3 Core');
        $this->view->form_errors = $errors;
        $this->view->form_success = $success;
        $this->view->languages = $languageModel->getActivatedLanguages();
        $this->view->form = $form;
    }

}