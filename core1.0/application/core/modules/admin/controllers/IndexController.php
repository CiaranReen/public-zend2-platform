<?php
class Admin_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $this->view->user = $user;
    }
    
    public function indexAction()
    {
        /* Initialize action controller here */
        $orderModel     = new Core_Order_Models_Order();
        $orderMapper     = new Core_Order_Models_OrderMapper();
        $searchLogModel = new Core_Search_Models_SearchLog();
        $productModel = new Core_Product_Models_Product();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $notificationMapper = new Core_Notification_Models_NotificationMapper();

        $allOrderStats  = $orderModel->getAllOrderStats();
        $totalRevenue = $orderMapper->allOrdersTotal();

        //TODO conversion rate
        //$conversionRate = $allOrderStats->totalNumber / $numberOfVisits;

        $history = $historyMapper->fetchLatestTen();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $settings       = new Core_Settings_Models_Settings();
        if($settings->getGoogleAnalytics()){
            $this->view->ga = $settings->getGoogleAnalytics();
        }
        else {
            $this->view->ga = 'Not set up';
        }

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->mostPopularProducts = $productModel->getMostPopularProducts(5);
        $this->view->topFiveSearchLogs = $searchLogModel->getTopFiveSearchLogs();
        $this->view->allOrderStats = $allOrderStats;
        $this->view->totalRevenue = $totalRevenue;
        $this->view->history = $history;
    }
}

