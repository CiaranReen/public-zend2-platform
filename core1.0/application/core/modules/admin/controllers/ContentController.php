<?php

class Admin_ContentController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();
        $this->view->user = $user;
    }

    public function indexAction() {
        $languageModel = new Core_Language_Models_Languages();
        $form = new Core_Language_Forms_Chooselanguage();
        $notificationMapper = new Core_Notification_Models_NotificationMapper();

        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $errors = '';
        $success = '';

        //Check to see whether the form has been submitted
        if(isset($_POST['submit']))
        {
            //If the form has been submitted
            if(!$form->isValid($_POST))
            {
                //If the form is not valid
                $errors = 'There were errors';
            }
            else
            {
                //If the form is valid
                $languageId = intval($_POST['language']);
                $languageModel->activateLanguage($languageId);
                $success = '<p class="notification success">The language was successfully activated for this site!</p>';
            }
        }

        $languages = $languageModel->getActivatedLanguages();
        $form->setupForm($languages);

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->form_errors = $errors;
        $this->view->form_success = $success;
        $this->view->form = $form;
    }

    public function activatelanguagesAction()
    {
        $languageModel = new Core_Language_Models_Languages();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $form = new Core_Language_Forms_Activatelanguage();
        $errors = '';
        $success = '';
        
        if(isset($_GET['deactivate']))
        {
            $id = $_GET['deactivate'];
            try
            {
                if(!is_numeric($id))
                {
                    throw new InvalidArgumentException(
                        'Cannot deactivate language as incorrect id was provided'
                    );
                }
            }
            catch(InvalidArgumentException $e)
            {
                echo $e->getMessage();
            }
            
            $languageMapper->deactivateLanguage($id);
        }
        
        //Check to see whether the form has been submitted
        if(isset($_POST['submit']))
        {
            //If the form has been submitted
            if(!$form->isValid($_POST)) 
            {
                //If the form is not valid
                $errors = '<p class="notification error">There were errors</p>';
            }
            else
            {
                //If the form is valid
                $languageId = intval($_POST['language']);
                $languageModel->activateLanguage($languageId);
                $success = '<p class="notification success">The language was successfully activated for this site!</p>';
            }
        }
        
        $unusedLanguageData = $languageModel->getUnactivatedLanguages();
        $form->setupAddForm($unusedLanguageData);
        
        //$page->setTitle('Manage Language Settings &middot; Admin Panel &middot; CO3 Core');
        $this->view->form_errors = $errors;
        $this->view->form_success = $success;
        $this->view->languages = $languageModel->getActivatedLanguages();
        $this->view->form = $form;
    }
    
    public function manageAction()
    {
        $languageModel = new Core_Language_Models_Languages();
        $form = new Core_Content_Forms_Editlanguage();
        
        $errors = '';
        $success = '';
        
        //Check to see whether the form has been submitted
        try
        {
            if(!isset($_POST['language']) || !is_numeric($_POST['language']))
            {
                throw new RuntimeException(
                    'Cannot edit content because no language has been selected'
                );
            }
        }
        catch(RuntimeException $e)
        {
            echo $e->getMessage();
        }
        
        $languageId = $_POST['language'];
        $language = $languageModel->getLanguageById($languageId);
        
        if(isset($_POST['editsubmit']))
        {
            $contentInput = array();
            
            foreach($_POST as $contentid => $postvar)
            {
                /*
                 * Need to check that this is EXACTLY equal to false because
                 * the position of content is always going to be 0 which is
                 * also interpreted as false.
                 */
                if(strpos($contentid, 'content') !== false)
                {
                    $idarray = explode('content',$contentid); 
                    $contentInput[$idarray[1]] = $postvar;
                }
            }
            
            $language->updateContent($contentInput);
        }
        
        $form->setupForm($language);
        
        $this->view->language = $language->getName();
        $this->view->flag = $language->getIsoShortcode().'.png';
        $this->view->form_errors = $errors;
        $this->view->form_success = $success;
        $this->view->form = $form;
    }
}