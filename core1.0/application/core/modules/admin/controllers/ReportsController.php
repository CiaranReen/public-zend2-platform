<?php

class Admin_ReportsController extends Zend_Controller_Action {


    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $user = Core_User_Models_User::getCurrent();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {
        $reportMapper = new Core_Report_Models_ReportMapper();
        $savedReports = $reportMapper->getSavedReports();
        $this->view->savedReports = $savedReports;

    }

    // normal reports
    public function loginsAction() {
        $userModel = new Core_User_Models_User();
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $loginData = $userModel->getLastLogin();
        $usergroups = $ugModel->getAllUserGroups();
        $this->view->logins = $loginData;
        $this->view->usergroups = $usergroups;
    }

    public function ordersAction() {
        $orderModel = new Core_Order_Models_Order();
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $orderData = $orderModel->getAllOrders('resultSet');
        $usergroups = $ugModel->getAllUserGroups();
        $this->view->orders = $orderData;
        $this->view->usergroups = $usergroups;
    }

    public function registrationAction() {
        $userModel = new Core_User_Models_User();
        $regData = $userModel->getRegistration();
        $this->view->registration = $regData;
    }

    public function dispatchAction() {
        $dispatchModel = new Core_Order_Models_Order();
        $dispatch = $dispatchModel->getDispatchReport();
        $this->view->dispatch = $dispatch;
    }

    public function soldAction() {
        $orderModel = new Core_Order_Models_Orderpart();
        $soldData = $orderModel->getSoldReports();
        $this->view->sold = $soldData;
    }

    public function stockAction() {
        $stockMapper = new Core_Stock_Models_StockMapper();
        $categoryModel = new Core_Category_Models_Category();
        $stockData = $stockMapper->getStockReport();
        $category = $categoryModel->fetchAllCategories();
        $this->view->stock = $stockData;
        $this->view->category = $category;
    }

    public function stockchangesAction() {
        $stockMapper = new Core_Stock_Models_StockMapper();
        $productMapper = new Core_Product_Models_ProductMapper();
        $categoryModel = new Core_Category_Models_Category();
        $stockData = $stockMapper->getStockReport();
        $category = $categoryModel->fetchAllCategories();
        $this->view->stock = $stockData;
        $this->view->category = $category;
    }

    public function vouchersAction() {
        $vModel = new Core_Voucher_Models_Voucher();
        $vouchers = $vModel->fetchAll();
        $this->view->vouchers = $vouchers;
    }

    // builder for reports
    public function buildAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        if ($this->getParam('submit') === 'Export As Csv' || $this->getParam('submit') === "exportAsCsv") {
            $reportModel = new Core_Report_Models_Report();
            if ($this->getParam('id') == true) {
                $id = $this->getParam('id');
                $reportObject = $reportModel->findById($id);

                //Get all the data from the model
                $columns = $reportObject->getColumns();
                $usergroups = $reportObject->getUsergroups();
                $categories = $reportObject->getCategories();
                $report = $reportObject->getType();

                //Now unserialize
                $columns = unserialize($columns);
                $usergroups = unserialize($usergroups);
                $categories = unserialize($categories);

            } else {
                $columns = $this->getParam('columns');

                if ($this->getParam('usergroups') != null) {
                    $usergroups = $this->getParam('usergroups');
                } else {
                    $usergroups = null;
                }

                if ($this->getParam('categories') != null) {
                    $categories = $this->getParam('categories');
                } else {
                    $categories = null;
                }

                $report = $reportModel->loadReport();
            }

            if ($this->getParam('from') != null) {
                $dateFrom = $this->getParam('from');
            } else {
                $dateFrom = '';
            }

            if ($this->getParam('to') != null) {
                $dateTo = $this->getParam('to');
            } else {
                $dateTo = '';
            }

            //Check the start date is not further ahead in time than the end date.
            if ($dateFrom <= $dateTo) {
                $reportModel->build($columns, $report, $categories, $usergroups, $dateFrom, $dateTo); // build the report
                die();

            } else {
                echo 'The start date cannot be greater than the end date.';
                die();
            }

        } else if ($this->getParam('submit') === 'Save') {
            $reportModel = new Core_Report_Models_Report();

            $reportModel->setName($this->getParam('name'))
                ->setColumns($this->getParam('columns'))
                ->setUsergroups($this->getParam('usergroups'))
                ->setCategories($this->getParam('categories'))
                ->setType($this->getParam('type'));

            $reportModel->save();
            $this->redirect('/admin/reports');
            //die();

        }
    }

    public function deleteAction() {
        $reportModel = new Core_Report_Models_Report();
        $id = $this->getParam('id');

        $reportModel->delete($id);
        $this->redirect('/admin/reports');
    }

    public function editAction() {
        $reportModel = new Core_Report_Models_Report();
        $id = $this->getParam('id');

        $reportModel->delete($id);
        $this->redirect('/admin/reports');
    }
}
