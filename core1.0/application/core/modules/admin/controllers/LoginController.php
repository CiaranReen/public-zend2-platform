<?php
class Admin_LoginController extends Zend_Controller_Action
{
    public function init()
    {
        $userModel = new Core_User_Models_User();
        $user = Core_User_Models_User::getCurrent();
        if($user && $user->isLoggedInAsAdmin()){
            $this->_redirect('/admin');
        }
        $this->_helper->layout->setLayout('login');
        $this->view->user = false;

    }
    
    public function indexAction() {
        $userModel  = new Core_User_Models_User();
        $loginForm  = new Core_User_Forms_AdminLogin();
        $request    = $this->getRequest();
        $this->view->title = 'Admin Login';
        
        $this->view->loginForm = $loginForm;
        if (!$this->getRequest()->isPost()) {
            // Render the blank form
        } else if (!$loginForm->isValid($request->getParams())) {
            $this->view->formErrors = 'There were errors';
        } else {

            // Passed input validation.
            // Check if this user is allowed to login to the admin
            $username = $loginForm->getValue('username');
            $password = $loginForm->getValue('password');
            $user = $userModel->processLogin($username, $password);
            if ($user->isLoggedInAsAdmin()) {
                $this->_redirect('/admin/'); // Redirect to admin homepage
            } else {
                // Failed login
                $this->view->formErrors = 'Your username or password were not recognised or you are not an Administrator. Please try again';
            }
        }
    }
}
 
