<?php

class Admin_VouchersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {
        $vModel = new Core_Voucher_Models_Voucher();
        $displayAll = $vModel->fetchAll();
        $this->view->vouchers = $displayAll;
    }

    public function editAction() {
        $vModel         = new Core_Voucher_Models_Voucher();
        $userMapper     = new Core_User_Models_UserMapper();
        $historyMapper  = new Core_History_Models_HistoryMapper();
        $form           = new Core_Voucher_Forms_Edit();

        $vId = $this->getParam('id');
        $vObject = $vModel->findById($vId);
        $users = $vModel->getAllUsersById($vId);
        $usergroups = $vModel->getAllUsergroupsById($vId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$_POST) {
            $usersArray = $userMapper->fetchUsers();
            $form->setUpEditForm($vObject, $usersArray);
            //if (isset($userIds)) {
            //$form->getElement('users')->setValue($userIds);
            //}
        } else {
            $vObject->setName($this->getParam('voucher_name'))
                ->setAmount($this->getParam('amount'))
                ->setCode($this->getParam('code'))
                ->setStatus($this->getParam('status'))
                ->setOperand($this->getParam('operand'))
                ->setStart($this->getParam('start_date'))
                ->setExpire($this->getParam('expiry_date'));
            $vObject->save($vObject);
            $historyMapper->saveAction('edited', 'voucher', $adminUserId, $this->getParam('voucher_name'));

            $url = "/admin/vouchers";
            $this->redirect($url);
        }

        $vObject = $vModel->findById($vId);

        $this->view->users = $users;
        $this->view->usergroups = $usergroups;
        $this->view->vouchers = $vObject;
        $this->view->form = $form;
    }

    public function addAction() {
        $vModel         = new Core_Voucher_Models_Voucher();
        $userMapper     = new Core_User_Models_UserMapper();
        $historyMapper  = new Core_History_Models_HistoryMapper();
        $form           = new Core_Voucher_Forms_Add();

        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            $usersArray = $userMapper->fetchUsers();
            $form->setUpAddForm($usersArray);
        } else {
            $vModel->setName($this->getParam('voucher_name'))
                ->setAmount($this->getParam('amount'))
                ->setCode($this->getParam('code'))
                ->setStatus($this->getParam('status'))
                ->setOperand($this->getParam('operand'))
                ->setStart($this->getParam('start_date'))
                ->setExpire($this->getParam('expiry_date'));

            $vModel->save($vModel);
            $historyMapper->saveAction('added', 'voucher', $adminUserId, $this->getParam('voucher_name'));
            $url = "/admin/vouchers";
            $this->redirect($url);
        }

        $this->view->form = $form;
    }

    public function changeusersAction() {
        $userModel      = new Core_User_Models_User();
        $ugModel        = new Core_Usergroup_Models_Usergroup();
        $vModel         = new Core_Voucher_Models_Voucher();

        $vId = $this->getParam('id');
        if (isset($vId)) {
            $usersInVoucher = $vModel->getAllAssignedUsers($vId);
            $usergroupsInVoucher = $vModel->getAllAssignedUsergroups($vId);
            $this->view->usersInVoucher = $usersInVoucher;
            $this->view->usergroupsInVoucher = $usergroupsInVoucher;
        }
        $users = $userModel->fetchUsers();
        $usergroups = $ugModel->getAllUserGroups();

        $this->view->id = $vId;
        $this->view->users = $users;
        $this->view->usergroups = $usergroups;
    }

    public function addusersAction() {
        $vuModel = new Core_Voucher_Models_VoucherUser();
        $vModel = new Core_Voucher_Models_Voucher();
        $vId = $this->getParam('id');
        $vObject = $vModel->findById($vId);
        $vuModel->saveUsers($vId, $_POST['users']);

        $url = "/admin/vouchers/edit/id/" . $vId;
        $this->redirect($url);
    }

    public function addusergroupsAction() {
        $vugModel = new Core_Voucher_Models_VoucherUsergroup();
        $vModel = new Core_Voucher_Models_Voucher();
        $vId = $this->getParam('id');
        $vObject = $vModel->findById($vId);
        if (isset($_POST['usergroups'])) {
            $vugModel->saveUsergroups($vId, $_POST['usergroups']);
        }
        $url = "/admin/vouchers/edit/id/" . $vId;
        $this->redirect($url);
    }

}