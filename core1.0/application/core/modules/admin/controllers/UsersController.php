<?php

class Admin_UsersController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();

        $notificationMapper = new Core_Notification_Models_NotificationMapper();
        $notification = $notificationMapper->fetchAll();

        foreach ($notification as $n) {
            $n->time = $notificationMapper->getTimeDifference($n->time);
        }

        $notificationCount = $notificationMapper->fetchAllUnseen();

        $this->view->notification = $notification;
        $this->view->notificationCount = $notificationCount;
        $this->view->user = $user;
    }

    public function indexAction() {
        $usersModel = new Core_User_Models_User();
        $getAllUsers = $usersModel->getAllUsers();
        $this->view->users = $getAllUsers;
    }

    public function adduserAction() {
        $userModel = new Core_User_Models_User();
        $userMapper = new Core_User_Models_UserMapper();
        $addressModel = new Core_Address_Models_Address();
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $form = new Core_User_Forms_Add();

        $userId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            // Render the form
            $countryList = $userMapper->getAllCountries();
            $usergroupList = $ugModel->getAllUsergroups();
            $form->setupAddForm($countryList, $usergroupList);
        } else {
            // Passed input validation.
            $addressModel->setLineOne($this->getParam('address'))
                ->setLineTwo($this->getParam('address2'))
                ->setCity($this->getParam('town'))
                ->setRegion($this->getParam('region'))
                ->setPostcode($this->getParam('postcode'))
                ->setCountryId($this->getParam('country'))
                ->setActive('1');
            $addressModel->save($addressModel);
            $lastInsertId = $addressModel->lastInsertId();

            $userModel->setForename($this->getParam('forename'))
                ->setSurname($this->getParam('surname'))
                ->setUsername($this->getParam('username'))
                ->setEmail($this->getParam('email'))
                ->setPassword($this->getParam('password'))
                ->setBillingAddressId($lastInsertId)
                ->setDeliveryAddressId($lastInsertId)
                ->setUserGroupId($this->getParam('usergroup'))
                ->setUserTypeId($this->getParam('user_type_id'))
                ->setActive('1');
            $userModel->save($userModel);

            $historyMapper->saveAction('added', 'user', $userId, $userModel->getForename(). ' ' . $userModel->getSurname());

            $url = "/admin/users";
            $this->redirect($url);
        }
        $this->view->form = $form;
    }

    public function edituserAction() {
        $userModel = new Core_User_Models_User();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $userGroupMapper = new Core_Usergroup_Models_UsergroupMapper();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $form = new Core_User_Forms_Edit();

        $userId = $this->getParam('id');
        $userObject = $userModel->findById($userId);
        $currency = $currencyMapper->getCurrencies();
        $company = $userModel->getCompanies();
        $userType = $userModel->getAllUserTypes();
        $userGroup = $userGroupMapper->getAllUserGroups();
        $adminUserId = Core_User_Models_User::getCurrent()->id;
        global $user;

        if (!$this->getRequest()->isPost()) {
            $form->setUpEditForm($userObject, $currency, $userType, $userGroup, $company);
        } else {
            $userObject->setForename($this->getParam('forename'))
                ->setSurname($this->getParam('surname'))
                ->setUsername($this->getParam('username'))
                ->setEmail($this->getParam('email'))
                ->setPhone($this->getParam('phone'))
                ->setCurrencyId($this->getParam('currency'))
                ->setUserTypeId($this->getParam('usertype'))
                ->setUserGroupId($this->getParam('usergroup'))
                ->setCompanyId($this->getParam('company'))
                ->setActive('1');

            $userObject->save();

            $historyMapper->saveAction('edited', 'user', $adminUserId, $userModel->getForename(). ' ' . $userModel->getSurname());

            $url = "/admin/users";
            $this->redirect($url);
        }

        $userObject = $userModel->findById($userId);

        $this->view->users = $userObject;
        $this->view->form = $form;
    }
    
    public function editpasswordAction() {
        $userModel = new Core_User_Models_User();
        $passwordModel = new Core_Password_Models_Password();
        $form = new Core_User_Forms_AdminResetPassword();
        $form->setUpEditForm();

        $userId = $this->getParam('id');
        $userObject = $userModel->findById($userId);
        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getParams())) {
            $newPassword = $passwordModel->encrypt($this->getParam('pwdconf'));
            $userObject->setPassword($newPassword)
                ->save();
            $url = "/admin/users";
            $this->redirect($url);
        }

        $this->view->form = $form;
    }

    public function editadminuserAction() {
        $userModel = new Core_User_Models_User();
        $adminEditForm = new Core_User_Forms_AdminEdit();

        $request = $this->getRequest();
        $userId = $this->getParam('id');
        $userObject = $userModel->findById($userId);

        if (!$request->isPost()) {
            $adminEditForm->setupEditForm($userObject);
            $this->view->form = $adminEditForm;
        } else {
            $userObject->setUsername($this->getParam('username'))
                ->setPassword($this->getParam('password'))
                ->saveNewPassword($userId);

            $this->redirect('/admin');
        }
    }

    public function usergroupsAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $getAllUgs = $ugModel->getAllUserGroups('object');
        $this->view->get_usergroups = $getAllUgs;
    }

    public function editusergroupAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $form = new Core_Usergroup_Forms_Edit();

        //Get data
        $ugId = $this->getParam('id');
        $ugObject = $ugModel->findById($ugId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            // Render the form
            $form->setupEditForm($ugObject);
        } else {
            // Passed input validation.
                $ugObject->setUsergroupName($this->getParam('name'))
                        ->setBusinessUnit($this->getParam('business_unit'))
                        ->setInvoice($this->getParam('invoice'))
                        ->setInvoiceLimit($this->getParam('invoice_limit'))
                        ->setInvoiceType($this->getParam('invoice_type'))
                        ->setCurrency($this->getParam('currency'))
                        ->setCurrencyInvoice($this->getParam('currency_invoice'))
                        ->setVat($this->getParam('vat'));
                $ugObject->save();
            $historyMapper->saveAction('edited', 'usergroup', $adminUserId, $this->getParam('name'));
            $url = "/admin/users/usergroups";
            $this->redirect($url);
        }

        $this->view->user_group = $ugObject;
        $this->view->form = $form;
    }

    public function addusergroupAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $form = new Core_Usergroup_Forms_Add();

        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            // Render the form
            $form->setupAddForm();
        } else {
            // Passed input validation.
            $ugData = array(
                'name' => $_POST['name'],
                'business_unit' => $_POST['business_unit'],
                'invoice' => $_POST['invoice'],
                'invoice_limit' => $_POST['invoice_limit'],
                'invoice_type' => $_POST['invoice_type'],
                'currency' => $_POST['currency'],
                'currency_invoice' => $_POST['currency_invoice'],
            );
            $ugModel->addUsergroup($ugData);
            $historyMapper->saveAction('added', 'usergroup', $adminUserId, $this->getParam('name'));
            $url = '/admin/users/usergroups';
            $this->redirect($url);
        }
        $this->view->form = $form;
    }

    public function deleteusergroupAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $historyMapper = new Core_History_Models_HistoryMapper();
        $form = new Core_Usergroup_Forms_Delete();

        $ugId = $this->getParam('id');
        $ugObject = $ugModel->findById($ugId);
        $adminUserId = Core_User_Models_User::getCurrent()->id;

        if (!$this->getRequest()->isPost()) {
            // Render the form
            $form->setUpDeleteForm($ugObject);
        } else {
            // Passed input validation.
            $values = $form->getValues();
            $ugModel->delete($ugId);
            $historyMapper->saveAction('deleted', 'usergroup', $adminUserId, $ugObject->getUgName());
            $url = "/admin/users/usergroups";
            $this->redirect($url);
        }

        $this->view->form = $form;
    }

    public function viewusersAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $ugId = $this->getParam('id');
        $ugObject = $ugModel->findById($ugId);

        if ($_POST) {
            $ugObject->saveCategoriesByUsergroup($_POST['categories']);
        }
        $users = $ugObject->getUsergroupUsers($ugId);
        $this->view->users = $users;
    }

    public function categoriesAction() {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $catModel = new Core_Usergroup_Models_UsergroupCategories();

        $ugId = $this->getParam('id');
        $ugObject = $ugModel->findById($ugId);

        if ($_POST) {
            $result = $catModel->saveCategoriesByUsergroup($ugObject, $this->getParam('categories'));
            if ($result === false) {
                $updateStatus = false;
                $updateMessage = 'There was a problem with your request and it could not be processed. Please try again or contact your webmaster.';
            } else {
                $updateStatus = true;
                $updateMessage = 'Your changes have been saved.';
            }
        }
        $categoryData = $ugModel->getAllCategories();
        $ugCats = $ugObject->getUsergroupCategories($ugId);

        $this->view->categories = $categoryData;
        $this->view->usergroup_categories = $ugCats;
    }

    public function assignvoucherAction() {
        $vModel = new Core_Voucher_Models_Voucher();
        $vuModel = new Core_Voucher_Models_VoucherUser();
        $form = new Core_User_Forms_Voucher();

        $vu = array();
        $vu[] = $this->getParam('id');

        if (!$_POST) {
            $voucherList = $vModel->fetchAll();
            $form->setupVoucherForm($voucherList);
        } else {
            $vId = $this->getParam('voucher');
            $vuModel->saveIndividualUsers($vId, $vu);
            $url = '/admin/users';
            $this->redirect($url);
        }

        $this->view->form = $form;
    }

}

?>