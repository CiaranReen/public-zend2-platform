<?php

/**
 * Polls Index controller
 */
class Admin_PollsController extends Zend_Controller_Action {

    /**
     * Dispatch the controller
     * This will cause a page to be generated and output will be sent to the browser
     *
     * @param array $params
     *
     * @return $this
     */
    
    public function init() {
        $this->_helper->layout->setLayout('admin');
        $navigation = new Core_Admin_Models_Navigation($this->getFrontController());
        $this->view->nav = $navigation->getNavigation();
        $userModel = new Core_User_Models_User();
        if(!$userModel->isLoggedInAsAdmin()){
            $this->_redirect('/admin/login');
        }
        $user = Core_User_Models_User::getCurrent();
        $this->view->user = $user;
    }
    
    public function indexAction() {
        $pollsModel = new Core_Polls_Models_Poll();
        $pollsReportModel = new Core_Polls_Models_PollReporting();
        $pollsProductsModel = new Core_Polls_Models_PollProducts();

        // Set data
        $barGraph = $pollsModel->createBarGraph();
        $lastModifiedFile = $pollsModel->getLastModifiedFile();
        $currentPoll = $pollsModel->getCurrentPoll();
        $userSubs = $pollsReportModel->getAllUserSubmissions();
        $getCount = $pollsReportModel->getCount();
        $getProducts = $pollsProductsModel->getProducts();
        
        //Display data
        $this->view->last_modified = $lastModifiedFile;
        $this->view->bar_graph = $barGraph;
        $this->view->current_poll = $currentPoll;
        $this->view->user_submissions = $userSubs;
        $this->view->get_count = $getCount;
        $this->view->get_products = $getProducts;
    }

    public function editAction() {
        $pollsModel = new Polls_Models_Polls_PollsModel();
        $form = new Polls_Forms_Index_EditForm();
        //Get data
        $id = $this->getParam('id');
        $productObject = $pollsModel->findById($id);

        if (!$_POST) {
            // Render the form
            $form->setupEditForm($productObject);
        } else if (!$form->isValid($_POST)) {
        } else {
            // Passed input validation.
            $productData = array(
                'name' => $_POST['name'],
                'image' => $_POST['image'],
            );
            $result = $productObject->saveProduct($productData);
            $url = "/admin/polls";
            $this->redirect($url);
            }
        $productObject = $pollsModel->findById($id);

        $this->view->products = $productObject;
    }

    public function deleteAction() {
        $pastPollsModel = new Core_Polls_Models_PastPolls();
        $pollsModel = new Core_Polls_Models_Poll();
        $pollsReportingModel = new Core_Polls_Models_PollReporting();
        $pollsProductsModel = new Core_Polls_Models_PollProducts();

        //set Data
        $pollsModel->saveGraphToFolder();
        $pastPollsModel->moveToPastPolls();
        $deleteUserSubmissions = $pollsReportingModel->deleteUserSubmissions();
        $deleteCurrentPoll = $pollsModel->deleteCurrentPoll();
        $deleteCurrentProducts = $pollsProductsModel->deleteCurrentProducts();
        
        $this->view->delete_user_submissions = $deleteUserSubmissions;
        $this->view->delete_current_poll = $deleteCurrentPoll;
        $this->view->delete_current_products = $deleteCurrentProducts;
    }

    public function helpAction() {
    }

    public function startAction() {
        $pollsModel = new Core_Polls_Models_Poll();
        $pollsProductModel = new Core_Polls_Models_PollProducts();
        $pollsImagesModel = new Core_Polls_Models_PollProductsImages();
        $form = new Core_Polls_Forms_StartForm();
        
        //Get data
        $pollObject = $pollsModel->findById();
        $form->setupStartForm($pollObject);
        
        if (!$_POST) {
            $form->setupStartForm($pollObject);
        } else {
            // Passed input validation.
            $pollData = array(
                'title' => $_POST['title'],
                'start' => $_POST['start'],
                'end'   => $_POST['end'],
            );
            $productData = array(
                'product1' => $_POST['product1'],
                'product2' => $_POST['product2'],
                'product3' => $_POST['product3'],
            );
            $location1 = $form->image1_url->getValue();
            $location2 = $form->image2_url->getValue();
            $location3 = $form->image3_url->getValue();
            $pollResult = $pollObject->savePoll($pollData);
            $productResult = $pollsProductModel->saveProduct($productData);
            $productImageResult = $pollsImagesModel->saveProductImage($location1, $location2, $location3);

            if ($pollResult && $productResult && $productImageResult === false) {
                // Update failed
                $updateStatus = false;
                $updateMessage = 'Sorry, there was a problem and the poll could not be created';
            } else {
                $url = "/admin/polls";
                $this->redirect($url);
            }
        }
        
        $pollObject = $pollsProductModel->findById();
        $checkPollExists = $pollsModel->getCurrentPoll();
        
        $this->view->new_poll = $pollObject;
        $this->view->current_poll = $checkPollExists;
        $this->view->form = $form;
    }

    public function pastpollsAction() {
        $pollsModel = new Core_Polls_Models_PollReporting();
        $pastPollsModel = new Core_Polls_Models_PastPolls();

        //set Data
        $pollsData = $pastPollsModel->getPastPolls();
        $submissions = $pollsModel->getAllSubmissions();
        $this->view->polls_data = $pollsData;
        $this->view->submissions = $submissions;
    }
}