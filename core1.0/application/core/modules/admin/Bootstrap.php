<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initConfig() 
    {
        Zend_Registry::set('config', $this->getOptions());
        Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator/controls.phtml');
	}
}