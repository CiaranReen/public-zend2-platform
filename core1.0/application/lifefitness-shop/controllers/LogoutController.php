<?php

class LogoutController extends Zend_Controller_Action
{

    public function init()
    {
        /* Log user out */
		$userModel = new Core_User_Models_User();
        
		$userModel->processLogout();
                
        $this->_redirect('/');

	}
    

}

