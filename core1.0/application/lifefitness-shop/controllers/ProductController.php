<?php

class ProductController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */

        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new Core_Navigation_Models_Navigation();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        $parentCategories = $categoryModel->getAllParentCategories();

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Product');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/lifefitness-shop/img/products/';
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->languages = $languageMapper->fetchAll();
    }

    public function indexAction() {
        /* Instantiate Models */
        $productModel = new Core_Product_Models_Product();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $navigationModel = new Core_Navigation_Models_Navigation();
        $packModel = new Core_Productpacks_Models_ProductpacksMapper();
        $currencyModel = new Core_Currency_Models_Currency();
        $basketruleModel     = new Core_Basketrule_Models_Basketrule();
        $userModel = new Core_User_Models_User();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();

        /* Get Request Parameters */
        $productId = (int) $this->getParam('id');
        //$packId         = $packModel->findById($ppId);
        $categoryId = $navigationModel->getPreviouslyViewedCategory()->categoryId; //get the category id that was previously viewed
        $productObject = $productModel->findById($productId);

        //check if user has selected a particular colour of a colourway product
        if ($productObject->hasColourways()) {
            if ($this->getParam('col')) {
                $colourwaySelected = (int) $this->getParam('col');
            } else {
                $colourwaySelected = $productObject->getDefaultImage()->colourway_id;
            }
            $this->view->colourwaySelected = $colourwaySelected;
        }

        //Get Post Data
        if ($this->getRequest()->isPost()) {
            $basketModel->addItem($_POST); //add item to basket
            $basketModel->reset(); //reset basket after adding item reset basket view
            $this->view->success = true; //set success message
            $this->view->basket = $basketModel; //update basket in view
        }

        /* Get Data */
        if ($productObject->getId() === NULL) {
            $this->_redirect('/category/index/id/' . $categoryId); //redirect to category page if product cannot be found
        }
        
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        }
        else
        {
            $currency = $currencyModel->load();
        }
        
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        $productStock = $productObject->getStock();
        if($productStock)
        {
            foreach($productStock as $stock)
            {
                $stock->setConversion($conversion);
                $stock->convertPrice();
            }
        }
        
        $categoryObject = $categoryModel->findById($categoryId);
        $parentCategoryObject = $categoryModel->findById($categoryObject->getParentId());
        $childCategories = $parentCategoryObject->getChildCategories();

        /* Set View Properties */
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        $this->view->childCategories = $childCategories;
        $this->view->product = $productObject;
        $this->view->productStock = $productStock;
    }

}

