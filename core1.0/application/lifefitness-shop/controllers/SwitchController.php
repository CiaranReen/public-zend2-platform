<?php

class SwitchController extends Zend_Controller_Action
{

    public function init()
    {
        $langChoice = stripslashes($_GET['lang']);
        
        try
        {
            if(!is_string($langChoice))
            {
                throw new InvalidArgumentException(
                    'Cannot change language because the choice isn\'t a string'
                );
            }
        }
        catch(InvalidArgumentException $e)
        {
            echo $e->getMessage();
        }
        
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $languageMapper->setSessionLanguage($langChoice);
        
        $this->_redirect('/');
        // action body
    }

    public function indexAction()
    {
        
    }

    
}

