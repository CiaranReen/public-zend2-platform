<?php

class CategoryController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new Core_Navigation_Models_Navigation();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        if ($this->getParam('id')) {
            $categoryId = (int) $this->getParam('id');
        } else {
            $categoryId = 1;
        }
        $parentCategories = $categoryModel->getAllParentCategories($categoryId);

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Product');
        $this->view->parentCategories = $parentCategories; //for header nav
        $this->view->basket = $basketModel;
        $this->view->navigation = $navigationModel;
        $this->view->languageSelected = $languageMapper->fetchByIp();
        $this->view->categoryUrl = '/category/index/id/';
        $this->view->productUrl = '/product/index/id/';
        $this->view->productImageLocation = '/lifefitness-shop/img/products/';
        $this->view->languages = $languageMapper->fetchAll();
    }

    public function indexAction() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $productModel = new Core_Product_Models_Product();
        $currencyModel = new Core_Currency_Models_Currency();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $ugModel = new Core_Usergroup_Models_Usergroup();

        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        } else {
            $currency = $currencyModel->load();
        }

        /* Get Request Parameters */
        if ($this->getParam('id')) {
            $categoryId = (int) $this->getParam('id');
        } else {
            $categoryId = 1;
        }
        if (isset($userModel->getCurrent()->userGroupId)) {
            $ugId = $userModel->getCurrent()->userGroupId;
        }

        /* Get Data */
        $categoryProducts = array();
        $categoryObject = $categoryModel->findById($categoryId);
        $childCategories = $categoryObject->getChildCategories();
        $mostPopular = $productModel->getMostPopularProducts();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        if (empty($childCategories)) {
            $parentCategoryId = $categoryObject->getParentId();
            $categoryModel2 = new Core_Category_Models_Category();
            $parentCategoryObject = $categoryModel2->findById($parentCategoryId);
            $childCategories = $parentCategoryObject->getChildCategories();
            $categoryProducts = $categoryObject->getProducts();
            $mostPopular = $productModel->getMostPopularProducts();
        }

        $this->view->childCategories = $childCategories;
        //Check the current user can view the categories
        if (isset($ugId) && $ugModel->isAssignedToCategories($ugId)) {
            $filteredCategories = array();
            foreach($childCategories as $childCategory){
                $ugCheck = $ugModel->usergroupAvailability($ugId, $childCategory->getId());
                if ($ugCheck === true) {
                    $filteredCategories[] = $childCategory;
                }
            }
            $this->view->childCategories = $filteredCategories;
        }


        /* Set View Properties */
        $this->view->category = $categoryObject;
        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        $this->view->mostPopular = $mostPopular;
        $this->view->categoryProducts = $categoryProducts;
    }

}
