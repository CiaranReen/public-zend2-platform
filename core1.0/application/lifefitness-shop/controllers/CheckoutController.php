<?php

class CheckoutController extends Zend_Controller_Action {

    public function init() {
        /* Instantiate Models */
        $userModel = new Core_User_Models_User();
        $categoryModel = new Core_Category_Models_Category();
        $basketModel = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $navigationModel = new Core_Navigation_Models_Navigation();
        $shippingModel = new Core_Shipping_Models_Shipping();

        /* Get Data */
        if ($userModel->isLoggedIn()) {
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        } else {
            $this->_redirect('/login/');
        }

        $items = $basketModel->getItems();
        $shippingModel->setBasket($items);

        if (empty($items)) {
            $this->_redirect('/basket/');
        }
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);

        /* Set Views */
        $this->view->headTitle('LifeFitness Shop - Checkout');
        $this->view->parentCategories   = $parentCategories; //for header nav
        $this->view->basket             = $basketModel;
        $this->view->currency           = $currency; //update currency symbol in view
        $this->view->conversion         = $conversion; //update currency conversion in view
        $this->view->shippingPrices     = $shippingModel->calculate();
        $this->view->categoryUrl        = '/category/index/id/';
        $this->view->productUrl         = '/product/index/id/';
        $this->view->productImageLocation = '/lifefitness-shop/img/products/';
        $this->view->navigation         = $navigationModel;
        $this->view->languageSelected   = $languageMapper->fetchByIp();
        $this->view->languages          = $languageMapper->fetchAll();
    }

    public function indexAction() {
        //$shippingOptionsMapper = new Core_Shipping_Models_ShippingOptionMapper();
        //$this->view->options = $shippingOptionsMapper->fetchAll();
        $currencyModel = new Core_Currency_Models_Currency();
        $basketModel = new Core_Basket_Models_Basket();
        $vModel = new Core_Voucher_Models_Voucher();
        $vuModel = new Core_Voucher_Models_VoucherUser();
        $vugModel = new Core_Voucher_Models_VoucherUsergroup();
        $userModel = new Core_User_Models_User();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $currency = $currencyMapper->find($user->getCurrencyId(), $currencyModel);
        }
        else
        {
            $currency = $currencyModel->load();
        }
        
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());

        $userId = $userModel->getCurrent()->id;
        $ugId = $userModel->getCurrent()->userGroupId;
        if (isset($_POST['voucher'])) {
            $check = $vModel->checkVoucher($_POST['voucher']);
            if (isset($check->code)) {
                $vId = $check->id;
                $userAccess = $vuModel->checkUserCanAccessVoucher($userId, $vId);
                $ugAccess = $vugModel->checkUsergroupCanAccessVoucher($ugId, $vId);
                if ($userAccess == true || $ugAccess == true) {
                    $success = 'Voucher successfully applied.';
                    $this->view->success = $success;
                    $this->view->voucher = $check;
                    $vModel->storeInSession($check);
                    $voucherSession = $vModel->getVoucherSession();
                    $this->view->voucherSession = $voucherSession;
                } else {
                    $error = 'Invalid voucher code.';
                    $this->view->error = $error;
                }
            } else {
                $error = 'Invalid voucher code.';
                $this->view->error = $error;
            }
        }

        $voucherSession = $vModel->getVoucherSession();
        if (isset($voucherSession)) {
            $this->view->voucherSession = $voucherSession;
        }

//        $vouchers = $vModel->fetchAll();

        $this->view->currency = $currency;
        $this->view->conversion = $conversion;
        //$this->view->vouchers = $vouchers;
        $this->view->basket = $basketModel;
    }

}

