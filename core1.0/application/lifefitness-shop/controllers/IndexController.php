<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
         /* Instantiate Models */
        $userModel          = new Core_User_Models_User();
        $categoryModel      = new Core_Category_Models_Category();
        $basketModel        = new Core_Basket_Models_Basket();
        $currencyModel      = new Core_Currency_Models_Currency();
        $languageMapper     = new Core_Language_Models_LanguagesMapper();
        $navigationModel    = new Core_Navigation_Models_Navigation();

        /* Get Data */
        if($userModel->isLoggedIn()){
            $user = $userModel->findById($userModel->getUserSession()->userId);
            $this->view->user = $user;
        }
        
        $parentCategories = $categoryModel->getAllParentCategories();
        $currency = $currencyModel->load();
        $conversion = $currencyModel->setConversionRate($currency->currencyId);
        //This is hard coded because the local IP address doesn't work!

        /* Set Views */
        $this->view->headTitle('LifeFitness E-Shop');
        $this->view->parentCategories   = $parentCategories; //for header nav
        $this->view->languageSelected 	= $languageMapper->fetchByIp();
        $this->view->languages          = $languageMapper->fetchActiveNotSelected($this->view->languageSelected->getId());
        $this->view->navigation         = $navigationModel;
        $this->view->basket             = $basketModel; //update basket in view
        $this->view->currency           = $currency; //update currency symbol in view
        $this->view->conversion         = $conversion; //update currency conversion in view
        $this->view->categoryUrl        = '/category/index/id/';
        $this->view->productUrl             = '/product/index/id/';
        $this->view->productImageLocation   = '/lifefitness-shop/img/products/';

    }

    public function indexAction()
    {
        $productsModel = new Core_Product_Models_Product();
        $highestRated = $productsModel->getHighestRated();
        $featured = $productsModel->getFeaturedProduct();
        $new = $productsModel->getNewProduct();
        
        $this->view->highestRated = $highestRated;
        $this->view->featured = $featured;
        $this->view->new = $new;
    }

    
}

