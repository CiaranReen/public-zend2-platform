<?php

class LanguageController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $language = new Application_Model_LanguageMapper();
        $this->view->entries = $language->fetchAll();
    }


}

