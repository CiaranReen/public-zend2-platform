<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    protected function _initDoctype() {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
    }

    protected function _initAppAutoload() {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'App',
            'basePath' => dirname(__FILE__),
        ));

        return $autoloader;
    }

    protected function _initAutoload() {
        Zend_Loader_Autoloader::getInstance()->registerNamespace('Core_');
    }

    protected function _initViewHelpers() {
        $view = new Zend_View();
        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }
    
    protected function _initConfig(){
        $config = new Zend_Config($this->getOptions());
        Zend_Registry::set('my_config', $config);
        return $config;
    }

}