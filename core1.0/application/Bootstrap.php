<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 24/03/14
 * Time: 14:43
 */
define('BASE_PATH', realpath(dirname(__FILE__) . '/../'));
define('APPLICATION_PATH', BASE_PATH . '/application');

defined('APPLICATION_ENV')
|| define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Include path
set_include_path(
    '.'
    . PATH_SEPARATOR . BASE_PATH . '/library'
    . PATH_SEPARATOR . get_include_path()
);
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance()->registerNamespace('Core_');
Zend_Loader_Autoloader::getInstance();

// Define application environment
require_once 'Zend/Application.php';
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    public function init() {

    }
}