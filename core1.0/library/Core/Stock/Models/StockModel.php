<?php
/**
 * class for Stock
 */

class Stock_Models_StockModel extends Stock_Abstract_Stock {

	protected $stockId;
    protected $code;
    protected $option;
    protected $colourwayId;
    protected $price;
    
    protected static $table = 'stock';
    
    public function setStockId($stockId){
        $this->stockId = $stockId;
        return $this;
    }
    
    public function getStockId(){
        return $this->stockId;
    }
    
    public function setCode($code){
        $this->code = $code;
        return $this;
    }
    
    public function getCode(){
        return $this->code;
    }
    
    public function setOption($option){
        $this->option = $option;
        return $this;
    }
    
    public function getOption(){
        return $this->option;
    }
    
    public function setColourwayId($colourwayId){
        $this->colourwayId = $colourwayId;
        return $this;
    }
    
    public function getColourwayId(){
        return $this->colourwayId;
    }
    
    public function setPrice($price){
        $this->price = $price;
        return $this;
    }
    
    public function getPrice(){
        return $this->price;
    }
    
    public function load(){
        try{
            $sql = "SELECT * FROM ".self::$table."
                    WHERE stock_id = '".$this->getStockId()."'";
            $row = $this->getDbAdapter()->fetchRow($sql);
            if(!empty($row)){
                $this->setCode($row['code']);
                $this->setOption($row['option']);
                $this->setColourwayId($row['colourway_id']);
                $this->setPrice($row['price']);
            }
            return $this;
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * gets the product id from the stock id
     * @param int $stock_id
     * @return int|boolean
     */
    public function getProductId(){
        try{
            $sql = "SELECT p.product_id FROM products p
                    INNER JOIN ".self::$table." s ON p.product_id = s.product_id
                    WHERE s.stock_id = '".$this->getStockId()."'";
            $row = $this->getDbAdapter()->fetchRow($sql);
            if(empty($row)){
                throw new Exception('No stocks found for product with stock id '.$this->getStockId());
            }
            return $row['product_id'];
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    public function getColourway(){
        
    }
}