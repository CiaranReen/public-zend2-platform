<?php

class Core_Stock_Models_StockMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Stock_Models_DbTable_Stock';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Stock_Models_Stock $stock)
    {
        $data = array(
            'product_id'    => $stock->getProductId(),
            'code'          => $stock->getCode(),
            'option'        => $stock->getOption(),
            'colourway_id'  => $stock->getColourwayId(),
            'price'         => $stock->getPrice(),
            'stock_level'   => $stock->getStockLevel(),
            'warning_level' => $stock->getWarningLevel(),
        );
 
        if (null === ($id = $stock->getId())) 
        {
            unset($data['stock_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('stock_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Stock_Models_Stock $stock)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $stock->setId($row->stock_id)
                ->setProductId($row->product_id)
                ->setCode($row->code)
                ->setOption($row->option)
                ->setColourwayId($row->colourway_id)
                ->setPrice($row->price)
                ->setStockLevel($row->stock_level)
                ->setWarningLevel($row->warning_level);
        return $stock;
    }
    
    public function findByCode($code, Core_Stock_Models_Stock $stock)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('code = ?', $code);
        $row = $this->getDbTable()->fetchRow($select);
        if (0 == count($row)) 
        {
            return;
        }
        $stock->findById($row->stock_id);
        return $stock;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row)
        {
            $entry = new Core_Stock_Models_Stock();
            $entry->findById($row->stock_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function fetchCompleteStockDetails($stockId){
        $resultSet = array();
        if (is_int($stockId)) {
            $select = $this->getDbTable()
                    ->select()->setIntegrityCheck(false)
                    ->from($this->_dbTable,  
                            array(
                                'product' => 'products.name', 
                                'colourway' => 'colourways.name', 
                                '*'
                            ))
                    ->joinUsing('products', 'product_id')
                    ->joinUsing('product_images', 
                            array(
                                'product_id', 
                                'colourway_id'
                            ))
                    ->joinUsing('colourways', 'colourway_id')
                    ->where('stock.stock_id = ?', $stockId)
                    ->order(array('stock.stock_id DESC'))
                    ->group('stock.stock_id');
            $resultSet = $this->getDbTable()->fetchAll($select);
            $row = $resultSet->current();
            
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language = $languageMapper->fetchByIp();
            if($language instanceof Core_Language_Models_Languages){
                if($language->getContent($row->product)){
                    $row->product = $language->getContent($row->product)->getContent();
                }
                if($language->getContent($row->colourway)){
                    $row->colourway = $language->getContent($row->colourway)->getContent();
                }
            }
        }
        return $row;
    }

    public function getStockReport() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('s' => 'stock'), array('*'))
            ->joinInner(array('p' => 'products'), 'p.product_id = s.product_id');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function delete($stockId){
        return $this->getDbTable()->delete(array('stock_id = ?' => $stockId)); //delete from db
    }

    public function deleteUsingProductId($productId, $colourwayId = NULL){
        if($colourwayId){
            $delete = array(
                'product_id = ?' => $productId,
                'colourway_id = ?' => $colourwayId
            );

        }
        else{
            $delete = array('product_id = ?' => $productId);
        }
        return $this->getDbTable()->delete($delete); //delete from db
    }
}

