<?php

class Core_Stock_Models_DbTable_Stock extends Zend_Db_Table_Abstract
{
    /** Table Name **/
    protected $_name = 'stock';
    
    protected $_referenceMap = array(
        'Product' => array(
            'columns' => array ('product_id'),
            'refTableClass' => 'Core_Product_Models_DbTable_Product',
            'refColumns' => array ('product_id')
        )
    );
}

