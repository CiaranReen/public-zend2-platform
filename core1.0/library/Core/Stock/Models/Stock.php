<?php

class Core_Stock_Models_Stock extends App_Model_Abstract
{
    protected $_id;
    protected $_productId;
    protected $_code;
    protected $_option;
    protected $_colourwayId;
    protected $_price;
    protected $_stockLevel;
    protected $_warningLevel;
    protected $_conversion;
    protected $_mapper;
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setProductId($productId)
    {
        $this->_productId = (int) $productId;
        return $this;
    }
 
    public function getProductId()
    {
        return $this->_productId;
    }
    
    public function setCode($code)
    {
        $this->_code = (string) $code;
        return $this;
    }
 
    public function getCode()
    {
        return $this->_code;
    }
    
    public function setOption($option)
    {
        $this->_option = (string) $option;
        return $this;
    }
 
    public function getOption()
    {
        return $this->_option;
    }
    
    public function setColourwayId($colourwayId)
    {
        $this->_colourwayId = (int) $colourwayId;
        return $this;
    }
 
    public function getColourwayId()
    {
        return $this->_colourwayId;
    }
    
    public function setPrice($price)
    {
        $this->_price = floatval($price);
        
        return $this;
    }
 
    public function getPrice()
    {
        return $this->_price;
    }
    
    public function setStockLevel($stockLevel)
    {
        $this->_stockLevel = (int) $stockLevel;
        return $this;
    }
 
    public function getStockLevel()
    {
        return $this->_stockLevel;
    }
    
    public function setWarningLevel($warningLevel)
    {
        $this->_warningLevel = (string) $warningLevel;
        return $this;
    }
 
    public function getWarningLevel()
    {
        return $this->_warningLevel;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Stock_Models_StockMapper());
		}
		return $this->_mapper;
    }
    
    public function setConversion($conversion)
    {
        $this->_conversion = (float) $conversion;
        return $this;
    }
 
    public function getConversion()
    {
        return $this->_conversion;
    }
    
    public function convertPrice()
    {
        $this->setPrice($this->getPrice() * $this->getConversion());
        return $this;
    }
    
    public function findById($stockId) {
        $this->getMapper()->find($stockId, $this);
        return $this;
    }
    
    public function findByCode($code) {
        $this->getMapper()->findByCode($code, $this);
        return $this;
    }
    
    public function getAllStock()
    {
        $allStock = $this->getMapper()->fetchAll();
        return $allStock;
    }
    
    public function getCompleteStockDetails(){
        return $this->getMapper()->fetchCompleteStockDetails($this->getId());
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
    
}

