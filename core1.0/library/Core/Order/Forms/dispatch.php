<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 9/12/2013
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */

class Core_Order_Forms_dispatch extends Zend_Form {

	public function init() {
        // Set properties of the form
        $this->setName('dispatch')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', '')
                ->setDescription('Enter Card Details')
                ->setOptions(array('class' => ''));


        // Delivery Company
        $deliveryCompany = $this->createElement('select', 'delivery_company')
                        ->setOptions(
                        array(
                                 'label'    => 'Delivery Company',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        $deliveryCompany->setMultiOptions(
                array(
                    '1'     => 'DPD',
                    '2'    => 'UPS',
                )
            );
        
        // Consignment Number
        $consignment_number = $this->createElement('text', 'consignment_number')
                        ->setOptions(
                        array(
                                 'label'    => 'Consignment Number',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        
        // Notes
        $notes = $this->createElement('textarea', 'comments')
                        ->setOptions(
                        array(
                                 'label'    => 'Comments',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
       

        // Submit Button
        $submit = $this->createElement('submit', 'submit')
                ->setOptions(
                        array(
                            'label' => 'Dispatch Order',
                            'class' => 'btn btn-primary',
                        )
                );

        // Add controls to the form
        $this   ->addElement($deliveryCompany)
                ->addElement($consignment_number)
                ->addElement($notes);

        $this->addElement($submit);
	}

}