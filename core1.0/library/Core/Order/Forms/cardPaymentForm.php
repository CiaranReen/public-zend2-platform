<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 19/11/2013
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */

class Core_Order_Forms_cardPaymentForm extends Zend_Form {

	public function init() {
        // Set properties of the form
        $this->setName('cardpayment')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'well form-inline')
                ->setDescription('Enter Card Details')
                ->setOptions(array('class' => 'well'));


        // Card Holder
        $cardHolder = $this->createElement('text', 'card_holder')
                        ->setOptions(
                        array(
                                 'label'    => 'Card Holder Name*',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        
        // Card Number
        $cardNumber = $this->createElement('text', 'card_number')
                        ->setOptions(
                        array(
                                 'label'    => 'Card Number*',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        
        // Card Type
        $cardType = $this->createElement('select', 'card_type')
                        ->setOptions(
                        array(
                                 'label'    => 'Card Type*',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        $cardType->setMultiOptions(array(
                                'VISA'      => 'Visa',
                                'MC'        => 'Master Card',
                                'SOLO'      => 'Solo',
                                'AMEX'      => 'Amex',
                                'MAESTRO'   => 'Maestro / Switch'
                                ));

        // Start Date
        $startDate = $this->createElement('text', 'start_date')
                ->setOptions(
                    array(
                        'id'        => 'start_date',
                        'label'     => 'Start Date (MM/YY)',
                        'placeholder' => 'MM/YY',
                        'required'  => false,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
        
        // Expiry Date
        $expiryDate = $this->createElement('text', 'expiry_date')
                ->setOptions(
                    array(
                        'id'        => 'end_date',
                        'label'     => 'End Date* (MM/YY)',
                        'placeholder' => 'MM/YY',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
        
        // Issue Number
        $issueNumber = $this->createElement('text', 'issue_number')
                        ->setOptions(
                        array(
                                 'label'    => 'Issue Number',
                                 'required' => false,
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );
        
        // CVV
        $cvv = $this->createElement('text', 'cvv')
                        ->setOptions(
                        array(
                                 'label'    => 'CVV*',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );

        // Payment Type
        $paymentType = $this->createElement('hidden', 'payment_type')
            ->setOptions(
                array(
                    'required' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                    'value'    => 'card',
                )
            );
       
        
        // Submit Button
        $submit = $this->createElement('submit', 'submit')
                ->setOptions(
                        array(
                            'label' => 'Confirm Payment',
                            'class' => 'btn btn-primary',
                        )
                );

        // Add controls to the form
        $this   ->addElement($cardHolder)
                ->addElement($cardNumber)
                ->addElement($cardType)
                ->addElement($startDate)
                ->addElement($expiryDate)
                ->addElement($issueNumber)
                ->addElement($cvv)
                ->addElement($paymentType);

        $this->addElement($submit);
	}

}