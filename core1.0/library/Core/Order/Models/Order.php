<?php

class Core_Order_Models_Order extends App_Model_Abstract {

    protected $_id;
    protected $_orderNumber;
    protected $_userId;
    protected $_billingAddressId;
    protected $_deliveryAddressId;
    protected $_currencyId;
    protected $_itemsTotal;
    protected $_deliveryCharge;
    protected $_vatCharge;
    protected $_discount;
    protected $_orderTotal;
    protected $_amountOutstanding;
    protected $_dateOrdered;
    protected $_dateProcessed;
    protected $_status;
    protected $_conversion;
    protected $_paymentType;
    
    const VAT = 0.20;
    
    protected $_orderParts = array();
    protected $_orderNotes = array();
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setOrderNumber($orderNumber) {
        $this->_orderNumber = (string) $orderNumber;
        return $this;
    }

    public function getOrderNumber() {
        return $this->_orderNumber;
    }

    public function setUserId($userId) {
        $this->_userId = (int) $userId;
        return $this;
    }

    public function getUserId() {
        return $this->_userId;
    }
    
    public function setConversion($conversion) {
        $this->_conversion = $conversion;
        return $this;
    }

    public function getConversion() {
        return $this->_conversion;
    }

    public function setBillingAddressId($billingAddressId) {
        $this->_billingAddressId = (int) $billingAddressId;
        return $this;
    }

    public function getBillingAddressId() {
        return $this->_billingAddressId;
    }

    public function setDeliveryAddressId($deliveryAddressId) {
        $this->_deliveryAddressId = (int) $deliveryAddressId;
        return $this;
    }

    public function getDeliveryAddressId() {
        return $this->_deliveryAddressId;
    }

    public function setCurrencyId($currencyId) {
        $this->_currencyId = (int) $currencyId;
        return $this;
    }

    public function getCurrencyId() {
        return $this->_currencyId;
    }

    public function setItemsTotal($itemsTotal) {
        $this->_itemsTotal = (float) $itemsTotal;
        return $this;
    }

    public function getItemsTotal() {
        return $this->_itemsTotal;
    }

    public function setDeliveryCharge($deliveryCharge) {
        $this->_deliveryCharge = (float) $deliveryCharge;
        return $this;
    }

    public function getDeliveryCharge() {
        return $this->_deliveryCharge;
    }

    public function setVatCharge($vatCharge) {
        $this->_vatCharge = (float) $vatCharge;
        return $this;
    }

    public function getVatCharge() {
        return $this->_vatCharge;
    }

    public function setDiscount($discount) {
        $this->_discount = (float) $discount;
        return $this;
    }

    public function getDiscount() {
        return $this->_discount;
    }

    public function setOrderTotal($orderTotal) {
        $this->_orderTotal = (float) $orderTotal;
        return $this;
    }

    public function getOrderTotal() {
        return $this->_orderTotal;
    }
    
    public function getAmountOutstanding() {
        return $this->_amountOutstanding;
    }

    public function setAmountOutstanding($amountOutstanding) {
        $this->_amountOutstanding = $amountOutstanding;
        return $this;
    }

    public function setDateOrdered($dateOrdered) {
        $this->_dateOrdered = (string) $dateOrdered;
        return $this;
    }

    public function getDateOrdered() {
        return $this->_dateOrdered;
    }

    public function setDateProcessed($dateProcessed) {
        $this->_dateProcessed = (string) $dateProcessed;
        return $this;
    }

    public function getDateProcessed() {
        return $this->_dateProcessed;
    }

    public function setStatus($status) {
        $this->_status = (string) $status;
        return $this;
    }

    public function getStatus() {
        return $this->_status;
    }
    
    public function setOrderParts($orderParts){
        $this->_orderParts = $orderParts;
        return $this;
    }
    
    public function getPaymentType() {
        return $this->_paymentType;
    }

    public function setPaymentType($paymentType) {
        $this->_paymentType = $paymentType;
        return $this;
    }

    public function getOrderParts() {
        if (empty($this->_orderParts)) {
            $orderpartModel = new Core_Order_Models_Orderpart();
            $this->_orderParts = $orderpartModel->getOrderPartsUsingOrderId($this->getId());
        }
        return $this->_orderParts;
    }
    
    public function getOrderNotes() {
        if (empty($this->_orderNotes)) {
            $ordernoteModel = new Core_Order_Models_Ordernote();
            $this->_orderNotes = $ordernoteModel->getOrderNotesUsingOrderId($this->getId());
        }
        return $this->_orderNotes;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Order_Models_OrderMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs order object using order id
     * @param int $orderId
     * @return \Core_Order_Models_Order
     */
    public function findById($orderId) {
        $this->getMapper()->find($orderId, $this);
        return $this;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function getAllOrders($type = 'object') {
        $allOrder = $this->getMapper()->fetchAll($type);
        return $allOrder;
    }

    public function fetchAllforReports() {
        $allOrder = $this->getMapper()->fetchAllforReports();
        return $allOrder;
    }

    /**
     * Gets the order session
     * @return array|bool
     */
    public function getSession() {
        $orderSession = new Zend_Session_Namespace('order');
        return $orderSession;
    }

    /**
     * @param $orderId
     * @param $orderNumber
     * @return bool
     */
    public function setSession($orderId, $orderNumber) {
        $this->getSession()->orderId = $orderId;
        $this->getSession()->orderNumber = $orderNumber;
        return true;
    }

    /**
     * clears the order session
     * @return boolean
     */
    public function clearSession() {
        $this->getSession()->unsetAll();
        return true;
    }

    /**
     * Checks if order has been generated by checking if the session order id is live
     * @return boolean
     */
    public function orderHasAlreadyBeenGenerated() {
        if ($this->getSession()->orderId) {
            return true;
        } else {
            return false;
        }
    }

    public function generateVatCharge($amount) {
        $itemsTotal = floatval($amount);
        $vat = self::VAT * $itemsTotal;
        return $vat;
    }

    /**
     * Generates the order number
     * @return string
     */
    public function generateOrderNumber() {
        //the following constants to be pulled from database in the near future
        $settings = new Core_Settings_Models_Settings();
        $orderNumberPrefix = $settings->getOrderNumberPrefix();
        $orderNumberStartPoint = $settings->getOrderNumberStartPoint();
        $orderNumberFormat = $settings->getOrderNumberFormat();
        //end of constants to be pulled from database
//        if ($this->orderHasAlreadyBeenGenerated()) {
//            return $this->getSession()->orderNumber;
//        }
        $lastOrderEntry = $this->getMapper()->fetchLastOrderEntry();
        if (null !== $lastOrderEntry) {
            $orderNumberStartPoint = $orderNumberStartPoint + $lastOrderEntry->getId();
            $orderNumberStartPoint++;
        }
        $orderNumber = str_replace(
                array("prefix", "ordernumber"),
                array($orderNumberPrefix, $orderNumberStartPoint),
                $orderNumberFormat
        );
        return $orderNumber;
    }

    /**
     * Generates the order options for the order object
     * @return array
     */

    public function generateOrderOptionsFromBasket()
    {
        $userModel      = new Core_User_Models_User();
        $usergroupModel = new Core_Usergroup_Models_Usergroup();
        $basketModel    = new Core_Basket_Models_Basket();
        $userId         = $userModel->getUserSession()->userId;
        $user           = $userModel->findById($userId);

        $itemsTotal = 0;
        $stockModel = new Core_Stock_Models_Stock();
        $basketStock = $basketModel->getStock();
        $basketruleModel = new Core_Basketrule_Models_Basketrule();
        if (!empty($basketStock)) {
            foreach ($basketStock as $key => $value) {
                if ($value) {
                    $stockObject = $stockModel->findById($key);
                    $itemPrice = round($stockObject->getPrice()  * $this->getConversion(), 2) ;
                    $itemPriceTotal = $itemPrice * $value;
                    $itemsTotal += $itemPriceTotal;
                }
            }
        }
        $currentUser = Core_User_Models_User::getCurrent();
        $totalDiscount = 0;
        if($currentUser instanceof Core_User_Models_User){
            $totalDiscount = $basketruleModel->getBasketDiscount($basketModel, $currentUser->getUserGroupId());
            $itemsTotal -= $totalDiscount;
        }
        
        //$itemsTotal     = $basketModel->getItemsTotal() * $this->getConversion();
        $delivery       = new Zend_Session_Namespace('delivery');
        $deliveryCharge = $delivery->deliveryCharge * $this->getConversion();

        $payment       = new Zend_Session_Namespace('payment');

        $vatCharge            = $this->generateVatCharge($itemsTotal);
        $usergroup      = $usergroupModel->findById($currentUser->getUserGroupId());
        if($usergroup instanceof Core_Usergroup_Models_Usergroup && $usergroup->getVat() === '1'){
            $vatCharge += (float) $this->generateVatCharge($deliveryCharge);
        }

        $orderTotal     = (floatval($vatCharge) + floatval($itemsTotal) + floatval($deliveryCharge));
        $orderTotal     = round($orderTotal, 2);
        $options        = array(
                            'orderNumber'       => $this->generateOrderNumber(),
                            'userId'            => $user->getId(),
                            'billingAddressId'  => $user->getBillingAddressId(),
                            'deliveryAddressId' => $user->getDeliveryAddressId(),
                            'currencyId'        => 1,
                            'itemsTotal'        => $itemsTotal,
                            'deliveryCharge'    => $deliveryCharge,
                            'vatCharge'         => $vatCharge,
                            'discount'          => $totalDiscount,
                            'orderTotal'        => $orderTotal,
                            'dateOrdered'       => date('Y-m-d', time()),
                            'dateProcessed'     => NULL,
                            'paymentType'       => $payment->paymentType,
                            'status'            => 'open',
                        );

        return $options;
    }

    /**
     * Generates an order object with the order number and order properties
     * @return \Core_Order_Models_Order
     */
    public function generateOrder() {
        $orderOptions = $this->generateOrderOptionsFromBasket();
        $this->setOptions($orderOptions);
//        if ($this->orderHasAlreadyBeenGenerated()) {
//            $this->setId($this->getSession()->orderId); //sets the id with the session id so the save updates the database entry instead of inserting a new one
//            $this->getMapper()->save($this);
//        } else {
//            $orderId = $this->getMapper()->save($this);
//            $this->setSession($orderId, $this->getOrderNumber()); //sets the order session id and order number
//        }
        return $this;
    }

    public function save() {
        return $this->getMapper()->save($this);
    }

    /**
     * Checks if an order has already been generated by checking for a session order id and returns the Order object 
     * using the saved session order id
     * @return type
     */
    public function getLiveOrder() {
        if ($this->orderHasAlreadyBeenGenerated()) {
            return $this->findById($this->getSession()->orderId);
        } else {
            return;
        }
    }

    /**
     * Processes the order payment
     * @param type $paymentData
     * @return type
     * @throws Exception
     */
    public function processPayment($paymentData) {
        $settings   = new Core_Settings_Models_Settings();
        $countryModel = new Core_Language_Models_Countries();
        $user = Core_User_Models_User::getCurrent();

        //Set SagePay Params
        $vpsProtocol = $settings->getVpsProtocol();
        $vendor = $settings->getVendor();
        $currency = 'GBP';
        $clientIP = $_SERVER['REMOTE_ADDR'];
        if (null === $user->getId()) {
            throw new Exception('User is not logged in');
            return;
        }

        //Instantiate SagePay Model with set params
        $sagepay = new Zend_Sagepay(array(
            'vpsProtocol' => $vpsProtocol,
            'vendor' => $vendor,
            'currency' => $currency,
            'log' => new Zend_Log(new Zend_Log_Writer_Firebug()), //for debugging. you could use a db or file log
            'clientIP' => $clientIP,
        ));

        //sets test mode and simulator mode to true
        if(APPLICATION_ENV != 'live'){
            $sagepay->setTestMode(true)->setSimulator(true);
        }

        $startDate = str_replace('/', '', $paymentData['start_date']);
        $expiryDate = str_replace('/', '', $paymentData['expiry_date']);

        //Instantiate SagePay CreditCard model with passed card details
        $creditCard = new Zend_Sagepay_Data_CreditCard(array(
            'card_holder' => $paymentData['card_holder'],
            'card_type' => $paymentData['card_type'], //Zend_Sagepay_Data_CreditCard::TYPE_VISA,
            'card_number' => $paymentData['card_number'],
            'start_date' => $startDate, //'0111',
            'expiry_date' => $expiryDate, //'0111',
            'cvv2' => $paymentData['cvv'], //'123',
        ));

        //get user billing & delivery addresses
        $userBillingAddress = $user->getBillingAddress();
        $userBillingCountryIso = $countryModel->findById($userBillingAddress->getCountryId())->getIso();
        $userDeliveryAddress = $user->getDeliveryAddress();
        $userDeliveryCountryIso = $countryModel->findById($userDeliveryAddress->getCountryId())->getIso();

        //Instantiate SagePay Address model with billing address details
        $billingAddress = new Zend_Sagepay_Data_Address(array(
            'surname' => $user->getSurname(),
            'firstnames' => $user->getForename(),
            'address1' => $userBillingAddress->getLineOne(),
            'address2' => $userBillingAddress->getLineTwo(),
            'city' => $userBillingAddress->getCity(),
            'postcode' => $userBillingAddress->getPostcode(),
            'country' => $userBillingCountryIso
        ));

        //Instantiate SagePay Address model with delivery address details
        $deliveryAddress = new Zend_Sagepay_Data_Address(array(
            'surname' => $user->getSurname(),
            'firstnames' => $user->getForename(),
            'address1' => $userDeliveryAddress->getLineOne(),
            'address2' => $userDeliveryAddress->getLineTwo(),
            'city' => $userDeliveryAddress->getCity(),
            'postcode' => $userDeliveryAddress->getPostcode(),
            'country' => $userDeliveryCountryIso
        ));

        //Order details
        $ourPaymentRef = $this->getOrderNumber();
        $orderTotal = number_format($this->getOrderTotal(), 2);
        $orderDescription = $settings->getOrderDescription();

        //processes the order through sagepay
        $response = $sagepay->doPayment($orderTotal, $orderDescription, $ourPaymentRef, $creditCard, $billingAddress, $deliveryAddress);
        return $response;
    }

    public function cancelLiveOrder() {
        
    }

    /**
     * Save order parts in database
     * @return \Core_Order_Models_Order
     */
    public function saveOrderParts() {
        $basketModel = new Core_Basket_Models_Basket();
        $basketItems = $basketModel->getDisplayData();
        foreach ($basketItems as $key => $basketItem) {
            $options = array(
                'orderId' => $this->getId(),
                'stockId' => $key,
                'price' => $basketItem['itemprice'],
                'quantity' => $basketItem['qty'],
            );
            $orderPartsModel = new Core_Order_Models_Orderpart();
            $orderPartsModel->setOptions($options);
            $orderPartsModel->save();
        }
        return $this;
    }

    /**
     * Confirm order by saving order parts
     * @return boolean
     */
    public function confirm() {
        $order = $this->generateOrder();
        $orderId = $order->save();
        $order->setId($orderId)->saveOrderParts();
        return $order->getOrderNumber();
    }

    /**
     * Gets all orders using a user's userid
     * @param int $userId
     * @return \Core_Order_Models_Order array
     */
    public function getOrdersByUserId($userId) {
        return $this->getMapper()->fetchOrdersByUserId($userId);
    }

    /**
     * Generates a packing slip for an order
     * @return \Zend_Pdf
     * @throws Exception
     */
    public function generatePackingSlip() {
        try {
            $settings = Core_Settings_Models_Settings::getSettings();
            $userModel = new Core_User_Models_User();
            $user = $userModel->findById($this->getUserId());

            $packingSlip = new Zend_Pdf("core/pdf/template_packingslip.pdf", null, true);
            $fontPath    = 'core/css/assets/fonts/';
            $page = $packingSlip->pages[0];

            $header = APP_PLATFORM . '/img/packingslip/header.jpg';
            $footer = APP_PLATFORM . '/img/packingslip/footer.jpg';
            $headerImage = Zend_Pdf_Image::imageWithPath($header);
            $footerImage = Zend_Pdf_Image::imageWithPath($footer);
            $page->drawImage($headerImage, 330, 720, 580, 740);
            $page->drawImage($footerImage, 100, 50, 500, 120);
            $customerName = $user->getForename() . ' ' . $user->getSurname();
            $addressLineOne = $user->getDeliveryAddress()->getLineOne();
            $addressLineTwo = $user->getDeliveryAddress()->getLineTwo();
            $city = $user->getDeliveryAddress()->getCity();
            $region = $user->getDeliveryAddress()->getRegion();
            $country = $user->getDeliveryAddress()->getCountry()->getCountry();

            $orderNumber = $this->getOrderNumber();
            $filename = $orderNumber . '-packingslip';
            $disclaimer = "Mistakes are possible! Please check your delivery carefully and inform Infinity Incorporated Ltd on info@".$settings->getSiteUrl();

            // specify font
            $fontBold = Zend_Pdf_Font::fontWithPath($fontPath . 'Helvetica_Bold.ttf');
            $fontNormal = Zend_Pdf_Font::fontWithPath($fontPath . 'Helvetica.ttf');

            // specify color
            $color = new Zend_Pdf_Color_HTML("black");
            $page->setFillColor($color);

            $page->setFont($fontBold, 20);
            $page->drawText('Packing Slip', 40, 720);

            $page->setFont($fontBold, 10);
            $posY = 700;
            $page->drawText($customerName, 50, $posY);
            $posY -= 15;
            $page->drawText($addressLineOne, 50, $posY);
            $posY -= 15;
            $page->drawText($addressLineTwo, 50, $posY);
            $posY -= 15;
            $page->drawText($city . ', ' . $region, 50, $posY);
            $posY -= 15;
            $page->drawText($country, 50, $posY);


            // another font

            $page->setFont($fontNormal, 10);

            // packingSlip information
            $page->drawText(date("d M, Y"), 70, 593);
            $page->drawText($orderNumber, 310, 593);

            // purchase items
            $posY = 520;
            if ($this->getOrderParts()) {
                foreach ($this->getOrderParts() as $orderPart) {
                    $productName = $orderPart->getPartStockDetails()->product;
                    $colourway = $orderPart->getPartStockDetails()->colourway;
                    $option = $orderPart->getPartStockDetails()->option;
                    $quantity = $orderPart->getQuantity();
                    $page->drawText($productName, 70, $posY);
                    $page->drawText($colourway . ' / ' . $option, 400, $posY);
                    $page->drawText($quantity, 530, $posY);
                    $posY -= 18;
                }
            }

            $page->setFont($fontNormal, 8);
            $page->drawText($disclaimer, 50, 155);

            // instruct browser to download the PDF
            header("Content-Type: application/x-pdf");
            header("Content-Disposition: attachment; filename=" . $filename . ".pdf");
            header("Cache-Control: no-cache, must-revalidate");

            // output the PDF
            echo $packingSlip->render();
        } catch (Zend_Pdf_Exception $e) {
            throw new Exception('Zend Pdf Error: ' . $e->getMessage());
        } catch (Exception $e) {
            throw new Exception('Application Error: ' . $e->getMessage());
        }
    }
    
    /**
     * Gets the number of orders placed in array of status
     * @param date $dateFrom
     * @param date $dateTo
     * @return object
     */
    public function getAllOrderStats($dateFrom = null, $dateTo = null){
        $open = 0;
        $processed = 0;
        $dispatched = 0;
        $awaiting = 0;
        $completed = 0;
        $totalNumber = 0;
        $totalSales = 0;
        if($this->getAllOrders()){
            foreach($this->getAllOrders() as $order){
                if($dateFrom && $dateTo){
                    if($order->getDateOrdered() > $dateFrom && $order->getDateOrdered() < $dateTo){
                        $status = $order->getStatus();
                        $$status++;
                        $totalNumber++;
                        $totalSales += $order->getOrderTotal();
                    }
                }
                else{
                    $status = $order->getStatus();
                    $$status++;
                    $totalNumber++;
                    $totalSales += $order->getOrderTotal();
                }
            }
        }
        $allOrderStats = array(
            'open'          => $open,
            'processed'     => $processed,
            'dispatched'    => $dispatched,
            'awaiting'      => $awaiting,
            'completed'     => $completed,
            'totalNumber'   => $totalNumber,
            'totalSales'    => $totalSales
        );
        return (object) ($allOrderStats);
    }

    /**
     * Checks to see if an order has been processed
     * @return boolean
     */
    public function hasBeenProcessed() {
        if ($this->getStatus() !== 'open') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks to see if an order can be dispatched
     * @return boolean
     */
    public function isDispatchable() {
        //any other conditions before an order can be dispatched
        return $this->hasBeenProcessed();
    }

    /**
     * Checks to see if an order has been dispatched
     * @return boolean
     */
    public function hasBeenDispatched() {
        if ($this->getStatus() === 'dispatched') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Processes order
     * @return boolean
     */
    public function process() {
        return $this->setStatus('processed')->save($this);
    }
    
    /**
     * Dispatches order
     * @return boolean
     */
    public function dispatch() {
        $emailer = new Core_Emailer_Models_Emailer();
        $emailer->sendOrderDispatchEmail($this);
        return $this->setStatus('dispatched')->save($this);
    }

    //Static function to get current order
    public static function getCurrent() {
        $order = new Core_Order_Models_Order();
        return $order->getLiveOrder();
    }
    
    //Function to get all dispatched orders for report
    public function getDispatchReport() {
        $dispatch = $this->getMapper()->getDispatchReport();
        return $dispatch;
    }

}

