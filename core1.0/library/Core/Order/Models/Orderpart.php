<?php

class Core_Order_Models_Orderpart extends App_Model_Abstract {

    protected $_id;
    protected $_orderId;
    protected $_stockId;
    protected $_price;
    protected $_quantity;
    protected $_discount;
    
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setOrderId($orderId) {
        $this->_orderId = (int) $orderId;
        return $this;
    }

    public function getOrderId() {
        return $this->_orderId;
    }

    public function setStockId($stockId) {
        $this->_stockId = (int) $stockId;
        return $this;
    }

    public function getStockId() {
        return $this->_stockId;
    }

    public function setPrice($price) {
        $this->_price = (float) $price;
        return $this;
    }

    public function getPrice() {
        return $this->_price;
    }

    public function setQuantity($quantity) {
        $this->_quantity = (int) $quantity;
        return $this;
    }

    public function getQuantity() {
        return $this->_quantity;
    }

    public function setDiscount($discount) {
        $this->_discount = (float) $discount;
        return $this;
    }

    public function getDiscount() {
        return $this->_discount;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Order_Models_OrderpartMapper());
        }
        return $this->_mapper;
    }
    
    /**
     * Gets the total cost of this order part
     * @return type
     */
    public function getLineTotal(){
        $total = $this->getQuantity() * $this->getPrice();
        return $total;
    }

    /**
     * Constructs orderpart object using order id
     * @param int $orderPartId
     * @return \Core_Order_Models_Orderpart
     */
    public function findById($orderPartId) {
        $this->getMapper()->find($orderPartId, $this);
        return $this;
    }
    
    public function getSoldReports() {
        $soldReport = $this->getMapper()->getSoldReports();
        return $soldReport;
    }

    /**
     * Gets all orderparts in database
     * @return array \Core_Order_Models_Order
     */
    public function getAllOrderParts($type = 'object') {
        $allOrderParts = $this->getMapper()->fetchAll($type);
        return $allOrderParts;
    }
    
    /**
     * Gets all order parts specific to an order id
     * @param type $orderId
     * @return type
     */
    public function getOrderPartsUsingOrderId($orderId){
        $allOrderParts = $this->getAllOrderParts();
        $entries    = array();
        foreach($allOrderParts as $orderPart){
            if($orderPart->getOrderId() === $orderId){
                $entries[] = $orderPart;
            }
        }
        return $entries;
    }
    
    /**
     * Gets the product model for this order part
     * @return \Core_Product_Models_Product
     */
    public function getPartStockDetails(){
        $stockModel     = new Core_Stock_Models_Stock();
        $stock          = $stockModel->findById($this->getStockId());
        if(!$stock instanceof Core_Stock_Models_Stock){
            throw new Exception ('Stock model cannot be instantiated');
        }
        return $stock->getCompleteStockDetails();
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }


}

