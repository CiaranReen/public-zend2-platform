<?php

class Core_Order_Models_OrderMapper {

    protected $_dbTable;

    const TABLE = 'Core_Order_Models_DbTable_Order';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Order_Models_Order $order) {
        $data = array(
            'order_number' => $order->getOrderNumber(),
            'user_id' => $order->getUserId(),
            'billing_address_id' => $order->getBillingAddressId(),
            'delivery_address_id' => $order->getDeliveryAddressId(),
            'currency_id' => $order->getCurrencyId(),
            'items_total' => $order->getItemsTotal(),
            'delivery_charge' => $order->getDeliveryCharge(),
            'vat_charge' => $order->getVatCharge(),
            'discount' => $order->getDiscount(),
            'order_total' => $order->getOrderTotal(),
            'amount_outstanding' => $order->getAmountOutstanding(),
            'date_ordered' => $order->getDateOrdered(),
            'date_processed' => $order->getDateProcessed(),
            'status' => $order->getStatus(),
            'payment_type' => $order->getPaymentType(),
        );

        if (null === ($id = $order->getId())) {
            unset($data['order_id']);
            $this->getDbTable()->insert($data);
            return $this->getDbTable()->getAdapter()->lastInsertId();
        } else {
            $this->getDbTable()->update($data, array('order_id = ?' => $id));
            return $id;
        }
    }

    public function find($id, Core_Order_Models_Order $order) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();
        $order->setId($row->order_id)
                ->setOrderNumber($row->order_number)
                ->setUserId($row->user_id)
                ->setBillingAddressId($row->billing_address_id)
                ->setDeliveryAddressId($row->delivery_address_id)
                ->setCurrencyId($row->currency_id)
                ->setItemsTotal($row->items_total)
                ->setDeliveryCharge($row->delivery_charge)
                ->setVatCharge($row->vat_charge)
                ->setDiscount($row->discount)
                ->setOrderTotal($row->order_total)
                ->setAmountOutstanding($row->amount_outstanding)
                ->setDateOrdered($row->date_ordered)
                ->setDateProcessed($row->date_processed)
                ->setStatus($row->status)
                ->setPaymentType($row->payment_type);
        return $order;
    }
    
    public function fetchAll($type = 'object')
    {
        $resultSet = $this->getDbTable()->fetchAll( 
                $this->getDbTable()->select()->order('order_id DESC')
        );
        if($type !== 'object'){
            return $resultSet;
        }
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Order_Models_Order();
            $entry->findById($row->order_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function getAllOrders() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('o' => 'orders'), array ('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }
    
    public function fetchAllforReports()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Order_Models_Order();
            $entry->findById($row);
            $entries[] = $entry;
        }
        return $entries; 
    }
    
    public function fetchLastOrderEntry() {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from('orders')
                ->order('order_id DESC')
                ->limit(1, 0);
        $resultSet = $this->getDbTable()->fetchRow($select);
        if ($resultSet) {
            $orderModel = new Core_Order_Models_Order();
            $lastOrderEntry = $orderModel->findById($resultSet->order_id);
            return $lastOrderEntry;
        } else {
            return;
        }
    }

    public function fetchOrdersByUserId($userId) {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from('orders')
                ->where('user_id = ?', $userId)
                ->order('order_id DESC');
        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        if ($resultSet) {
            foreach ($resultSet as $row) {
                $orderModel = new Core_Order_Models_Order();
                $entries[] = $orderModel->findById($row->order_id);
            }
            return $entries;
        } else {
            return;
        }
    }
    
    public function getDispatchReport() {
        $status = 'dispatched';
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('o' => 'orders'), array('*'))
                ->where('status = ?', $status);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function allOrdersTotal() {
        $stmt = $this->getDbTable()->select()
            ->from(array('o' => 'orders'), array('sum(order_total) as order_total'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);

        foreach ($resultSet as $rs) {
            $orderTotal = $rs;
        }
        return $orderTotal;
    }

}
