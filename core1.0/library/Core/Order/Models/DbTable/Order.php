<?php

class Core_Order_Models_DbTable_Order extends Zend_Db_Table_Abstract
{
    /** Table Name **/
        protected $_name = 'orders';
        
        protected $_dependentTables = array('order_parts');
    
}

