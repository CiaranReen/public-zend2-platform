<?php

class Core_Order_Models_Ordernote extends App_Model_Abstract {

    protected $_id;
    protected $_orderId;
    protected $_userId;
    protected $_username;
    protected $_note;
    protected $_dateAdded;
    
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setOrderId($orderId) {
        $this->_orderId = (int) $orderId;
        return $this;
    }

    public function getOrderId() {
        return $this->_orderId;
    }

    public function setUserId($userId) {
        $this->_userId = (int) $userId;
        return $this;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function setNote($note) {
        $this->_note = (string) $note;
        return $this;
    }

    public function getNote() {
        return $this->_note;
    }
    
    public function getDateAdded() {
        return $this->_dateAdded;
    }

    public function setDateAdded($dateAdded) {
        $this->_dateAdded = $dateAdded;
        return $this;
    }

        
    public function setUsername($username){
        $this->_username = (string) $username;
        return $this;
    }
    
    public function getUsername(){
        if (null === $this->_username) {
            $userModel = new Core_User_Models_User();
            $user = $userModel->findById($this->getUserId());
            $this->setUsername($user->getUsername());
        }
        return $this->_username;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Order_Models_OrdernoteMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs ordernote object using order id
     * @param int $orderNoteId
     * @return \Core_Order_Models_Ordernote
     */
    public function findById($orderNoteId) {
        $this->getMapper()->find($orderNoteId, $this);
        return $this;
    }

    /**
     * Gets all ordernotes in database
     * @return array \Core_Order_Models_Order
     */
    public function getAllOrderNotes($type = 'object') {
        $allOrderNotes = $this->getMapper()->fetchAll($type);
        return $allOrderNotes;
    }
    
    /**
     * Gets all order notes specific to an order id
     * @param type $orderId
     * @return type
     */
    public function getOrderNotesUsingOrderId($orderId){
        $allOrderNotes = $this->getAllOrderNotes();
        $entries    = array();
        foreach($allOrderNotes as $orderNote){
            if($orderNote->getOrderId() === $orderId){
                $entries[] = $orderNote;
            }
        }
        return $entries;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }


}

