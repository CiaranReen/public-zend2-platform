<?php

class Core_Order_Models_OrdernoteMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Order_Models_DbTable_Ordernote';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Order_Models_Ordernote $orderNote)
    {
        $data = array(
            'order_id'  => $orderNote->getOrderId(),
            'user_id'  => $orderNote->getUserId(),
            'note'     => $orderNote->getNote(),
            'date_added' => $orderNote->getDateAdded(),
        );
 
        if (null === ($id = $orderNote->getId())) 
        {
            unset($data['order_note_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('order_note_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Order_Models_Ordernote $orderNote)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
    
        $row = $result->current();
        $orderNote->setId($row->order_note_id)
                ->setOrderId($row->order_id)
                ->setUserId($row->user_id)
                ->setNote($row->note)
                ->setDateAdded($row->date_added);
        return $orderNote;
    }
    
    public function fetchAll($type = 'object')
    {
        $resultSet = $this->getDbTable()->fetchAll();
        if($type !== 'object'){
            return $resultSet;
        }
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Order_Models_Ordernote();
            $entry->findById($row->order_note_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
}

