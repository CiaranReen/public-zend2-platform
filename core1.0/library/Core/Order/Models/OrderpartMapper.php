<?php

class Core_Order_Models_OrderpartMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Order_Models_DbTable_Orderpart';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Order_Models_Orderpart $orderPart)
    {
        $data = array(
            'order_id'  => $orderPart->getOrderId(),
            'stock_id'  => $orderPart->getStockId(),
            'price'     => $orderPart->getPrice(),
            'quantity'  => $orderPart->getQuantity(),
            'discount'  => $orderPart->getDiscount(),
        );
 
        if (null === ($id = $orderPart->getId())) 
        {
            unset($data['order_part_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('order_part_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Order_Models_Orderpart $orderPart)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
    
        $row = $result->current();
        $orderPart->setId($row->order_part_id)
                ->setOrderId($row->order_id)
                ->setStockId($row->stock_id)
                ->setPrice($row->price)
                ->setQuantity($row->quantity)
                ->setDiscount($row->discount);
        return $orderPart;
    }
    
    public function getSoldReports() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('op' => 'order_parts'), array('*'))
                ->joinInner(array('s' => 'stock'), 's.stock_id = op.stock_id')
                ->joinInner(array('p' => 'products'), 'p.product_id = s.product_id');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }
    
    public function fetchAll($type = 'object')
    {
        $resultSet = $this->getDbTable()->fetchAll();
        if($type !== 'object'){
            return $resultSet;
        }
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Order_Models_Orderpart();
            $entry->findById($row->order_part_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
}

