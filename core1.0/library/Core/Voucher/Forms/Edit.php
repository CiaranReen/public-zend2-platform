<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
class Core_Voucher_Forms_Edit extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEditForm($voucher) {

        // Set properties of the form
        $this->setName('editVoucher')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Edit voucher');

        // Name Field
        $name = $this->createElement('text', 'voucher_name')
                ->setOptions(
                        array(
                            'label' => 'Name',
                            'required' => true,
                            'class' => 'required',
                            'filters' => array('StringTrim', 'StripTags'),
                        )
                )->setValue($voucher->getName());

        // Amount Field
        $amount = $this->createElement('text', 'amount')
                        ->setOptions(
                                array(
                                    'label' => 'Amount',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($voucher->getAmount());
        
        $operand = $this->createElement('text', 'operand')
                        ->setOptions(
                                array(
                                    'label' => 'Operand',
                                    'required' => true,
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($voucher->getOperand());
        
        // Code field
        $code = $this->createElement('text', 'code')
                        ->setOptions(
                                array(
                                    'label' => 'Code',
                                    'required' => true,
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($voucher->getCode());

        // Start Date
        $startDate = $this->createElement('text', 'start_date')
            ->setOptions(
                array(
                    'id'        => 'start_date',
                    'class'     => 'datepicker',
                    'label'     => 'Start Date',
                    'required'  => true,
                    'filters'   => array('StringTrim', 'StripTags')
                )
            )->setValue($voucher->getStart());

        // Expiry Date
        $expiryDate = $this->createElement('text', 'expiry_date')
            ->setOptions(
                array(
                    'id'        => 'end_date',
                    'class'     => 'datepicker',
                    'label'     => 'End Date',
                    'required'  => true,
                    'filters'   => array('StringTrim', 'StripTags')
                )
            )->setValue($voucher->getExpire());
        
        $status = $this->createElement('radio', 'status')
                        ->setOptions(
                                array(
                                    'label' => 'Status',
                                    'required' => true,
                                )
                        )->setMultiOptions(array('active' => 'Active', 'inactive' => 'Inactive'))
                ->setValue($voucher->getStatus());

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($name)
                ->addElement($amount)
                ->addElement($operand)
                ->addElement($code)
                ->addElement($startDate)
                ->addElement($expiryDate)
                ->addElement($status)
                ->addElement($submit);
    }

}