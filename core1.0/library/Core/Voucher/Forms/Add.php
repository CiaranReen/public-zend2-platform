<?php

/**
 * Delete a user
 */
class Core_Voucher_Forms_Add extends Zend_Form {

    public function init() {
        
    }

    public function setupAddForm($userList) {
        /* @var $user User_Abstract_User */

        // Set properties of the form
        $this->setName('addVoucher')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Add Voucher');

        // Voucher Name Field
        $name = $this->createElement('text', 'voucher_name')
                ->setOptions(
                array(
                    'label' => 'Name',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Amount Field
        $amount = $this->createElement('text', 'amount')
                ->setOptions(
                array(
                    'label' => 'Amount',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        $operand = $this->createElement('text', 'operand')
                ->setOptions(
                array(
                    'label' => 'Operand',
                    'required' => true,
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        
        // Code Field
        $code = $this->createElement('text', 'code')
                ->setOptions(
                array(
                    'label' => 'Code',
                    'required' => true,
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Start Date
        $startDate = $this->createElement('text', 'start_date')
            ->setOptions(
                array(
                    'id'        => 'start_date',
                    'class'     => 'datepicker',
                    'label'     => 'Start Date',
                    'required'  => true,
                    'filters'   => array('StringTrim', 'StripTags')
                )
            );

        // Expiry Date
        $expiryDate = $this->createElement('text', 'expiry_date')
            ->setOptions(
                array(
                    'id'        => 'end_date',
                    'class'     => 'datepicker',
                    'label'     => 'End Date',
                    'required'  => true,
                    'filters'   => array('StringTrim', 'StripTags')
                )
            );

        $status = $this->createElement('radio', 'status')
                        ->setOptions(
                                array(
                                    'label' => 'Status',
                                    'required' => true,
                                )
                        )->setMultiOptions(array('active' => 'Active', 'inactive' => 'Inactive'));

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Add Voucher', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($name)
                ->addElement($amount)
                ->addElement($operand)
                ->addElement($code)
                ->addElement($startDate)
                ->addElement($expiryDate)
                ->addElement($status)
                ->addElement($submit);
    }

}