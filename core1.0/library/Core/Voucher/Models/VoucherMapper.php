<?php

class Core_Voucher_Models_VoucherMapper {

    protected $_dbTable;

    const TABLE = 'Core_Voucher_Models_DbTable_Voucher';

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }

        $this->vuModel = new Core_Voucher_Models_VoucherUser();
        $this->vuMapper = new Core_Voucher_Models_VoucherUserMapper();
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function fetchAll() {
        $stmt = $this->getDbTable()->fetchAll();
        //var_dump($stmt); die();
        return $stmt;
    }

    public function find($vId, Core_Voucher_Models_Voucher $vModel) {
        $result = $this->getDbTable()->find($vId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $vModel->setId($row->id)
            ->setName($row->voucher_name)
            ->setAmount($row->amount)
            ->setOperand($row->operand)
            ->setCode($row->code)
            ->setStart($row->start)
            ->setExpire($row->expire)
            ->setStatus($row->status);
        return $vModel;
    }

    public function save(Core_Voucher_Models_Voucher $voucher) {
        $data = array(
            'id' => $voucher->getId(),
            'voucher_name' => $voucher->getName(),
            'amount' => $voucher->getAmount(),
            'code' => $voucher->getCode(),
            'operand' => $voucher->getOperand(),
            'start' => $voucher->getStart(),
            'expire' => $voucher->getExpire(),
            'status' => $voucher->getStatus()
        );

        if (null === ($id = $voucher->getId())) {
            unset($data['id']);
            $voucher->setId($this->getDbTable()->insert($data));
            $this->vuMapper->addVoucherUsers($voucher);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    public function getAllUsersById($vId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vu' => 'voucher_user'), array('vu.user_id', 'u.forename', 'u.surname'))
            ->joinInner(array('u' => 'user'), 'vu.user_id = u.user_id')
            ->where('v_id =' . $vId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function getAllUsergroupsById($vId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vu' => 'voucher_user_group'), array('vu.user_group_id', 'ug.user_group_name'))
            ->joinInner(array('ug' => 'user_groups'), 'vu.user_group_id = ug.user_group_id')
            ->where('v_id =' . $vId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function getAllAssignedUsers($vId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vu' => 'voucher_user'), array('vu.user_id'))
            ->where('v_id =' . $vId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        $userIds = array();
        foreach($resultSet as $result) {
            $userIds[] = $result->user_id;
        }
        return $userIds;
    }

    public function getAllAssignedUsergroups($vId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vu' => 'voucher_user_group'), array('vu.user_group_id'))
            ->where('v_id = ?', $vId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        $userIds = array();
        foreach($resultSet as $result) {
            $userIds[] = $result->user_group_id;
        }
        return $userIds;
    }

    public function checkVoucher($voucher) {
        $currentDate = date("Y-m-d");
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('v' => 'voucher'), array('*'))
            ->where('code = ?', $voucher)
            ->where('start <= ?', $currentDate)
            ->where('expire >= ?', $currentDate);
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        return $resultSet;
    }

    public function delete($voucher) {
        $where = "id = '" . $voucher->id . "'";
        $result = $this->getDbTable()->delete($where);
        return $result;
    }

}

