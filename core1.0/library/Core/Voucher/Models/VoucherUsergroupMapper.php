<?php

class Core_Voucher_Models_VoucherUsergroupMapper {

    protected $_dbTable;

    const TABLE = 'Core_Voucher_Models_DbTable_VoucherUsergroup';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
    
    public function fetchAll() {
        $stmt = $this->getDbTable()->fetchAll();
        //var_dump($stmt); die();
        return $stmt;
    }
    
    public function find($vId, Core_Voucher_Models_Voucher $vModel) {
        $result = $this->getDbTable()->find($vId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $vModel->setId($row->id)
                ->setName($row->voucher_name)
                ->setAmount($row->amount)
                ->setOperand($row->operand)
                ->setCode($row->code)
                ->setStatus($row->status);
        return $vModel;
    }
    
    /**
     * This method is for actually adding new products to a NEW voucher
     * 
     * @param Core_Voucher_Models_VoucherUser $voucher
     * @return int Inserted ID
     */
    public function addVoucherUsers($voucher) {
        $users = $voucher->getUsers();
        //echo '<pre>'; var_dump($users); die();
        foreach ($users as $user) {
            $data = array(
                'v_id' => $voucher->getId(),
                'user_id' => $user->id,
            );
            $this->getDbTable()->insert($data);
        }
    }
    
    public function saveUsergroups($vu, array $users) {
        $where = "v_id = '" . $vu . "'";
        $resultSet = $this->getDbTable()->delete($where);
        foreach ($users as $userId) {
            $userData = array(
                'v_id' => $vu,
                'user_group_id' => $userId,
            );
            //var_dump($this->getDbTable()); die();
            $resultSet = $this->getDbTable()->insert($userData);
        }
        return $resultSet;
    }
    
    public function checkUsergroupCanAccessVoucher($ugId, $vId, $returnType = false) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('vu' => 'voucher_user_group'), array('*'))
                ->where('user_group_id =?', $ugId)
                ->where('v_id =?', $vId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            $returnType = true;
        }
        return $returnType;
    }
    
}

