<?php

class Core_Voucher_Models_VoucherUser extends App_Model_Abstract
{
    protected $_id;
    protected $_name;
    protected $_amount;
    protected $_code;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Voucher_Models_VoucherUserMapper());
        }
        return $this->_mapper;
    }
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getAmount() {
        return $this->_amount;
    }

    public function setAmount($amount) {
        $this->_amount = $amount;
        return $this;
    }
    
    public function getCode() {
        return $this->_code;
    }

    public function setCode($code) {
        $this->_code = $code;
        return $this;
    }

    
    /************************************************/
    /************* START OF METHODS *****************/
    /************************************************/
    public function saveUsers($vId, $vu) {
        $this->getMapper()->saveUsers($vId, $vu);
        return $this;
    }
    
    public function saveIndividualUsers($vId, $vu) {
        $this->getMapper()->saveIndividualUsers($vId, $vu);
        return $this;
    }
    
    public function checkUserCanAccessVoucher($userId, $vId) {
        $returnType = $this->getMapper()->checkUserCanAccessVoucher($userId, $vId);
        return $returnType;
    }
}

