<?php

class Core_Voucher_Models_Voucher extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_amount;
    protected $_code;
    protected $_users;
    protected $_operand;
    protected $_start;
    protected $_expire;
    protected $_status;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Voucher_Models_VoucherMapper());
        }
        return $this->_mapper;
    }

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    public function getAmount() {
        return $this->_amount;
    }

    public function setAmount($amount) {
        $this->_amount = $amount;
        return $this;
    }

    public function getCode() {
        return $this->_code;
    }

    public function setCode($code) {
        $this->_code = $code;
        return $this;
    }
    
    public function getOperand() {
        return $this->_operand;
    }

    public function setOperand($operand) {
        $this->_operand = $operand;
        return $this;
    }
    
    public function getStart() {
        return $this->_start;
    }

    public function setStart($start) {
        $this->_start = $start;
        return $this;
    }

    public function getExpire() {
        return $this->_expire;
    }

    public function setExpire($expire) {
        $this->_expire = $expire;
        return $this;
    }
    
    public function getStatus() {
        return $this->_status;
    }

    public function setStatus($status) {
        $this->_status = $status;
        return $this;
    }

    public function getUsers() {
        return $this->_users;
    }

    public function setUsers($users) {
        $this->_users = $users;
        return $this;
    }

    /************************************************/
    /************* START OF METHODS *****************/
    /************************************************/

    public function fetchAll() {
        $fetchAll = $this->getMapper()->fetchAll();
        return $fetchAll;
    }

    public function findById($dId) {
        $this->getMapper()->find($dId, $this);
        return $this;
    }

    public function save($vModel) {
        return $this->getMapper()->save($vModel);
    }

    public function getAllUsersById($vId) {
        $fetchUsers = $this->getMapper()->getAllUsersById($vId);
        return $fetchUsers;
    }
    
    public function getAllUsergroupsById($vId) {
        $fetchUsers = $this->getMapper()->getAllUsergroupsById($vId);
        return $fetchUsers;
    }
    
    public function getAllAssignedUsers($vId) {
        $fetchUsers = $this->getMapper()->getAllAssignedUsers($vId);
        return $fetchUsers;
    }
    
    public function getAllAssignedUsergroups($vId) {
        $fetchUsers = $this->getMapper()->getAllAssignedUsergroups($vId);
        return $fetchUsers;
    }
    
    public function checkVoucher($voucher) {
        $check = $this->getMapper()->checkVoucher($voucher);
        return $check;
    }
    
    public function delete($voucher) {
        $result = $this->getMapper()->delete($voucher);
        return $result;
    }
    
    public function storeInSession($voucher) {
        if($voucher instanceof Core_Voucher_Models_Voucher){
            $voucherSession = new Zend_Session_Namespace('voucherSession');
            $voucherSession->voucher = $voucher;
        } else {
            return false;
        }
    }
    
    public function getVoucherSession() {
        $voucherSession = new Zend_Session_Namespace('voucherSession');
        return $voucherSession->voucher;
    }

}

