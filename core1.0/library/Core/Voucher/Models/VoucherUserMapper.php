<?php

class Core_Voucher_Models_VoucherUserMapper {

    protected $_dbTable;

    const TABLE = 'Core_Voucher_Models_DbTable_VoucherUser';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
    
    public function fetchAll() {
        $stmt = $this->getDbTable()->fetchAll();
        //var_dump($stmt); die();
        return $stmt;
    }
    
    public function find($vId, Core_Voucher_Models_Voucher $vModel) {
        $result = $this->getDbTable()->find($vId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $vModel->setId($row->id)
                ->setName($row->voucher_name)
                ->setAmount($row->amount)
                ->setOperand($row->operand)
                ->setCode($row->code)
                ->setStatus($row->status);
        return $vModel;
    }
    
    /**
     * This method is for actually adding new users to a NEW voucher
     * 
     * @param Core_Voucher_Models_VoucherUser $voucher
     * @return int Inserted ID
     */
    public function addVoucherUsers($voucher) {
        $users = $voucher->getUsers();
        //echo '<pre>'; var_dump($users); die();
        foreach ($users as $user) {
            $data = array(
                'v_id' => $voucher->getId(),
                'user_id' => $user->id,
            );
            $this->getDbTable()->insert($data);
        }
    }
    
    public function saveUsers($vId, array $vu) {
        $where = "v_id = '" . $vId . "'";
        $resultSet = $this->getDbTable()->delete($where);
        foreach ($vu as $user) {    
            $userData = array(
                'v_id' => $vId,
                'user_id' => $user,
            );
            $resultSet = $this->getDbTable()->insert($userData);
        }
        return $resultSet;
    }
    
    public function saveIndividualUsers($vId, array $vu) {
        foreach ($vu as $user) {
            $where = "user_id = '" . $user . "'";
            $resultSet = $this->getDbTable()->delete($where);
            $userData = array(
                'v_id' => $vId,
                'user_id' => $user,
            );
            $resultSet = $this->getDbTable()->insert($userData);
        }
        return $resultSet;
    }
    
    public function checkUserCanAccessVoucher($userId, $vId, $returnType = false) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('vu' => 'voucher_user'), array('*'))
                ->where('user_id =?', $userId)
                ->where('v_id =?', $vId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            $returnType = true;
        }
        return $returnType;
    }
    
}

