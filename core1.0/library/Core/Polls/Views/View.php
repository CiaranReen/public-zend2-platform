<?php
/**
 * @package CO3-Core Platform
 *
 * Generates the view for User pages
 */

class Polls_Views_View extends View_Abstract_View {

	/**
	 * Render the page
	 */
	public function renderPage() {
                
		$documentRoot = Application_Environment::getClientWebsiteDocumentRoot();

		// Use the templates in this path
		$templatePath = $documentRoot . '/modules/Polls/Views/' . $this->getTheme() . '/' . $this->getCurrentPage();
		// ...and fallback to the templates here the above can't be found
		$baseTemplatePath = $documentRoot . '/modules/Admin/Views/' . $this->getBaseTheme() . '/Base';

		// Define the display order of the templates
		$this->getInclude($templatePath, $baseTemplatePath, '/htmlHead.phtml');
		$this->getInclude($templatePath, $baseTemplatePath, '/pageHead.phtml');
                $this->getInclude($templatePath, $baseTemplatePath, '/navigation.phtml');
		$this->getInclude($templatePath, $baseTemplatePath, '/content.phtml');
		$this->getInclude($templatePath, $baseTemplatePath, '/pageFooter.phtml');
		$this->getInclude($templatePath, $baseTemplatePath, '/htmlFooter.phtml');

	}

}