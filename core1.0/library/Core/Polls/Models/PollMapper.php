<?php

class Core_Polls_Models_PollMapper {
    
    protected $_dbTable;
    
    /**
     * 
     * @param type $dbTable
     * @return \Core_Polls_Models_PollMapper
     * @throws Exception
     */
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Polls_Models_DbTable_Poll');
        }
        return $this->_dbTable;
    }
    
    /**
     * 
     * @return \Polls_Models_PollsModel
     */
    public function findById() {
        $poll = new Core_Polls_Models_Poll();
        $row = $this->getDbTable()->select()
                ->from(array('p' => 'products'),
                        array('*'))
                ->where('product_id = ?', $poll->getProductId());
        $poll->setProductId($row);
        return $poll;
    }
    
    /**
     * 
     * @return type
     */
    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();
        return $resultSet;
    }
    
    public function savePollInDb(Core_Polls_Models_Poll $poll) {
        $pollData = array(
            'title' => $poll->getTitle(),
            'start' => $poll->getStartDate(),
            'end' => $poll->getEndDate(),
        );

        $affectedRows = $this->getDbTable()->insert($pollData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }
    
    /**
     * 
     * @param type $pollData
     * @return \Core_Polls_Models_PollMapper
     */
    public function savePoll($pollData) {
        $this->setTitle($pollData['title'])
                ->setStartDate($pollData['start'])
                ->setEndDate($pollData['end']);

        $this->savePollInDb($this);

        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function deleteCurrentPoll() {
        $where = '1=1';
        $resultSet = $this->getDbTable()->delete($where);
        return $resultSet;
    }
    
    /**
     * 
     * @return type
     */
    public function getAllUserSubmissions() {
        $sql = "SELECT u.forename, u.surname, u.email, p.submission, p.time, u.user_id  FROM user u
                INNER JOIN poll_reporting p
                WHERE p.user_id = u.user_id
                ORDER BY submission DESC";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }
}
?>
