<?php

class Core_Polls_Models_PastPolls {
    
    protected $_dbTable;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
    
    /**
     * 
     * @return type
     */
    public function getPastPolls() {
        $ppm = new Core_Polls_Models_PastPollsMapper();
        $getPastPolls = $ppm->getPastPolls();
        return $getPastPolls;
    }
    
    public function moveToPastPolls() {
        $ppm = new Core_Polls_Models_PastPollsMapper();
        $movePastPolls = $ppm->moveToPastPolls();
        return $movePastPolls;
        }
}
?>
