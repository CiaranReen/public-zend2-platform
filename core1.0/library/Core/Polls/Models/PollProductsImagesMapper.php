<?php

class Core_Polls_Models_PollProductsImagesMapper {
    
    protected $_dbTable;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Polls_Models_DbTable_PollProductsImages');
        }
        return $this->_dbTable;
    }
    
    /**
     * 
     * @param Polls_Abstract_Polls $product
     * @return boolean
     */
    public function saveProductImageInDb(Core_Polls_Models_PollProductsModel $productImage) {
        $productImageData = array(
            'image1_url' => $productImage->getImageOne(),
            'image2_url' => $productImage->getImageTwo(),
            'image3_url' => $productImage->getImageThree(),
        );
        $this->getDbTable()->delete();
        $this->getDbTable()->insert($productImageData);
    }
    
    /**
     * 
     * @param type $productData
     * @return \Polls_Models_PollsModel
     */
    public function saveProductImage($location1, $location2, $location3) {
        $this->setImageOne($location1);
        $this->setImageTwo($location2);
        $this->setImageThree($location3);
        $this->saveProductImageInDb($this);
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getProductImages() {
        $row = $this->getDbTable()->fetchAll();
        return $row;
    }
}
?>
