<?php

class Core_Polls_Models_PollProducts {
    
    protected $productId;
    protected $product1;
    protected $product2;
    protected $product3;
    protected $_mapper;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
    
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setProductId($productId) {
        $this->productId = $productId;
        
        return $this;
    }

    public function getProductId() {
        return $this->productId;
    }
    
    public function setProductNameOne($product1) {
        $this->product1 = $product1;
        return $this;
    }
    
    public function getProductNameOne() {
        return $this->product1;
    }
    
    public function setProductNameTwo($product2) {
        $this->product2 = $product2;
        return $this;
    }
    
    public function getProductNameTwo() {
        return $this->product2;
    }
    
    public function setProductNameThree($product3) {
        $this->product3 = $product3;
        return $this;
    }
    
    public function getProductNameThree() {
        return $this->product3;
    }
    
    /*************** Set Mapper here ****************/
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Polls_Models_PollProductsMapper());
		}
		return $this->_mapper;
    }
    
    
    public function saveProductInDb(Core_Polls_Models_PollProducts $product) {
        $productData = array(
            'product1' => $product->getProductNameOne(),
            'product2' => $product->getProductNameTwo(),
            'product3' => $product->getProductNameThree(),
        );
        $affectedRows = $this->getMapper()->getDbTable()->insert($productData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }
    
    /**
     * 
     * @param type $productData
     * @return \Polls_Models_PollsModel
     */
    public function saveProduct($productData) {
        $this->setProductNameOne($productData['product1']);
        $this->setProductNameTwo($productData['product2']);
        $this->setProductNameThree($productData['product3']);
        $this->saveProductInDb($this);
        return $this;
    }
    
    public function deleteCurrentProducts() {
        $pollProductsMapper = new Core_Polls_Models_PollProductsMapper();
        $pollProductsMapper->deleteCurrentProducts();
    }
    
    /**
     * 
     * @return type
     */
    public function getProducts() {
        $pollProductsMapper = new Core_Polls_Models_PollProductsMapper();
        $getProducts = $pollProductsMapper->getProducts();
        return $getProducts;
    }
    
    /**
     * 
     * @return \Polls_Models_PollsModel
     */
    public function findById() {
        $pollProductMapper = new Core_Polls_Models_PollProductsMapper();
        $getId = $pollProductMapper->findById();
        return $getId;
    }
}
?>