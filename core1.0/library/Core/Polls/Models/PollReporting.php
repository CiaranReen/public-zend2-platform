<?php

class Core_Polls_Models_PollReporting {
    
    protected $_dbTable;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
    
    public function getCount() {
        $pollReportingMapper = new Core_Polls_Models_PollReportingMapper();
        $getCount = $pollReportingMapper->countSubmissions();
        return $getCount;
    }
    
    /**
     * 
     * @return type
     */
    public function getAllUserSubmissions() {
        $pollReportingMapper = new Core_Polls_Models_PollReportingMapper();
        $getSubs = $pollReportingMapper->getAllUserSubmissions();
        return $getSubs;
    }
    
    public function getAllSubmissions() {
        $pollReportingMapper = new Core_Polls_Models_PollReportingMapper();
        $getSubs = $pollReportingMapper->getAllSubmissions();
        return $getSubs;
    }
    
    /**
     * 
     * @return type
     */
    public function deleteUserSubmissions() {
        $pollReportingMapper = new Core_Polls_Models_PollReportingMapper();
        $delete = $pollReportingMapper->deleteUserSubmissions();
        return $delete;
    }
}
?>
