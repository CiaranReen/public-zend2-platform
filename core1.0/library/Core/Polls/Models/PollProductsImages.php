<?php

class Core_Polls_Models_PollProductsImages {
    
    protected $_mapper;
    protected $image1;
    protected $image2;
    protected $image3;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        } 
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
    
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    
    /********** SETTERS AND GETTERS *************/
    public function setImageOne($image1) {
        $this->image1 = $image1;

        return $this;
    }

    public function getImageOne() {
        return $this->image1;
    }

    public function setImageTwo($image2) {
        $this->image2 = $image2;

        return $this;
    }

    public function getImageTwo() {
        return $this->image2;
    }

    public function setImageThree($image3) {
        $this->image3 = $image3;

        return $this;
    }

    public function getImageThree() {
        return $this->image3;
    }
    
    /************** Call Mapper here ************/
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Polls_Models_PollProductsImagesMapper());
        }
        return $this->_mapper;
    }

    public function saveProductImageInDb(Core_Polls_Models_PollProductsImages $productImage) {
        $productImageData = array(
            'image1_url' => $productImage->getImageOne(),
            'image2_url' => $productImage->getImageTwo(),
            'image3_url' => $productImage->getImageThree(),
        );
        $where = '1=1';
        $this->getMapper()->getDbTable()->delete($where);
        $this->getMapper()->getDbTable()->insert($productImageData);
    }

    public function saveProductImage($location1, $location2, $location3) {
        $this->setImageOne($location1);
        $this->setImageTwo($location2);
        $this->setImageThree($location3);
        $this->saveProductImageInDb($this);
        return $this;
    }
}
?>
