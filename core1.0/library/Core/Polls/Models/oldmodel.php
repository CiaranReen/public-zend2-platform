<?php

class Polls_Models_PollsModel extends Polls_Abstract_Polls {

    /**
     * 
     * @return \Polls_Models_PollsModel
     */
    public function load() {
        $row = $this->getDbAdapter()
                ->fetchRow("SELECT * FROM products WHERE product_id = ?", array('product_id' => $this->getProductId()));
        $this->setProductId($row['product_id'])
                ->setProductName($row['name']);
        return $this;
    }

    /**
     * 
     * @return type
     */
    public function getAllUserSubmissions() {
        $sql = "SELECT u.forename, u.surname, u.email, p.submission, p.time, u.user_id  FROM user u
                INNER JOIN poll_reporting p
                WHERE p.user_id = u.user_id
                ORDER BY submission DESC";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }
    
    public function getAllSubmissions() {
        $sql = "SELECT submission FROM poll_reporting";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }
    
    /**
     * 
     * @param Polls_Abstract_Polls $product
     * @return boolean
     */
    public function saveProductInDb(Polls_Abstract_Polls $product) {
        $productData = array(
            'product1' => $product->getProductNameOne(),
            'product2' => $product->getProductNameTwo(),
            'product3' => $product->getProductNameThree(),
        );
        $affectedRows = $this->getDbAdapter()->insert('poll_products', $productData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }
    
    /**
     * 
     * @param type $productData
     * @return \Polls_Models_PollsModel
     */
    public function saveProduct($productData) {
        $this->setProductNameOne($productData['product1']);
        $this->setProductNameTwo($productData['product2']);
        $this->setProductNameThree($productData['product3']);
        $this->saveProductInDb($this);
        return $this;
    }
    
    /**
     * 
     * @param Polls_Abstract_Polls $poll
     * @return boolean
     */
    public function savePollInDb(Polls_Abstract_Polls $poll) {
        $pollData = array(
            'title' => $poll->getTitle(),
            'start' => $poll->getStartDate(),
            'end' => $poll->getEndDate(),
        );
        $affectedRows = $this->getDbAdapter()->insert('poll', $pollData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }
    
    /**
     * 
     * @param type $pollData
     * @return \Polls_Models_PollsModel
     */
    public function savePoll($pollData) {
        $this->setTitle($pollData['title'])
                ->setStartDate($pollData['start'])
                ->setEndDate($pollData['end']);
        $this->savePollInDb($this);
        return $this;
    }
    
    /**
     * 
     * @param Polls_Abstract_Polls $product
     * @return boolean
     */
    public function saveProductImageInDb(Polls_Abstract_Polls $productImage) {
        $productImageData = array(
            'image1_url' => $productImage->getImageOne(),
            'image2_url' => $productImage->getImageTwo(),
            'image3_url' => $productImage->getImageThree(),
        );
        $this->getDbAdapter()->delete('poll_products_images');
        $affectedRows = $this->getDbAdapter()->insert('poll_products_images', $productImageData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }
    
    /**
     * 
     * @param type $productData
     * @return \Polls_Models_PollsModel
     */
    public function saveProductImage($location1, $location2, $location3) {
        $this->setImageOne($location1);
        $this->setImageTwo($location2);
        $this->setImageThree($location3);
        $this->saveProductImageInDb($this);
        return $this;
    }

    /**
     * 
     * @return type
     */
    public function deleteUserSubmissions() {
        $row = $this->getDbAdapter()->delete('poll_reporting');
        return $row;
    }
    
    /**
     * 
     * @return type
     */
    public function deleteCurrentPoll() {
        $row = $this->getDbAdapter()->delete('poll');
        return $row;
    }

    public function deleteCurrentProducts() {
        $row = $this->getDbAdapter()->delete('poll_products');
        return $row;
    }

    /**
     * 
     * @return boolean
     */
    public function moveToPastPolls() {
        $currentPoll = $this->getCurrentPoll();
        if (!empty($currentPoll)) {
            foreach ($currentPoll as $poll) {
                $row = $this->getDbAdapter()->insert('past_polls', $poll);
                $graph = array(
                    'graph_url' => $this->getLastModifiedFile()
                );
                $last_id = $this->getDbAdapter()->lastInsertId();
                $where = 'poll_id=' . $last_id;
                $row2 = $this->getDbAdapter()->update('past_polls', $graph, $where);
                return $row;
            }
            return $row;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return type
     */
    public function getCount() {
        $sql = "SELECT submission FROM poll_reporting";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }

    /**
     * 
     * @return type
     */
    public function getProducts() {
        $sql = "SELECT product1, product2, product3 FROM poll_products";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }

    /**
     * 
     * @return type
     */
    public function getPastPolls() {
        $sql = "SELECT * FROM past_polls";
        $row = $this->getDbAdapter()->fetchAll($sql);
        return $row;
    }
    
    //This creates the graph, removes the last graph image and updates the file in the graphs folder, ready to be rendered
    public function createBarGraph() {
        $this->getCount();
        $sub1Count = 0;
        $sub2Count = 0;
        $sub3Count = 0;
        foreach ($this->getCount() as $count) {
            if ($count['submission'] == 1) {
                $sub1Count += 1;
            } elseif ($count['submission'] == 2) {
                $sub2Count += 1;
            } elseif ($count['submission'] == 3) {
                $sub3Count += 1;
            }
        }
        $datay = array($sub1Count, $sub2Count, $sub3Count);

        $graph = new Graph(400, 280);
        $graph->SetScale('textlin');

        $graph->SetShadow();

        // Adjust the margin a bit to make more room for titles
        $graph->SetMargin(40, 30, 20, 40);

        // Create a bar pot
        $bplot = new BarPlot($datay);

        // Adjust fill color
        $bplot->SetFillColor('orange');
        $graph->Add($bplot);

        // Setup the titles
        $graph->title->Set('# of Submissions');
        $graph->xaxis->title->Set('Product #');
        $graph->yaxis->title->Set('Submissions');

        $graph->title->SetFont(FF_FONT1, FS_BOLD);
        $graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
        $graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);

        $gdImgHandler = $graph->Stroke(_IMG_HANDLER);

        //delete the old graph
        $this->removeLatestModifiedFile();
        // Display the new graph
        $fileName = "/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/" . $this->getRandomFileName() . ".png";
        $graph->img->Stream($fileName);
    }

    //generate a random name for the new graph
    /**
     * 
     * @return string
     */
    public function getRandomFileName() {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $length = 6;
        $fileName = '';
        for ($i = 0; $i < $length; $i++) {
            $fileName .= $chars[mt_rand(0, 35)];
        }
        return $fileName;
    }

    //remove the last modified graph from the /img folder 
    public function removeLatestModifiedFile() {
        $files = glob("/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/*.png");
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);
        $latest_file = key($files);
        unlink($latest_file);
    }

    //Get the last modified graph in the /img folder
    /**
     * 
     * @return type
     */
    public function getLastModifiedFile() {
        $files = glob("/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/*.png");
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);
        $latest_file = key($files);
        $pre = "/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/";

        if (substr($latest_file, 0, strlen($pre)) === $pre) {
            $latest_file = substr($latest_file, strlen($pre));
        }
        return $latest_file;
    }

    public function saveGraphToFolder() {
        $filePath = "/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/savedGraphs/";
        $image = $this->getLastModifiedFile();
        copy("/var/www/co3-lynx/co3-core/modules/Admin/Public/img/graph/$image", "$filePath/$image");
    }

    /**
     * 
     * @param type $product
     * @param type $user
     * @return type
     */
    public function getSubmissionData($product, $user) {
        if ($product && $user) {
            $value = array(
                'submission' => $product,
                'user_id' => $user,
            );
            //var_dump($value);
            $row = $this->getDbAdapter()->insert('poll_reporting', $value);

            return $row;
        }
    }
    
    /**
     * 
     * @return type
     */
    public function getProductImages() {
        $sql = "SELECT * FROM poll_products_images";
        
        $row = $this->getDbAdapter()->fetchAll($sql);
        
        return $row;
    }

}

?>
