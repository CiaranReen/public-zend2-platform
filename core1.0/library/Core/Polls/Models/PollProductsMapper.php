<?php

class Core_Polls_Models_PollProductsMapper {
    
    protected $_dbTable;
    
    /**
     * 
     * @param type $dbTable
     * @return \Core_Polls_Models_PollProductsMapper
     * @throws Exception
     */
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Polls_Models_DbTable_PollProducts');
        }
        return $this->_dbTable;
    }
    
    /**
     * 
     * @param Core_Polls_Models_PollProductsModel $product
     */
    public function saveProductInDb(Core_Polls_Models_PollProductsModel $product) {
        $data = array(
            'product1' => $product->getProductNameOne(),
            'product2' => $product->getProductNameTwo(),
            'product3' => $product->getProductNameThree(),
        );
            $this->getDbTable()->insert($data);
    }
    
    /**
     * 
     * @param type $productData
     * @return \Core_Polls_Models_PollProductsMapper
     */
    public function saveProduct($productData) {
        $this->setProductNameOne($productData['product1']);
        $this->setProductNameTwo($productData['product2']);
        $this->setProductNameThree($productData['product3']);
        $this->save($this);
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getPastPolls() {
        $row = $this->getDbTable()->fetchAll();
        return $row;
    }
    
    /**
     * 
     * @return type
     */
    public function deleteCurrentProducts() {
        $where = '1=1';
        $row = $this->getDbTable()->delete($where);
        return $row;
    }
    
    /**
     * 
     * @return type
     */
    public function getProducts() {
        $row = $this->getDbTable()->select()
                ->from(array('p' => 'poll_products'),
                        array('product1', 'product2', 'product3'));
        return $row;
    }
    
    /**
     * 
     * @return \Polls_Models_PollsModel
     */
    public function findById() {
        $pollProduct = new Core_Polls_Models_PollProducts();
        $row = $this->getDbTable()->select()
                ->from(array('p' => 'products'),
                        array('*'))
                ->where('product_id = ?', $pollProduct->getProductId());
        $pollProduct->setProductId($row);
        return $row;
    }
}
?>