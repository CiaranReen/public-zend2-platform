<?php

class Core_Polls_Models_PollReportingMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Polls_Models_DbTable_PollReporting');
        }
        return $this->_dbTable;
    }

    /**
     * 
     * @return type
     */
    public function getAllUserSubmissions() {
        $row = $this->getDbTable()->select()
                ->from(array('p' => 'poll_reporting'), array('u.forename', 'u.surname', 'u.email', 'u.user_id', 'p.submission', 'p.time'))
                ->joinInner(array('u' => 'user'), 'u.user_id = p.user_id')
                ->order(array('submission DESC'));
        return $row;
    }

    public function getAllSubmissions() {
        $row = $this->getDbTable()->select()
                ->from(array('p' => 'poll_reporting'), array('submission'));
        return $row;
    }

    /**
     * 
     * @return type
     */
    public function deleteUserSubmissions() {
        $where = '1=1';
        $row = $this->getDbTable()->delete($where);
        return $row;
    }
    
    /**
     * 
     * @return type
     */
    public function countSubmissions() {
        $resultSet = $this->getDbTable()->select()
                ->from(array('p' => 'poll_reporting'), array('submission'));
        return $resultSet;
    }
    
    /**
     * 
     * @param type $product
     * @param type $user
     * @return type
     */
    //Ajax pass
    public function getSubmissionData($product, $user) {
        if ($product && $user) {
            $value = array(
                'submission' => $product,
                'user_id' => $user,
            );
            $row = $this->getDbTable()->insert($value);

            return $row;
        }
    }
}

?>
