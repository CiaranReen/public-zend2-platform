<?php

class Core_Polls_Models_PastPollsMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Polls_Models_DbTable_PastPolls');
        }
        return $this->_dbTable;
    }

    /**
     * 
     * @return boolean
     */
    public function moveToPastPolls() {
        $pollModel = new Core_Polls_Models_Poll();
        $currentPoll = $pollModel->getCurrentPoll();
        if (!empty($currentPoll)) {
            foreach ($currentPoll as $poll) {
                /*$row = $this->getDbTable()->insert($poll);
                $graph = array(
                    'graph_url' => $this->getLastModifiedFile()
                );
                $last_id = $this->getDbTable()->lastInsertId();
                $where = 'poll_id=' . $last_id;
                $row2 = $this->getDbTable()->update($graph, $where);
                return $row;*/echo '<pre>'; print_r($poll);
            }
            //return $row;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return type
     */
    public function getPastPolls() {
        $row = $this->getDbTable()->fetchAll();
        return $row;
    }

}

?>
