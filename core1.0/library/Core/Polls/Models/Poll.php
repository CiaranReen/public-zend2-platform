<?php

include_once('/var/www/co3-lynx/core2/library/Core/Polls/jpgraph/src/jpgraph.php');
include_once('/var/www/co3-lynx/core2/library/Core/Polls/jpgraph/src/jpgraph_bar.php');

class Core_Polls_Models_Poll {

    protected $productId;
    protected $name;
    protected $image;
    protected $title;
    protected $start;
    protected $end;
    protected $product1;
    protected $product2;
    protected $product3;
    protected $image1;
    protected $image2;
    protected $image3;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setTitle($title) {
        $this->title = $title;
        return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setStartDate($start) {
        $this->start = $start;

        return $this;
    }

    public function getStartDate() {
        return $this->start;
    }

    public function setEndDate($end) {
        $this->end = $end;

        return $this;
    }

    public function getEndDate() {
        return $this->end;
    }

    public function setProductId($productId) {
        $this->productId = $productId;

        return $this;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function setProductNameOne($product1) {
        $this->product1 = $product1;

        return $this;
    }

    public function getProductNameOne() {
        return $this->product1;
    }

    public function setProductNameTwo($product2) {
        $this->product2 = $product2;

        return $this;
    }

    public function getProductNameTwo() {
        return $this->product2;
    }

    public function setProductNameThree($product3) {
        $this->product3 = $product3;

        return $this;
    }

    public function getProductNameThree() {
        return $this->product3;
    }

    public function setImageOne($image1) {
        $this->image1 = $image1;

        return $this;
    }

    public function getImageOne() {
        return $this->image1;
    }

    public function setImageTwo($image2) {
        $this->image2 = $image2;

        return $this;
    }

    public function getImageTwo() {
        return $this->image2;
    }

    public function setImageThree($image3) {
        $this->image3 = $image3;

        return $this;
    }

    public function getImageThree() {
        return $this->image3;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Polls_Models_PollMapper());
        }
        return $this->_mapper;
    }

    public function getCurrentPoll() {
        $pollMapper = new Core_Polls_Models_PollMapper();
        $currentPoll = $pollMapper->fetchAll();
        return $currentPoll;
    }

    public function savePollInDb(Core_Polls_Models_Poll $poll) {
        $pollData = array(
            'title' => $poll->getTitle(),
            'start' => $poll->getStartDate(),
            'end' => $poll->getEndDate(),
        );

        $affectedRows = $this->getMapper()->getDbTable()->insert($pollData);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }

    /**
     * 
     * @param type $pollData
     * @return \Core_Polls_Models_PollMapper
     */
    public function savePoll($pollData) {
        $this->setTitle($pollData['title'])
                ->setStartDate($pollData['start'])
                ->setEndDate($pollData['end']);
        $this->savePollInDb($this);
        return $this;
    }

    /**
     * 
     * @return type
     */
    public function deleteCurrentPoll() {
        $pollMapper = new Core_Polls_Models_PollMapper();
        $delete = $pollMapper->deleteCurrentPoll();
        return $delete;
    }

    /**
     * 
     * @return \Polls_Models_PollsModel
     */
    public function findById() {
        $pollMapper = new Core_Polls_Models_PollMapper();
        $getId = $pollMapper->findById();
        return $getId;
    }

    //This creates the graph, removes the last graph image and updates the file in the graphs folder, ready to be rendered
    public function createBarGraph() {
        $pollReporting = new Core_Polls_Models_PollReporting();
        $counts = $pollReporting->getCount();
        $sub1Count = 0;
        $sub2Count = 0;
        $sub3Count = 0;
        //var_dump($counts);
        foreach ($counts as $count) {
            if ($count['submission'] == 1) {
                $sub1Count += 1;
            } elseif ($count['submission'] == 2) {
                $sub2Count += 1;
            } elseif ($count['submission'] == 3) {
                $sub3Count += 1;
            }
        }
        $datay = array($sub1Count, $sub2Count, $sub3Count);

        $graph = new Graph(400, 280);
        $graph->SetScale('textlin');

        $graph->SetShadow();

        // Adjust the margin a bit to make more room for titles
        $graph->SetMargin(40, 30, 20, 40);

        // Create a bar pot
        $bplot = new BarPlot($datay);

        // Adjust fill color
        $bplot->SetFillColor('orange');
        $graph->Add($bplot);

        // Setup the titles
        $graph->title->Set('# of Submissions');
        $graph->xaxis->title->Set('Product #');
        $graph->yaxis->title->Set('Submissions');

        $graph->title->SetFont(FF_FONT1, FS_BOLD);
        $graph->yaxis->title->SetFont(FF_FONT1, FS_BOLD);
        $graph->xaxis->title->SetFont(FF_FONT1, FS_BOLD);

        $gdImgHandler = $graph->Stroke(_IMG_HANDLER);

        //delete the old graph
        $this->removeLatestModifiedFile();
        // Display the new graph
        $fileName = "/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/" . $this->getRandomFileName() . ".png";
        $graph->img->Stream($fileName);
    }

    //generate a random name for the new graph
    /**
     * 
     * @return string
     */
    public function getRandomFileName() {
        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $length = 6;
        $fileName = '';
        for ($i = 0; $i < $length; $i++) {
            $fileName .= $chars[mt_rand(0, 35)];
        }
        return $fileName;
    }

    //remove the last modified graph from the /img folder 
    public function removeLatestModifiedFile() {
        $files = glob("/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/*.png");
        if (file_exists("/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/*")) {
            $files = array_combine($files, array_map("filemtime", $files));
            arsort($files);
            $latest_file = key($files);
            unlink($latest_file);
        }
    }

    //Get the last modified graph in the /img folder
    /**
     * 
     * @return type
     */
    public function getLastModifiedFile() {
        $files = glob("/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/*.png");
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);
        $latest_file = key($files);
        $pre = "/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/";

        if (substr($latest_file, 0, strlen($pre)) === $pre) {
            $latest_file = substr($latest_file, strlen($pre));
        }
        return $latest_file;
    }

    public function saveGraphToFolder() {
        $filePath = "/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/savedGraphs/";
        $image = $this->getLastModifiedFile();
        copy("/var/www/co3-lynx/core2/public/lifefitness-shop/img/graphs/$image", "$filePath/$image");
    }

}

?>
