<?php

abstract class Polls_Abstract_Polls extends Frontend_Models_Abstract_AbstractModel implements Polls_Interface_Polls {

    protected $productId;
    protected $name;
    protected $image;
    protected $title;
    protected $start;
    protected $end;
    protected $product1;
    protected $product2;
    protected $product3;
    
    public function setProductId($productId) {
        $this->productId = $productId;
        
        return $this;
    }

    public function getProductId() {
        return $this->productId;
    }

    public function setProductName($name) {
        $this->name = $name;
        
        return $this;
    }

    public function getProductName() {
        return $this->name;
    }
    
    
    public function setImage($image) {
        $this->image = $image;
        
        return $this;
    }

    public function getImage() {
        return $this->image;
    }
    
    public function setTitle($title) {
        $this->title = $title;
        
        return $this;
    }
    
    public function getTitle() {
        return $this->title;
    }
    
    public function setStartDate($start) {
        $this->start = $start;
        
        return $this;
    }
    
    public function getStartDate() {
        return $this->start;
    }
    
    public function setEndDate($end) {
        $this->end = $end;
        
        return $this;
    }
    
    public function getEndDate() {
        return $this->end;
    }

}
?>
