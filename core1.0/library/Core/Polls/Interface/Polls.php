<?php

interface Polls_Interface_Polls {

    public function setProductId($productId);

    public function getProductId();

    public function setProductName($name);

    public function getProductName();
    
    public function setProductNameOne($product1);
    
    public function getProductNameOne();
    
    public function setProductNameTwo($product2);
    
    public function getProductNameTwo();
    
    public function setProductNameThree($product3);
    
    public function getProductNameThree();

    public function setImage($image);

    public function getImage();
    
    public function setTitle($title);
    
    public function getTitle();
    
    public function setStartDate($start);
    
    public function getStartDate();
    
    public function setEndDate($end);
    
    public function getEndDate();
}

?>
