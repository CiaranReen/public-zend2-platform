<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 08/10/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_Polls_Forms_StartForm extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param Polls_Abstract_Polls $product
     */
    public function setupStartForm($poll) {
        /* @var $user Polls_Abstract_Polls */

        // Set properties of the form
        $this->setName('startPoll')
                ->setMethod('post')
                ->setAttrib('class', 'form-signin')
                ->setDescription('Start a new poll');

        // Name Field
        $title = $this->createElement('text', 'title')
                        ->setOptions(
                                array(
                                    'label' => 'Poll Title: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getTitle());

        // Name Field
        $start = $this->createElement('text', 'start')
                        ->setOptions(
                                array(
                                    'label' => 'Start date (DD/MM/YY): ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getStartDate());

        // Name Field
        $end = $this->createElement('text', 'end')
                        ->setOptions(
                                array(
                                    'label' => 'End date (DD/MM/YY): ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getEndDate());

        //Product fields
        $product1 = $this->createElement('text', 'product1')
                        ->setOptions(
                                array(
                                    'label' => 'Product Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getProductNameOne());

        $product2 = $this->createElement('text', 'product2')
                        ->setOptions(
                                array(
                                    'label' => 'Product Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getProductNameTwo());

        $product3 = $this->createElement('text', 'product3')
                        ->setOptions(
                                array(
                                    'label' => 'Product Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($poll->getProductNameThree());

        $image1 = $this->createElement('file', 'image1_url')
                        ->setOptions(
                                array(
                                    'required' => false,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setDestination('/var/www/co3-lynx/core2/public/lifefitness-shop/img/poll-products');

        $image2 = $this->createElement('file', 'image2_url')
                        ->setOptions(
                                array(
                                    'required' => false,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setDestination('/var/www/co3-lynx/core2/public/lifefitness-shop/img/poll-products');

        $image3 = $this->createElement('file', 'image3_url')
                        ->setOptions(
                                array(
                                    'required' => false,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setDestination('/var/www/co3-lynx/core2/public/lifefitness-shop/img/poll-products');
        
        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Create', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($title)
                ->addElement($start)
                ->addElement($end)
                ->addElement($product1)
                ->addElement($product2)
                ->addElement($product3)
                ->addElement($image1)
                ->addElement($image2)
                ->addElement($image3)
                ->addElement($submit);
    }

}