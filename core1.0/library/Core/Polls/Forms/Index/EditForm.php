<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Polls_Forms_Index_EditForm extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param Polls_Abstract_Polls $product
     */
    public function setupEditForm($product) {
        /* @var $user Polls_Abstract_Polls */

        // Set properties of the form
        $this->setName('editProduct')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Edit Product');

        // Name Field
        $name = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Product Admin Identifier: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($product->getProductName());

        // Image Field
        $image = $this->createElement('image', 'image')
                        ->setOptions(
                                array(
                                    'label' => 'Upload image:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($product->getImage());                        

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Update', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($name)
                ->addElement($image)
                ->addElement($submit);
    }

}