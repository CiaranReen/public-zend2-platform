<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 22/10/2013
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */

class Core_Address_Forms_AddressForm extends Zend_Form {

	public function init() {
        $this->setName('Address')
			->setMethod(self::METHOD_POST)
			->setAttrib('class', 'form-signin');

        // Line one Field
        $lineOne = $this->createElement('text', 'lineOne')
                    ->setOptions(
                array(
                     'label'    => 'Line one',
                     'required' => true,
                     'class' => 'required',
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Line two Field
        $lineTwo = $this->createElement('text', 'lineTwo')
                   ->setOptions(
                array(
                     'label'    => 'Line two',
                     'required' => true,
                     'class' => 'required',
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // City Field
        $city = $this->createElement('text', 'city')
                        ->setOptions(
                array(
                     'label'    => 'City',
                     'required' => true,
                     'class' => 'required',
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Region Field
        $region = $this->createElement('text', 'region')
                    ->setOptions(
                array(
                     'label'    => 'Region',
                     'required' => true,
                     'class' => 'required',
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Postcode Field
        $postcode = $this->createElement('text', 'postcode')
                    ->setOptions(
                array(
                     'label'    => 'Postcode',
                     'required' => true,
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Country Field
        $countryId = $this->createElement('select', 'countryId')
                    ->setOptions(
                array(
                     'label'    => 'Country',
                     'required' => true,
                     'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Postcode Field
        $addressId = $this->createElement('hidden', 'addressId')
            ->setOptions(
                array(
                    'required' => false,
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
        
        $countriesMapper = new Core_Language_Models_CountriesMapper();
        foreach ($countriesMapper->fetchAll() as $country) {
            $countryId->addMultiOption($country->getId(), $country->getCountry()); 
        }
        $options = $countryId->getMultiOptions();
        asort($options);
        $countryId->setMultiOptions($options)->setValue(1);
        
        
        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($lineOne)
            ->addElement($lineTwo)
            ->addElement($city)
            ->addElement($region)
            ->addElement($postcode)
            ->addElement($countryId)
            ->addElement($addressId)
            ->addElement($submit);
	}

    /**
     * @param Core_Address_Models_Address $address
     * @return void|Zend_Form
     */
    public function populate(Core_Address_Models_Address $address) {
		/* @var $address Address_Abstract_Address */
        if($address->getId() !== NULL){
            $this->getElement('lineOne')->setValue($address->getLineOne());
            $this->getElement('lineTwo')->setValue($address->getLineTwo());
            $this->getElement('city')->setValue($address->getCity());
            $this->getElement('region')->setValue($address->getRegion());
            $this->getElement('postcode')->setValue($address->getPostcode());
            $this->getElement('countryId')->setValue($address->getCountryId());
            $this->getElement('addressId')->setValue($address->getId());
        }

	}

}