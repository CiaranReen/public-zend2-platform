<?php

class Core_Address_Models_AddressMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Address_Models_DbTable_Address';
    
        public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function find($id, Core_Address_Models_Address $address)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $address->setId($row->address_id)
                    ->setLineOne($row->line_one)
                    ->setLineTwo($row->line_two)
                    ->setCity($row->city)
                    ->setRegion($row->region)
                    ->setPostcode($row->postcode)
                    ->setCountryId($row->country_id)
                    ->setUserId($row->user_id)
                    ->setActive($row->active);
        return $address;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Address_Models_Address();
            $entry->findById($row->address_id);
            $entries[] = $entry;
        }
        return $entries;
    }

    // Save an address to the db
    public function save(Core_Address_Models_Address $address) {
        $data = array(
            'line_one' => $address->getLineOne(),
            'line_two' => $address->getLineTwo(),
            'city' => $address->getCity(),
            'region' => $address->getRegion(),
            'postcode' => $address->getPostcode(),
            'country_id' => $address->getCountryId(),
            'user_id' => $address->getUserId(),
            'active' => $address->getActive(),
        );

        if (null === ($id = $address->getId())) {
            unset($data['address_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('address_id = ?' => $id));
        }
    }
}

