<?php

class Core_Address_Models_Address extends App_Model_Abstract {

    protected $_id;
    protected $_lineOne;
    protected $_lineTwo;
    protected $_city;
    protected $_region;
    protected $_postcode;
    protected $_countryId;
    protected $_userId;
    protected $_active;

    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setLineOne($lineOne) {
        $this->_lineOne = (string) $lineOne;
        return $this;
    }

    public function getLineOne() {
        return $this->_lineOne;
    }

    public function setLineTwo($lineTwo) {
        $this->_lineTwo = (string) $lineTwo;
        return $this;
    }

    public function getLineTwo() {
        return $this->_lineTwo;
    }

    public function setCity($city) {
        $this->_city = (string) $city;
        return $this;
    }

    public function getCity() {
        return $this->_city;
    }

    public function setRegion($region) {
        $this->_region = (string) $region;
        return $this;
    }

    public function getRegion() {
        return $this->_region;
    }

    public function setPostcode($postcode) {
        $this->_postcode = (string) $postcode;
        return $this;
    }

    public function getPostcode() {
        return $this->_postcode;
    }

    public function setCountryId($countryId) {
        $this->_countryId = (int) $countryId;
        return $this;
    }

    public function getCountryId() {
        return $this->_countryId;
    }

    public function setUserId($userId) {
        $this->_userId = (int) $userId;
        return $this;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function setActive($active){
        $this->_active = (int) $active;
        return $this;
    }

    public function getActive(){
        return $this->_active;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Address_Models_AddressMapper());
        }
        return $this->_mapper;
    }

    // Save an address to the db
    public function save() {
        $this->getMapper()->save($this);
        return $this;
    }

    // Set the address variables ready for input to the db
    public function saveAddress($addressData) {
        $this->setLineOne($addressData['line_one'])
                ->setLineTwo($addressData['line_two'])
                ->setCity($addressData['town'])
                ->setRegion($addressData['region'])
                ->setPostcode($addressData['postcode'])
                ->setCountryId($addressData['country'])
                ->setUserId($addressData['user_id'])
                ->setActive($addressData['active']);
        $id = $this->getMapper()->save($this);
        //echo '<pre>'; var_dump($id); die();
        return $id;
    }
    
    public function lastInsertId() {
        return $this->getMapper()->getDbTable()->getAdapter()->lastInsertId();
    }

    /**
     * Constructs address object using address id
     * @param int $addressId
     * @return \Core_Address_Models_Address
     */
    public function findById($addressId) {
        $this->getMapper()->find($addressId, $this);
        return $this;
    }

    /**
     * Gets all categories in database
     * @return array \Core_Address_Models_Address
     */
    public function getAllAddresses() {
        $allAddresses = $this->getMapper()->fetchAll();
        return $allAddresses;
    }

    public function isActive(){
        if($this->getActive() === 1){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Gets the country object for the address using the country id
     * @return \Core_Language_Models_Countries
     */
    public function getCountry(){
        if(null === $this->getCountryId()){
            return ;
        }
        $countryModel = new Core_Language_Models_Countries();
        return $countryModel->findById($this->getCountryId());
    }

    public function getAllUserAddresses($userId){
        $userAddresses = array();
        $allAddresses = $this->getMapper()->fetchAll();
        foreach($allAddresses as $address){
            if($address->getUserId() === $userId && $address->isActive()){
                $userAddresses[] = $address;
            }
        }
        return $userAddresses;
    }
}

