<?php

class Core_Product_Models_VendorProductManufacturers extends App_Model_Abstract {

    protected $_id;
    protected $_name;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

}