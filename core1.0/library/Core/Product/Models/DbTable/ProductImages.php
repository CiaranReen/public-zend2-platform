<?php

class Core_Product_Models_DbTable_ProductImages extends Zend_Db_Table_Abstract
{
    /** Table Name **/
    protected $_name = 'product_images';
    
    protected $_referenceMap = array(
        'Product' => array(
            'columns'           => array('product_id'),
            'refTableClass'     => 'Core_Product_Models_DbTable_Product',
            'refColumns'        => array('product_id'),
            'onDelete'          => self::CASCADE,
            'onUpdate'          => self::RESTRICT
        ),
        'Colourway' => array(
            'columns'           => array('colourway_id'),
            'refTableClass'     => 'Core_Colourway_Models_DbTable_Colourway',
            'refColumns'        => array('colourway_id'),
            'onDelete'          => self::CASCADE,
            'onUpdate'          => self::RESTRICT
        ),
    );
}

