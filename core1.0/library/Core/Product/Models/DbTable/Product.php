<?php

class Core_Product_Models_DbTable_Product extends Zend_Db_Table_Abstract
{
    /** Table Name **/
    protected $_name = 'products';
    
    protected $_dependentTables = array(
        'stock', 
        'product_categories', 
        'product_images', 
        'product_availability'
    );
}

