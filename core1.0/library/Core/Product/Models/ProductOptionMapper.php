<?php

class Core_Product_Models_ProductOptionMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Product_Models_DbTable_ProductOption';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Product_Models_ProductOption $productOption)
    {
        $data = array(
            'option'    => $productOption->getOption(),
            'shortcode' => $productOption->getShortcode()
        );
 
        if (null === ($id = $productOption->getId())) 
        {
            unset($data['option_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('option_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Product_Models_ProductOption $productOption)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $productOption->setId($row->option_id)
                ->setOption($row->option)
                ->setShortcode($row->shortcode);
        
        return $productOption;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $productOption = new Core_Product_Models_ProductOption();
            
            $productOption->findById($row->option_id);
            
            $entries[] = $productOption;
        }
        
        return $entries;
    }
}

