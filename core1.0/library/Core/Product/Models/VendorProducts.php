<?php

class Core_Product_Models_VendorProducts extends App_Model_Abstract {

    protected $_id;
    protected $_vendorPartNo;
    protected $_description;
    protected $_manufacturerId;
    protected $_manufacturerPartNo;
    protected $_cost;

    protected $_mapper;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $vendorPartNo
     * @return $this
     */
    public function setVendorPartNo($vendorPartNo)
    {
        $this->_vendorPartNo = $vendorPartNo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVendorPartNo()
    {
        return $this->_vendorPartNo;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * @param mixed $manufacturerId
     */
    public function setManufacturerId($manufacturerId)
    {
        $this->_manufacturerId = $manufacturerId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getManufacturerId()
    {
        return $this->_manufacturerId;
    }

    /**
     * @param $manufacturerPartNo
     * @return $this
     */
    public function setManufacturerPartNo($manufacturerPartNo)
    {
        $this->_manufacturerPartNo = $manufacturerPartNo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getManufacturerPartNo()
    {
        return $this->_manufacturerPartNo;
    }

    /**
     * @param $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->_cost = $cost;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->_cost;
    }


    /**
     * @param $mapper
     * @return $this
     */
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Product_Models_VendorProductsMapper());
        }
        return $this->_mapper;
    }

    //Get all vendor products for adding a product
    public function fetchAllVendorProducts() {
        return $this->getMapper()->fetchAllVendorProducts();
    }
    
    //fetch all for viewing all
    public function fetchAllVPs() {
        $fetchvp = $this->getMapper()->fetchAllVPs();
        return $fetchvp;
    }

    public function getSearch($string) {
        $search = $this->getMapper()->filterSearch($string);
        return $search;
    }

    public function readDataFromCsv($file) {
        $data = $this->getMapper()->readDataFromCsv($file);
        return $data;
    }
    
    public function updateContent($content, $id) {
        $content = $this->getMapper()->updateContent($content, $id);
        return $content;
    }

}