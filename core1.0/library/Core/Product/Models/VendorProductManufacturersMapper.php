<?php

class Core_Product_Models_VendorProductManufacturersMapper {

    protected $_dbTable;

    const TABLE = 'Core_Product_Models_DbTable_VendorProductManufacturers';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Product_Models_VendorProductManufacturers $vendorProductManufacturer) {
        $data = array(
            'name' => $vendorProductManufacturer->getName()
        );

        if (null === ($id = $vendorProductManufacturer->getId())) {
            unset($data['manufacturer_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('manufacturer_id = ?' => $id));
        }
    }

    public function find($id, Core_Product_Models_VendorProductManufacturers $vendorProductManufacturer) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $vendorProductManufacturer->setId($row->manufacturer_id)
                ->setName($row->name);

        return $vendorProductManufacturer;
    }

    public function fetchAll() {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = array();
        foreach ($resultSet as $row) {
            $manufacturers = new Core_Product_Models_VendorProductManufacturers();

            $manufacturers->setId($row->manufacturer_id)
                    ->setName($row->name);

            $entries[] = $manufacturers;
        }

        return $entries;
    }

    public function saveManufacturer($id, $name) {
        $data = array(
            'name' => $name
        );
        $where = "manufacturer_id = '" . $id ."'";
        $save = $this->getDbTable()->update($data, $where);
        if ($save === 1) {
            return true;
        } else {
            return false;
        }
    }



}

