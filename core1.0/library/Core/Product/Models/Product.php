<?php

class Core_Product_Models_Product extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_description;
    protected $_code;
    protected $_weight;
    protected $_pack;
    protected $_live;
    protected $_stockAction;
    protected $_highest;
    protected $_featured;
    protected $_new;
    protected $_packSizeWidth;
    protected $_packSizeLength;
    protected $_packSizeDepth;
    protected $_active;

    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setDescription($description) {
        $this->_description = (string) $description;
        return $this;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function setCode($code) {
        $this->_code = (string) $code;
        return $this;
    }

    public function getCode() {
        return $this->_code;
    }

    public function setWeight($weight) {
        $this->_weight = (int) $weight;
        return $this;
    }

    public function getWeight() {
        return $this->_weight;
    }

    public function setPack($pack) {
        $this->_pack = (int) $pack;
        return $this;
    }

    public function getPack() {
        return $this->_pack;
    }

    public function setLive($live) {
        $this->_live = (string) $live;
        return $this;
    }

    public function getLive() {
        return $this->_live;
    }

    public function setStockAction($stockAction) {
        $this->_stockAction = (string) $stockAction;
        return $this;
    }

    public function getStockAction() {
        return $this->_stockAction;
    }

    public function setHighest($highest) {
        $this->_highest = $highest;
        return $this;
    }

    public function getHighest() {
        return $this->_highest;
    }

    public function setFeatured($featured) {
        $this->_featured = $featured;
        return $this;
    }

    public function getFeatured() {
        return $this->_featured;
    }

    public function getNew() {
        return $this->_new;
    }

    public function setNew($new) {
        $this->_new = $new;
        return $this;
    }

    public function getPackSizeWidth()
    {
        return $this->_packSizeWidth;
    }

    public function setPackSizeWidth($packSizeWidth)
    {
        $this->_packSizeWidth = $packSizeWidth;
        return $this;
    }

    public function getPackSizeLength()
    {
        return $this->_packSizeLength;
    }

    public function setPackSizeLength($packSizeLength)
    {
        $this->_packSizeLength = $packSizeLength;
        return $this;
    }

    public function getPackSizeDepth()
    {
        return $this->_packSizeDepth;
    }

    public function setPackSizeDepth($packSizeDepth)
    {
        $this->_packSizeDepth = $packSizeDepth;
        return $this;
    }

    public function setActive($active){
        $this->_active = (int) $active;
        return $this;
    }

    public function getActive(){
        return $this->_active;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Product_Models_ProductMapper());
        }
        return $this->_mapper;
    }

    public function findById($productId) {
        $this->getMapper()->find($productId, $this);
        return $this;
    }

    /**
     * Gets all products in database using the db mapper object
     * @return array Core_Product_Models_Product
     */
    public function getAllProducts() {
        $allProducts = $this->getMapper()->fetchAll();
        return $allProducts;
    }

    /**
     * Gets all stock objects for a product from the db mapper object
     * @return array /Core_Stock_Models_Stock
     * @throws Exception
     */
    public function getStock() {
        $rows = $this->getMapper()->fetchAllProductStock($this->getId());
        if (empty($rows)) {
            throw new Exception('No stock found for this product');
        }
        return $rows;
    }

    /**
     * Gets all category objects for a product from the db mapper object
     * @return type
     * @throws Exception
     */
    public function getCategories() {
        $categories = $this->getMapper()->fetchAllProductCategories($this->getId());
        if (empty($categories)) {
            throw new Exception('No categories found for this product');
        }
        return $categories;
    }

    /**
     * Gets all assigned usergroups for a product from the db mapper object
     * @return Core_User_Models_User $usergroup
     * @throws Exception
     */
    public function getAssignedUsergroups() {
        $assignedUsergroups = $this->getMapper()->fetchAllAssignedUsergroups($this->getId());
        if (empty($assignedUsergroups)) {
            throw new Exception('No assigned usergroups found for this product');
        }
        return $assignedUsergroups;
    }

    /**
     * Gets the max price of a product
     * This is because a product has multiple stock with various prices so if the prices vary we can get the minimum
     * In the case the price is the same across stock we still get the price of the stock
     * @return type
     */
    public function getMaxPrice() {
        $allStock = $this->getStock();
        foreach ($allStock as $stock) {
            if (!isset($maxPrice)) {
                $maxPrice = $stock->getPrice();
            }
            $maxPrice = ($maxPrice > $stock->getPrice() ? $maxPrice : $stock->getPrice());
        }
        return $maxPrice;
    }

    /**
     * Gets the min price of a product
     * This is because a product has multiple stock with various prices so if the prices vary we can get the minimum
     * In the case the price is the same across stock we still get the price of the stock
     * @return type
     */
    public function getMinPrice($vat = null) {
        $allStock = $this->getStock();
        foreach ($allStock as $stock) {
            if (!isset($minPrice)) {
                $minPrice = $stock->getPrice();
            }
            $minPrice = ($minPrice > $stock->getPrice() ? $stock->getPrice() : $minPrice);

        }
        if (isset($vat) && $vat === '0') {
            $vatPrice = $minPrice * 1.2;
            return $vatPrice;
        } else {
            return $minPrice;
        }
    }

    /**
     * Gets all product images from the db mapper object
     * @return array
     * @throws Exception
     */
    public function getImages() {
        $rows = $this->getMapper()->fetchAllProductImages($this->getId());
        if (empty($rows)) {
            throw new Exception('No product images for this product');
        }
        return $rows;
    }

    /**
     * Gets the default image for the product
     * @return string
     */
    public function getDefaultImage()
    {
        $allImages = $this->getImages();
        $defaultImage = '';
        foreach ($allImages as $image)
        {
            if ($image->is_default == 'Y')
            {
                $defaultImage = $image;
            }
        }

        return $defaultImage;
    }

    /**
     * Checks if product has colourways
     * @return boolean
     */
    public function hasColourways() {
        $allStock = $this->getStock();
        $hasColourways = false;
        foreach ($allStock as $stock) {
            if ($stock->getColourwayId() !== NULL) {
                $hasColourways = true;
            }
        }
        return $hasColourways;
    }

    /**
     * Gets all product colourways and returns as colourway objects
     * @return array \Core_Colourway_Models_Colourway | boolean
     * @throws Exception
     */
    public function getColourways() {
        if ($this->hasColourways() === false) {
            throw new Exception('this product has no colourways');
        }
        $allStock = $this->getStock();
        foreach ($allStock as $stock) {
            $colourwayIds[] = $stock->getColourwayId();
        }
        $uniqueColourwayIds = array_unique($colourwayIds);
        foreach ($uniqueColourwayIds as $colourwayId) {
            $colourway = new Core_Colourway_Models_Colourway();
            $colourways[] = $colourway->findById($colourwayId);
        }
        return $colourways;
    }

    /**
     * Gets all images for a particular colourway of a product
     * @param int $colourwayId
     * @return array
     * @throws Exception
     */
    public function getColourwayImagesByColourwayId($colourwayId) {
        $colourwayImages = array();
        if ($this->hasColourways() === false) {
            throw new Exception('this product has no colourways');
        }
        $productImages = $this->getImages();
        foreach ($productImages as $image) {
            if ($image->colourway_id == $colourwayId) {
                $colourwayImages[] = $image;
            }
        }
        return $colourwayImages;
    }

    /**
     * Gets all options saved for this product stock
     * @return array
     */
    public function getOptions(){
        $options = array();
        $allStock = $this->getStock();
        foreach($allStock as $stock){
            $options[] = $stock->getOption();
        }
        return $options;
    }

    /*     * ****************************************************************************************
     *                          ADDING A PRODUCT TO THE DATABASE
     * *************************************************************************************** */

    /**
     * gets the zend product session when adding a product to the database
     * @return \Zend_Session_Namespace
     */
    public function getTempProductInfoSession() {
        $tempProductInfoSession = new Zend_Session_Namespace('temp_product_info');
        return $tempProductInfoSession;
    }

    public function getTempVpInfoSession() {
        $tempProductInfoSession = new Zend_Session_Namespace('vp');
        return $tempProductInfoSession;
    }

    /**
     * updates the product session when adding a product to the basket
     * @param array $data
     * @return boolean
     */
    public function updateTempProductInfoSession($data) {
        try {
            if (!empty($data)) {
                $this->getTempProductInfoSession()->array = $data;
                return true;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    /**
     * get temporary saved product info
     * @return array|boolean
     */
    public function getTempProductInfo() {
        try {
            $tempProductInfo = $this->getTempProductInfoSession()->array;
            return $tempProductInfo;
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    /**
     * save temp product info
     * @param array $data
     * @return boolean
     */
    public function saveTempProductInfo($data, $key = 'details') {
        try {
            $tempProductInfo = $this->getTempProductInfo();
            //var_dump($tempProductInfo); die();
            //check if data is from first page of adding a product
            if (!empty($data)) {
                $tempProductInfo[$key] = $data;
            }
            $this->updateTempProductInfoSession($tempProductInfo);
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    /**
     * clear temporary product info
     * @return boolean
     */
    public function clearTempProductInfoSession() {
        $this->getTempProductInfoSession()->unsetAll();
        return true;
    }


    /**
     * generate all possible stock from all variants chosen for product
     * @return array
     */
    public function generateStockFromVariants() {
        $colourwayModel = new Core_Colourway_Models_Colourway();
        $generatedStock = array();
        $tempProductInfo = $this->getTempProductInfo();
        //var_dump($tempProductInfo);
        $productDetails = $tempProductInfo['details'];
        $productOptions = $tempProductInfo['variants']['options'];
        $productColourways = $tempProductInfo['variants']['colourways'];
        $i = 0;
        if (!empty($productOptions) && !empty($productColourways)) {
            foreach ($productOptions as $optionId => $option) {
                foreach ($productColourways as $colourwayId => $colourway) {
                    $colourwayObject = $colourwayModel->findById($colourwayId);
                    $generatedStock[$i]['code'] = $productDetails['code'] . '_' . $option . '_' . $colourwayId;
                    $generatedStock[$i]['colourway_id'] = $colourwayId;
                    $generatedStock[$i]['colourway'] = $colourwayObject->getCode();
                    $generatedStock[$i]['option'] = $option;
                    $i++;
                }
            }
        }
        return $generatedStock;
    }

    /**
     * @param $data
     * @return bool
     */
    public function saveProduct($data) {
        try {

            $tempProductInfo = $this->getTempProductInfo();
            $generatedStock = $this->generateStockFromVariants();

            //save product details
            $productDetails = $tempProductInfo['details'];
            $productColourways = $tempProductInfo['variants']['colourways'];

            $languageModel      = new Core_Language_Models_Languages();
            $activatedLanguages = $languageModel->getActivatedLanguages();
            $nameContent        = new Core_Content_Models_Content();
            $descriptionContent = new Core_Content_Models_Content();
            $productImagesMapper = new Core_Product_Models_ProductImagesMapper();
            $stockMapper = new Core_Stock_Models_StockMapper();

            if(isset($productDetails['id'])){
                $product = $this->findById($productDetails['id']);
                if($product->getNameContent()){
                    $nameContent->setId($product->getNameContent()->content_id);
                }
                if($product->getDescriptionContent()){
                    $descriptionContent->setId($product->getDescriptionContent()->content_id);
                }
            }
            $name           = strtolower($productDetails['name_gb']);
            $search         = array(' ', "'");
            $replace        = array('_', '');
            $nameIdentifier = str_replace($search, $replace, $name);
            $nameContent->setIdentifier($nameIdentifier);
            foreach($activatedLanguages as $language){
                $shortcode  = $language->getIsoShortcode();
                $content    = $productDetails['name_' . $shortcode];
                if($content){
                    $nameContentId = $nameContent->setIsoShortcode($shortcode)
                        ->setContent($content)
                        ->saveContent();
                    if(!$nameContent->getId()){
                        $nameContent->setId($nameContentId);
                    }
                }
            }
            $description            = strtolower($productDetails['name_gb']) . '_desc';
            $descriptionIdentifier  = str_replace($search, $replace, $description);
            $descriptionContent->setIdentifier($descriptionIdentifier);

            foreach($activatedLanguages as $language){
                $shortcode  = $language->getIsoShortcode();
                $content    = $productDetails['description_' . $shortcode];
                if($content){
                    $descriptionContentId = $descriptionContent->setIsoShortcode($shortcode)
                        ->setContent($content)
                        ->saveContent();
                    if(!$descriptionContent->getId()){
                        $descriptionContent->setId($descriptionContentId);
                    }
                }
            }

            $options = array(
                'name' => $nameIdentifier,
                'description' => $descriptionIdentifier,
                'code' => $productDetails['code'],
                'weight' => $productDetails['weight'],
                'pack' => $productDetails['pack'],
                'live' => $productDetails['live'],
                'stockAction' => $productDetails['stock_action'],
                'active' => 1,
            );

            if(isset($productDetails['id'])){
                $options['id'] = (int) $productDetails['id'];
                $this->setOptions($options)->save();
                $productId = $this->getId();
            }
            else{
                $productId = $this->setOptions($options)->save();
            }


            //save vendor products
            $vpModel = new Core_Product_Models_VendorProductsLink();
            if(isset($productDetails['vendor_product_id']) && is_array($productDetails['vendor_product_id'])){
                $vpModel->setProductId($productId)
                    ->setVpIds($productDetails['vendor_product_id'])
                    ->save();
            }

            //save product categories
            $productCategoriesModel = new Core_Product_Models_ProductCategories();
            $productCategoriesModel->setProductId($productId)
                ->setCategoryIds($productDetails['categories'])
                ->save();

            //save product usergroups
            $productAvailabilityModel = new Core_Product_Models_ProductAvailability();
            $productAvailabilityModel->setProductId($productId)
                ->setUsergroupIds($productDetails['usergroups'])
                ->save();

            //save product stock
            foreach ($generatedStock as $key => $stock) {
                $stockModel = new Core_Stock_Models_Stock();
                $previousStockRecord = $stockModel->findByCode($stock['code']);
                if(null !== $previousStockRecord->getId()){
                    $previousStockRecord
                        ->setPrice($data['price'][$key])
                        ->setStockLevel($data['stocklevel'][$key])
                        ->setWarningLevel($data['stockwarning'][$key]);
                    $previousStockRecord->save();
                }
                else{
                    $stockModel->setProductId($productId)
                        ->setCode($stock['code'])
                        ->setOption($stock['option'])
                        ->setColourwayId($stock['colourway_id'])
                        ->setPrice($data['price'][$key])
                        ->setStockLevel($data['stocklevel'][$key])
                        ->setWarningLevel($data['stockwarning'][$key]);
                    $stockModel->save();
                }
            }

            //get current colourway images
            if(isset($productDetails['id'])){
                $productImages = $productImagesMapper->fetchUsingProductId($productDetails['id'], new Core_Product_Models_ProductImages());
                foreach($productImages as $image){
                    $currentImages[] = $image->getImageUrl();
                }
                $colourways = $product->getColourways();
                foreach ($colourways as $colourway){
                    $currentColourways[] = $colourway->getId();
                }
            }

            //save product colourway images
            $newImages = array();
            foreach ($productColourways as $colourwayId => $images) {
                $newColourways[] = $colourwayId;
                foreach ($images as $order => $imageUrl) {
                    $newImages[] = $imageUrl;
                    $productImagesModel = new Core_Product_Models_ProductImages();
                    $previousImageRecord = $productImagesModel->findByImageUrl($imageUrl);
                    if(null !== $previousImageRecord->getId()){
                        $previousImage = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/' . APP_PLATFORM . '/img/products/' . $previousImageRecord->getImageUrl();
                        unset( $previousImage );
                        $previousImageRecord->setImageUrl($imageUrl);
                        $previousImageRecord->save();
                    }
                    else{
                        $order++;
                        $isDefault = ($order == 1 ? 'Y' : 'N');
                        $productImagesModel->setProductId($productId)
                            ->setColourwayId($colourwayId)
                            ->setImageUrl($imageUrl)
                            ->setOrder($order)
                            ->setIsDefault($isDefault);
                        $productImagesModel->save();
                    }
                }
            }

            //delete removed images
            if($currentImages){
                foreach($currentImages as $currentImage){
                    if(!in_array($currentImage, $newImages)){
                        $productImagesMapper->deleteUsingImageUrl($currentImage);
                    }
                }
            }

            //delete removed stock
            if($currentColourways){
                foreach($currentColourways as $currentColourway){
                    if(!in_array($currentColourway, $newColourways)){
                        $stockMapper->deleteUsingProductId($productId, $currentColourway);
                    }
                }
            }
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function save(){
        return $this->getMapper()->save($this);
    }

    /*     * ****************************************************************************************
     *                          END OF ADDING A PRODUCT TO THE DATABASE
     * *************************************************************************************** */

    public function saveHighest($product) {
        $this->setHighest($product['highest']);
        $this->getMapper()->saveHighest($this);
        return $this;
    }

    public function saveFeatured($product) {
        $this->setFeatured($product['featured']);
        $this->getMapper()->saveFeatured($this);
        return $this;
    }

    public function saveNew($new) {
        $this->setNew($new['new']);
        $this->getMapper()->saveNew($this);
        return $this;
    }

    public function getHighestRated() {
        $highestRated = $this->getMapper()->getHighestRated();
        return $highestRated;
    }

    public function getFeaturedProduct() {
        $featured = $this->getMapper()->getFeaturedProduct();
        return $featured;
    }

    public function getNewProduct() {
        $new = $this->getMapper()->getNewProduct();
        return $new;
    }

    public function getMostPopularProducts($no = 4) {
        $mostPopular = $this->getMapper()->getMostPopularProducts($no);
        return $mostPopular;
    }

    public function getNameByLanguage(Core_Language_Models_Languages $language = null){
        if(!$language){
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language       = $languageMapper->fetchByIp();
        }
        $contentMapper  = new Core_Content_Models_ContentMapper();
        $content        = $contentMapper->fetchIndividual($language->getIsoShortcode(), $this->_name);
        if(!$content){
            return ;
        }
        return $content->getContent();
    }

    public function getDescriptionByLanguage(Core_Language_Models_Languages $language = null){
        if(!$language){
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language       = $languageMapper->fetchByIp();
        }
        $contentMapper  = new Core_Content_Models_ContentMapper();
        $content        = $contentMapper->fetchIndividual($language->getIsoShortcode(), $this->_description);
        if(!$content){
            return ;
        }
        return $content->getContent();
    }

    /**
     * Gets all different language translations for this category's name
     * @return \Core_Content_Models_ContentMapper rowset
     */
    public function getNameContent(){
        $contentMapper = new Core_Content_Models_ContentMapper();
        $content = $contentMapper->fetchByIdentifier($this->_name);
        return $content;
    }

    /**
     * Gets all different language translations for this category's description
     * @return \Core_Content_Models_ContentMapper rowset
     */
    public function getDescriptionContent(){
        $contentMapper = new Core_Content_Models_ContentMapper();
        $content = $contentMapper->fetchByIdentifier($this->_description);
        return $content;
    }

}

