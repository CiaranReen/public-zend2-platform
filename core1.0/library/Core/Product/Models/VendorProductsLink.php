<?php

class Core_Product_Models_VendorProductsLink extends App_Model_Abstract {

    protected $_vpIds = array();
    protected $_productId;
    protected $_mapper;

    public function setVpIds(array $ids) {
        $this->_vpIds = $ids;
        return $this;
    }

    public function getVpIds() {
        return $this->_vpIds;
    }

    public function getProductId() {
        return $this->_productId;
    }

    public function setProductId($productId) {
        $this->_productId = $productId;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Product_Models_VendorProductsLinkMapper());
        }
        return $this->_mapper;
    }

    public function save() {
        return $this->getMapper()->save($this);
    }
    
    public function getAllVendorProductsById($productId) {
        $products = $this->getMapper()->getAllVendorProductsById($productId);
        return $products;
    }

}