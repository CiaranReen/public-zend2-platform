<?php

class Core_Product_Models_ProductImages extends App_Model_Abstract
{
    protected $_id;
    protected $_productId;
    protected $_imageUrl;
    protected $_colourwayId;
    protected $_order;
    protected $_isDefault;
    
    protected $_mapper;
    
    public function setId($id){
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getId(){
        return $this->_id;
    }
    
    public function setProductId($productId)
    {
        $this->_productId = (int) $productId;
        return $this;
    }
 
    public function getProductId()
    {
        return $this->_productId;
    }
 
    public function setImageUrl($imageUrl){
        $this->_imageUrl = (string) $imageUrl;
        return $this;
    }
    
    public function getImageUrl(){
        return $this->_imageUrl;
    }
    
    public function setColourwayId($colourwayId){
        $this->_colourwayId = (int) $colourwayId;
        return $this;
    }
    
    public function getColourwayId(){
        return $this->_colourwayId;
    }
    
    public function setOrder($order){
        $this->_order = (int) $order;
        return $this;
    }
    
    public function getOrder(){
        return $this->_order;
    }
    
    public function setIsDefault($isDefault){
        $this->_isDefault = (string) $isDefault;
        return $this;
    }
    
    public function getIsDefault(){
        return $this->_isDefault;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Product_Models_ProductImagesMapper());
		}
		return $this->_mapper;
    }
    
    public function find($id){
        $this->getMapper()->find($id, $this);
        return $this;
    }
    
    public function findByImageUrl($imageUrl){
        $this->getMapper()->findByImageUrl($imageUrl, $this);
        return $this;
    }
    
    public function getAllProductImages($productId){
        $allProductImages = $this->getMapper()->fetchUsingProductId($productId, $this);
        return $allProductImages;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
    
    public function delete(){
        $this->getMapper()->delete($this);
    }
    
    public function deleteUsingImageUrl($image_url){
        return $this->getMapper()->deleteUsingImageUrl($image_url);
    }
}