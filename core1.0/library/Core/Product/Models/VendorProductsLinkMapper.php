<?php

class Core_Product_Models_VendorProductsLinkMapper extends App_Model_Abstract {

    protected $_dbTable;

    const TABLE = 'Core_Product_Models_DbTable_VendorProductsLink';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Product_Models_VendorProductsLink $vpModel) {
        $where = "product_id = '" . $vpModel->getProductId() . "'";
        $resultSet = $this->getDbTable()->delete($where);
        $ids = $vpModel->getVpIds();
        foreach ($ids as $id) {
            $data = array(
                'vendor_product_id' => $id,
                'product_id' => $vpModel->getProductId()
            );
            $resultSet = $this->getDbTable()->insert($data);
        }
        return $resultSet;
    }
    
    public function getAllVendorProductsById($productId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('vpl' => 'vp_products_link'), array('vpl.vendor_product_id', 'vpl.product_id', 'vp.description'))
                ->joinInner(array('vp' => 'vp_products'), 'vpl.vendor_product_id = vp.vendor_product_id')
                ->where('product_id =' . $productId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

}
