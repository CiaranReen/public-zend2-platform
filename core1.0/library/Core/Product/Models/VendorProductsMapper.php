<?php

class Core_Product_Models_VendorProductsMapper extends App_Model_Abstract {

    protected $_dbTable;

    const TABLE = 'Core_Product_Models_DbTable_VendorProducts';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function find($id) {
        $vendorProduct = new Core_Product_Models_VendorProducts;
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $vendorProduct->setId($row->vendor_product_id)
            ->setVendorPartNo($row->vendor_part_no)
            ->setDescription($row->description)
            ->setManufacturerId($row->manufacturer_id)
            ->setManufacturerPartNo($row->manufacturer_part_no)
            ->setCost($row->cost);
        return $vendorProduct;
    }

    public function fetchAll() {
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->from($this->_dbTable);
        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new Core_Product_Models_VendorProducts();
            $entry->findById($row->vendor_product_id);
            $entries[] = $entry;
        }
        return $entries;
    }

    public function save(Core_Product_Models_VendorProducts $vendorProductModel) {
        $data = array (
            'vendor_part_no'        => $vendorProductModel->getVendorPartNo(),
            'description'           => $vendorProductModel->getDescription(),
            'manufacturer_id'       => $vendorProductModel->getManufacturerId(),
            'manufacturer_part_no'  => $vendorProductModel->getManufacturerPartNo(),
            'cost'                  => $vendorProductModel->getCost()
        );

        if ($vendorProductModel->getId() === null) {
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('vendor_product_id = ?' => $vendorProductModel->getId()));
        }
    }

    public function getSearch($string) {
        $cache = Zend_Registry::get('Memcache');
        if (!$searchResults = $cache->load('vp')) {
            //Look up the $topStories
            $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('vp' => 'vp_products'), array('*'))
                ->where('description LIKE ?', '%' . $string . '%')
                ->orWhere('vendor_part_no LIKE ?', '%' . $string . '%')
                ->orWhere('manufacturer_part_no LIKE ?', '%' . $string . '%');
            $resultSet = $this->getDbTable()->fetchAll($stmt);
            return $resultSet;
        }
    }



    public function fetchAllVendorProducts() {
        //$cache = Zend_Registry::get('Memcache');
        //$key = 'allvp';
        //if (($result = $cache->load($key)) === false) {
        //Look up the $topStories
        $result = $this->getDbTable()->fetchAll();
        //$cache->save($result, 'allvp');
        return $result;
    }

    public function filterSearch($string) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vp' => 'vp_products'), array('*'))
            ->where('description LIKE ?', '%' . $string . '%')
            ->orWhere('vendor_part_no LIKE ?', '%' . $string . '%')
            ->orWhere('manufacturer_part_no LIKE ?', '%' . $string . '%');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function readDataFromCsv($file) {
        $fh = fopen($file, 'r'); // 1

        if ($fh) {
            while (($row = fgetcsv($fh) ) !== false) {
                //var_dump($row[0]); die();
                $data = array(
                    'vendor_part_no' => $row[0],
                    'description' => $row[1],
                    'manufacturer_id' => $row[2],
                    'manufacturer_part_no' => $row[3],
                    'cost' => $row[4]
                );
                $this->getDbTable()->insert($data);
            }
        }
        fclose($fh);
    }

    public function updateContent($content, $id) {
        $data = array(
            'description' => $content,
        );
        $where = 'vendor_product_id ='. $id;
        $this->getDbTable()->update($data, $where);
    }

    public function fetchAllManufacturers() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('vpm' => 'vp_manufacturers'), array ('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function delete($vendorProductId) {
        $where = "vendor_product_id = '" . $vendorProductId . "'";
        $delete = $this->getDbTable()->delete($where);
        return $delete;
    }

}
