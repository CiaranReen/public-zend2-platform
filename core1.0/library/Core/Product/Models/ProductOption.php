<?php

class Core_Product_Models_ProductOption extends App_Model_Abstract
{
    protected $_id;
    protected $_option;
    protected $_shortcode;
    
    protected $_mapper;
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setOption($option)
    {
        $this->_option = (string) $option;
        return $this;
    }
 
    public function getOption()
    {
        return $this->_option;
    }
    
    public function setShortcode($shortcode)
    {
        $this->_shortcode = (string) $shortcode;
        return $this;
    }
 
    public function getShortcode()
    {
        return $this->_shortcode;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Product_Models_ProductOptionMapper());
		}
		return $this->_mapper;
    }
    
    public function findById($orderId){
        $this->getMapper()->find($orderId, $this);
        return $this;
    }
    
    public function getAllOptions(){
        $allOptions = $this->getMapper()->fetchAll();
        return $allOptions;
    }
}