<?php

class Core_Product_Models_ProductImagesMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Product_Models_DbTable_ProductImages';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Product_Models_ProductImages $productImages)
    {
        $data = array(
            'product_id'    => $productImages->getProductId(),
            'image_url'     => $productImages->getImageUrl(),
            'colourway_id'  => $productImages->getColourwayId(),
            'order'         => $productImages->getOrder(),
            'is_default'    => $productImages->getIsDefault(),
        );
 
        if (null === ($id = $productImages->getId())) 
        {
            unset($data['id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }
 
    public function find($id, Core_Product_Models_ProductImages $productImage)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $productImage->setId($row->id)
                ->setProductId($row->product_id)
                ->setImageUrl($row->image_url)
                ->setColourwayId($row->colourway_id)
                ->setOrder($row->order)
                ->setIsDefault($row->is_default);
        return $productImage;
    }
    
    public function findByImageUrl($imageUrl, Core_Product_Models_ProductImages $productImage)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('image_url = ?', $imageUrl);
        $row = $this->getDbTable()->fetchRow($select);
        if (0 == count($row)) 
        {
            return;
        }
        $productImage->find($row->id);
        return $productImage;
    }
 
    public function fetchUsingProductId($productId)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $productImages = new Core_Product_Models_ProductImages();
            
            $productImages->find($row->id);
            
            $entries[] = $productImages;
        }
        
        return $entries;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $productImages = new Core_Product_Models_ProductImages();
            
            $productImages->findByProductId($row->product_id);
            
            $entries[] = $productImages;
        }
        
        return $entries;
    }
    
    public function delete(Core_Product_Models_ProductImages $productImage)
    {
        $imageFile = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/' . APP_PLATFORM . '/img/products/' . $productImage->getImageUrl();
        unlink($imageFile); //remove actual image file
        return $this->getDbTable()->delete(array('id = ?' => $productImage->getId())); //delete from db
    }
    
    public function deleteUsingImageUrl($imageUrl){
        $imageFile = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/' . APP_PLATFORM . '/img/products/' . $imageUrl;
        unlink($imageFile); //remove actual image file
        return $this->getDbTable()->delete(array('image_url = ?' => $imageUrl)); //delete from db
    }
}

