<?php

class Core_Product_Models_ProductCategoriesMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Product_Models_DbTable_ProductCategories';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Product_Models_ProductCategories $productCategories)
    {
        $this->getDbTable()->delete(array('product_id = ?' => $productCategories->getProductId()));
        foreach($productCategories->getCategoryIds() as $categoryId){
            $data = array( 
                'category_id'   => $categoryId,
                'product_id'    => $productCategories->getProductId()
            );

            $this->getDbTable()->insert($data);
        } 
    }
 
    public function find($productId, Core_Product_Models_ProductCategories $productCategories)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach($resultSet as $row){
            $categoryIds[] = $row->category_id;
        }
        $productCategories->setProductId($productId);
        $productCategories->setCategoryIds($categoryIds);
        return $productCategories;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $productCategories = new Core_Product_Models_ProductCategories();
            
            $productCategories->findByProductId($row->product_id);
            
            $entries[] = $productCategories;
        }
        
        return $entries;
    }
}

