<?php

class Core_Product_Models_ProductAvailability extends App_Model_Abstract
{
    protected $_productId;
    protected $_usergroupIds = array();
    
    protected $_mapper;
    
    public function setProductId($productId)
    {
        $this->_productId = (int) $productId;
        return $this;
    }
 
    public function getProductId()
    {
        return $this->_productId;
    }
 
    public function setUsergroupIds(array $usergroupIds)
    {
        $this->_usergroupIds = $usergroupIds;
        return $this;
    }
 
    public function getUsergroupIds()
    {
        return $this->_usergroupIds;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Product_Models_ProductAvailabilityMapper());
		}
		return $this->_mapper;
    }
    
    public function findByProductId($productId){
        $this->getMapper()->find($productId, $this);
        return $this;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
}