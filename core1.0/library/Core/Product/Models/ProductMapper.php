<?php

class Core_Product_Models_ProductMapper {

    protected $_dbTable;

    const TABLE = 'Core_Product_Models_DbTable_Product';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Product_Models_Product $product) {
        $data = array(
            'name' => $product->getName(),
            'description' => $product->getDescription(),
            'code' => $product->getCode(),
            'weight' => $product->getWeight(),
            'pack' => $product->getPack(),
            'live' => $product->getLive(),
            'stock_action' => $product->getStockAction(),
            'added' => new Zend_Db_Expr('NOW()'),
            'active' => $product->getActive(),
        );

        if (null === ($id = $product->getId())) {
            unset($data['product_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('product_id = ?' => $id));
        }
    }

    public function find($id, Core_Product_Models_Product $product) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $product->setId($row->product_id)
                ->setName($row->name)
                ->setDescription($row->description)
                ->setCode($row->code)
                ->setWeight($row->weight)
                ->setPack($row->pack)
                ->setLive($row->live)
                ->setStockAction($row->stock_action)
                ->setPackSizeWidth($row->pack_size_width)
                ->setPackSizeLength($row->pack_size_length)
                ->setPackSizeDepth($row->pack_size_depth)
                ->setActive($row->active);
        return $product;
    }

    public function fetchAll() {
        $active = 1; //should return all products that are active since inactive products are deleted
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->from($this->_dbTable)
            ->where('active = ?', $active);
        $resultSet = $this->getDbTable()->fetchAll($select);
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new Core_Product_Models_Product();
            $entry->findById($row->product_id);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * Gets all stock objects of a product using the product id from the 'stock' table in the database
     * @param int $productId
     * @return array /Core_Stock_Models_Stock
     */
    public function fetchAllProductStock($productId) {
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from($this->_dbTable)
                ->joinUsing('stock', 'product_id')
                ->where('products.product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        if (count($resultSet) > 0) {
            foreach ($resultSet as $row) {
                $entry = new Core_Stock_Models_Stock();
                $entries[] = $entry->findById($row->stock_id);
            }
            return $entries;
        } else {
            return false;
        }
    }

    /**
     * Gets all product images for the product from the 'product_images' table in the database
     * @param type $productId
     * @return type
     */
    public function fetchAllProductImages($productId) {
        $resultSet = array();
        if (is_int($productId)) {
            $select = $this->getDbTable()
                    ->select()->setIntegrityCheck(false)
                    ->from($this->_dbTable, array())
                    ->joinUsing('product_images', 'product_id')
                    ->where('products.product_id = ?', $productId)
                    ->order(array('product_images.order ASC', 'product_images.colourway_id'));
            $resultSet = $this->getDbTable()->fetchAll($select);
        }
        return $resultSet;
    }

    /**
     * Gets all product category objets for the product from the 'product_categories' table in the database
     * @param type $productId
     * @return type
     */
    public function fetchAllProductCategories($productId) {
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from($this->_dbTable)
                ->joinUsing('product_categories', 'product_id')
                ->where('products.product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach ($resultSet as $row) {
            $entry = new Core_Category_Models_Category();
            $entries[] = $entry->findById($row->category_id);
        }
        return $entries;
    }

    /**
     * Gets all assigned usergroup objets for the product from the 'product_availability' table in the database
     * @param type $productId
     * @return type
     */
    public function fetchAllAssignedUsergroups($productId) {
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from($this->_dbTable)
                ->joinUsing('product_availability', 'product_id')
                ->where('products.product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach ($resultSet as $row) {
            $entry = new Core_Usergroup_Models_Usergroup();
            $entries[] = $entry->findById($row->user_group_id);
        }
        return $entries;
    }

    /*     * ******************************************************************************** */
    /*     * ******** METHODS FOR SETTING HIGHEST RATED, FEATURED, AND NEW PRODUCTS ********* */
    /*     * ******************************************************************************** */

    public function resetHighest() {
        $data = array(
            'highest' => '0',
        );
        $where = '1=1';
        $this->getDbTable()->update($data, $where);
    }

    public function resetFeatured() {
        $data = array(
            'featured' => '0',
        );
        $where = '1=1';
        $this->getDbTable()->update($data, $where);
    }

    public function resetNew() {
        $data = array(
            'new' => '0',
        );
        $where = '1=1';
        $this->getDbTable()->update($data, $where);
    }

    public function saveHighest(Core_Product_Models_Product $product) {
        $this->resetHighest();
        $data = array(
            'highest' => '1',
        );
        $where = 'product_id = ' . $product->getHighest();
        return $this->getDbTable()->update($data, $where);
    }

    public function saveFeatured(Core_Product_Models_Product $product) {
        $this->resetFeatured();
        $data = array(
            'featured' => '1',
        );
        $where = 'product_id = ' . $product->getFeatured();
        return $this->getDbTable()->update($data, $where);
    }

    public function saveNew(Core_Product_Models_Product $product) {
        $this->resetNew();
        $data = array(
            'new' => '1',
        );
        $where = 'product_id = ' . $product->getNew();
        return $this->getDbTable()->update($data, $where);
    }

    public function getHighestRated() {
        $highest = '1';
        $isDefault = 'Y';
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from(array('p' => 'products'), array('*'))
                ->joinInner(array('pi' => 'product_images'), 'p.product_id = pi.product_id')
                ->where('highest = ?', $highest)
                ->where('is_default = ?', $isDefault)
                ->where('p.active = ?', '1');
        $resultSet = $this->getDbTable()->fetchRow($select);
        return $resultSet;
    }

    public function getFeaturedProduct() {
        $featured = '1';
        $isDefault = 'Y';
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from(array('p' => 'products'), array('*'))
                ->joinInner(array('pi' => 'product_images'), 'p.product_id = pi.product_id')
                ->where('featured = ?', $featured)
                ->where('is_default = ?', $isDefault)
                ->where('p.active = ?', '1');
        $resultSet = $this->getDbTable()->fetchRow($select);
        return $resultSet;
    }

    public function getNewProduct() {
        $new = '1';
        $isDefault = 'Y';
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from(array('p' => 'products'), array('*'))
                ->joinInner(array('pi' => 'product_images'), 'p.product_id = pi.product_id')
                ->where('new = ?', $new)
                ->where('is_default = ?', $isDefault)
                ->where('p.active = ?', '1');
        $resultSet = $this->getDbTable()->fetchRow($select);
        return $resultSet;
    }

    /**************** END HIGHEST RATED, FEATURED, AND NEW METHODS *****************/

    public function getMostPopularProducts($no) {
        $isDefault = 'Y';
        $stmt = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from(array('op' => 'order_parts'), array('p.name', 'op.stock_id', 'p.product_id', 'count(p.product_id) as count'))
                ->joinInner(array('s' => 'stock'), 'op.stock_id = s.stock_id')
                ->joinInner(array('p' => 'products'), 's.product_id = p.product_id')
                ->joinInner(array('pi' => 'product_images'), 's.product_id = pi.product_id')
                ->where('pi.is_default = ?', $isDefault)
                ->where('p.active = ?', '1')
                ->group('p.product_id')
                ->order('count DESC')
                ->limit($no);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $language = $languageMapper->fetchByIp();
        foreach($resultSet as $key=>$row){
            if($language instanceof Core_Language_Models_Languages){
                if($language->getContent($row->name)){
                    $resultSet[$key]->name = $language->getContent($row->name)->getContent();
                }
            }
        }
        return $resultSet;
    }

}

