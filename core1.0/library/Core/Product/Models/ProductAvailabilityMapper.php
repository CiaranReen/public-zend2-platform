<?php

class Core_Product_Models_ProductAvailabilityMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Product_Models_DbTable_ProductAvailability';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Product_Models_ProductAvailability $productAvailability)
    {
        $this->getDbTable()->delete(array('product_id = ?' => $productAvailability->getProductId()));
        foreach($productAvailability->getUsergroupIds() as $usergroupId){
            $data = array( 
                'product_id'    => $productAvailability->getProductId(),
                'user_group_id' => $usergroupId
            );
            $this->getDbTable()->insert($data); 
        }
    }
 
    public function find($productId, Core_Product_Models_ProductAvailability $productAvailability)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('product_id = ?', $productId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach($resultSet as $row){
            $usergroupIds[] = $row->user_group_id;
        }
        $productAvailability->setProductId($productId);
        $productAvailability->setUsergroupIds($usergroupIds);
        return $productAvailability;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $productAvailability = new Core_Product_Models_ProductAvailability();
            
            $productAvailability->findByProductId($row->product_id);
            
            $entries[] = $productAvailability;
        }
        
        return $entries;
    }
}

