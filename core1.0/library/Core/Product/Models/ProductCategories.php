<?php

class Core_Product_Models_ProductCategories extends App_Model_Abstract
{
    protected $_productId;
    protected $_categoryIds = array();
    
    protected $_mapper;
    
    public function setProductId($productId)
    {
        $this->_productId = (int) $productId;
        return $this;
    }
 
    public function getProductId()
    {
        return $this->_productId;
    }
 
    public function setCategoryIds(array $categoryIds)
    {
        $this->_categoryIds = $categoryIds;
        return $this;
    }
 
    public function getCategoryIds()
    {
        return $this->_categoryIds;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Product_Models_ProductCategoriesMapper());
		}
		return $this->_mapper;
    }
    
    public function findByProductId($productId){
        $this->getMapper()->find($productId, $this);
        return $this;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
}