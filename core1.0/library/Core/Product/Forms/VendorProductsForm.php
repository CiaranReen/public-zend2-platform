<?php

class Core_Product_Forms_VendorProductsForm extends Zend_Form {

    public function init() {
        $manufacturersMapper = new Core_Product_Models_VendorProductManufacturersMapper();
        $manufacturers = $manufacturersMapper->fetchAll();

        $this->addElement('text', 'vendor_part_no', array(
            'label'      => 'Vendor Part No.',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));


        $this->addElement('text', 'description', array(
            'label'      => 'Description',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        $manufacturer = $this->createElement('select', 'manufacturer', array(
            'label'      => 'Manufacturer',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));
        foreach ($manufacturers as $m) {
            $manufacturer->addMultiOptions(array($m->id => $m->name));
        }
        $this->addElement($manufacturer);

        $this->addElement('text', 'manufacturer_part_no', array(
            'label'      => 'Manufacturer Part No.',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        $this->addElement('text', 'cost', array(
            'label'      => 'Cost',
            'required'   => true,
            'filters'    => array('StringTrim'),
        ));

        // Submit Button
        $submit = $this->createElement('submit', 'submit')
            ->setOptions(
                array(
                    'label' => 'Save and Return',
                    'class' => 'btn btn-primary',
                )
            );

        $this->addElement($submit);
    }

    public function edit($vendorProductsObject) {
        $this->init();
        $this->setDescription('Edit Vendor Product Details');

        $this->addElement('hidden', 'vendor_product_id');

        $this->getElement('vendor_product_id')->setValue($vendorProductsObject->id);
        $this->getElement('vendor_part_no')->setValue($vendorProductsObject->vendorPartNo);
        $this->getElement('description')->setValue($vendorProductsObject->description);
        $this->getElement('manufacturer')->setValue($vendorProductsObject->manufacturerId);
        $this->getElement('manufacturer_part_no')->setValue($vendorProductsObject->manufacturerPartNo);
        $this->getElement('cost')->setValue($vendorProductsObject->cost);
    }


}