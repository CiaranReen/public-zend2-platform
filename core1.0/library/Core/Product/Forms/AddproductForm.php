<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 8/11/2013
 * Time: 10:00
 * To change this template use File | Settings | File Templates.
 */
class Core_Product_Forms_AddproductForm extends Zend_Form {

    public function init() {
        $categoryModel  = new Core_Category_Models_Category();
        $userGroupModel = new Core_Usergroup_Models_Usergroup();
        $languageModel  = new Core_Language_Models_Languages();
    
        // Set properties of the form
        $this->setName('product')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', '')
                ->setDescription('Add New Product')
                ->setOptions(array('class' => ''));

        $vpName = $this->createElement('text', 'vendor_product_id')
                ->setOptions(
                array(
                    'label' => 'Vendor Products',
                    'required' => true,
                    'id' => 'search',
                )
        );
        
        $hiddenVp = $this->createElement('hidden', 'hidden_vp')
                ->setOptions(
                array(
                    'label' => 'Selected Vendor Products',
                    'class' => 'hidden-vp',
                )
        );

        //This is the field that $vpName selection will be put into
//        $selected = $this->createElement('text', 'selected')
//                ->setOptions(
//                array(
//                    'required' => true,
//                    'class' => 'required',
//                )
//        );

        // Languages
        $languages = $this->createElement('select', 'languages');

        $languages->setOptions(
                array(
                    'label'     => 'Select Language',
                    'required'  => true,
                    'filters'   => array('StringTrim', 'StripTags')
                )
            );
        if($languageModel->getActivatedLanguages()){
            $languages->addMultiOptions($languageModel->getPairs($languageModel->getActivatedLanguages()));
        }else{
            $languages->addMultiOptions($languageModel->getPairs());
        }
        $this->addElement($languages);

        foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
            $languageId     = $activatedLanguage->getId();
            $language       = $activatedLanguage->getName();
            $isoShortcode   = $activatedLanguage->getIsoShortcode();
            if($isoShortcode === 'gb'){
                $required = true;
            }else{
                $required = false;
            }
            $this->addElement(
                $this->createElement('text', 'name_'.$isoShortcode)
                        ->setOptions(
                        array(
                                 'label'    => 'Product Name [ '.$language.' ]',
                                 'class'    => 'required lang-name',
                                 'required' => $required,
                                 'id'       => 'name-' . $languageId,
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                )
            );
            $this->addElement(
                $this->createElement('textarea', 'description_'.$isoShortcode)
                        ->setOptions(
                        array(
                                 'label'    => 'Product Description [ '.$language.' ]',
                                 'class'    => 'required field span3 lang-description',
                                 'id'       => 'description-' . $languageId,
                                 'rows'  => 4,
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                )
            );
        }

        // Categories
        $categories = $this->createElement('multiCheckbox', 'categories[]');
        if ($categoryModel->getPairs()) {
            $categories->setOptions(
                    array(
                        'label' => 'Categories',
                        'label_class' => 'checkbox inline',
                        'required' => true,
                        'class' => 'checkbox inline',
                        'filters' => array('StringTrim', 'StripTags')
                    )
            );
            $categories->addMultiOptions($categoryModel->getPairs($categoryModel->getAllChildCategories()));
        }

        // Usergroups
        $usergroups = $this->createElement('multiCheckbox', 'usergroups[]');
        if ($userGroupModel->getPairs()) {
            $usergroups->setOptions(
                    array(
                        'label' => 'Product Availability',
                        'label_class' => 'checkbox inline',
                        'required' => true,
                        'class' => 'checkbox inline',
                        'filters' => array('StringTrim', 'StripTags')
                    )
            );
            $usergroups->addMultiOptions($userGroupModel->getPairs());
        }
        // product options
        $productPrice = $this->createElement('text', 'product_price')
                ->setOptions(
                array(
                    'label' => 'Product Price',
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // weight in grams
        $weight = $this->createElement('text', 'weight')
                ->setOptions(
                array(
                    'label' => 'Weight (in grams)',
                    'class' => 'required input-small',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        // packsize
        $packSize = $this->createElement('text', 'pack')
                ->setOptions(
                array(
                    'label' => 'Pack size (if this product is not a pack leave empty)',
                    'class' => 'required input-small',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        //product out of stock
        $stockAction = $this->createElement('radio', 'stock_action');
        $stockAction->setOptions(array(
            'label' => 'If the product is out of stock',
            'label_class' => 'radio inline',
            'required' => true,
            'class' => 'required input-small',
            'filters' => array('StringTrim', 'StripTags'),
        ));
        $stockAction->setMultiOptions(array('hide' => 'Hide the product', 'show' => 'Show the product with an out of stock message'));
        // product code
        $productCode = $this->createElement('text', 'code')
                ->setOptions(
                array(
                    'label' => 'Product Code',
                    'required' => true,
                    'class' => 'required input',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        //set product live
        $live = $this->createElement('radio', 'live');
        $live->setOptions(array(
            'label' => 'Would you like to set this product live straight away:',
            'label_class' => 'radio inline',
            'required' => true,
            'class' => 'required input-small',
            'filters' => array('StringTrim', 'StripTags'),
        ));
        $live->setMultiOptions(array('N' => 'No', 'Y' => 'Yes'));
        // Submit Button
        $submit = $this->createElement('submit', 'submit')
                ->setOptions(
                array(
                    'label' => 'Next',
                    'class' => 'btn btn-primary',
                )
        );

        // Add controls to the form
        $this->addElement($productCode)
                ->addElement($categories)
                ->addElement($usergroups)
                ->addElement($productPrice)
                ->addElement($weight)
                ->addElement($packSize)
                ->addElement($stockAction)
                ->addElement($live)
                ->addElement($vpName)
                ->addElement($hiddenVp);

        $this->addElement($submit);
    }

    public function populateUsingModel(Core_Product_Models_Product $product) {
        $this->init();
        $this->setDescription('Edit "' . $product->getNameByLanguage() . '" Details');
        
        $languageModel      = new Core_Language_Models_Languages();
        foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
            $isoShortcode   = $activatedLanguage->getIsoShortcode();
            if($product->getNameContent()){
                $this->getElement('name_'.$isoShortcode)->setValue($product->getNameByLanguage($activatedLanguage));
            }
            if($product->getDescriptionContent()){
                $this->getElement('description_'.$isoShortcode)->clearFilters()->setValue($product->getDescriptionByLanguage($activatedLanguage));
            }
        }
        
        $this->getElement('code')->setValue($product->getCode());
        $productCategories = $product->getCategories();
        if($productCategories){
            foreach ($productCategories as $productCategory) {
                $categoryIds[] = $productCategory->getId();
            }
            $this->getElement('categories')->setValue($categoryIds);
        }
        $assignedUsergroups = $product->getAssignedUsergroups();
        if($assignedUsergroups){
            foreach ($assignedUsergroups as $assignedUsergroup) {
                $usergroupIds[] = $assignedUsergroup->getUsergroupId();
            }
            $this->getElement('usergroups')->setValue($usergroupIds);
        }
        $this->getElement('product_price')->setValue($product->getMinPrice());
        $this->getElement('weight')->setValue($product->getWeight());
        $this->getElement('pack')->setValue($product->getPack());
        $this->getElement('stock_action')->setValue($product->getStockAction());
        $this->getElement('code')->setValue($product->getCode());
        $this->getElement('live')->setValue($product->getLive());
    }

}