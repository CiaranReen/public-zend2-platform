<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 8/11/2013
 * Time: 10:00
 * To change this template use File | Settings | File Templates.
 */

class Core_Product_Forms_Addvendorproducts extends Zend_Form {

	public function init() {
            
        }
        
        public function setUpAddvendorproducts($vpList) {

        // Set properties of the form
        $this->setName('product')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'well form-inline')
                ->setDescription('Add New Vendor Products');


        // Name
        $name = $this->createElement('multiselect', 'name')
                        ->setOptions(
                        array(
                                 'label'    => 'Vendor Product Name',
                                 'required' => true,
                                 'class' => 'chosen-select',
                        )
                );
        foreach ($vpList as $vp) {
            $name->addMultiOption($vp->vendor_product_id, $vp->description);
        }
        
        $selected = $this->createElement('text', 'selected')
                        ->setOptions(
                        array(
                                 'required' => true,
                                 'class'    => 'required',
                        )
                );
        // Submit Button
        $submit = $this->createElement('submit', 'submit')
                ->setOptions(
                        array(
                            'label' => 'Next',
                            'class' => 'btn',
                        )
                );

        // Add controls to the form
        $this   ->addElement($name)
                ->addElement($selected)
                ->addElement($submit);
	}
}