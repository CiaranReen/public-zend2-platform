<?php

class Core_Product_Forms_EditManufacturers extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupManufacturersForm($v) {

        // Set properties of the form
        $this->setName('setManufacturers'.$v->name)
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Set Manufacturers');

        $id = $this->createElement('hidden', $v->name . $v->manufacturer_id)->setValue($v->manufacturer_id);

        // manufacturer Field
        $manufacturer = $this->createElement('text', $v->name)
                ->setOptions(
                array(
                    'required' => true,
                    'class' => 'required',
                )
        )->setValue($v->name);

        $submit = $this->createElement('submit', $v->manufacturer_id)->setOptions(array('label' => 'Change', 'class' => 'btn btn-primary'));

        $this->addElement($id)
            ->addElement($manufacturer)
            ->addElement($submit);
        return $this;
    }

}