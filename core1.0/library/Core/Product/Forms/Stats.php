<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 01/07/2013
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
class Core_Product_Forms_Stats extends Zend_Form {

    public function init() {
        /* Form Elements & Other Definitions Here ... */
    }

    public function setUpStatForm($productList) {
        // Set properties of the form
        $this->setName('setStats')
                ->setMethod('post')
                ->setAttrib('class', 'form-signin')
                ->setDescription('Set Stats');

        // Name Field
        $highest = $this->createElement('select', 'highest')
                ->setLabel('Highest Rated: ')
                ->setRequired(true);

        foreach ($productList as $product) {
            $highest->addMultiOption($product->getId(), $product->getNameByLanguage());
        }

        $featured = $this->createElement('select', 'featured')
                ->setLabel('Featured Product: ')
                ->setRequired(true);

        foreach ($productList as $product) {
            $featured->addMultiOption($product->getId(), $product->getNameByLanguage());
        }
        
        $new = $this->createElement('select', 'new')
                ->setLabel('New Product: ')
                ->setRequired(true);

        foreach ($productList as $product) {
            $new->addMultiOption($product->getId(), $product->getNameByLanguage());
        }

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Set', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($highest)
                ->addElement($featured)
                ->addElement($new)
                ->addElement($submit);
    }

}