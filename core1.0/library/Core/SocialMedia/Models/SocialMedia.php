<?php

class Core_SocialMedia_Models_SocialMedia extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_link;
    protected $_active;
    
    protected $_mapper;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param $link
     * @return $this
     */
    public function setLink($link) {
        $this->_link = (string) $link;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLink() {
        return $this->_link;
    }

    /**
     * @param $active
     * @return $this
     */
    public function setActive($active) {
        $this->_active = $active;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getActive() {
        return $this->_active;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_SocialMedia_Models_SocialMediaMapper());
        }
        return $this->_mapper;
    }

    /***** Start of methods ******/

    /**
     * @param $dId
     * @return $this
     */
    public function findById($dId) {
        $this->getMapper()->find($dId, $this);
        return $this;
    }

    /**
     * @return mixed
     */
    public function save() {
        $save = $this->getMapper()->save($this);
        return $save;
    }

}

