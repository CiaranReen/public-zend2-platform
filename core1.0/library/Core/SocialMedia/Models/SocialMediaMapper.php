<?php

class Core_SocialMedia_Models_SocialMediaMapper {

    protected $_dbTable;
    const TABLE = 'Core_SocialMedia_Models_DbTable_SocialMedia';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    /**
     * @param $smId
     * @param Core_SocialMedia_Models_SocialMedia $smModel
     * @return bool|Core_SocialMedia_Models_SocialMedia
     */
    public function find($smId, Core_SocialMedia_Models_SocialMedia $smModel) {
        $result = $this->getDbTable()->find($smId);
        if (0 == count($result)) {
            return false;
        }
        $row = $result->current();
        $smModel->setId($row->id)
            ->setName($row->name)
            ->setLink($row->link)
            ->setActive($row->active);
        return $smModel;
    }

    /**
     * @param Core_SocialMedia_Models_SocialMedia $sm
     * @return mixed
     */
    public function save(Core_SocialMedia_Models_SocialMedia $sm) {
        $data = array(
            'link'          => $sm->getLink(),
            'active'        => $sm->getActive()
        );

        return $this->getDbTable()->update($data, array('id = ?' => $sm->getId()));
    }

    /**
     * @return mixed
     */
    public function getActiveSocialMedia() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sm' => 'social_media'), array ('*'))
            ->where('active = 1');

        $resultSet = $this->getDbTable()->fetchAll($stmt);

        return $resultSet;
    }

    /**
     * @return mixed
     */
    public function getAvailableSocialMedia() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sm' => 'social_media'), array ('*'))
            ->where('active = 0');

        $resultSet = $this->getDbTable()->fetchAll($stmt);

        return $resultSet;
    }

    public function saveActivePost($activeId) {
        $data = array (
            'active' => '1',
        );

        if (is_array($activeId)) {
            foreach ($activeId as $id) {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        }
    }

    public function saveAvailablePost($availableId) {
        $data = array (
            'active' => '0',
        );

        if (is_array($availableId)) {
            foreach ($availableId as $id) {
                $this->getDbTable()->update($data, array('id = ?' => $id));
            }
        }
    }

}

