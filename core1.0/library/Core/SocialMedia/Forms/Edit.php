<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class Core_SocialMedia_Forms_Edit extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEditForm($sm) {

        // Set properties of the form
        $this->setName('editSocialMedia')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Edit Social Media');

        // Surname Field
        $link = $this->createElement('text', 'link')
            ->setOptions(
                array(
                    'label'    => 'Link',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($sm->getLink());

        // Email address Field
        $active = $this->createElement('radio', 'active')
            ->setOptions(
                array(
                    'label'    => 'Active?',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setMultiOptions(array('1' => 'Yes', '0' => 'No'))
            ->setValue($sm->getActive());

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($link)
            ->addElement($active)
            ->addElement($submit);

    }

}