<?php

class Core_User_Models_MailingListMapper {

    protected $_dbTable;

    const TABLE = 'Core_User_Models_DbTable_MailingList';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
    
    public function sendMail() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('m' =>'mailing_list'), array ('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }
    
    public function save($userId) {
        $data = array(
            'user_id' => $userId,
        );
        $resultSet = $this->getDbTable()->insert($data);
        return $resultSet;
    }

}

