<?php

class Core_User_Models_UserMapper {

    protected $_dbTable;

    const TABLE = 'Core_User_Models_DbTable_User';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_User_Models_User $user) {
        $data = array(
            'user_type_id' => $user->getUserTypeId(),
            'email' => $user->getEmail(),
            'username' => $user->getUsername(),
            'password' => $user->getPassword(),
            'phone' => $user->getPhone(),
            'billing_address_id' => $user->getBillingAddressId(),
            'delivery_address_id' => $user->getDeliveryAddressId(),
            'created_at' => $user->getCreatedAt(),
            'last_login' => $user->getLastLog(),
            'forename' => $user->getForename(),
            'surname' => $user->getSurname(),
            'user_group_id' => $user->getUserGroupId(),
            'company_id' => $user->getCompanyId(),
            'currency_id' => $user->getCurrencyId(),
            'active' => $user->getActive(),
            'gym_id' => $user->getGym()
        );

        if (null === ($id = $user->getId())) {
            unset($data['user_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('user_id = ?' => $id));
        }
    }

    public function registerUser($userData) {
        $userModel = new Core_User_Models_User();
        $addressModel = new Core_Address_Models_Address();
        $lastInsertId = $addressModel->lastInsertId();
        $passwordModel = new Core_Password_Models_Password();
        $userModel->setEmail($userData['email'])
            ->setUsername($userData['username'])
            ->setPassword($passwordModel->encrypt($userData['password']))
            ->setPhone($userData['phone'])
            ->setBillingAddressId($lastInsertId)
            ->setDeliveryAddressId($lastInsertId)
            ->setCreatedAt(new Zend_Db_Expr('NOW()'))
            ->setLastLog(new Zend_Db_Expr('NOW()'))
            ->setForename($userData['forename'])
            ->setSurname($userData['surname'])
            ->setUserTypeId('1')
            ->setUserGroupId('1')
            ->setCurrencyId('1')
            ->setCompanyId('1')
            ->setActive('1')
            ->setGym($userData['gym']);
        $this->save($userModel);
        return $this;
    }

    public function find($id, Core_User_Models_User $user)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return false;
        }
        $row = $result->current();
        $user->setId($row->user_id)
                ->setUserTypeId($row->user_type_id)
                ->setEmail($row->email)
                ->setUsername($row->username)
                ->setPassword($row->password)
                ->setBillingAddressId($row->billing_address_id)
                ->setDeliveryAddressId($row->delivery_address_id)
                ->setCreatedAt($row->created_at)
                ->setLastLog($row->last_login)
                ->setPhone($row->phone)
                ->setForgotPasswordKey($row->forgot_password_key)
                ->setForename($row->forename)
                ->setSurname($row->surname)
                ->setUserGroupId($row->user_group_id)
                ->setCompanyId($row->company_id)
                ->setCurrencyId($row->currency_id)
                ->setActive($row->active);
        return $user;
    }

    public function fetchUsers() {
        $resultSet = $this->getDbTable()->fetchAll();
        return $resultSet;
    }

    public function fetchAll() {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('user_id', 'forename', 'surname', 'username', 'email', 'active'))
                ->joinInner(array('ut' => 'user_type'), 'u.user_type_id = ut.user_type_id')
                ->joinInner(array('ug' => 'user_groups'), 'u.user_group_id = ug.user_group_id');
        $resultSet = $this->getDbTable()->fetchAll($select);
        return $resultSet;
    }

    public function delete($userId) {
        $where = "user_id = '" . $userId . "'";
        $resultSet = $this->getDbTable()->delete($where);
        return $resultSet;
    }
    
    public function getHash($username) {
        $getHash = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('password'))
                ->where('u.username = ?', $username);
        $result = $this->getDbTable()->fetchRow($getHash);
        
        if($result) {
            $hash = $result->password;
        }else{
            return ;
        }
        return $hash;
    }

    public function saveNewPassword($userId, Core_User_Models_User $user) {
        $passwordModel = new Core_Password_Models_Password();
        $password = $passwordModel->encrypt($user->getPassword());
        $data = array (
            'password' => $password
        );
        $where = "user_id = '" . $userId . "'";
        $update = $this->getDbTable()->update($data, $where);
        return $update;
    }

    public function searchForUser($query) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('u' => 'user'), array('*'))
            ->where('u.email = ?', $query);
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        if (count($resultSet) > 0) {
            return $resultSet;
        } else {
            return null;
        }
    }

    public function saveCurrency($currency, $userId) {
        $data = array (
            'currency_id' => $currency,
        );
        $where = "user_id = '" . $userId . "'";
        $save = $this->getDbTable()->update($data, $where);
        if ($save === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getAllUserTypes() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('ut' => 'user_type'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function getCompanies() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('c' => 'company'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }


    public function getUserById($userId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('u' => 'user'), array('*'))
            ->where('u.user_id = ?', $userId);
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        if (count($resultSet) > 0) {
            return $resultSet;
        } else {
            return null;
        }
    }
   
    /**
     * Gets a user object using their username and password
     * 
     * @param string $username
     * @param string $password
     * @param Core_User_Models_User $user
     * @return \Core_User_Models_User
     */
    public function findByAuthDetails($username, $password, Core_User_Models_User $user) {
        $passwordModel = new Core_Password_Models_Password();
        $select = $this->getDbTable()
                ->select()->setIntegrityCheck(false)
                ->from($this->_dbTable)
                ->where('user.username = ?', $username);
        $row = $this->getDbTable()->fetchRow($select);
        if (0 == count($row)) {
            return ;
        }
        $verified = $passwordModel->verify($password, $row->password);
        if(!$verified){
            return ;
        }
        $user->findById($row->user_id);
        return $user;
    }

    /**
     * @param $email
     * @param Core_User_Models_User $user
     * @return Core_User_Models_User
     */
    public function findByEmail($email, Core_User_Models_User $user) {
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->from($this->_dbTable)
            ->where('user.email = ?', $email);
        $row = $this->getDbTable()->fetchRow($select);
        if (0 == count($row)) {
            return ;
        }
        $user->findById($row->user_id);
        return $user;
    }

    /**
     * Updates a users address id after an address has been saved to the address table
     * The last address insert id is passed as the address id and the type is passed so it knows what to save **/
    /**
     * @param $type
     * @param $id
     * @param Core_User_Models_User $user
     * @return mixed
     */
    public function saveAddressId($type, $id, Core_User_Models_User $user) {
        $data = array(
            $type . '_address_id' => (int) $id,
        );
        return $this->getDbTable()->update($data, array('user_id = ?' => $user->getId()));
    }

    public function getAllCountries() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('c' => 'countries'), array('c.countryid', 'c.country'))
                ->order('c.country ASC');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function getAllGyms() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('g' => 'gym'), array('*'))
            ->order('g.name ASC');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    /*     * ******************************************************************** */
    /*     * ******************USER RELATED REPORTS ***************************** */
    /*     * ******************************************************************** */

    public function getLastLogin() {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('forename', 'surname', 'last_login'));
        $row = $this->getDbTable()->fetchAll($select);
        return $row;
    }

    public function getRegistration() {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('forename', 'surname', 'created_at'));
        $row = $this->getDbTable()->fetchAll($select);
        return $row;
    }



}

