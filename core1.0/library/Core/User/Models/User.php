<?php

class Core_User_Models_User extends App_Model_Abstract {

    protected $_id;
    protected $_userTypeId;
    protected $_email;
    protected $_username;
    protected $_password;
    protected $_phone;
    protected $_billingAddressId;
    protected $_deliveryAddressId;
    protected $_createdAt;
    protected $_lastLogin;
    protected $_forgotPasswordKey;
    protected $_forename;
    protected $_surname;
    protected $_userGroupId;
    protected $_companyId;
    protected $_active;
    protected $_gym;
    protected $_mapper;
    protected $_currencyId;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function setUserTypeId($userTypeId) {
        $this->_userTypeId = (int) $userTypeId;
        return $this;
    }

    public function getUserTypeId() {
        return $this->_userTypeId;
    }

    public function setEmail($email) {
        $this->_email = (string) $email;
        return $this;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function setUsername($username) {
        $this->_username = (string) $username;
        return $this;
    }

    public function getUsername() {
        return $this->_username;
    }

    public function setPassword($password) {
        $this->_password = (string) $password;
        return $this;
    }

    public function getPassword() {
        return $this->_password;
    }

    public function getPhone() {
        return $this->_phone;
    }

    public function setPhone($phone) {
        $this->_phone = $phone;
        return $this;
    }

    public function setBillingAddressId($billingAddressId) {
        $this->_billingAddressId = (int) $billingAddressId;
        return $this;
    }

    public function getBillingAddressId() {
        return $this->_billingAddressId;
    }

    public function setDeliveryAddressId($deliveryAddressId) {
        $this->_deliveryAddressId = (int) $deliveryAddressId;
        return $this;
    }

    public function getDeliveryAddressId() {
        return $this->_deliveryAddressId;
    }

    public function setCreatedAt($createdAt) {
        $this->_createdAt = $createdAt;
        return $this;
    }

    public function getCreatedAt() {
        return $this->_createdAt;
    }

    public function setLastLog($lastLogin) {
        $this->_lastLogin = (string) $lastLogin;
        return $this;
    }

    public function getLastLog() {
        return $this->_lastLogin;
    }

    public function setForgotPasswordKey($forgotPasswordKey) {
        $this->_forgotPasswordKey = (string) $forgotPasswordKey;
        return $this;
    }

    public function getForgotPasswordKey() {
        return $this->_forgotPasswordKey;
    }

    public function setForename($forename) {
        $this->_forename = (string) $forename;
        return $this;
    }

    public function getForename() {
        return $this->_forename;
    }

    public function setSurname($surname) {
        $this->_surname = (string) $surname;
        return $this;
    }

    public function getSurname() {
        return $this->_surname;
    }

    public function setUserGroupId($usergroupId) {
        $this->_userGroupId = (int) $usergroupId;
        return $this;
    }

    public function getUserGroupId() {
        return $this->_userGroupId;
    }

    public function setCompanyId($companyId) {
        $this->_companyId = (int) $companyId;
        return $this;
    }

    public function getCompanyId() {
        return $this->_companyId;
    }

    public function setCurrencyId($currencyId) {
        $this->_currencyId = (int) $currencyId;
        return $this;
    }

    public function getCurrencyId() {
        return $this->_currencyId;
    }

    public function setLastLogin($lastLogin) {
        $this->_lastLogin = $lastLogin;
    }

    public function setActive($active){
        $this->_active = (int) $active;
        return $this;
    }

    public function getActive(){
        return $this->_active;
    }

    /**
     * @param $gym
     * @return $this
     */
    public function setGym($gym) {
        $this->_gym = $gym;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGym() {
        return $this->_gym;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_User_Models_UserMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs user object using user id
     * @param int $userId
     * @return \Core_User_Models_User
     */
    public function findById($userId) {
        $this->getMapper()->find($userId, $this);
        return $this;
    }

    /**
     * Gets all users in database
     * @return array \Core_User_Models_User
     */
    public function getAllUsers() {
        $allUser = $this->getMapper()->fetchAll();
        return $allUser;
    }

    public function fetchUsers() {
        $allUser = $this->getMapper()->fetchUsers();
        return $allUser;
    }

    /**
     * Gets a user session
     *
     * @return array|bool
     */
    public function getUserSession() {
        $userSession = new Zend_Session_Namespace('user');
        return $userSession;
    }
    
    public function lastInsertId() {
        return $this->getMapper()->getDbTable()->getAdapter()->lastInsertId();
    }

    /**
     * Gets a user session
     *
     * @return bool
     */
    public function setUserSession($userId = NULL) {
        if ($userId === NULL) {
            $userId = $this->getId();
        }
        $this->getUserSession()->userId = $this->getId();
        $this->getUserSession()->isLoggedIn = true;
        return true;
    }

    public function clearUserSession() {
        $this->getUserSession()->unsetAll();
        return true;
    }

    /**
     * Detect if a user is logged in
     *
     * @return bool
     */
    public function isLoggedIn() {
        if ($this->getUserSession()->userId && $this->getUserSession()->isLoggedIn === true) {
            return true;
        } else {
            return false;
        }
    }
    
    public function isLoggedInAsAdmin(){
        $isAdmin = false;
        if ($this->getUserSession()->userId && $this->getUserSession()->isLoggedIn === true) {
            $user = $this->findById($this->getUserSession()->userId);
            if($user instanceof Core_User_Models_User && $user->getUserTypeId() === 2){
                $isAdmin = true;
            }
        }
        return $isAdmin;
    }

    /**
     * Login a user
     *
     * @param $username
     * @param $password
     *
     * @return array|bool
     */
    public function processLogin($username, $password) {
        $this->getMapper()->findByAuthDetails($username, $password, $this);
        if ($this->getId() && $this->getActive() !== 0) {
            $this->setUserSession($this);
        }
        return $this;
    }

    /**
     * Logout the current user
     *
     */
    public function processLogout() {
        // Wipe all session data
        $this->clearUserSession();
        $order = new Core_Order_Models_Order();
        $basket = new Core_Basket_Models_Basket();
        $order->clearSession();
        $basket->emptyBasket();
        return true;
    }
    
    /**
     * Logout admin
     * @return boolean
     */
    public function processAdminLogout() {
        // Wipe all session data
        $this->clearUserSession();
        return true;
    }

    /**
     * Gets the users billing address as an object of the Address model
     * @return \Core_Address_Models_Address
     */
    public function getBillingAddress() {
        $addressModel = new Core_Address_Models_Address();
        if ($this->getBillingAddressId()) {
            $address = $addressModel->findById($this->getBillingAddressId());
            if(!$address->isActive()){
                return ;
            }
            return $address;
        } else {
            return;
        }
    }

    /**
     * Gets the users delivery address as an object of the Address model
     * @return \Core_Address_Models_Address
     */
    public function getDeliveryAddress() {
        $addressModel = new Core_Address_Models_Address();
        if ($this->getDeliveryAddressId()) {
            $address = $addressModel->findById($this->getDeliveryAddressId());
            if(!$address->isActive()){
                return ;
            }
            return $address;

        } else {
            return;
        }
    }


    /*
     * Save The User
     */
    public function save() {
        return $this->getMapper()->save($this);
    }

    public function saveNewPassword($userId) {
        return $this->getMapper()->saveNewPassword($userId, $this);
    }

    public function delete($userId) {
        $this->getMapper()->delete($userId);
        return $this;
    }

    /**
     * Save Address Id and set the internal address id in the user model
     * @param string $type
     * @param int $id
     * @return boolean
     */
    public function updateAddressId($type, $id) {
        $this->getMapper()->saveAddressId($type, $id, $this); //updates the address id of the user
        $setFunctionName = 'set' . ucfirst($type) . 'AddressId'; //save the address function name using the type e.g. setBillingAddressId()
        $this->$setFunctionName($id); //sets the address id using the functionname generated from the address type
        return true;
    }
    
    //For Reports
    public function getLastLogin() {
        $lastLogin = $this->getMapper()->getLastLogin();
        return $lastLogin;
    }

    public function getRegistration() {
        $registration = $this->getMapper()->getRegistration();
        return $registration;
    }

    public function getOrders() {
        $orderModel = new Core_Order_Models_Order();
        return $orderModel->getOrdersByUserId($this->getId());
    }
    
    public function resetPassword($currentPassword, $newPassword){
        $passwordModel = new Core_Password_Models_Password();
        $verifiedPassword = $passwordModel->verify($currentPassword, $this->getPassword());
        if($verifiedPassword === true) {
            $this->setPassword($passwordModel->encrypt($newPassword));
            return $this->save();
        }
        else {
            return false;
        }
    }

    public function getColumnNamesLastLogin() {
        $columns = $this->getMapper()->getColumnNamesLastLogin();
        return $columns;
    }

    public function getAllUserTypes() {
        $userType = $this->getMapper()->getAllUserTypes();
        return $userType;
    }

    public function getCompanies() {
        $company = $this->getMapper()->getCompanies();
        return $company;
    }

    public function savePassword() {
        $newPassword = $this->getMapper()->savePassword($this);
        return $newPassword;
    }


    /************************************************************
                        ALL STATIC FUNCTIONS
    ************************************************************/
    //Static function to get current logged in user
    public static function getCurrent() {
        $userModel = new Core_User_Models_User();
        if (!$userModel->isLoggedIn()) {
            return false;
        }
        $user = $userModel->findById($userModel->getUserSession()->userId);
        return $user;
    }

}

