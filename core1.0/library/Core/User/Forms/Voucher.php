<?php

class Core_User_Forms_Voucher extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupVoucherForm($voucherList) {

        // Set properties of the form
        $this->setName('assignVoucher')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Assign Voucher');

        // Forename Field
        $voucher = $this->createElement('select', 'voucher')
                ->setOptions(
                array(
                    'required' => true,
                    'class' => 'required',
                )
        );
        foreach ($voucherList as $vl) {
            $voucher->addMultiOption($vl->id, $vl->voucher_name);
        }

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Assign', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($voucher)
                ->addElement($submit);
    }

}