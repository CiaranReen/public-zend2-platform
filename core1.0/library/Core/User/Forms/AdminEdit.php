<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class Core_User_Forms_AdminEdit extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEditForm($user) {

        // Set properties of the form
        $this->setName('editUser')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Edit user');

        $this->addElement('hidden', 'return', array(
            'value' => Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),
        ));

        // Username Field
        $username = $this->createElement('text', 'username')
            ->setOptions(
                array(
                    'label'    => 'Username',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($user->getUsername());

        // Phone Field
        $password = $this->createElement('password', 'password')
            ->setOptions(
                array(
                    'label'    => 'Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($username)
            ->addElement($password)
            ->addElement($submit);

    }

}