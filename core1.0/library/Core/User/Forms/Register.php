<?php

class Core_User_Forms_Register extends Zend_Form
{

    public function init()
    {

    }

    public function setupRegForm($countryList, $gymList)
    {

        // Set properties of the form
        $this->setName('RegisterUser')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Register user');

        // Forename Field
        $forename = $this->createElement('text', 'forename')
            ->setOptions(
                array(
                    'label' => 'Forename*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Alnum');

        // Surname Field
        $surname = $this->createElement('text', 'surname')
            ->setOptions(
                array(
                    'label' => 'Surname*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $username = $this->createElement('text', 'username')
            ->setOptions(
                array(
                    'label' => 'Username*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->addValidator(new Zend_Validate_Db_NoRecordExists(
                    array(
                        'field' => 'username',
                        'table' => 'user'
                    ))
            );

        // Address Field
        $company = $this->createElement('text', 'company')
            ->setOptions(
                array(
                    'label' => 'Company',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $address = $this->createElement('text', 'address')
            ->setOptions(
                array(
                    'label' => 'Address*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $address2 = $this->createElement('text', 'address2')
            ->setOptions(
                array(
                    'label' => 'Address (line 2)',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );
        $town = $this->createElement('text', 'town')
            ->setOptions(
                array(
                    'label' => 'Town/City*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $region = $this->createElement('text', 'region')
            ->setOptions(
                array(
                    'label' => 'Region*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $postcode = $this->createElement('text', 'postcode')
            ->setOptions(
                array(
                    'label' => 'Postcode*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $country = $this->createElement('select', 'country')
            ->setOptions(
                array(
                    'label' => 'Country*',
                    'required' => true,
                    'class' => 'required',
                )
            );
        foreach ($countryList as $cl) {
            $country->addMultiOption($cl->countryid, $cl->country);
        }
        $country->setValue(1);

        $vat = $this->createElement('text', 'vat')
            ->setOptions(
                array(
                    'label' => 'Vat Number',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $phone = $this->createElement('text', 'phone')
            ->setOptions(
                array(
                    'label' => 'Phone Number',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $email = $this->createElement('text', 'email')
            ->setOptions(
                array(
                    'label' => 'Email Address*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->addValidator('EmailAddress', true)
            ->addValidator(new Zend_Validate_Db_NoRecordExists(
                    array(
                        'field' => 'email',
                        'table' => 'user'
                    ))
            );

        $confirmEmail = $this->createElement('text', 'confirmemail')
            ->setOptions(
                array(
                    'label' => 'Confirm Email Address*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Identical', false, array('token' => 'email'))
            ->addErrorMessage('Your email addresses do not match.');

        $password = $this->createElement('password', 'password')
            ->setOptions(
                array(
                    'label' => 'Password*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            );

        $confirmPswd = $this->createElement('password', 'confirm_pswd')
            ->setOptions(
                array(
                    'label' => 'Confirm Password*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Identical', false, array('token' => 'password'))
            ->addErrorMessage('Your passwords do not match.');

        $gym = $this->createElement('select', 'gym')
            ->setOptions(
                array(
                    'label' => 'Gym*',
                    'required' => false,
                    'class' => 'required',
                )
            );
        $gym->addMultiOption('', 'I\'m currently not signed up to a FightScience Gym');
        foreach ($gymList as $gl) {
            $gym->addMultiOption($gl->id, $gl->name);
        }

        $terms = $this->createElement('checkbox', 'terms')
            ->setOptions(
                array(
                    'label' => 'I accept the <a href="/terms" target="_blank">terms and conditions.</a>',
                    'required' => true,
                    'class' => 'required',
                    'uncheckedValue' => null,
                )
            )->addErrorMessage('You must accept the terms and conditions');

        $emailoffers = $this->createElement('checkbox', 'emailoffers')
            ->setOptions(
                array(
                    'label' => 'If you would prefer not to receive information and special offers via email please tick this box',
                )
            );

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Register', 'class' => 'btn btn-primary', 'escape' => false));


        // Add controls to the form
        $this->addElement($forename)
            ->addElement($surname)
            ->addElement($username)
            ->addElement($company)
            ->addElement($address)
            ->addElement($address2)
            ->addElement($town)
            ->addElement($region)
            ->addElement($postcode)
            ->addElement($country)
            ->addElement($vat)
            ->addElement($phone)
            ->addElement($email)
            ->addElement($confirmEmail)
            ->addElement($password)
            ->addElement($confirmPswd)
            ->addElement($gym)
            ->addElement($terms)
            ->addElement($emailoffers)
            ->addElement($submit);
    }

}