<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */
class Core_User_Forms_Add extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param User_Abstract_User $user
     */
    public function setupAddForm($countryList, $usergroupList) {
        /* @var $user User_Abstract_User */

        // Set properties of the form
        $this->setName('editUser')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Edit user');

        // Forename Field
        $forename = $this->createElement('text', 'forename')
                ->setOptions(
                array(
                    'label' => 'Forename',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Surname Field
        $surname = $this->createElement('text', 'surname')
                ->setOptions(
                array(
                    'label' => 'Surname',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Email address Field
        $emailAddress = $this->createElement('text', 'email')
                ->setOptions(
                array(
                    'label' => 'Email address',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Username Field
        $username = $this->createElement('text', 'username')
                ->setOptions(
                array(
                    'label' => 'Username',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        
        $address = $this->createElement('text', 'address')
                ->setOptions(
                array(
                    'label' => 'Address*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        $address2 = $this->createElement('text', 'address2')
                ->setOptions(
                array(
                    'label' => 'Address (line 2)',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        $town = $this->createElement('text', 'town')
                ->setOptions(
                array(
                    'label' => 'Town/City*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        $region = $this->createElement('text', 'region')
                ->setOptions(
                array(
                    'label' => 'Region*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        $postcode = $this->createElement('text', 'postcode')
                ->setOptions(
                array(
                    'label' => 'Postcode*',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        
        // Username Field
        $password = $this->createElement('password', 'password')
                ->setOptions(
                array(
                    'label' => 'Password',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );
        
        // Username Field
        $country = $this->createElement('select', 'country')
                ->setOptions(
                array(
                    'label' => 'Country',
                    'required' => true,
                    'class' => 'required',
                )
        );
        foreach ($countryList as $c) {
            $country->addMultiOption($c->countryid, $c->country);
        }
        
        // Username Field
        $usergroup = $this->createElement('select', 'usergroup')
                ->setOptions(
                array(
                    'label' => 'Usergroup',
                    'required' => true,
                    'class' => 'required',
                )
        );
        foreach ($usergroupList as $ug) {
            $usergroup->addMultiOption($ug->user_group_id, $ug->user_group_name);
        }
        
        $userType = $this->createElement('radio', 'user_type_id')
                ->setOptions(
                array(
                    'label' => 'User Type',
                    'required' => true,
                    'class' => 'required',
                )
        )->addMultiOptions(array('1' => 'Admin', '2' => 'Customer'));

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($forename)
                ->addElement($surname)
                ->addElement($emailAddress)
                ->addElement($username)
                ->addElement($address)
                ->addElement($address2)
                ->addElement($town)
                ->addElement($region)
                ->addElement($postcode)
                ->addElement($password)
                ->addElement($country)
                ->addElement($usergroup)
                ->addElement($userType)
                ->addElement($submit);
    }

}