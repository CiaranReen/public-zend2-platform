<?php
/**
 * Delete a user
 */

class Core_User_Forms_Delete extends Zend_Form {

	public function init() {

	}

	public function setupDeleteForm($user) {

		// Set properties of the form
		$this->setName('deleteUser')
		->setMethod(self::METHOD_POST)
		->setAttrib('class', 'form-signin')
		->setDescription('Delete user');

		// Forename Field
		$forename = $this->createElement('text', 'forename')
					->setOptions(
							array(
								 'label'    => 'Forename',
								 'disabled' => true,
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						)
					->setValue($user->getForename());

		// Surname Field
		$surname = $this->createElement('text', 'surname')
				   ->setOptions(
						   array(
								'label'    => 'Surname',
								'disabled' => true,
								'filters'  => array('StringTrim', 'StripTags'),
						   )
					   )->setValue($user->getSurname());

		// Email address Field
		$emailAddress = $this->createElement('text', 'email')
						->setOptions(
								array(
									 'label'    => 'Email address',
									 'disabled' => true,
									 'filters'  => array('StringTrim', 'StripTags'),
								)
							)->setValue($user->getEmail());

		// Username Field
		$username = $this->createElement('text', 'username')
					->setOptions(
							array(
								 'label'    => 'Username',
								 'disabled' => true,
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						)->setValue($user->getUsername());

		// Submit Button
		$submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Delete user', 'class' => 'btn btn-primary'));

		// Add controls to the form
		$this->addElement($forename)
		->addElement($surname)
		->addElement($emailAddress)
		->addElement($username)
		->addElement($submit);

	}

}