<?php
/* Created by JetBrains PhpStorm.
* User: Simon
* Date: 27/06/2013
* Time: 15:34
* To change this template use File | Settings | File Templates.
*/

class Core_User_Forms_ResetPassword extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupResetForm() {

        // Set properties of the form
        $this->setName('editUser')
            ->setMethod(self::METHOD_POST)
            ->setDescription('Edit user');

        $oldPassword = $this->createElement('password', 'old_password')
            ->setOptions(
                array(
                    'label'    => 'Old Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        $password = $this->createElement('password', 'new_password')
            ->setOptions(
                array(
                    'label'    => 'New Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        $pwdconf = $this->createElement('password', 'pwdconf')
            ->setOptions(
                array(
                    'label'    => 'Re-type Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Identical', false, array('token' => 'new_password'))
            ->addErrorMessage('Your passwords do not match.');

        // Submit Button
        $submit = $this->createElement('submit', 'submitresetpassword')->setOptions(array('label' => 'Reset', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($oldPassword)
            ->addElement($password)
            ->addElement($pwdconf)
            ->addElement($submit);

    }
}