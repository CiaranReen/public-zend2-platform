<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class Core_User_Forms_Edit extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEditForm($user, $currencyList, $userTypeList, $userGroupList, $companyList) {

        // Set properties of the form
        $this->setName('editUser')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Edit user');

        // Forename Field
        $forename = $this->createElement('text', 'forename')
            ->setOptions(
                array(
                    'label'    => 'Forename',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )
            ->setValue($user->getForename());

        // Surname Field
        $surname = $this->createElement('text', 'surname')
            ->setOptions(
                array(
                    'label'    => 'Surname',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($user->getSurname());

        // Email address Field
        $emailAddress = $this->createElement('text', 'email')
            ->setOptions(
                array(
                    'label'    => 'Email address',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($user->getEmail());

        // Username Field
        $username = $this->createElement('text', 'username')
            ->setOptions(
                array(
                    'label'    => 'Username',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($user->getUsername());

        // Phone Field
        $phone = $this->createElement('text', 'phone')
            ->setOptions(
                array(
                    'label'    => 'Phone',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($user->getPhone());

        // Currency Field
        $currency = $this->createElement('select', 'currency')
            ->setOptions(
                array(
                    'label'    => 'Currency',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
        foreach ($currencyList as $c) {
            $currency->addMultiOption($c->currency_id, $c->currency);
        }
        $currency->setValue($user->getCurrencyId());

        // Usertype Field
        $userType = $this->createElement('select', 'usertype')
            ->setOptions(
                array(
                    'label'    => 'User Type',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
            foreach ($userTypeList as $ut) {
                $userType->addMultiOption($ut->user_type_id, $ut->user_type_name);
            }
        $userType->setValue($user->getUserTypeId());

        // Usergroup Field
        $userGroup = $this->createElement('select', 'usergroup')
            ->setOptions(
                array(
                    'label'    => 'Usergroup',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
        foreach ($userGroupList as $ug) {
            $userGroup->addMultiOption($ug->user_group_id, $ug->user_group_name);
        }
        $userGroup->setValue($user->getUserGroupId());

        // Company Field
        $company = $this->createElement('select', 'company')
            ->setOptions(
                array(
                    'label'    => 'Company',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
        foreach ($companyList as $cl) {
            $company->addMultiOption($cl->company_id, $cl->name);
        }
        $company->setValue($user->getCompanyId());

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($forename)
            ->addElement($surname)
            ->addElement($emailAddress)
            ->addElement($username)
            ->addElement($phone)
            ->addElement($currency)
            ->addElement($userType)
            ->addElement($userGroup)
            ->addElement($company)
            ->addElement($submit);

    }

}