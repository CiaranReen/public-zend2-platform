<?php
 /* Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class Core_User_Forms_AdminResetPassword extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEditForm() {

        // Set properties of the form
        $this->setName('editUser')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Edit user');

        // Forename Field
        $password = $this->createElement('password', 'password')
            ->setOptions(
                array(
                    'label'    => 'New Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Surname Field
        $pwdconf = $this->createElement('password', 'pwdconf')
            ->setOptions(
                array(
                    'label'    => 'Re-type Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Identical', false, array('token' => 'password'))
            ->addErrorMessage('Your passwords do not match.');

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save changes', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($password)
            ->addElement($pwdconf)
            ->addElement($submit);

    }
}