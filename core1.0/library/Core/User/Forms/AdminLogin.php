<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 17/10/2013
 * Time: 17:10
 * To change this template use File | Settings | File Templates.
 */

class Core_User_Forms_AdminLogin extends Zend_Form
{
    
    public function init()
    {
		// Set properties of the form
		$this->setName('contactUs')
			->setMethod(self::METHOD_POST)
			->setAttrib('class', 'form-signin')
			->setDescription('Admin Login');

		// User name Field
		$username = $this->createElement('text', 'username')
						->setOptions(
							array(
								 'label'    => 'Username',
								 'required' => true,
								 'class'    => 'required',
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						);
		// Password Field
		$password = $this->createElement('password', 'password')
						->setOptions(
							array(
								 'label'    => 'Password',
								 'required' => true,
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						);

		// Submit Button
		$submit = $this->createElement('submit', 'submit')
				  ->setOptions(
					array(
						 'label' => 'Login',
						 'class' => 'login-form-wrapper-btn',
					)
				);

		// Add controls to the form
		$this->addElement($username)
			->addElement($password)
			->addElement($submit);
    }

}