<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 24/03/14
 * Time: 10:28
 */

require_once '/var/www/co3-lynx/core/library/Core/BaseDbTest.php';

class Core_User_Tests_UserMapperTest extends BaseDbTest {

    /**
     * @var Core_User_Models_UserMapper
     */
    protected $_mapper;


    public function testTrue()
    {
        $this->assertTrue(true);
    }

    public function testDbTableReturnsCorrectTable()
    {
        $dbTable = $this->_mapper->getDbTable();
        $this->assertInstanceOf('Core_User_Models_DbTable_User', $dbTable);
        return $dbTable;
    }


}
 