<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 27/06/2013
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class Admin_Forms_Login_LoginForm extends Zend_Form {

	public function init() {

		// Set properties of the form
		$this->setName('contactUs')
			->setMethod(self::METHOD_POST)
			->setAttrib('class', 'form-signin')
			->setDescription('Login to the Admin Panel');

		// User name Field
		$username = $this->createElement('text', 'username')
						->setOptions(
							array(
								 'label'    => 'Name',
								 'required' => true,
								 'class'    => 'required',
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						);
		// Password Field
		$password = $this->createElement('password', 'password')
						->setOptions(
							array(
								 'label'    => 'Password',
								 'required' => true,
								 'filters'  => array('StringTrim', 'StripTags'),
							)
						);

		// Submit Button
		$submit = $this->createElement('submit', 'submit')
				  ->setOptions(
					array(
						 'label' => 'Login',
						 'class' => 'btn btn-primary'
					)
				);

		// Add controls to the form
		$this->addElement($username)
			->addElement($password)
			->addElement($submit);

	}

}