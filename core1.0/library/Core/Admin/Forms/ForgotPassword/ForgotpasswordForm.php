<?php
/**
* Class Admin_Forms_Login_LoginForm
*/

class Admin_Forms_Forgotpassword_ForgotpasswordForm extends Zend_Form {

	public function init() {

		// Set properties of the form
		$this->setName('forgotPassword')
			->setMethod(self::METHOD_POST)
			->setAttrib('class', 'form-signin');

		return $this;
	}

	public function forgotPasswordForm() {
		$this->init();

		// User name Field
		$username = $this->createElement('text', 'username')
					->setOptions(
				array(
					 'label'    => 'Username',
					 'required' => true,
					 'class'    => 'required',
					 'filters'  => array('StringTrim', 'StripTags'),
				)
			);

		// Submit Button
		$submit = $this->createElement('submit', 'submit')
				  ->setOptions(
				array(
					 'label' => 'Reset password',
					 'class' => 'btn btn-primary'
				)
			);

		// Add controls to the form
		$this->addElement($username)
		->addElement($submit)
		->setDescription('Reset your password');

		return $this;
	}

	public function resetPasswordForm() {
		$this->init();

		// Password Field
		$password = $this->createElement('password', 'password')
					->setOptions(
				array(
					 'label'    => 'Enter your new password: ',
					 'required' => true,
					 'filters'  => array('StringTrim', 'StripTags'),
				)
			);

		// Re-enter Password Field
		$reenterPassword = $this->createElement('password', 'repeatpassword')
					->setOptions(
				array(
					 'label'    => 'Re-enter your new password: ',
					 'required' => true,
					 'filters'  => array('StringTrim', 'StripTags'),
				)
			);


		// Submit Button
		$submit = $this->createElement('submit', 'submit')
				  ->setOptions(
				array(
					 'label' => 'Reset password',
					 'class' => 'btn btn-primary'
				)
			);

		// Add controls to the form
		$this->addElement($password)
		->addElement($reenterPassword)
		->addElement($submit)
		->setDescription('Choose a new password');

		return $this;
	}

}