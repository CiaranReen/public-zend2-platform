<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Simon
 * Date: 26/06/2013
 * Time: 12:28
 * To change this template use File | Settings | File Templates.
 */

class Admin_Controllers_Index_IndexController extends Admin_Controllers_Abstract_AbstractController {

	/**
	 * Dispatch the controller
	 * This will cause a page to be generated and output will be sent to the browser
	 *
	 * @param array $params
	 *
	 * @return $this
	 */
	public function index($params) {
		$this->preDispatch();
		
		/* @var $page Admin_Views_View */
		$page = new Admin_Views_View('Index');

		// Set template variables
		$page->setTitle('Admin Panel - CO3 Core');

		// Get enabled modules for nav bar
		$page->setContent('enabled_modules', $this->getEnabledModules());

		// Render template
		$page->renderPage();

		return $this;
	}

	/**
	 * Anything which needs to be done before the page is loaded should be done here
	 *
	 * @return $this
	 */
	protected function preDispatch() {
		parent::isLoggedIn();

		return $this;
	}

}