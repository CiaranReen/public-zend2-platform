<?php
/**
 * @package CO3-Core Platform
 *
 * Logs out an admin user
 */

class Admin_Controllers_Logout_LogoutController extends Admin_Controllers_Abstract_AbstractController {

	/**
	 * Dispatch the controller
	 * This will cause a page to be generated and output will be sent to the browser
	 *
	 * @param array $params
	 *
	 * @return $this|void
	 */
	public function index($params) {
		$this->preDispatch();

		$session = new Session_Session();
		$session->processLogout($session->getData('user_id'));

		// Redirect user to login page
		header('Location: /admin/login');

	}

}