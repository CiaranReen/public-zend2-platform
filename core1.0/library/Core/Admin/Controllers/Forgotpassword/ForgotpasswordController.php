<?php
/**
 * @package CO3-Core Platform
 *
 * Displays and processes the Admin forgot password form
 */

class Admin_Controllers_Forgotpassword_ForgotpasswordController extends Admin_Controllers_Abstract_AbstractController {

	/**
	 * Dispatch the controller
	 * This will cause a page to be generated and output will be sent to the browser
	 *
	 * @param array $params
	 *
	 * @return $this
	 */
	public function index($params) {
		$this->preDispatch();
		
		$view = new Zend_View();
		$form = new Admin_Forms_Forgotpassword_ForgotpasswordForm();
		$form = $form->forgotPasswordForm();

		// Pass to the view
		/* @var $page Frontend_Views_View */
		$page = new Admin_Views_View('Forgotpassword');

		// Set template variables
		$page->setTitle('Reset your password &middot; CO3 Core Platform');

		$page->setContent('form_title', $form->getDescription());

		if(!$_POST) {
			// Render the blank form
			$page->setContent('form', $form->render($view));

		} else if(!$form->isValid($_POST)) {

			$page->setContent('form_status', 'There were errors');
			// Renders form with error messages
			$page->setContent('form', $form->render($view));

		} else {

			// Passed input validation.

			// Check if this user exists
			$username = $form->getValue('username');

			$userModel = new User_Models_User_UserModel();
			$user = $userModel->getUserByUsername($username);

			if($user !== false) {
				// User exists
				// Send email with forgot password key
				Zend_Loader::loadClass('Zend_Mail');
				// Prepare email
				$subject = "Password reset request";
				$message = "To reset your password, please click on the following link, or copy and paste it into your browser.\n";
				$message .= "http://" . $_SERVER['HTTP_HOST'] . "/admin/forgotpassword/resetpassword/id/" . $user['user_id'] . "/key/" . $user['forgot_password_key'] . "/ \n";
				$message .= "If you did not request for your password to be reset, you can ignore this email.\n";
				$mail = new Zend_Mail();
				$mail->addTo($user['email']);
//				$mail->addTo('elliott@creativeemporium.co.uk');
				$mail->setSubject($subject);
				$mail->setBodyText($message, 'utf-8');
				$mail->setFrom('co3@creativeemporium.co.uk', 'CO3 @ Creative Emporium');

				// Send it!
				$sent = false;
				try {
					$mail->send();
					$sent = true;
					$page->setContent('form_status', 'An email has been sent to the email address associated with username');
				} catch (Exception $e){
					// Mail failed to send
					$page->setContent('form_status', 'Sorry, an error occurred which meant your reset password email could not be sent');
				}

				// Used to apply correct Bootstrap CSS class to status message
				$page->setContent('sent', $sent);

			} else {
				// Fail

				$page->setContent('form_status', 'Sorry, your username was not recognised');
				$page->setContent('form', $form->render($view));
			}

		}

		// Render template
		$page->renderPage();

		return $this;
	}

	/**
	 * Display and process the reset password form
	 *
	 * @param array $params Array containing the User ID (index: id) and forgot password key (index: key)
	 *
	 * @return $this
	 */
	public function resetpassword($params) {
		$this->preDispatch();

		/* @var $page Admin_Views_View */
		$page = new Admin_Views_View('Resetpassword');

		// Set template variables
		$page->setTitle('Choose a new password &middot; CO3 Core Platform');

		// Make sure the forgot_password_key from the request URI matches the one in the DB for this User

		/* @var $user User_Abstract_User */
		$userFactory = new User_Factory();
		$user = $userFactory->loadUserClassForId($params['id']);

		if($user->getForgotPasswordKey() == $params['key']) {
			// This is a valid password reset request

			// Display the choose a new password form
			$view = new Zend_View();
			$form = new Admin_Forms_Forgotpassword_ForgotpasswordForm();
			$resetPasswordForm = $form->resetPasswordForm();

			$page->setContent('form_title', $resetPasswordForm->getDescription());

			if(!$_POST) {
				// Render the blank form
				$page->setContent('form', $resetPasswordForm->render($view));

			} else if(!$resetPasswordForm->isValid($_POST)) {

				// Failed input validation
				$page->setContent('form_status', 'There were errors');
				// Renders form with error messages
				$page->setContent('form', $resetPasswordForm->render($view));

			} else {

				// Passed input validation.

				$values = $form->getValues();

				if($values['password'] === $values['repeatpassword']) {
					// Passwords match, save new password and generate new forgot password key

					// Don't encrypt the password at this point, this happens in the model
					$user->setPassword($values['password']);
					$user->setForgotPasswordKey(date('Y-m-d H:i:s'));
					$user->saveUser();

					$page->setContent('form_status', 'Your password has been changed.');
					$page->setContent('update', true);

				} else {
					// Fail

					$page->setContent('form_status', 'Sorry, your passwords do not match. Please try again.');
					$page->setContent('form', $resetPasswordForm->render($view));
					$page->setContent('update', false);
				}

			}
		} else {
			// This is not a valid request. Something dodgy may be going on.
			$page->setContent('update', false);
			$message = 'Sorry, there was a problem resetting your password. ';
			$message .= 'The system detected that this is not a valid password reset request. ';
			$message .= 'To protect your account and prevent your password being reset without your knowledge, ';
			$message .= 'the system has terminated this reset password request. ';
			$page->setContent('form_status', $message);
		}

		// Render template
		$page->renderPage();

		return $this;
	}

}