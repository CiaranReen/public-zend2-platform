<?php
/**
 * @package CO3-Core Platform
 *
 * Displays and processes the Admin login form
 */

class Admin_Controllers_Login_LoginController extends Admin_Controllers_Abstract_AbstractController {

	/**
	 * Dispatch the controller
	 * This will cause a page to be generated and output will be sent to the browser
	 *
	 * @param array $params
	 *
	 * @return $this
	 */
	public function index($params) {
		$this->preDispatch();
		
		$view = new Zend_View();
		$form = new Admin_Forms_Login_LoginForm();

		// Pass to the view
		/* @var $page Frontend_Views_View */
		$page = new Admin_Views_View('Login');

		// Set template variables
		$page->setTitle('Sign in &middot; CO3 Core Platform');

		$page->setContent('form_title', $form->getDescription());

		if(!$_POST) {
			// Render the blank form
			$page->setContent('form', $form->render($view));

		} else if(!$form->isValid($_POST)) {

			$page->setContent('form_errors', 'There were errors');
			// Renders form with error messages
			$page->setContent('form', $form->render($view));

		} else {

			// Passed input validation.

			// Check if this user is allowed to login to the admin
			$username = $form->getValue('username');
			$password = $form->getValue('password');

			$session = new Session_Session();
			$user = $session->processLogin($username, $password);
			if($user !== false) {
				// Successful login

				// Add user data to session
				$session->setData('user_id', $user['user_id']);
				$session->setData('is_logged_in', true);

				// Redirect to admin homepage
				header('Location: /admin/index');

			} else {
				// Failed login

				$page->setContent('form_errors', 'Your username or password were not recognised');

				$page->setContent('form', $form->render($view));
			}

		}

		// Render template
		$page->renderPage();

		return $this;
	}

}