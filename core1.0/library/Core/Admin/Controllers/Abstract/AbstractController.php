<?php
/**
 * Class Admin_Controllers_Abstract_AbstractController
 *
 * Abstract class for Admin controllers
 */

abstract class Admin_Controllers_Abstract_AbstractController {

	/**
	 * Dispatch the controller
	 * This will cause a page to be generated and output will be sent to the browser
	 *
	 * @param array $params Optional parameter(s)
	 *
	 * @return $this
	 */
	public function index($params) {
		$this->preDispatch();

		return $this;
	}

	/**
	 * Execute any pre-dispatch actions here
	 *
	 * @return $this
	 */
	protected function preDispatch() {

		return $this;
	}

	/**
	 * Check to see if we're logged in
	 *
	 * @param string $redirect URL to redirect to
	 *
	 * @return bool
	 */
	protected function isLoggedIn($redirect = '/admin/login') {
		$session = new Session_Session();

		if(!$session->isLoggedIn()) {
			header('Location: ' . $redirect);
		}

		return true;
	}

	/**
	 * Get an array of enabled modules
	 *
	 * @return array
	 */
	protected function getEnabledModules() {

		// Generate an array of enabled modules
		$enabledModules = array();
		// Don't display these module names in the admin navigation
		$skipModules = array('Admin', 'Frontend', 'Ajax', 'Basket', 'Checkout');
		// Get module config for all modules
		$modulesConfig = Application_Environment::getCompleteModuleConfig();

		foreach($modulesConfig as $module => $config) {
			if(!in_array($module, $skipModules) && Application_Environment::isModuleEnabled($module)) {
				$enabledModules[$module] = $config;
			}
		}

		return $enabledModules;
	}

}