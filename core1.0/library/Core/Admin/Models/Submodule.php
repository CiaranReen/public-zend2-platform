<?php

class Core_Admin_Models_Submodule
{
    protected $id;
    protected $name;
    protected $pathname;
 
    public function setId($id)
    {
        $this->id = $id;
        
        return $this;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setPathname($pathname)
    {
        $this->pathname = $pathname;
        
        return $this;
    }
    
    public function getPathname()
    {
        return $this->pathname;
    }
}

