<?php

class Core_Admin_Models_Navigation
{
    protected $navigation;
    protected $frontController;
    
    /**
     * This Navigation will not work unless it knows which Front Controller to use!
     * 
     * @param Zend_controller_Front $frontcontroller
     */
    public function __construct($frontcontroller) 
    {
        $this->setFrontController($frontcontroller);
    }
    
    public function getNavigation()
    {
        if(!isset($this->navigation))
        {
            $this->setNavigation();
        }
        
        return $this->navigation;
    }
    
    protected function setFrontController($frontcontroller)
    {
        $this->frontController = $frontcontroller;
    }
    
    protected function getFrontController()
    {
        return $this->frontController;
    }
    
    protected function setNavigation()
    {
        $navigationArray = array();
        $modules = new Core_Admin_Models_ModulesMapper();
        $loadedMods = $modules->fetchAll();
        
        foreach($loadedMods as $loadedMod)
        {
            $loadedMod->setSubmodules($modules->fetchSubmodules($loadedMod->getId()));
            $navigationArray[] = $loadedMod;
        }
        
        $this->navigation = $navigationArray;
        
        return $this;
    }
    
    protected function setNavigationOld()
    {
        $navigationArray = array();
        $modules = new Core_Admin_Models_ModulesMapper();
        $loadedMods = $modules->fetchAll();
        $acl = array();
        
        foreach ($this->getFrontController()->getControllerDirectory() as $module => $path) 
        {
            if($module == 'admin')
            {
                foreach (scandir($path) as $file) 
                {
                    if (strstr($file, "Controller.php") !== false) 
                    {
                        include_once $path . DIRECTORY_SEPARATOR . $file;

                        foreach (get_declared_classes() as $class) 
                        {
                            if (is_subclass_of($class, 'Zend_Controller_Action')) 
                            {
                                $controller = strtolower(substr($class, 0, strpos($class, "Controller")));

                                foreach (get_class_methods($class) as $action) 
                                {
                                    if (strstr($action, "Action") !== false) 
                                    {
                                        $acl[$controller][$action] = $action;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }
        
        foreach($loadedMods as $module)
        {
            if($module->getAdminConfigurable() == 1)
            {
                $subnav = array();
                
                foreach($acl as $controller => $actions)
                {
                    $moduleName = strtolower(str_replace('admin_', '', $controller));
                    if($module->getModuleName() == $moduleName)
                    {
                        foreach($actions as $action)
                        {
                            $actionName = str_replace('Action', '', $action);
                            
                            $subnav[] = array(
                                'label' => $actionName,
                                'uri' => '/admin/'.$module->getModulePathName().'/'.$actionName,
                                'resource' => 'nav-admin'
                            );
                        }
                    }
                }
                
                $navigationArray[] = array(
                    'label' => $module->getModuleName(),
                    'uri' => '/admin/'.$module->getModulePathName(),
                    'resource' => 'nav-admin',
                    'pages' => $subnav
                );
            }
        }
        
        $this->navigation = new Zend_Navigation($navigationArray);
        
        return $this;
    }
    
    public function getSubNav($currentPage)
    {
        $modules = new Core_Admin_Models_ModulesMapper();
        $submodules = $modules->fetchSubmodules($currentPage);
        
        return $submodules;
    }
}