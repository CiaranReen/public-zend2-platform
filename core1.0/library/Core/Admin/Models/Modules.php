<?php

class Core_Admin_Models_Modules
{
    protected $_id;
    protected $_moduleName;
    protected $_modulePathName;
    protected $_adminConfigurable;
    protected $_submodules;
    protected $_moduleIcon;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid module property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid module property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setId($id)
    {
        $this->_id = (int)$id;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setModuleName($moduleName)
    {
        $this->_moduleName = (string)$moduleName;
        return $this;
    }

    public function getModuleName()
    {
        return $this->_moduleName;
    }

    public function setModuleIcon($moduleIcon)
    {
        $this->_moduleIcon = (string)$moduleIcon;
        return $this;
    }

    public function getModuleIcon()
    {
        return $this->_moduleIcon;
    }

    public function setModulePathName($modulePathName)
    {
        $this->_modulePathName = (string)$modulePathName;
        return $this;
    }

    public function getModulePathName()
    {
        return $this->_modulePathName;
    }

    public function setAdminConfigurable($adminConfigurable)
    {
        $this->_adminConfigurable = (string)$adminConfigurable;

        return $this;
    }

    public function getAdminConfigurable()
    {
        return $this->_adminConfigurable;
    }

    public function setSubmodules($submodules)
    {
        try {
            if (!empty($submodules) && !is_array($submodules)) {
                throw new InvalidArgumentException(
                    'Cannot assign submodules to module because 
                        argument parsed was not an array'
                );
            }

            foreach ($submodules as $submodule) {
                if (!$submodule instanceof Core_Admin_Models_Submodule) {
                    throw new InvalidArgumentException(
                        'Cannot assign submodules to module because 
                            1 or more submodules was not parsed as an object'
                    );
                }
            }
        } catch (InvalidArgumentException $e) {
            echo $e->getMessage();
        }

        $this->_submodules = $submodules;
        return $this;
    }

    public function getSubmodules()
    {
        return $this->_submodules;
    }
}

