<?php

class Core_Admin_Models_ModulesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Admin_Models_DbTable_Modules');
        }
        return $this->_dbTable;
    }

    public function save(Core_Admin_Models_Modules $module)
    {
        $data = array(
            'module_name' => $module->getModuleName(),
            'module_path_name' => $module->getModulePathName(),
            'admin_configurable' => $module->getAdminConfigurable(),
            'module_icon' => $module->getModuleIcon(),
        );

        if (null === ($id = $module->getId())) {
            unset($data['module_id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('module_id = ?' => $id));
        }
    }

    public function find($id, Core_Admin_Models_Modules $module)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $module->setId($row->module_id)
            ->setModuleName($row->module_name)
            ->setModulePathName($row->module_path_name)
            ->setAdminConfigurable($row->admin_configurable)
            ->setModuleIcon($row->module_icon);
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = array();
        foreach ($resultSet as $row) {
            if ($row->admin_configurable == 1) {
                $entry = new Core_Admin_Models_Modules();
                $entry->setId($row->module_id)
                    ->setModuleName($row->module_name)
                    ->setModulePathName($row->module_path_name)
                    ->setAdminConfigurable($row->admin_configurable)
                    ->setModuleIcon($row->module_icon);
                $entries[] = $entry;
            }
        }

        return $entries;
    }

    public function fetchSubmodules($moduleId)
    {
        $submodules = array();
        $select = new Zend_Db_Select($this->getDbTable()->getAdapter());

        $sql = $select->from('modules', 'submodules.*')
            ->joinInner('modules_submodules', '`modules`.`module_id` = `modules_submodules`.`module_id`')
            ->joinInner('submodules', '`modules_submodules`.`submodule_id` = `submodules`.`submodule_id`')
            ->where('modules.module_id = ?', $moduleId);

        $stmt = $this->getDbTable()->getAdapter()->query($sql);
        $result = $stmt->fetchAll();

        foreach ($result as $row) {
            $submodule = new Core_Admin_Models_Submodule();
            $submodule->setId($row['submodule_id']);
            $submodule->setName($row['name']);
            $submodule->setPathname($row['submodule_path_name']);
            $submodules[] = $submodule;
        }

        return $submodules;
    }
}

