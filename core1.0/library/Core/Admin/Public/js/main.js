$(document).ready(function(){
    
    /**FOR CATEGORY PAGE COLOURWAYS ROLLOVER**/
    if($('.colourway-product-select').doesExist()) {
        var categoryProductColourways = new Array();
        $('.colourway-product-select').each(function(){
            var productId                           = $(this).attr('productid');
            var productColourways                   = $.getProductColourways(productId);
            categoryProductColourways[productId]    = productColourways;
        });
        $('.colourway-product-select').hover(function(){
            var productId           = $(this).attr('productid');
            var productColourways   = categoryProductColourways[productId];
            var colourwayIdString   = $(this).attr('id').split('-');
            var colourwayId         = colourwayIdString[2];
            var productUrl          = "/frontend/product/view/id/" + productId + "/v/" + colourwayId;
            var colourwayImage      = productColourways[colourwayId]['images'][1];
            $(this).parent().find('.productImage').html('<img src="/modules/Admin/Public/img/product-images/'+colourwayImage+'" />');
            $(this).closest('li').find('a').attr('href' , productUrl);
            return false;
        });
    }
    /**END OF CATEGORY PAGE ROLLOVER**/
    /***************** ACTIVATED ON PRODUCT PAGE WHEN ADDTOBASKET BUTTON EXISTS *****************/
    if($('#addToBasket').doesExist()) {
        //shows/hides added product popup if a product has been added to basket
        if($( "#added-product-popup" ).hasClass('show')){
            $( "#added-product-popup" ).show();
            $( "#added-product-popup" ).effect( "bounce", 1000 );
            $( "#added-product-popup" ).fadeOut(2000);
        }
        
        $('#addToBasket').attr({title:'Please enter a quantity'}).tooltip();
        
        var qtyInputs   = new Array();
        var productId   = $('#productId').val();
        qtyInputs       = $.getProductQtyInputs(productId);
        $.each( qtyInputs, function( key, value ) {
            var isEmpty = true;
            $('#'+value).blur(function(){
                var qtyEntered = $(this).val();
                if(parseInt(qtyEntered) && qtyEntered > 0){
                    isEmpty = false;
                }
                if(isEmpty == false){
                    $('#addToBasket').attr({title:''});
                    $('#addToBasket').tooltip('disable');
                }
            });
        });
        
        $('#addToBasket').click(function(){
            var proceed = false;
            $.each( qtyInputs, function( key, qtyInput ) {
                var total   = 0;
                total       += $('#' + qtyInput).val();
                if(parseInt(total) && total > 0){
                    proceed = true;
                }
            });
            return proceed;
        });
        
        
         /**Gets product colourways**/
        var productColourways   = $.getProductColourways(productId);
        
        if(productColourways){
            $.each( productColourways, function( key, value ) {
                if($('.colourway-row-'+key).hasClass('default')){
                    $('.colourway-row-'+key).show();
                    var hex         = $('#colourway-select-'+key).attr('hex').substring(1);
                    var tickClass   = (parseInt(hex, 16) > 0xffffff/2) ? 'black-tick':'white-tick';
                    $('#colourway-select-'+key).addClass(tickClass);
                }
                else{
                    $('.colourway-row-'+key).hide();
                }
            });

            $('.colourway-select').on('click',function(){
                $.each( productColourways, function( key, value ) {
                    $('.colourway-row-'+key).hide();
                    $('#colourway-select-'+key).removeClass('black-tick white-tick');
                });
                var colourwayIdString   = $(this).attr('id').split('-');
                var colourwayId         = colourwayIdString[2];
                $('.colourway-row-'+colourwayId).show();
                //add colour selected tick
                var hex = $('#colourway-select-'+colourwayId).attr('hex').substring(1);
                var tickClass = (parseInt(hex, 16) > 0xffffff/2) ? 'black-tick':'white-tick';
                $('#colourway-select-'+colourwayId).addClass(tickClass);
                var colourwayImages     = productColourways[colourwayId]['images'];
                if(colourwayImages){
                    var mainImage           =  colourwayImages[1];
                    $('#prodImageWrapper').html('<img src="/modules/Admin/Public/img/product-images/'+mainImage+'"/>');
                    $.each( colourwayImages, function( key, value ) {
                        $('#colourway-thumbnail-'+key).html('<img src = "/modules/Admin/Public/img/product-images/'+value+'"/>');
                    });
                }
                
            });
            
            $('.colourway-thumbnail-select').click(function(){
                $('#prodImageWrapper').html($(this).html());
            });
        }
    }
    /***************** END OF PRODUCT PAGE SCRIPT*****************/
    
    
    /********** ACTIVATED ON BASKET PAGE WHEN GOTOCHECKOUT BUTTON EXISTS *****************/
    if($('#goToCheckout').doesExist()) {
        $('.update').hide();
        var basket      = $.getBasket();
        var qtyInputs   = new Array();
        qtyInputs       = $.getBasketQtyInputs();
        
        $.each( qtyInputs, function( key, qtyInput ) {
            var isEmpty = true;
            $('#'+qtyInput).keyup(function(){
                $('.update').hide();
                $(this).parent().find('.update').show();
                var qtyEntered = $(this).val();
                if(parseInt(qtyEntered) && qtyEntered > 0){
                    isEmpty = false;
                }
                if(isEmpty == false){
                    //alert('working');
                }
            });
        });
        
        $('.update').click(function(){
            var newQty  = $(this).parent().find('.qty').val();
            var stockId = $(this).attr('stockId');
            $.updateItemQty(stockId, newQty);
            location.reload();
            return false;
        });
    }
    
    /********** ACTIVATED ON BASKET PAGE WHEN GOTOCHECKOUT BUTTON EXISTS *****************/
    $('.more').hide();
    var toggle = 0;
    $('.view-more').click(function(){
        if(toggle === 0){
            toggle = 1;
            $('.view-more').html('View Less &#9650;');
        }else{
            toggle = 0;
            $('.view-more').html('View More &#9660;');
        }
        $( ".more" ).slideToggle( "slow", function() {});
        return false;
    });
    /********** ACTIVATED ON BASKET PAGE WHEN GOTOCHECKOUT BUTTON EXISTS *****************/
});