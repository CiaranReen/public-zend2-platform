$(document).ready(function(){
    /******CHECKS IF AN ELEMENT EXISTS ON A PAGE*****/
    $.fn.doesExist = function(){
        return jQuery(this).length > 0;
    };
    /*************END OF ELEMENT CHECKER**************/
    
    
    /***************** GETS ALL PRODUCT STOCK FROM HIDDEN PRODUCT ELEMENT ON PRODUCT PAGE *****************/
    $.getProductStock = function(id){
        var stock = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/Product/getProductStock/",
            data: { productid: id},
            async: false,
            success: function(data) {
                stock = $.parseJSON(data);
            }
        });
        
        return stock;
    };
    
    /***************** END OF PRODUCT STOCK RETRIEVAL *****************/
    
    $.getProductColourways = function(id){
        var colourways  = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/Product/getProductColourways/",
            data: { productid: id},
            async: false,
            success: function(data) {
                colourways = jQuery.parseJSON(data);
            }
        });
        
        return colourways;
    };
    /************ GETS ALL PRODUCT STOCK QUANTITY INPUT FIELD IDS FOR VALIDATION OR ANYTHING ELSE **************/
    $.getProductQtyInputs = function(id){
        var stockIds        = new Array();
        var qtyInput        = new Array();
        var productStock    = $.getProductStock(id);
        $.each( productStock, function( key, value ) {
            var stockId   = value['stock_id'];
            stockIds[key] = stockId;
            qtyInput[key] = 'qty_'+stockId;
        });
        return qtyInput;
    };
        
    /******CHECKS IF AN ELEMENT EXISTS ON A PAGE*****/
    $.getBasket = function(){
        var basket  = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/Basket/getBasket/",
            async: false,
            success: function(data) {
                basket = $.parseJSON(data);
            }
        });

        return basket;
    };
    
    $.getBasketStockIds = function(){
        var basket      = $.getBasket();
        var stockIds    = new Array();
        var i = 0;
        $.each( basket, function( key, value ) {
            stockIds[i] = key;
            i++;
        });
        return stockIds;
    };

    $.getBasketQtyInputs = function(){
        var qtyInput    = new Array();
        var basket      = $.getBasket();
        $.each( basket, function( key, value ) {
            var stockId = key;
            qtyInput[key] = 'qty_'+stockId;
        });
        return qtyInput;
    };
    
    $.updateItemQty = function(stockId, newQty){
        $.ajax({
            type: "POST",
            data: {id: stockId, newQty: newQty},
            url: "/Ajax/Basket/updateItemQty/",
            async: false,
            success: function() {
                
            }
        });
        return true;
    };
               
        
});