<?php

/**
 * Class Core_Quotewerks_Models_Quotewerks
 *
 * Quotewerks Functionality
 */
class Core_Quotewerks_Models_Quotewerks extends App_Model_Abstract
{
    const APP_VERSION_MAJOR = '4.5';
    const APP_VERSION_MINOR = '1.01';
    const DATE_TIME_STANDARD = 'ISO 8601';
    const ALTERNATE_CURRENCY = 'EUR  2000-05-09';

    public function getDocumentHeader(Core_Order_Models_Order $order){
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $userModel = new Core_User_Models_User();
        $addressModel = new Core_Address_Models_Address();

        if(!$order instanceof Core_Order_Models_Order){
            return ;
        }
        $adminUser = Core_User_Models_User::getCurrent();
        $user = $userModel->findById($order->getUserId());
        $billingAddress = $addressModel->findById($order->getBillingAddressId());
        $deliveryAddress = $addressModel->findById($order->getDeliveryAddressId());
        $currency = $currencyMapper->find($order->getCurrencyId(), new Core_Currency_Models_Currency);
        $createdDate = date('Y-m-d') . 'T' . date('H:i:s') . 'Z';


        $documentHeader = '<?xml version="1.0" encoding="UTF-8" ?>
            <QuoteWerksXML>
                <AppVersionMajor>' . self::APP_VERSION_MAJOR . '</AppVersionMajor>
                <AppVersionMinor>' . self::APP_VERSION_MINOR . '</AppVersionMinor>
                <FileFormat>1</FileFormat>
                <DecimalSymbol>.</DecimalSymbol>
                <DateTimeStandard>' . self::DATE_TIME_STANDARD . '</DateTimeStandard>
                <Created>' . $createdDate . '</Created>
                <Comments></Comments>
                <Documents>
                <Document id="1">
                <DocumentHeader>
                <AlternateCurrency>' . self::ALTERNATE_CURRENCY . '</AlternateCurrency>
                <AlternateGrandTotal>' . $order->getOrderTotal() . '</AlternateGrandTotal>
                <AlternateGSTTax>0</AlternateGSTTax>
                <AlternateLocalTax>0</AlternateLocalTax>
                <AlternateShippingAmount>0</AlternateShippingAmount>
                <AlternateSubTotal>' . $order->getOrderTotal() . '</AlternateSubTotal>
                <AlternateTotalTax>0</AlternateTotalTax>
                <BillToAddress1>'. $billingAddress->getLineOne().'</BillToAddress1>
                <BillToAddress2>'. $billingAddress->getLineTwo().'</BillToAddress2>
                <BillToAddress3></BillToAddress3>
                <BillToCity>'. $billingAddress->getCity() .'</BillToCity>
                <BillToCMAccountNo></BillToCMAccountNo>
                <BillToCompany></BillToCompany>
                <BillToContact>'. $user->getForename() . ' ' . $user->getSurname() . '</BillToContact>
                <BillToCountry>'. $billingAddress->getCountry()->getCountry() .'</BillToCountry>
                <BillToEmail>'. $user->getEmail() . '</BillToEmail>
                <BillToFax></BillToFax>
                <BillToFaxExt></BillToFaxExt>
                <BillToPhone>'. $user->getPhone() . '</BillToPhone>
                <BillToPhoneExt></BillToPhoneExt>
                <BillToPostalCode>'. $billingAddress->getPostcode() .'</BillToPostalCode>
                <BillToState></BillToState>
                <BillToTitle></BillToTitle>
                <ClosingNotes></ClosingNotes>
                <CommissionAmount>0</CommissionAmount>
                <CommissionStructure>Structure1 0</CommissionStructure>
                <ConvertedBy>' . $adminUser->getForename() . ' ' . $adminUser->getSurname() .'</ConvertedBy>
                <ConvertedOn>' . $createdDate . '</ConvertedOn>
                <ConvertedRef></ConvertedRef>
                <Created>' . $createdDate . '</Created>
                <CreatedBy>' . $adminUser->getForename() . ' ' . $adminUser->getSurname() .'</CreatedBy>
                <CustomDate01></CustomDate01>
                <CustomDate02></CustomDate02>
                <CustomNumber01></CustomNumber01>
                <CustomNumber02></CustomNumber02>
                <CustomText01></CustomText01>
                <CustomText02>' . $order->getOrderNumber() .'</CustomText02>
                <CustomText03></CustomText03>
                <CustomText04></CustomText04>
                <CustomText05></CustomText05>
                <CustomText06></CustomText06>
                <CustomText07></CustomText07>
                <CustomText08></CustomText08>
                <CustomText09></CustomText09>
                <CustomText10></CustomText10>
                <CustomText11></CustomText11>
                <CustomText12></CustomText12>
                <CustomText13></CustomText13>
                <CustomText14></CustomText14>
                <CustomText15></CustomText15>
                <CustomText16></CustomText16>
                <CustomText17></CustomText17>
                <CustomText18>Other</CustomText18>
                <CustomText19></CustomText19>
                <CustomText20>Customer</CustomText20>
                <DocDate>' . $createdDate . '</DocDate>
                <DocDueDate>' . $createdDate . '</DocDueDate>
                <DocName> - ' . $user->getForename() . ' ' . $user->getSurname() . '- ' . $order->getOrderNumber() .'</DocName>
                <DocNo></DocNo>
                <DocStatus>12. Full Payment Required</DocStatus>
                <DocType>ORDER</DocType>
                <ExchangeRate>1</ExchangeRate>
                <FOB>Ex Works</FOB>
                <GrandTotal>' . $order->getOrderTotal() . '</GrandTotal>
                <GSTTax>0</GSTTax>
                <IntegrationData></IntegrationData>
                <InternalNotes>Order Total: ' . $order->getOrderTotal() . '
                Order Currency: ' . $currency->getShortCode() . '
                ' . ($order->getPaymentType() === 'invoice' ? 'Invoice Currency: ' . $currency->getShortCode() . 'Invoice Type: proforma' : '') . '
                </InternalNotes>
                <IntroductionNotes></IntroductionNotes>
                <LastModified></LastModified>
                <LocalTax>0</LocalTax>
                <LocalTaxRate>0</LocalTaxRate>
                <Locked>no</Locked>
                <ProfitAmount></ProfitAmount>
                <ProjectNo></ProjectNo>
                <PSTIsCompounded>0</PSTIsCompounded>
                <PurchasingNotes></PurchasingNotes>
                <RevisionMasterDocNo></RevisionMasterDocNo>
                <SalesRep>' . $adminUser->getForename() . ' ' . $adminUser->getSurname() .'</SalesRep>
                <ShippingAmount>0</ShippingAmount>
                <ShipToAddress1>' . $deliveryAddress->getLineOne() . '</ShipToAddress1>
                <ShipToAddress2>' . $deliveryAddress->getLineTwo() . '</ShipToAddress2>
                <ShipToAddress3></ShipToAddress3>
                <ShipToCity>' . $deliveryAddress->getCity() . '</ShipToCity>
                <ShipToCMAccountNo></ShipToCMAccountNo>
                <ShipToCompany></ShipToCompany>
                <ShipToContact>' . $user->getForename() . ' '. $user->getSurname() . '</ShipToContact>
                <ShipToCountry>' . $deliveryAddress->getCountry()->getCountry() . '</ShipToCountry>
                <ShipToEmail>' . $user->getEmail() . '</ShipToEmail>
                <ShipToFax></ShipToFax>
                <ShipToFaxExt></ShipToFaxExt>
                <ShipToPhone>' . $user->getPhone() . '</ShipToPhone>
                <ShipToPhoneExt></ShipToPhoneExt>
                <ShipToPostalCode>' . $deliveryAddress->getPostcode() . '</ShipToPostalCode>
                <ShipToState></ShipToState>
                <ShipToTitle></ShipToTitle>
                <ShipVia>TBC</ShipVia>
                <SoldToAddress1>' . $deliveryAddress->getLineOne() . '</SoldToAddress1>
                <SoldToAddress2>' . $deliveryAddress->getLineTwo() . '</SoldToAddress2>
                <SoldToAddress3></SoldToAddress3>
                <SoldToCity>' . $deliveryAddress->getCity() . '</SoldToCity>
                <SoldToCMAccountNo></SoldToCMAccountNo>
                <SoldToCMLinkRecID></SoldToCMLinkRecID>
                <SoldToCMOppRecID></SoldToCMOppRecID>
                <SoldToCompany></SoldToCompany>
                <SoldToContact>' . $user->getForename() . ' '. $user->getSurname() . '</SoldToContact>
                <SoldToCountry>' . $deliveryAddress->getCountry()->getCountry() . '</SoldToCountry>
                <SoldToEmail>' . $user->getEmail() . '</SoldToEmail>
                <SoldToFax></SoldToFax>
                <SoldToFaxExt></SoldToFaxExt>
                <SoldToPhone>' . $user->getPhone() . '</SoldToPhone>
                <SoldToPhoneExt></SoldToPhoneExt>
                <SoldToPONumber>' . $order->getOrderNumber() . '</SoldToPONumber>
                <SoldToPostalCode>' . $deliveryAddress->getPostcode() . '</SoldToPostalCode>
                <SoldToPriceProfile></SoldToPriceProfile>
                <SoldToState></SoldToState>
                <SoldToTitle></SoldToTitle>
                <SubTotal>' . $order->getOrderTotal() . '</SubTotal>
                <Superceeded>0</Superceeded>
                <Terms>Order Total: ' . $order->getOrderTotal() . '</Terms>
                <TotalTax>0</TotalTax>
                <SoldToCMCallRecID></SoldToCMCallRecID>
                <GSTTaxRate></GSTTaxRate>
                </DocumentHeader>';


        return $documentHeader;
    }

    public function getDocumentItems(Core_Order_Models_Order $order){
        $vendorProductsLinkModel = new Core_Product_Models_VendorProductsLink();
        $stockMapper = new Core_Stock_Models_StockMapper();

        $orderDate = date('Y-m-d', strtotime($order->getDateOrdered()));

        $documentItems = '<DocumentItems>';

        if($order->getOrderParts()){
            $i = 1;
            foreach($order->getOrderParts() as $orderPart){
                $productDescription = $orderPart->getPartStockDetails()->product . ' - ' .$orderPart->getPartStockDetails()->colourway . ' (' . $orderPart->getPartStockDetails()->option . ')' . ' x ' . $orderPart->getQuantity();
                $documentItems .= '<DocumentItem id="' . $i . '">
                    <AlternateExtendedCost>0</AlternateExtendedCost>
                    <AlternateExtendedList>0</AlternateExtendedList>
                    <AlternateExtendedPrice>' . $orderPart->getPrice() . '</AlternateExtendedPrice>
                    <AlternateSalesTax>0</AlternateSalesTax>
                    <AlternateShippingAmount>0</AlternateShippingAmount>
                    <AlternateUnitCost>0</AlternateUnitCost>
                    <AlternateUnitList>0</AlternateUnitList>
                    <AlternateUnitPrice>' . $orderPart->getPrice() . '</AlternateUnitPrice>
                    <CloseProbability>0</CloseProbability>
                    <CostModifier></CostModifier>
                    <CustomDate01></CustomDate01>
                    <CustomDate02></CustomDate02>
                    <CustomMemo01></CustomMemo01>
                    <CustomMemo02></CustomMemo02>
                    <CustomNumber01>0</CustomNumber01>
                    <CustomNumber02>0</CustomNumber02>
                    <CustomNumber03>0</CustomNumber03>
                    <CustomNumber04>0</CustomNumber04>
                    <CustomNumber05>0</CustomNumber05>
                    <CustomText01></CustomText01>
                    <CustomText02></CustomText02>
                    <CustomText03></CustomText03>
                    <CustomText04></CustomText04>
                    <CustomText05></CustomText05>
                    <CustomText06></CustomText06>
                    <CustomText07></CustomText07>
                    <CustomText08></CustomText08>
                    <CustomText09></CustomText09>
                    <CustomText10></CustomText10>
                    <CustomText11></CustomText11>
                    <CustomText12></CustomText12>
                    <CustomText13></CustomText13>
                    <CustomText14></CustomText14>
                    <Description>' . $productDescription . '</Description>
                    <ExtendedCost>0</ExtendedCost>
                    <ExtendedList>0</ExtendedList>
                    <ExtendedPrice>0</ExtendedPrice>
                    <ExtendedWeight>0</ExtendedWeight>
                    <Formula></Formula>
                    <ItemType></ItemType>
                    <LineAttributes>0</LineAttributes>
                    <LineType>8</LineType>
                    <Manufacturer></Manufacturer>
                    <ManufacturerPartNumber></ManufacturerPartNumber>
                    <Notes></Notes>
                    <OrderDate>' . $orderDate . '</OrderDate>
                    <PONumber></PONumber>
                    <PriceModifier></PriceModifier>
                    <PriceProfile></PriceProfile>
                    <QtyBase>' . $orderPart->getQuantity() . '</QtyBase>
                    <QtyGroupMultiplier>1</QtyGroupMultiplier>
                    <QtyMultiplier1>1</QtyMultiplier1>
                    <QtyMultiplier2>1</QtyMultiplier2>
                    <QtyMultiplier3>1</QtyMultiplier3>
                    <QtyMultiplier4>1</QtyMultiplier4>
                    <QtyTotal>' . $orderPart->getQuantity() . '</QtyTotal>
                    <SalesTax>0</SalesTax>
                    <ShippingAmount>0</ShippingAmount>
                    <SONumber></SONumber>
                    <TaxCode>N</TaxCode>
                    <UnitCost>0</UnitCost>
                    <UnitList>0</UnitList>
                    <UnitOfMeasure></UnitOfMeasure>
                    <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
                    <UnitOfPricing></UnitOfPricing>
                    <UnitOfPricingFactor>1</UnitOfPricingFactor>
                    <UnitPrice>' . $orderPart->getPrice() . '</UnitPrice>
                    <UnitWeight>0</UnitWeight>
                    <Vendor></Vendor>
                    <VendorPartNumber></VendorPartNumber>
                    <ItemURL></ItemURL>
                </DocumentItem>';

                $i++;

                $stock = $stockMapper->find($orderPart->getStockId(), new Core_Stock_Models_Stock());
                $vendorProducts = $vendorProductsLinkModel->getAllVendorProductsById($stock->getProductId());
                if($vendorProducts){
                    $vpCount = 0;
                    foreach($vendorProducts as $vendorProduct){
                        $vpmMapper      = new Core_Product_Models_VendorProductManufacturersMapper();
                        $vpManufacturer = $vpmMapper->find($vendorProduct->manufacturer_id, new Core_Product_Models_VendorProductManufacturers());
                        $vpDescription  = $orderPart->getPartStockDetails()->colourway . ' (' . $orderPart->getPartStockDetails()->option . ')' . ' x ' . $orderPart->getQuantity();
                        $documentItems .= '<DocumentItem id="' . $i . '">
                            <AlternateExtendedCost>' . $vendorProduct->cost . '</AlternateExtendedCost>
                            <AlternateExtendedList>0</AlternateExtendedList>
                            <AlternateExtendedPrice>0</AlternateExtendedPrice>
                            <AlternateSalesTax>0</AlternateSalesTax>
                            <AlternateShippingAmount>0</AlternateShippingAmount>
                            <AlternateUnitCost>' . $vendorProduct->cost . '</AlternateUnitCost>
                            <AlternateUnitList>0</AlternateUnitList>
                            <AlternateUnitPrice>0</AlternateUnitPrice>
                            <CloseProbability>0</CloseProbability>
                            <CostModifier></CostModifier>
                            <CustomDate01></CustomDate01>
                            <CustomDate02></CustomDate02>
                            <CustomMemo01></CustomMemo01>
                            <CustomMemo02></CustomMemo02>
                            <CustomNumber01>0</CustomNumber01>
                            <CustomNumber02>0</CustomNumber02>
                            <CustomNumber03>0</CustomNumber03>
                            <CustomNumber04>0</CustomNumber04>
                            <CustomNumber05>0</CustomNumber05>
                            <CustomText01></CustomText01>
                            <CustomText02></CustomText02>
                            <CustomText03></CustomText03>
                            <CustomText04></CustomText04>
                            <CustomText05></CustomText05>
                            <CustomText06></CustomText06>
                            <CustomText07></CustomText07>
                            <CustomText08></CustomText08>
                            <CustomText09></CustomText09>
                            <CustomText10></CustomText10>
                            <CustomText11></CustomText11>
                            <CustomText12></CustomText12>
                            <CustomText13></CustomText13>
                            <CustomText14></CustomText14>
                            <Description>' . $vendorProduct->description . ' ' . ($vpCount === 0 ? $vpDescription : '') .'</Description>
                            <ExtendedCost>' . $vendorProduct->cost . '</ExtendedCost>
                            <ExtendedList>0</ExtendedList>
                            <ExtendedPrice>0</ExtendedPrice>
                            <ExtendedWeight>0</ExtendedWeight>
                            <Formula></Formula>
                            <ItemType></ItemType>
                            <LineAttributes>14</LineAttributes>
                            <LineType>1</LineType>
                            <Manufacturer></Manufacturer>
                            <ManufacturerPartNumber>' . $vendorProduct->manufacturer_part_no . '</ManufacturerPartNumber>
                            <Notes></Notes>
                            <OrderDate>' . $orderDate . '</OrderDate>
                            <PONumber></PONumber>
                            <PriceModifier></PriceModifier>
                            <PriceProfile></PriceProfile>
                            <QtyBase>1</QtyBase>
                            <QtyGroupMultiplier>' . $orderPart->getQuantity() . '</QtyGroupMultiplier>
                            <QtyMultiplier1>1</QtyMultiplier1>
                            <QtyMultiplier2>1</QtyMultiplier2>
                            <QtyMultiplier3>1</QtyMultiplier3>
                            <QtyMultiplier4>1</QtyMultiplier4>
                            <QtyTotal>' . $orderPart->getQuantity() . '</QtyTotal>
                            <SalesTax>0</SalesTax>
                            <ShippingAmount>0</ShippingAmount>
                            <SONumber></SONumber>
                            <TaxCode>N</TaxCode>
                            <UnitCost>' . $vendorProduct->cost . '</UnitCost>
                            <UnitList>0</UnitList>
                            <UnitOfMeasure>ea</UnitOfMeasure>
                            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
                            <UnitOfPricing>ea</UnitOfPricing>
                            <UnitOfPricingFactor>1</UnitOfPricingFactor>
                            <UnitPrice>' . ($vpCount === 0 ? $orderPart->getPrice() : 0) . '</UnitPrice>
                            <UnitWeight>0</UnitWeight>
                            <Vendor>' . $vpManufacturer->getName() . '</Vendor>
                            <VendorPartNumber>' . $vendorProduct->vendor_part_no . '</VendorPartNumber>
                            <ItemURL></ItemURL>
                        </DocumentItem>';

                        $i++; $vpCount++;
                    }
                }

            }
        }

        if ($order->getDeliveryCharge()) {
            $itemDescription = 'Shipping Cost';
            $documentItems .= '<DocumentItem id="' . $i . '">
                    <AlternateExtendedCost>0</AlternateExtendedCost>
                    <AlternateExtendedList>0</AlternateExtendedList>
                    <AlternateExtendedPrice>0</AlternateExtendedPrice>
                    <AlternateSalesTax>0</AlternateSalesTax>
                    <AlternateShippingAmount>0</AlternateShippingAmount>
                    <AlternateUnitCost>0</AlternateUnitCost>
                    <AlternateUnitList>0</AlternateUnitList>
                    <AlternateUnitPrice>0</AlternateUnitPrice>
                    <CloseProbability>0</CloseProbability>
                    <CostModifier></CostModifier>
                    <CustomDate01></CustomDate01>
                    <CustomDate02></CustomDate02>
                    <CustomMemo01></CustomMemo01>
                    <CustomMemo02></CustomMemo02>
                    <CustomNumber01>0</CustomNumber01>
                    <CustomNumber02>0</CustomNumber02>
                    <CustomNumber03>0</CustomNumber03>
                    <CustomNumber04>0</CustomNumber04>
                    <CustomNumber05>0</CustomNumber05>
                    <CustomText01></CustomText01>
                    <CustomText02></CustomText02>
                    <CustomText03></CustomText03>
                    <CustomText04></CustomText04>
                    <CustomText05></CustomText05>
                    <CustomText06></CustomText06>
                    <CustomText07></CustomText07>
                    <CustomText08></CustomText08>
                    <CustomText09></CustomText09>
                    <CustomText10></CustomText10>
                    <CustomText11></CustomText11>
                    <CustomText12></CustomText12>
                    <CustomText13></CustomText13>
                    <CustomText14></CustomText14>
                    <Description>' . $itemDescription . '</Description>
                    <ExtendedCost>0</ExtendedCost>
                    <ExtendedList>0</ExtendedList>
                    <ExtendedPrice>0</ExtendedPrice>
                    <ExtendedWeight>0</ExtendedWeight>
                    <Formula></Formula>
                    <ItemType></ItemType>
                    <LineAttributes>0</LineAttributes>
                    <LineType>1</LineType>
                    <Manufacturer></Manufacturer>
                    <ManufacturerPartNumber></ManufacturerPartNumber>
                    <Notes></Notes>
                    <OrderDate>' . $orderDate . '</OrderDate>
                    <PONumber></PONumber>
                    <PriceModifier></PriceModifier>
                    <PriceProfile></PriceProfile>
                    <QtyBase>1</QtyBase>
                    <QtyGroupMultiplier>1</QtyGroupMultiplier>
                    <QtyMultiplier1>1</QtyMultiplier1>
                    <QtyMultiplier2>1</QtyMultiplier2>
                    <QtyMultiplier3>1</QtyMultiplier3>
                    <QtyMultiplier4>1</QtyMultiplier4>
                    <QtyTotal>1</QtyTotal>
                    <SalesTax>0</SalesTax>
                    <ShippingAmount>0</ShippingAmount>
                    <SONumber></SONumber>
                    <TaxCode>N</TaxCode>
                    <UnitCost>' . $order->getDeliveryCharge() . '</UnitCost>
                    <UnitList>0</UnitList>
                    <UnitOfMeasure></UnitOfMeasure>
                    <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
                    <UnitOfPricing></UnitOfPricing>
                    <UnitOfPricingFactor></UnitOfPricingFactor>
                    <UnitPrice>0</UnitPrice>
                    <UnitWeight>0</UnitWeight>
                    <Vendor>Shipping</Vendor>
                    <VendorPartNumber>Shipping</VendorPartNumber>
                    <ItemURL></ItemURL>
                </DocumentItem>';
        }

        return $documentItems;
    }

    public function getDocumentFooter(){
        $documentFooter = '</DocumentItems>
                            </Document>
                            </Documents>
                            </QuoteWerksXML>';

        return $documentFooter;
    }

    public function getQuoteWorksXML(Core_Order_Models_Order $order){
        $xml = $this->getDocumentHeader($order) . $this->getDocumentItems($order) . $this->getDocumentFooter();
        return $xml;
    }

    public function export(Core_Order_Models_Order $order)
    {
        header("content-type: text/xml");
        header("Content-Disposition: attachment; filename=" . $order->getOrderNumber() . ".xml");
        header("Pragma: no-cache");
        header("Expires: 0");
        $xml = $this->getQuoteWorksXML($order);
        echo $xml;
        exit();
    }
}