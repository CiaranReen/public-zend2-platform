<?php

/**
 * Class Core_Quotewerks_Models_Quotewerks
 *
 * Quotewerks Functionality
 */
class Core_Quotewerks_Models_Quotewerks extends App_Model_Abstract {
    protected $_version = '4.5';
    protected $_versionMinor = '1.01';
    protected $_fileFormat = '1';
    public function buildOrderXML(Core_Order_Models_Order $order){
        if(!$order instanceof Core_Order_Models_Order){
            throw new Exception('Order passed is not an instance of its module');
        }
        
        $settings = new Core_Settings_Models_Settings();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $usergroupModel = new Core_Usergroup_Models_Usergroup();
        $addressModel = new Core_Address_Models_Address();
        
        $user = Core_User_Models_User::getCurrent();
        if(!$user instanceof Core_User_Models_User){
            throw new Exception ('User passed is not an instance of the its module');
        }
        $usergroup = $usergroupModel->findById($user->getUserGroupId());
        $billingAddress = $addressModel->findById($order->getBillingAddressId());
        $deliveryAddress = $addressModel->findById($order->getDeliveryAddressId());
        $orderId = $order->getId();
        $vatDecimal = 0;
        $vatRate = 0;
        $currencyRate = 1.23;
        $dueDays = 14;

        if ($order->getVatCharge() > 0) {
            $vatDecimal = 1.20;
        }

        $createdUser = $user->getForename() . ' ' . $user->getSurname();
        $company = $settings->getCompanyName();
        $companystr = $settings->getOrderNumberPrefix();
        $totalAmount = $order->getOrderTotal();
        $outstandingAmount = $order->getAmountOutstanding();

        if ($outstandingAmount == 0.00) {
            $docStatus = '10. Order Paid - Needs Invoicing';
        } 
        else {
            if ($totalAmount == $outstandingAmount) {
                $docStatus = '12. Full Payment Required';
            } 
            else {

                $docStatus = '11. Part Payment Required';
            }
        }

        $outstanding = '';
        if ($order->getPaymentType()) {
            $outstanding = 'Outstanding amount to be paid by:  ' . $order->getPaymentType();
        }

        $currencyId = $order->getCurrencyId();
        $currency = $currencyMapper->find($currencyId, new Core_Currency_Models_Currency);
        $paymentTerms = 'Order Total: ' . $totalAmount . ', Outstanding: ' . $outstandingAmount . ', '. $outstanding;
        $internalNotes = "Order Total: " . $totalAmount . "\r\nOutstanding: " . $outstandingAmount . "\r\n". $outstanding . 
        "\r\nOrder Currency: " . $currency->getCurrency() . 
        "\r\nInvoice Currency: " . $usergroup->getCurrencyInvoice() . 
        "\r\nInvoice Type: " . $usergroup->getInvoiceType();

        $grandTotal = $order->getOrderTotal();
        $tax = $order->getVatCharge();

        if ($currency->getCurrency() == 'eur') {

            $grandTotal = $grandTotal / $currencyRate;
            $tax = $tax / $currencyRate;

        }
        $ordernumber = str_replace($companystr,'',$order->getOrderNumber());

        // MAIN QUOTEWERKS DATA
        $xml ="<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n";
        $xml .="<QuoteWerksXML>\r\n";
        $xml .="<AppVersionMajor>4.5</AppVersionMajor>\r\n";
        $xml .="<AppVersionMinor>1.01</AppVersionMinor>\r\n";
        $xml .="<FileFormat>1</FileFormat>\r\n";
        $xml .="<DecimalSymbol>.</DecimalSymbol>\r\n";
        $xml .="<DateTimeStandard>ISO 8601</DateTimeStandard>\r\n";
        $xml .="<Created>" . date("Y-m-d",strtotime($order->getDateOrdered())) . "T" . date("H-i-s",strtotime($order->getDateOrdered())) . "Z</Created>\r\n";
        $xml .="<Comments></Comments>\r\n";


        // ORDER SPECIFIC HEADER
        $xml .="<Documents>\r\n";
        $xml .="<Document id=\"1\">\r\n";
        $xml .="<DocumentHeader>\r\n";
        $xml .="<AlternateCurrency>EUR " . utf8_encode('�') . " 2000-05-09</AlternateCurrency>\r\n";
        $xml .="<AlternateGrandTotal>" . round($grandTotal,2) . "</AlternateGrandTotal>\r\n";
        $xml .="<AlternateGSTTax>0</AlternateGSTTax>\r\n";
        $xml .="<AlternateLocalTax>" . round($tax,2) . "</AlternateLocalTax>\r\n";
        $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>\r\n";
        $xml .="<AlternateSubTotal>" . round($order->getOrderTotal(),2) . "</AlternateSubTotal>\r\n";
        $xml .="<AlternateTotalTax>" . round($tax,2) . "</AlternateTotalTax>\r\n";

        $xml .="<BillToAddress1>" . utf8_encode($billingAddress->getLineOne()) . "</BillToAddress1>\r\n";
        $xml .="<BillToAddress2>" . utf8_encode($billingAddress->getLineTwo()) . "</BillToAddress2>\r\n";
        $xml .="<BillToAddress3></BillToAddress3>\r\n";
        $xml .="<BillToCity>" . utf8_encode($billingAddress->getCity()) . "</BillToCity>\r\n";
        $xml .="<BillToCMAccountNo></BillToCMAccountNo>\r\n";
        $xml .="<BillToCompany>" . utf8_encode($user->getCompanyId()) . "</BillToCompany>\r\n";
        $xml .="<BillToContact>" . utf8_encode($user->getForename() . ' ' . $user->getSurname()) . "</BillToContact>\r\n";
        $xml .="<BillToCountry>" . utf8_encode($billingAddress->getCountry()->getCountry()) . "</BillToCountry>\r\n";
        $xml .="<BillToEmail>" . utf8_encode($user->getEmail()) . "</BillToEmail>\r\n";
        $xml .="<BillToFax></BillToFax>\r\n";
        $xml .="<BillToFaxExt></BillToFaxExt>\r\n";
        $xml .="<BillToPhone>" . utf8_encode($user->getPhone()) . "</BillToPhone>\r\n";
        $xml .="<BillToPhoneExt></BillToPhoneExt>\r\n";
        $xml .="<BillToPostalCode>" . utf8_encode($billingAddress->getPostcode()) . "</BillToPostalCode>\r\n";
        $xml .="<BillToState>" . utf8_encode($billingAddress->getRegion()) . "</BillToState>\r\n";
        $xml .="<BillToTitle></BillToTitle>\r\n";
        $xml .="<ClosingNotes></ClosingNotes>\r\n";
        $xml .="<CommissionAmount>0</CommissionAmount>\r\n";
        $xml .="<CommissionStructure>Structure1 0</CommissionStructure>\r\n";
        $xml .="<ConvertedBy>" . $createdUser . "</ConvertedBy>\r\n";
        $xml .="<ConvertedOn>" . date("Y-m-d",strtotime($order->getDateOrdered())) . "T" . date("H-i-s",strtotime($order->getDateOrdered())) . "Z</ConvertedOn>\r\n";
        $xml .="<ConvertedRef></ConvertedRef>\r\n";
        $xml .="<Created>" . date("Y-m-d",strtotime($order->getDateOrdered())) . "T" . date("H-i-s",strtotime($order->getDateOrdered())) . "Z</Created>\r\n";
        $xml .="<CreatedBy>" . $createdUser . "</CreatedBy>\r\n";
        $xml .="<CustomDate01></CustomDate01>\r\n";
        $xml .="<CustomDate02></CustomDate02>\r\n";
        $xml .="<CustomNumber01></CustomNumber01>\r\n";
        $xml .="<CustomNumber02></CustomNumber02>\r\n";
        $xml .="<CustomText01></CustomText01>\r\n";
        $xml .="<CustomText02>" . $ordernumber . "</CustomText02>\r\n";
        $xml .="<CustomText03></CustomText03>\r\n";
        $xml .="<CustomText04></CustomText04>\r\n";
        $xml .="<CustomText05></CustomText05>\r\n";
        $xml .="<CustomText06></CustomText06>\r\n";
        $xml .="<CustomText07></CustomText07>\r\n";
        $xml .="<CustomText08></CustomText08>\r\n";
        $xml .="<CustomText09></CustomText09>\r\n";
        $xml .="<CustomText10></CustomText10>\r\n";
        $xml .="<CustomText11></CustomText11>\r\n";
        $xml .="<CustomText12></CustomText12>\r\n";
        $xml .="<CustomText13></CustomText13>\r\n";
        $xml .="<CustomText14></CustomText14>\r\n";
        $xml .="<CustomText15></CustomText15>\r\n";
        $xml .="<CustomText16>Health " . utf8_encode('&amp;') . " Fitness</CustomText16>\r\n";
        $xml .="<CustomText17></CustomText17>\r\n";
        $xml .="<CustomText18>Other</CustomText18>\r\n";
        $xml .="<CustomText19></CustomText19>\r\n";
        $xml .="<CustomText20>Customer</CustomText20>\r\n";
        $xml .="<DocDate>" . date("Y-m-d",strtotime($order->getDateOrdered())) . "T" . date("H-i-s",strtotime($order->getDateOrdered())) . "Z</DocDate>\r\n";
        $xml .="<DocDueDate>" . $dueDays . "T" . date("H-i-s",strtotime($order->getDateOrdered())) . "Z</DocDueDate>\r\n";
        $xml .="<DocName>" . $user->getCompanyId() . " - " . $user->getForename() . " " . $user->getSurname() . " - " . $order->getOrderNumber() . "</DocName>\r\n";
        $xml .="<DocNo></DocNo>\r\n";
        $xml .="<DocStatus>" . $docStatus . "</DocStatus>\r\n";
        $xml .="<DocType>ORDER</DocType>\r\n";
        $xml .="<ExchangeRate>1</ExchangeRate>\r\n";
        $xml .="<FOB>Ex Works</FOB>\r\n";
        $xml .="<GrandTotal>" . round($grandTotal,2) . "</GrandTotal>\r\n";
        $xml .="<GSTTax>0</GSTTax>\r\n";

        $xml .="<IntegrationData></IntegrationData>\r\n";
        $xml .="<InternalNotes>" . $internalNotes . "</InternalNotes>\r\n";
        $xml .="<IntroductionNotes></IntroductionNotes>\r\n";
        $xml .="<LastModified></LastModified>\r\n";
        $xml .="<LocalTax>" . round($tax,2) . "</LocalTax>\r\n";
        $xml .="<LocalTaxRate>" . $vatRate . "</LocalTaxRate>\r\n";
        $xml .="<Locked>no</Locked>\r\n";
        $xml .="<ProfitAmount></ProfitAmount>\r\n";
        $xml .="<ProjectNo></ProjectNo>\r\n";
        $xml .="<PSTIsCompounded>0</PSTIsCompounded>\r\n";
        $xml .="<PurchasingNotes></PurchasingNotes>\r\n";
        $xml .="<RevisionMasterDocNo></RevisionMasterDocNo>\r\n";
        $xml .="<SalesRep>" . $createdUser . "</SalesRep>\r\n";
        $xml .="<ShippingAmount>0</ShippingAmount>\r\n";
        $xml .="<ShipToAddress1>" . $deliveryAddress->getLineOne() . "</ShipToAddress1>\r\n";
        $xml .="<ShipToAddress2>" . $deliveryAddress->getLineTwo() . "</ShipToAddress2>\r\n";
        $xml .="<ShipToAddress3></ShipToAddress3>\r\n";
        $xml .="<ShipToCity>" . $deliveryAddress->getCity() . "</ShipToCity>\r\n";
        $xml .="<ShipToCMAccountNo></ShipToCMAccountNo>\r\n";
        $xml .="<ShipToCompany>" .  $user->getCompanyId() . "</ShipToCompany>\r\n";
        $xml .="<ShipToContact>" . $user->getForename() . " " . $user->getSurname() . "</ShipToContact>\r\n";
        $xml .="<ShipToCountry>" . $deliveryAddress->getCountry()->getCountry() . "</ShipToCountry>\r\n";
        $xml .="<ShipToEmail>" . $user->getEmail() . "</ShipToEmail>\r\n";
        $xml .="<ShipToFax></ShipToFax>\r\n";
        $xml .="<ShipToFaxExt></ShipToFaxExt>\r\n";
        $xml .="<ShipToPhone>" . $user->getPhone() . "</ShipToPhone>\r\n";
        $xml .="<ShipToPhoneExt></ShipToPhoneExt>\r\n";
        $xml .="<ShipToPostalCode>" . $deliveryAddress->getPostcode() . "</ShipToPostalCode>\r\n";
        $xml .="<ShipToState>" . $deliveryAddress->getRegion() . "</ShipToState>\r\n";
        $xml .="<ShipToTitle></ShipToTitle>\r\n";
        $xml .="<ShipVia>TBC</ShipVia>\r\n";
        $xml .="<SoldToAddress1>" . $billingAddress->getLineOne() . "</SoldToAddress1>\r\n";
        $xml .="<SoldToAddress2>" . $billingAddress->getLineTwo() . "</SoldToAddress2>\r\n";
        $xml .="<SoldToAddress3></SoldToAddress3>\r\n";
        $xml .="<SoldToCity>" . $billingAddress->getCity() . "</SoldToCity>\r\n";
        $xml .="<SoldToCMAccountNo></SoldToCMAccountNo>\r\n";
        $xml .="<SoldToCMLinkRecID></SoldToCMLinkRecID>\r\n";
        $xml .="<SoldToCMOppRecID></SoldToCMOppRecID>\r\n";
        $xml .="<SoldToCompany>" . $company . "</SoldToCompany>\r\n";
        $xml .="<SoldToContact>" . $user->getForename() . " " . $user->getSurname() . "</SoldToContact>\r\n";
        $xml .="<SoldToCountry>" . $billingAddress->getCountry()->getCountry() . "</SoldToCountry>\r\n";
        $xml .="<SoldToEmail>" . $user->getEmail() . "</SoldToEmail>\r\n";
        $xml .="<SoldToFax></SoldToFax>\r\n";
        $xml .="<SoldToFaxExt></SoldToFaxExt>\r\n";
        $xml .="<SoldToPhone>" . $user->getPhone() . "</SoldToPhone>\r\n";
        $xml .="<SoldToPhoneExt></SoldToPhoneExt>\r\n";
        $xml .="<SoldToPONumber>" . $ordernumber . "</SoldToPONumber>\r\n";
        $xml .="<SoldToPostalCode>" . $billingAddress->getPostcode() . "</SoldToPostalCode>\r\n";
        $xml .="<SoldToPriceProfile></SoldToPriceProfile>\r\n";
        $xml .="<SoldToState>" . $billingAddress->getRegion() . "</SoldToState>\r\n";
        $xml .="<SoldToTitle></SoldToTitle>\r\n";
        $xml .="<SubTotal>" . round($order->getOrderTotal(),2) . "</SubTotal>\r\n";
        $xml .="<Superceeded>0</Superceeded>\r\n";
        $xml .="<Terms>" . $paymentTerms . "</Terms>\r\n";
        $xml .="<TotalTax>" . round($tax,2) . "</TotalTax>\r\n";
        $xml .="<SoldToCMCallRecID></SoldToCMCallRecID>\r\n";
        $xml .="<GSTTaxRate></GSTTaxRate>\r\n";
        $xml .="</DocumentHeader>\r\n";
        $xml .="<DocumentItems>\r\n";


        // ITEMS
        $itemsGroupedPerCategory = array();
        if($order->getOrderParts()){
            foreach($order->getOrderParts() as $orderPart){
                $stockModel = new Core_Stock_Models_Stock();
                $productModel = new Core_Product_Models_Product();
                $productCategoryModel = new Core_Product_Models_ProductCategories();
                $stockId = (int) $orderPart->getStockId();
                $stock = $stockModel->findById($stockId);
                $productId = $stock->getProductId();
                $product = $productModel->findById($productId);
                $productCategory = $productCategoryModel->findByProductId($productId);
                $categoryId = reset($productCategory->getCategoryIds());
                $itemsGroupedPerCategory[$categoryId][] = $orderPart;
            }
        }
        $line_count = 1;
        foreach($itemsGroupedPerCategory as $categoryId => $orderParts){
            $categoryModel = new Core_Category_Models_Category();
            $category = $categoryModel->findById($categoryId);
            $xml .="<DocumentItem id=\"" . $line_count . "\">";
            $xml .="<AlternateExtendedCost>0</AlternateExtendedCost>";
            $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
            $xml .="<AlternateExtendedPrice>0</AlternateExtendedPrice>";
            $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
            $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
            $xml .="<AlternateUnitCost>0</AlternateUnitCost>";
            $xml .="<AlternateUnitList>0</AlternateUnitList>";
            $xml .="<AlternateUnitPrice>0</AlternateUnitPrice>";
            $xml .="<CloseProbability>0</CloseProbability>";
            $xml .="<CostModifier></CostModifier>";
            $xml .="<CustomDate01></CustomDate01>";
            $xml .="<CustomDate02></CustomDate02>";
            $xml .="<CustomMemo01></CustomMemo01>";
            $xml .="<CustomMemo02></CustomMemo02>";
            $xml .="<CustomNumber01>0</CustomNumber01>";
            $xml .="<CustomNumber02>0</CustomNumber02>";
            $xml .="<CustomNumber03>0</CustomNumber03>";
            $xml .="<CustomNumber04>0</CustomNumber04>";
            $xml .="<CustomNumber05>0</CustomNumber05>";
            $xml .="<CustomText01></CustomText01>";
            $xml .="<CustomText02></CustomText02>";
            $xml .="<CustomText03></CustomText03>";
            $xml .="<CustomText04></CustomText04>";
            $xml .="<CustomText05></CustomText05>";
            $xml .="<CustomText06></CustomText06>";
            $xml .="<CustomText07></CustomText07>";
            $xml .="<CustomText08></CustomText08>";
            $xml .="<CustomText09></CustomText09>";
            $xml .="<CustomText10></CustomText10>";
            $xml .="<CustomText11></CustomText11>";
            $xml .="<CustomText12></CustomText12>";
            $xml .="<CustomText13></CustomText13>";
            $xml .="<CustomText14></CustomText14>";
            $xml .="<Description>" . $category->getNameByLanguage() . "</Description>";
            $xml .="<ExtendedCost>0</ExtendedCost>";
            $xml .="<ExtendedList>0</ExtendedList>";
            $xml .="<ExtendedPrice>0</ExtendedPrice>";
            $xml .="<ExtendedWeight>0</ExtendedWeight>";
            $xml .="<Formula></Formula>";
            $xml .="<ItemType></ItemType>";
            $xml .="<LineAttributes>0</LineAttributes>";
            $xml .="<LineType>2</LineType>";
            $xml .="<Manufacturer></Manufacturer>";
            $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
            $xml .="<Notes></Notes>";
            $xml .="<OrderDate></OrderDate>";
            $xml .="<PONumber></PONumber>";
            $xml .="<PriceModifier></PriceModifier>";
            $xml .="<PriceProfile></PriceProfile>";
            $xml .="<QtyBase>1</QtyBase>";
            $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
            $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
            $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
            $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
            $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
            $xml .="<QtyTotal>1</QtyTotal>";
            $xml .="<SalesTax>0</SalesTax>";
            $xml .="<ShippingAmount>0</ShippingAmount>";
            $xml .="<SONumber></SONumber>";
            $xml .="<TaxCode>N</TaxCode>";
            $xml .="<UnitCost>0</UnitCost>";
            $xml .="<UnitList>0</UnitList>";
            $xml .="<UnitOfMeasure></UnitOfMeasure>";
            $xml .="<UnitOfMeasureFactor>1</UnitOfMeasureFactor>";
            $xml .="<UnitOfPricing></UnitOfPricing>";
            $xml .="<UnitOfPricingFactor>1</UnitOfPricingFactor>";
            $xml .="<UnitPrice>0</UnitPrice>";
            $xml .="<UnitWeight>0</UnitWeight>";
            $xml .="<Vendor></Vendor>";
            $xml .="<VendorPartNumber></VendorPartNumber>";
            $xml .="<ItemURL></ItemURL>";
            $xml .="</DocumentItem>";

            $line_count++;
            foreach ($orderParts as $orderPart){
                $xml .="<DocumentItem id=\"" . $line_count . "\">";
                $xml .="<AlternateExtendedCost>0</AlternateExtendedCost>";
                $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
                $extendedPriceEuro = $orderPart->getQuantity() * $orderPart->getPrice(); //convert to euro
                $xml .="<AlternateExtendedPrice>" . round($extendedPriceEuro,2) . "</AlternateExtendedPrice>";
                $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
                $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
                $xml .="<AlternateUnitCost>0</AlternateUnitCost>";
                $xml .="<AlternateUnitList>0</AlternateUnitList>";
                $xml .="<AlternateUnitPrice>" . round($orderPart->getPrice(),2) . "</AlternateUnitPrice>";
                $xml .="<CloseProbability>0</CloseProbability>";
                $xml .="<CostModifier></CostModifier>";
                $xml .="<CustomDate01></CustomDate01>";
                $xml .="<CustomDate02></CustomDate02>";
                $xml .="<CustomMemo01></CustomMemo01>";
                $xml .="<CustomMemo02></CustomMemo02>";
                $xml .="<CustomNumber01>0</CustomNumber01>";
                $xml .="<CustomNumber02>0</CustomNumber02>";
                $xml .="<CustomNumber03>0</CustomNumber03>";
                $xml .="<CustomNumber04>0</CustomNumber04>";
                $xml .="<CustomNumber05>0</CustomNumber05>";
                $xml .="<CustomText01></CustomText01>";
                $xml .="<CustomText02></CustomText02>";
                $xml .="<CustomText03></CustomText03>";
                $xml .="<CustomText04></CustomText04>";
                $xml .="<CustomText05></CustomText05>";
                $xml .="<CustomText06></CustomText06>";
                $xml .="<CustomText07></CustomText07>";
                $xml .="<CustomText08></CustomText08>";
                $xml .="<CustomText09></CustomText09>";
                $xml .="<CustomText10></CustomText10>";
                $xml .="<CustomText11></CustomText11>";
                $xml .="<CustomText12></CustomText12>";
                $xml .="<CustomText13></CustomText13>";
                $xml .="<CustomText14></CustomText14>";

                $product = $orderPart->getPartStockDetails()->product;
                $product .= ' - ' . $orderPart->getPartStockDetails()->colourway;
                $product .= ' ( ' . $orderPart->getPartStockDetails()->option .' ) ';
                if ($orderPart->getPartStockDetails()->colourway) {
                    $product .= ' - ' . $orderPart->getPartStockDetails()->colourway;
                }
                if ($orderPart->getPartStockDetails()->option) {
                    $product .= ' (' . $orderPart->getPartStockDetails()->option . ') x ' . $orderPart->getQuantity();
                } 
                else {
                    $product .= ' x ' . $orderPart->getQuantity();
                }

                $product = str_replace('&','&amp;',$product);

                $xml .="<Description>" . utf8_encode($product) . "</Description>";


                $xml .="<ExtendedCost>0</ExtendedCost>";
                $xml .="<ExtendedList>0</ExtendedList>";
                $extendedPrice = $orderPart->getQuantity() * $orderPart->getPrice();
                $xml .="<ExtendedPrice>" . round($extendedPrice,2) . "</ExtendedPrice>";
                $xml .="<ExtendedWeight>0</ExtendedWeight>";
                $xml .="<Formula></Formula>";
                $xml .="<ItemType></ItemType>";
                $xml .="<LineAttributes>0</LineAttributes>";
                $xml .="<LineType>8</LineType>";
                $xml .="<Manufacturer></Manufacturer>";
                $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
                $xml .="<Notes></Notes>";
                $xml .="<OrderDate></OrderDate>";
                $xml .="<PONumber></PONumber>";
                $xml .="<PriceModifier></PriceModifier>";
                $xml .="<PriceProfile></PriceProfile>";
                $xml .="<QtyBase>" . $orderPart->getQuantity(). "</QtyBase>";
                $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
                $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
                $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
                $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
                $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
                $xml .="<QtyTotal>" . $orderPart->getQuantity() . "</QtyTotal>";
                $xml .="<SalesTax>0</SalesTax>";
                $xml .="<ShippingAmount>0</ShippingAmount>";
                $xml .="<SONumber></SONumber>";
                $xml .="<TaxCode>Y</TaxCode>";
                $xml .="<UnitCost></UnitCost>";
                $xml .="<UnitList>0</UnitList>";
                $xml .="<UnitOfMeasure></UnitOfMeasure>";
                $xml .="<UnitOfMeasureFactor>1</UnitOfMeasureFactor>";
                $xml .="<UnitOfPricing></UnitOfPricing>";
                $xml .="<UnitOfPricingFactor>1</UnitOfPricingFactor>";
                $xml .="<UnitPrice>" . round($orderPart->getPrice(),2) . "</UnitPrice>";
                $xml .="<UnitWeight>0</UnitWeight>";
                $xml .="<Vendor></Vendor>";
                $xml .="<VendorPartNumber></VendorPartNumber>";
                $xml .="<ItemURL></ItemURL>";
                $xml .="</DocumentItem>";

                $line_count++;
            }
        }
        //$category_array = '';
        
//        $sql_cats = "SELECT * FROM `categories` 
//                     ORDER BY `category`";
//        $result_cats = $db->query($sql_cats);
//        while ($row_cats = $result_cats->fetch()) {
//
//            $category = str_replace('&','and',$row_cats['category']);
//
//            if (count($category_array) > 0) {
//
//                $used_products = implode(',',$category_array);
//
//                $sql_prods = "SELECT p.productid, p.name, s.option, c.name as colourway, o.* FROM orders o
//                              JOIN stock s ON s.stockid = o.stockid
//                              JOIN products p ON p.productid = s.productid
//                              JOIN productcategories pc ON pc.productid = p.productid
//                              LEFT JOIN colourways c ON c.colourwayid = s.colourway
//                              WHERE pc.categoryid = '" . $row_cats['categoryid'] . "' 
//                              AND o.allorderid = '" . $orderId . "'
//                              AND o.orderid NOT IN (" . $used_products . ")";
//
//            } else {
//
//                $sql_prods = "SELECT p.productid, p.name, s.option, c.name as colourway, o.* FROM orders o
//                              JOIN stock s ON s.stockid = o.stockid
//                              JOIN products p ON p.productid = s.productid
//                              JOIN productcategories pc ON pc.productid = p.productid
//                              LEFT JOIN colourways c ON c.colourwayid = s.colourway
//                              WHERE pc.categoryid = '" . $row_cats['categoryid'] . "' 
//                              AND o.allorderid = '" . $orderId . "'";
//
//            }
//
//
//            $result_prods = $db->query($sql_prods);
//
//            if ($result_prods->size() > 0) {
//
//
//                
//
//
//
//                while ($row_prods = $result_prods->fetch()) {
//
//                    
//                    $category_array[] = $row_prods['orderid'];
//
//                    // THIS IS WHERE IT NEEDS TO GO FOR VENDOR PRODUCTS
//                    //var_dump($row_prods);
//                    $vendor_count = 0;
//
//                    $sql_vendors = "SELECT * FROM `productvendorproducts` WHERE `product` = '" . $row_prods['productid'] . "' ORDER BY `order`";
//
//                    $result_vendors = $db->query($sql_vendors);
//                    //echo '<p>'.$sql_vendors.'</p>';
//                    while ($row_vendors = $result_vendors->fetch()) {
//
//
//                        if ($row_vendors['vendorproduct'] > 0) {
//
//                            $sql_vp = "SELECT vm.name as manufacturer, v.* FROM vendorproducts v
//                                        LEFT JOIN vendormanufacturers vm on vm.manufacturerid = v.manufacturerid
//                                        WHERE vendorid = '" . $row_vendors['vendorproduct'] . "'";
//                            $result_vp = $db->query($sql_vp);
//                            $row_vp = $result_vp->fetch();
//
//                        //	echo '<p>'.$sql_vp.'</p>';
//
//                            if ($vendor_count == 0) {
//
//                                $vendor_unit_price = $price_gbp;
//                                $vendor_alternate_price = $price_eur;
//                                $vendor_extended_price = $vendor_unit_price * $row_prods['quantity'];
//                                $vendor_alternate_extended_price = $vendor_alternate_price * $row_prods['quantity'];
//
//                                $product = $row_vp['description'];
//
//                                if ($row_prods['colourway']) {
//                                    $product .= ' - ' . $row_prods['colourway'];
//                                }
//                                // PRODUCT
//                                if ($row_prods['option'] == 'NA') {
//
//                                    $product .= ' x ' . $row_prods['quantity'];
//
//                                } else {
//
//                                    $product .= ' (' . $row_prods['option'] . ') x ' . $row_prods['quantity'];
//
//                                }
//
//                                // EXTRA OPTIONS
//                                $sql_options = "SELECT * FROM orderoptions oo 
//                                                JOIN productextras pe ON pe.extraid = oo.extraid
//                                                WHERE oo.orderid = '" . $row_prods['orderid'] . "' 
//                                                AND pe.reference != 'bagged'
//                                                ORDER BY oo.order";
//                                $result_options = $db->query($sql_options);
//
//                                while ($row_options = $result_options->fetch()) {
//
//                                        if (strlen($row_options['reference']) > 1) {
//
//                                            $product .= ', ' . $row_options['reference'];
//
//                                        } else {
//
//                                            $product .= ', ' .  $row_options['text'];
//
//                                        }
//
//                                            $product .= ': ' . $row_options['answer'];
//
//                                }
//
//
//                                $product = str_replace('&','&amp;',$product);
//
//                            } else {
//
//                            //	$vendor_unit_cost = 0;
//                                $vendor_unit_price = 0;
//                                $vendor_alternate_price = 0;
//                                $vendor_extended_price = 0;
//                                $vendor_alternate_extended_price = 0;
//
//                                $product = $row_vp['description'];
//                            }
//
//                            // WHAT IS THE VENDOR EXTENDED COST
//                            $vendor_extended_cost = $row_vp['unitCost'] * $row_prods['quantity'];
//
//                            // WHAT IS THE VENDOR UNIT COST
//                            $vendor_unit_cost = $row_vp['unitCost'];
//
//                            // WHAT IS THE VENDOR EXTENDED COST
//                            $vendor_extended_cost = $row_vp['unitCost'] * $row_prods['quantity'];
//
//                            // WHAT IS THE ALTERNATE VENDOR EXTENDED COST
//                            $vendor_alternate_extended_cost = $vendor_extended_cost * $currencyRate;
//
//                            // WHAT IS THE ALTERNATE VENDOR UNIT COST
//                            $vendor_alternate_unit_cost = $vendor_unit_cost * $currencyRate;
//
//                            //customMemo01
//                            $xml .="<DocumentItem id=\"" . $line_count . "\">";
//                            $xml .="<AlternateExtendedCost>" . round($vendor_alternate_extended_cost,2) . "</AlternateExtendedCost>";
//                            $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
//                            $xml .="<AlternateExtendedPrice>" . round($vendor_alternate_extended_price,2) . "</AlternateExtendedPrice>";
//                            $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
//                            $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
//                            $xml .="<AlternateUnitCost>" . round($vendor_alternate_unit_cost,2) . "</AlternateUnitCost>";
//                            $xml .="<AlternateUnitList>0</AlternateUnitList>";
//                            $xml .="<AlternateUnitPrice>" . round($vendor_alternate_price,2) . "</AlternateUnitPrice>";
//                            $xml .="<CloseProbability>0</CloseProbability>";
//                            $xml .="<CostModifier></CostModifier>";
//                            $xml .="<CustomDate01></CustomDate01>";
//                            $xml .="<CustomDate02></CustomDate02>";
//                            $xml .="<CustomMemo01>" . $row_vp['customMemo01'] . "</CustomMemo01>";
//                            $xml .="<CustomMemo02></CustomMemo02>";
//                            $xml .="<CustomNumber01>0</CustomNumber01>";
//                            $xml .="<CustomNumber02></CustomNumber02>";
//                            $xml .="<CustomNumber03></CustomNumber03>";
//                            $xml .="<CustomNumber04>0</CustomNumber04>";
//                            $xml .="<CustomNumber05>0</CustomNumber05>";
//                            $xml .="<CustomText01></CustomText01>";
//                            $xml .="<CustomText02></CustomText02>";
//                            $xml .="<CustomText03>" . $row_vp['jobsheet'] . "</CustomText03>";
//                            $xml .="<CustomText04></CustomText04>";
//                            $xml .="<CustomText05></CustomText05>";
//                            $xml .="<CustomText06></CustomText06>";
//                            $xml .="<CustomText07></CustomText07>";
//                            $xml .="<CustomText08></CustomText08>";
//                            $xml .="<CustomText09></CustomText09>";
//                            $xml .="<CustomText10></CustomText10>";
//                            $xml .="<CustomText11></CustomText11>";
//                            $xml .="<CustomText12></CustomText12>";
//                            $xml .="<CustomText13></CustomText13>";
//                            $xml .="<CustomText14></CustomText14>";
//
//
//                            $xml .="<Description>" . utf8_encode($product) . "</Description>";
//                            $xml .="<ExtendedCost>" . round($vendor_extended_cost,2) . "</ExtendedCost>";
//                            $xml .="<ExtendedList>0</ExtendedList>";
//                            $xml .="<ExtendedPrice>" . round($vendor_extended_price,2) . "</ExtendedPrice>";
//                            $xml .="<ExtendedWeight>0</ExtendedWeight>";
//                            $xml .="<Formula></Formula>";
//                            $xml .="<ItemType></ItemType>";
//                            $xml .="<LineAttributes>14</LineAttributes>";
//                            $xml .="<LineType>1</LineType>";
//                            $xml .="<Manufacturer></Manufacturer>";
//                            $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
//                            $xml .="<Notes></Notes>";
//                            $order_date = date("Y-m-d",mktime( date('H'), date('i'), 0, date('m'), date('d') + 1, date('Y')));
//                            $xml .="<OrderDate>" . $order_date . "</OrderDate>";
//                            $xml .="<PONumber></PONumber>";
//                            $xml .="<PriceModifier></PriceModifier>";
//                            $xml .="<PriceProfile></PriceProfile>";
//                            $xml .="<QtyBase>1</QtyBase>";
//                            $xml .="<QtyGroupMultiplier>" . $row_prods['quantity'] . "</QtyGroupMultiplier>";
//                            $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
//                            $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
//                            $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
//                            $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
//                            $xml .="<QtyTotal>" . $row_prods['quantity'] . "</QtyTotal>";
//                            $xml .="<SalesTax>0</SalesTax>";
//                            $xml .="<ShippingAmount>0</ShippingAmount>";
//                            $xml .="<SONumber></SONumber>";
//                            $xml .="<TaxCode>Y</TaxCode>";
//                            $xml .="<UnitCost>" . $vendor_unit_cost . "</UnitCost>";
//                            $xml .="<UnitList>0</UnitList>";
//                            $xml .="<UnitOfMeasure>" . $row_vp['unitOfMeasure'] . "</UnitOfMeasure>";
//                            $xml .="<UnitOfMeasureFactor>" . $row_vp['unitOfMeasureFactor'] . "</UnitOfMeasureFactor>";
//                            $xml .="<UnitOfPricing>" . $row_vp['unitOfPricing'] . "</UnitOfPricing>";
//                            $xml .="<UnitOfPricingFactor>" . $row_vp['unitOfPricingFactor'] . "</UnitOfPricingFactor>";
//                            $xml .="<UnitPrice>" . round($vendor_unit_price,2) . "</UnitPrice>";
//                            $xml .="<UnitWeight>0</UnitWeight>";
//                            $vendor = str_replace('&','&amp;',$row_vp['manufacturer']);
//                            $xml .="<Vendor>" . utf8_encode($vendor) . "</Vendor>";
//                            $vendorPartNo = str_replace('&','&amp;',$row_vp['vendorPartNo']);
//                            $xml .="<VendorPartNumber>" . utf8_encode($vendorPartNo) . "</VendorPartNumber>";
//                            $xml .="<ItemURL></ItemURL>";
//                            $xml .="</DocumentItem>";
//
//                            $line_count++;
//                            $vendor_count++;
//
//                        }
//
//
//                    }
//
//                }
//
//            }
//
//        }

        // THEN DELIVERY
        $xml .="<DocumentItem id=\"" . $line_count . "\">";
        $xml .="<AlternateExtendedCost>0</AlternateExtendedCost>";
        $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
        $xml .="<AlternateExtendedPrice>0</AlternateExtendedPrice>";
        $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
        $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
        $xml .="<AlternateUnitCost>0</AlternateUnitCost>";
        $xml .="<AlternateUnitList>0</AlternateUnitList>";
        $xml .="<AlternateUnitPrice>0</AlternateUnitPrice>";
        $xml .="<CloseProbability>0</CloseProbability>";
        $xml .="<CostModifier></CostModifier>";
        $xml .="<CustomDate01></CustomDate01>";
        $xml .="<CustomDate02></CustomDate02>";
        $xml .="<CustomMemo01></CustomMemo01>";
        $xml .="<CustomMemo02></CustomMemo02>";
        $xml .="<CustomNumber01>0</CustomNumber01>";
        $xml .="<CustomNumber02>0</CustomNumber02>";
        $xml .="<CustomNumber03>0</CustomNumber03>";
        $xml .="<CustomNumber04>0</CustomNumber04>";
        $xml .="<CustomNumber05>0</CustomNumber05>";
        $xml .="<CustomText01></CustomText01>";
        $xml .="<CustomText02></CustomText02>";
        $xml .="<CustomText03></CustomText03>";
        $xml .="<CustomText04></CustomText04>";
        $xml .="<CustomText05></CustomText05>";
        $xml .="<CustomText06></CustomText06>";
        $xml .="<CustomText07></CustomText07>";
        $xml .="<CustomText08></CustomText08>";
        $xml .="<CustomText09></CustomText09>";
        $xml .="<CustomText10></CustomText10>";
        $xml .="<CustomText11></CustomText11>";
        $xml .="<CustomText12></CustomText12>";
        $xml .="<CustomText13></CustomText13>";
        $xml .="<CustomText14></CustomText14>";
        $xml .="<Description>Delivery Cost</Description>";
        $xml .="<ExtendedCost>0</ExtendedCost>";
        $xml .="<ExtendedList>0</ExtendedList>";
        $xml .="<ExtendedPrice>0</ExtendedPrice>";
        $xml .="<ExtendedWeight>0</ExtendedWeight>";
        $xml .="<Formula></Formula>";
        $xml .="<ItemType></ItemType>";
        $xml .="<LineAttributes>0</LineAttributes>";
        $xml .="<LineType>2</LineType>";
        $xml .="<Manufacturer></Manufacturer>";
        $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
        $xml .="<Notes></Notes>";
        $xml .="<OrderDate></OrderDate>";
        $xml .="<PONumber></PONumber>";
        $xml .="<PriceModifier></PriceModifier>";
        $xml .="<PriceProfile></PriceProfile>";
        $xml .="<QtyBase>1</QtyBase>";
        $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
        $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
        $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
        $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
        $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
        $xml .="<QtyTotal>1</QtyTotal>";
        $xml .="<SalesTax>0</SalesTax>";
        $xml .="<ShippingAmount>0</ShippingAmount>";
        $xml .="<SONumber></SONumber>";
        $xml .="<TaxCode>N</TaxCode>";
        $xml .="<UnitCost>0</UnitCost>";
        $xml .="<UnitList>0</UnitList>";
        $xml .="<UnitOfMeasure></UnitOfMeasure>";
        $xml .="<UnitOfMeasureFactor>1</UnitOfMeasureFactor>";
        $xml .="<UnitOfPricing></UnitOfPricing>";
        $xml .="<UnitOfPricingFactor>1</UnitOfPricingFactor>";
        $xml .="<UnitPrice>0</UnitPrice>";
        $xml .="<UnitWeight>0</UnitWeight>";
        $xml .="<Vendor></Vendor>";
        $xml .="<VendorPartNumber></VendorPartNumber>";
        $xml .="<ItemURL></ItemURL>";
        $xml .="</DocumentItem>";

        $line_count++;


        $product = 'Shipping Costs';




        if ($order->getCurrencyId() == '2') {

            $priceEur = $order->getDeliveryCharge();
            $priceGbp = $order->getDeliveryCharge() / $currencyRate;

        } else {

            $priceEur = $order->getDeliveryCharge() * $currencyRate;
            $priceGbp = $order->getDeliveryCharge();

        }


        $vat = $order->getVatCharge();
        //customMemo01
        $xml .="<DocumentItem id=\"" . $line_count . "\">";
        $xml .="<AlternateExtendedCost>" . round($priceEur,2) . "</AlternateExtendedCost>";
        $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
        $xml .="<AlternateExtendedPrice>" . round($priceEur,2) . "</AlternateExtendedPrice>";
        $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
        $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
        $xml .="<AlternateUnitCost>" . round($priceEur,2) . "</AlternateUnitCost>";
        $xml .="<AlternateUnitList>0</AlternateUnitList>";
        $xml .="<AlternateUnitPrice>" . round($priceEur,2) . "</AlternateUnitPrice>";
        $xml .="<CloseProbability>0</CloseProbability>";
        $xml .="<CostModifier></CostModifier>";
        $xml .="<CustomDate01></CustomDate01>";
        $xml .="<CustomDate02></CustomDate02>";
        $xml .="<CustomMemo01></CustomMemo01>";
        $xml .="<CustomMemo02></CustomMemo02>";
        $xml .="<CustomNumber01>0</CustomNumber01>";
        $xml .="<CustomNumber02>0</CustomNumber02>";
        $xml .="<CustomNumber03>0</CustomNumber03>";
        $xml .="<CustomNumber04>0</CustomNumber04>";
        $xml .="<CustomNumber05>0</CustomNumber05>";
        $xml .="<CustomText01></CustomText01>";
        $xml .="<CustomText02></CustomText02>";
        $xml .="<CustomText03></CustomText03>";
        $xml .="<CustomText04></CustomText04>";
        $xml .="<CustomText05></CustomText05>";
        $xml .="<CustomText06></CustomText06>";
        $xml .="<CustomText07></CustomText07>";
        $xml .="<CustomText08></CustomText08>";
        $xml .="<CustomText09></CustomText09>";
        $xml .="<CustomText10></CustomText10>";
        $xml .="<CustomText11></CustomText11>";
        $xml .="<CustomText12></CustomText12>";
        $xml .="<CustomText13></CustomText13>";
        $xml .="<CustomText14></CustomText14>";




        $xml .="<Description>" . utf8_encode($product) . "</Description>";
        $xml .="<ExtendedCost>" . round($priceGbp,2) . "</ExtendedCost>";
        $xml .="<ExtendedList>0</ExtendedList>";
        $xml .="<ExtendedPrice>" . round($priceGbp,2) . "</ExtendedPrice>";
        $xml .="<ExtendedWeight>0</ExtendedWeight>";
        $xml .="<Formula></Formula>";
        $xml .="<ItemType></ItemType>";
        $xml .="<LineAttributes>0</LineAttributes>";
        $xml .="<LineType>1</LineType>";
        $xml .="<Manufacturer></Manufacturer>";
        $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
        $xml .="<Notes></Notes>";
        $orderDate = date("Y-m-d",mktime( date('H'), date('i'), 0, date('m'), date('d') + 1, date('Y')));
        $xml .="<OrderDate>" . $orderDate . "</OrderDate>";
        $xml .="<PONumber></PONumber>";
        $xml .="<PriceModifier></PriceModifier>";
        $xml .="<PriceProfile></PriceProfile>";
        $xml .="<QtyBase>1</QtyBase>";
        $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
        $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
        $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
        $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
        $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
        $xml .="<QtyTotal>1</QtyTotal>";
        $xml .="<SalesTax></SalesTax>";
        $xml .="<ShippingAmount>0</ShippingAmount>";
        $xml .="<SONumber></SONumber>";
        $xml .="<TaxCode>Y</TaxCode>";
        $xml .="<UnitCost>5.00</UnitCost>";
        $xml .="<UnitList>0</UnitList>";
        $xml .="<UnitOfMeasure></UnitOfMeasure>";
        $xml .="<UnitOfMeasureFactor></UnitOfMeasureFactor>";
        $xml .="<UnitOfPricing></UnitOfPricing>";
        $xml .="<UnitOfPricingFactor></UnitOfPricingFactor>";
        $xml .="<UnitPrice>" . round($priceGbp,2) . "</UnitPrice>";
        $xml .="<UnitWeight>0</UnitWeight>";
        $xml .="<Vendor>Shipping</Vendor>";
        $xml .="<VendorPartNumber>Shipping</VendorPartNumber>";
        $xml .="<ItemURL></ItemURL>";
        $xml .="</DocumentItem>";

        $line_count++;

//        if ($row_user['creditinvoice'] != 'yes') {
//
//            if ($row_allorder['leftToPay'] < $row_allorder['total']) {
//
//                $voucher_value = $row_allorder['total'] - $row_allorder['leftToPay'];
//                $voucher_value = 0 - $voucher_value;
//
//                $xml .="<DocumentItem id=\"" . $line_count . "\">";
//                $xml .="<AlternateExtendedCost>0</AlternateExtendedCost>";
//                $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
//                $xml .="<AlternateExtendedPrice>0</AlternateExtendedPrice>";
//                $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
//                $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
//                $xml .="<AlternateUnitCost>0</AlternateUnitCost>";
//                $xml .="<AlternateUnitList>0</AlternateUnitList>";
//                $xml .="<AlternateUnitPrice>0</AlternateUnitPrice>";
//                $xml .="<CloseProbability>0</CloseProbability>";
//                $xml .="<CostModifier></CostModifier>";
//                $xml .="<CustomDate01></CustomDate01>";
//                $xml .="<CustomDate02></CustomDate02>";
//                $xml .="<CustomMemo01></CustomMemo01>";
//                $xml .="<CustomMemo02></CustomMemo02>";
//                $xml .="<CustomNumber01>0</CustomNumber01>";
//                $xml .="<CustomNumber02>0</CustomNumber02>";
//                $xml .="<CustomNumber03>0</CustomNumber03>";
//                $xml .="<CustomNumber04>0</CustomNumber04>";
//                $xml .="<CustomNumber05>0</CustomNumber05>";
//                $xml .="<CustomText01></CustomText01>";
//                $xml .="<CustomText02></CustomText02>";
//                $xml .="<CustomText03></CustomText03>";
//                $xml .="<CustomText04></CustomText04>";
//                $xml .="<CustomText05></CustomText05>";
//                $xml .="<CustomText06></CustomText06>";
//                $xml .="<CustomText07></CustomText07>";
//                $xml .="<CustomText08></CustomText08>";
//                $xml .="<CustomText09></CustomText09>";
//                $xml .="<CustomText10></CustomText10>";
//                $xml .="<CustomText11></CustomText11>";
//                $xml .="<CustomText12></CustomText12>";
//                $xml .="<CustomText13></CustomText13>";
//                $xml .="<CustomText14></CustomText14>";
//                $xml .="<Description>Voucher</Description>";
//                $xml .="<ExtendedCost>0</ExtendedCost>";
//                $xml .="<ExtendedList>0</ExtendedList>";
//                $xml .="<ExtendedPrice>0</ExtendedPrice>";
//                $xml .="<ExtendedWeight>0</ExtendedWeight>";
//                $xml .="<Formula></Formula>";
//                $xml .="<ItemType></ItemType>";
//                $xml .="<LineAttributes>0</LineAttributes>";
//                $xml .="<LineType>2</LineType>";
//                $xml .="<Manufacturer></Manufacturer>";
//                $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
//                $xml .="<Notes></Notes>";
//                $xml .="<OrderDate></OrderDate>";
//                $xml .="<PONumber></PONumber>";
//                $xml .="<PriceModifier></PriceModifier>";
//                $xml .="<PriceProfile></PriceProfile>";
//                $xml .="<QtyBase>1</QtyBase>";
//                $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
//                $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
//                $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
//                $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
//                $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
//                $xml .="<QtyTotal>1</QtyTotal>";
//                $xml .="<SalesTax>0</SalesTax>";
//                $xml .="<ShippingAmount>0</ShippingAmount>";
//                $xml .="<SONumber></SONumber>";
//                $xml .="<TaxCode>N</TaxCode>";
//                $xml .="<UnitCost>0</UnitCost>";
//                $xml .="<UnitList>0</UnitList>";
//                $xml .="<UnitOfMeasure></UnitOfMeasure>";
//                $xml .="<UnitOfMeasureFactor>1</UnitOfMeasureFactor>";
//                $xml .="<UnitOfPricing></UnitOfPricing>";
//                $xml .="<UnitOfPricingFactor>1</UnitOfPricingFactor>";
//                $xml .="<UnitPrice>0</UnitPrice>";
//                $xml .="<UnitWeight>0</UnitWeight>";
//                $xml .="<Vendor></Vendor>";
//                $xml .="<VendorPartNumber></VendorPartNumber>";
//                $xml .="<ItemURL></ItemURL>";
//                $xml .="</DocumentItem>";
//
//                $line_count++;
//
//
//
//
//                $product = $companyName.' E-Voucher';
//
//
//
//
//
//                if ($row_user['currency'] == 'eur') {
//
//                    $price_eur = $voucher_value;
//                    $price_gbp = $voucher_value / $currencyRate;
//
//                } else {
//
//                    $price_eur = $voucher_value * $currencyRate;
//                    $price_gbp = $voucher_value;
//
//                }
//
//
//                //customMemo01
//                $xml .="<DocumentItem id=\"" . $line_count . "\">";
//                $xml .="<AlternateExtendedCost>" . round($price_eur,2) . "</AlternateExtendedCost>";
//                $xml .="<AlternateExtendedList>0</AlternateExtendedList>";
//                $xml .="<AlternateExtendedPrice>" . round($price_eur,2) . "</AlternateExtendedPrice>";
//                $xml .="<AlternateSalesTax>0</AlternateSalesTax>";
//                $xml .="<AlternateShippingAmount>0</AlternateShippingAmount>";
//                $xml .="<AlternateUnitCost>" . round($price_eur,2) . "</AlternateUnitCost>";
//                $xml .="<AlternateUnitList>0</AlternateUnitList>";
//                $xml .="<AlternateUnitPrice>" . round($price_eur,2) . "</AlternateUnitPrice>";
//                $xml .="<CloseProbability>0</CloseProbability>";
//                $xml .="<CostModifier></CostModifier>";
//                $xml .="<CustomDate01></CustomDate01>";
//                $xml .="<CustomDate02></CustomDate02>";
//                $xml .="<CustomMemo01></CustomMemo01>";
//                $xml .="<CustomMemo02></CustomMemo02>";
//                $xml .="<CustomNumber01>0</CustomNumber01>";
//                $xml .="<CustomNumber02>0</CustomNumber02>";
//                $xml .="<CustomNumber03>0</CustomNumber03>";
//                $xml .="<CustomNumber04>0</CustomNumber04>";
//                $xml .="<CustomNumber05>0</CustomNumber05>";
//                $xml .="<CustomText01></CustomText01>";
//                $xml .="<CustomText02></CustomText02>";
//                $xml .="<CustomText03></CustomText03>";
//                $xml .="<CustomText04></CustomText04>";
//                $xml .="<CustomText05></CustomText05>";
//                $xml .="<CustomText06></CustomText06>";
//                $xml .="<CustomText07></CustomText07>";
//                $xml .="<CustomText08></CustomText08>";
//                $xml .="<CustomText09></CustomText09>";
//                $xml .="<CustomText10></CustomText10>";
//                $xml .="<CustomText11></CustomText11>";
//                $xml .="<CustomText12></CustomText12>";
//                $xml .="<CustomText13></CustomText13>";
//                $xml .="<CustomText14></CustomText14>";
//
//
//
//
//                $xml .="<Description>" . utf8_encode($product) . "</Description>";
//                $xml .="<ExtendedCost>" . round($price_gbp,2) . "</ExtendedCost>";
//                $xml .="<ExtendedList>0</ExtendedList>";
//                $xml .="<ExtendedPrice>" . round($price_gbp,2) . "</ExtendedPrice>";
//                $xml .="<ExtendedWeight>0</ExtendedWeight>";
//                $xml .="<Formula></Formula>";
//                $xml .="<ItemType></ItemType>";
//                $xml .="<LineAttributes>0</LineAttributes>";
//                $xml .="<LineType>1</LineType>";
//                $xml .="<Manufacturer></Manufacturer>";
//                $xml .="<ManufacturerPartNumber></ManufacturerPartNumber>";
//                $xml .="<Notes></Notes>";
//                $order_date = date("Y-m-d",mktime( date('H'), date('i'), 0, date('m'), date('d') + 1, date('Y')));
//                $xml .="<OrderDate>" . $order_date . "</OrderDate>";
//                $xml .="<PONumber></PONumber>";
//                $xml .="<PriceModifier></PriceModifier>";
//                $xml .="<PriceProfile></PriceProfile>";
//                $xml .="<QtyBase>1</QtyBase>";
//                $xml .="<QtyGroupMultiplier>1</QtyGroupMultiplier>";
//                $xml .="<QtyMultiplier1>1</QtyMultiplier1>";
//                $xml .="<QtyMultiplier2>1</QtyMultiplier2>";
//                $xml .="<QtyMultiplier3>1</QtyMultiplier3>";
//                $xml .="<QtyMultiplier4>1</QtyMultiplier4>";
//                $xml .="<QtyTotal>1</QtyTotal>";
//                $xml .="<SalesTax></SalesTax>";
//                $xml .="<ShippingAmount>0</ShippingAmount>";
//                $xml .="<SONumber></SONumber>";
//                $xml .="<TaxCode>Y</TaxCode>";
//                $xml .="<UnitCost>5.00</UnitCost>";
//                $xml .="<UnitList>0</UnitList>";
//                $xml .="<UnitOfMeasure></UnitOfMeasure>";
//                $xml .="<UnitOfMeasureFactor></UnitOfMeasureFactor>";
//                $xml .="<UnitOfPricing></UnitOfPricing>";
//                $xml .="<UnitOfPricingFactor></UnitOfPricingFactor>";
//                $xml .="<UnitPrice>" . round($price_gbp,2) . "</UnitPrice>";
//                $xml .="<UnitWeight>0</UnitWeight>";
//                $xml .="<Vendor>Shipping</Vendor>";
//                $xml .="<VendorPartNumber>Shipping</VendorPartNumber>";
//                $xml .="<ItemURL></ItemURL>";
//                $xml .="</DocumentItem>";
//
//                $line_count++;
//
//
//            }
//        }

        $xml .="</DocumentItems>\r\n";

        $xml .="</Document>\r\n";
        $xml .="</Documents>\r\n";
        $xml .="</QuoteWerksXML>\r\n";

        return $xml;
    }
    
public function buildXML(Core_Order_Models_Order $order){
    if(!$order instanceof Core_Order_Models_Order){
        throw new Exception('Order passed is not an instance of its module');
    }

    $settings = new Core_Settings_Models_Settings();
    $currencyMapper = new Core_Currency_Models_CurrencyMapper();
    $usergroupModel = new Core_Usergroup_Models_Usergroup();
    $addressModel = new Core_Address_Models_Address();

    $user = Core_User_Models_User::getCurrent();
    if(!$user instanceof Core_User_Models_User){
        throw new Exception ('User passed is not an instance of the its module');
    }
    $usergroup = $usergroupModel->findById($user->getUserGroupId());
    $billingAddress = $addressModel->findById($order->getBillingAddressId());
    $deliveryAddress = $addressModel->findById($order->getDeliveryAddressId());
    $orderId = $order->getId();
    $vatDecimal = 0;
    $vatRate = 0;
    $currencyRate = 1.23;
    $dueDays = 14;

    if ($order->getVatCharge() > 0) {
        $vatDecimal = 1.20;
    }

    $createdUser = $user->getForename() . ' ' . $user->getSurname();
    $company = $settings->getCompanyName();
    $companystr = $settings->getOrderNumberPrefix();
    $totalAmount = $order->getOrderTotal();
    $outstandingAmount = $order->getAmountOutstanding();

    if ($outstandingAmount == 0.00) {
        $docStatus = '10. Order Paid - Needs Invoicing';
    } 
    else {
        if ($totalAmount == $outstandingAmount) {
            $docStatus = '12. Full Payment Required';
        } 
        else {

            $docStatus = '11. Part Payment Required';
        }
    }

    $outstanding = '';
    if ($order->getPaymentType()) {
        $outstanding = 'Outstanding amount to be paid by:  ' . $order->getPaymentType();
    }

    $currencyId = $order->getCurrencyId();
    $currency = $currencyMapper->find($currencyId, new Core_Currency_Models_Currency);
    $paymentTerms = 'Order Total: ' . $totalAmount . ', Outstanding: ' . $outstandingAmount . ', '. $outstanding;
    $internalNotes = "Order Total: " . $totalAmount . "\r\nOutstanding: " . $outstandingAmount . "\r\n". $outstanding . 
    "\r\nOrder Currency: " . $currency->getCurrency() . 
    "\r\nInvoice Currency: " . $usergroup->getCurrencyInvoice() . 
    "\r\nInvoice Type: " . $usergroup->getInvoiceType();

    $grandTotal = $order->getOrderTotal();
    $tax = $order->getVatCharge();

    if ($currency->getCurrency() == 'eur') {

        $grandTotal = $grandTotal / $currencyRate;
        $tax = $tax / $currencyRate;

    }
    $ordernumber = str_replace($companystr,'',$order->getOrderNumber());
$xml = <<<EOT
<?xml version="1.0" encoding="UTF-8" ?>
    <QuoteWerksXML>
        <AppVersionMajor>{$this->_version}</AppVersionMajor>
        <AppVersionMinor>{$this->_versionMinor}</AppVersionMinor>
        <FileFormat>{$this->_fileFormat}</FileFormat>
        <DecimalSymbol>.</DecimalSymbol>
        <DateTimeStandard>ISO 8601</DateTimeStandard>
        <Created>2010-07-17T11-29-22Z</Created>
        <Comments></Comments>
        <Documents>
        <Document id="1">
        <DocumentHeader>
        <AlternateCurrency>EUR-LIFEFIT  2000-05-09</AlternateCurrency>
        <AlternateGrandTotal>304.79</AlternateGrandTotal>
        <AlternateGSTTax>0</AlternateGSTTax>
        <AlternateLocalTax>0</AlternateLocalTax>
        <AlternateShippingAmount>0</AlternateShippingAmount>
        <AlternateSubTotal>374.89</AlternateSubTotal>
        <AlternateTotalTax>0</AlternateTotalTax>
        <BillToAddress1>SiemensstraÎ²e3</BillToAddress1>
        <BillToAddress2>85716 UnterschleiÎ²heim</BillToAddress2>
        <BillToAddress3></BillToAddress3>
        <BillToCity></BillToCity>
        <BillToCMAccountNo></BillToCMAccountNo>
        <BillToCompany>Life Fitness GMBH</BillToCompany>
        <BillToContact>Stefan Geier</BillToContact>
        <BillToCountry>Germany</BillToCountry>
        <BillToEmail></BillToEmail>
        <BillToFax></BillToFax>
        <BillToFaxExt></BillToFaxExt>
        <BillToPhone></BillToPhone>
        <BillToPhoneExt></BillToPhoneExt>
        <BillToPostalCode></BillToPostalCode>
        <BillToState></BillToState>
        <BillToTitle></BillToTitle>
        <ClosingNotes></ClosingNotes>
        <CommissionAmount>0</CommissionAmount>
        <CommissionStructure>Structure1 0</CommissionStructure>
        <ConvertedBy>Charlotte Hooper</ConvertedBy>
        <ConvertedOn>2010-07-17T11-29-22Z</ConvertedOn>
        <ConvertedRef></ConvertedRef>
        <Created>2010-07-17T11-29-22Z</Created>
        <CreatedBy>Charlotte Hooper</CreatedBy>
        <CustomDate01></CustomDate01>
        <CustomDate02></CustomDate02>
        <CustomNumber01></CustomNumber01>
        <CustomNumber02></CustomNumber02>
        <CustomText01></CustomText01>
        <CustomText02>6405019446</CustomText02>
        <CustomText03></CustomText03>
        <CustomText04></CustomText04>
        <CustomText05></CustomText05>
        <CustomText06></CustomText06>
        <CustomText07></CustomText07>
        <CustomText08></CustomText08>
        <CustomText09></CustomText09>
        <CustomText10></CustomText10>
        <CustomText11></CustomText11>
        <CustomText12></CustomText12>
        <CustomText13></CustomText13>
        <CustomText14></CustomText14>
        <CustomText15></CustomText15>
        <CustomText16>Health &amp; Fitness</CustomText16>
        <CustomText17></CustomText17>
        <CustomText18>Other</CustomText18>
        <CustomText19></CustomText19>
        <CustomText20>Customer</CustomText20>
        <DocDate>2010-07-17T11-29-22Z</DocDate>
        <DocDueDate>2014-01-21T11-29-22Z</DocDueDate>
        <DocName>Life Fitness - Sebastiano De Paola - lfitness6405019446</DocName>
        <DocNo></DocNo>
        <DocStatus>10. Order Paid - Needs Invoicing</DocStatus>
        <DocType>ORDER</DocType>
        <ExchangeRate>1</ExchangeRate>
        <FOB>Ex Works</FOB>
        <GrandTotal>304.79</GrandTotal>
        <GSTTax>0</GSTTax>
        <IntegrationData></IntegrationData>
        <InternalNotes>Order Total: 374.89
        Outstanding: 0.00

        Order Currency: eur
        Invoice Currency: gbp
        Invoice Type: proforma</InternalNotes>
        <IntroductionNotes></IntroductionNotes>
        <LastModified></LastModified>
        <LocalTax>0</LocalTax>
        <LocalTaxRate>0</LocalTaxRate>
        <Locked>no</Locked>
        <ProfitAmount></ProfitAmount>
        <ProjectNo></ProjectNo>
        <PSTIsCompounded>0</PSTIsCompounded>
        <PurchasingNotes></PurchasingNotes>
        <RevisionMasterDocNo></RevisionMasterDocNo>
        <SalesRep>Charlotte Hooper</SalesRep>
        <ShippingAmount>0</ShippingAmount>
        <ShipToAddress1>Horebstrasse 11</ShipToAddress1>
        <ShipToAddress2></ShipToAddress2>
        <ShipToAddress3></ShipToAddress3>
        <ShipToCity>Eschenbach</ShipToCity>
        <ShipToCMAccountNo></ShipToCMAccountNo>
        <ShipToCompany></ShipToCompany>
        <ShipToContact>Sebastiano De Paola</ShipToContact>
        <ShipToCountry>Switzerland</ShipToCountry>
        <ShipToEmail>sebastiano.depaola@lifefitness.com</ShipToEmail>
        <ShipToFax></ShipToFax>
        <ShipToFaxExt></ShipToFaxExt>
        <ShipToPhone></ShipToPhone>
        <ShipToPhoneExt></ShipToPhoneExt>
        <ShipToPostalCode>8733</ShipToPostalCode>
        <ShipToState></ShipToState>
        <ShipToTitle></ShipToTitle>
        <ShipVia>TBC</ShipVia>
        <SoldToAddress1>Horebstrasse 11</SoldToAddress1>
        <SoldToAddress2></SoldToAddress2>
        <SoldToAddress3></SoldToAddress3>
        <SoldToCity>Eschenbach</SoldToCity>
        <SoldToCMAccountNo></SoldToCMAccountNo>
        <SoldToCMLinkRecID></SoldToCMLinkRecID>
        <SoldToCMOppRecID></SoldToCMOppRecID>
        <SoldToCompany>Life Fitness</SoldToCompany>
        <SoldToContact>Sebastiano De Paola</SoldToContact>
        <SoldToCountry>Switzerland</SoldToCountry>
        <SoldToEmail>sebastiano.depaola@lifefitness.com</SoldToEmail>
        <SoldToFax></SoldToFax>
        <SoldToFaxExt></SoldToFaxExt>
        <SoldToPhone></SoldToPhone>
        <SoldToPhoneExt></SoldToPhoneExt>
        <SoldToPONumber>6405019446</SoldToPONumber>
        <SoldToPostalCode>8733</SoldToPostalCode>
        <SoldToPriceProfile></SoldToPriceProfile>
        <SoldToState></SoldToState>
        <SoldToTitle></SoldToTitle>
        <SubTotal>374.89</SubTotal>
        <Superceeded>0</Superceeded>
        <Terms>Order Total: 374.89, Outstanding: 0.00, </Terms>
        <TotalTax>0</TotalTax>
        <SoldToCMCallRecID></SoldToCMCallRecID>
        <GSTTaxRate></GSTTaxRate>
        </DocumentHeader>
        <DocumentItems>
        <DocumentItem id="1">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="2">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>10.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>10.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Polo Shirt - White (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>8.54</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>8.54</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="3">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>6.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>6.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Sleeveless T-Shirt - White (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>5.28</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>5.28</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="4">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>8</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>8</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength Men's T Shirt - Grey (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>6.5</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>6.5</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="5">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="6">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>4</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>4</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength Cap - Black</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>3.25</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>3.25</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="7">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="8">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>12.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>12.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength Men's Sleeveless Training Shirt - Black/White (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>10.16</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>10.16</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="9">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>16</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>16</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Hammer Strength Training Shorts - Black/White (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>13.01</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>13.01</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="10">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="11">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>4</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>4</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Hat - Black</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>3.25</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>3.25</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="12">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>13</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>13</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Luxury Pen - Silver</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>10.57</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>10.57</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="13">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>102</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>34</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Clock - White</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>82.93</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>3</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>3</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>27.64</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="14">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>1.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>1.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Gymsack - Black</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>1.22</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>1.22</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="15">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Men's</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="16">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>19.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>19.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Hooded Jumper - Black (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>15.85</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>15.85</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="17">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>11.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>11.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Sports Polo Shirt - Red (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>9.35</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>9.35</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="18">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>11.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>11.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Sports Polo Shirt - Black (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>9.35</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>9.35</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="19">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Men's</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="20">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>14.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>14.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Long Sleeve Training Shirt - White/Black (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>11.79</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>11.79</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="21">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>12.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>12.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Men's Short Sleeve Training Shirt - White/Red (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>10.16</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>10.16</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="22">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>16.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>16.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Mens Sports Polo Shirt - White/Red (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>13.41</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>13.41</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="23">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>33</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>33</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Training Jacket - Black with Reflective strip (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>26.83</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>26.83</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="24">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>23.5</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>23.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Training Pants - Black with Reflective strip (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>19.11</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>19.11</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="25">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>27</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>13.5</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness Training Shorts - Black/White (M)</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>21.95</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>2</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>2</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost></UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>10.98</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="26">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Delivery Cost</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="27">
            <AlternateExtendedCost>27.39</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>27.39</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>27.39</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>27.39</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Shipping Costs</Description>
            <ExtendedCost>22.27</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>22.27</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate>2014-01-08</OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax></SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost>5.00</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor></UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor></UnitOfPricingFactor>
            <UnitPrice>22.27</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor>Shipping</Vendor>
            <VendorPartNumber>Shipping</VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="28">
            <AlternateExtendedCost>0</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>0</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>0</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>0</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Voucher</Description>
            <ExtendedCost>0</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>0</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>2</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate></OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax>0</SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>N</TaxCode>
            <UnitCost>0</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor>1</UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor>1</UnitOfPricingFactor>
            <UnitPrice>0</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor></Vendor>
            <VendorPartNumber></VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        <DocumentItem id="29">
            <AlternateExtendedCost>-374.89</AlternateExtendedCost>
            <AlternateExtendedList>0</AlternateExtendedList>
            <AlternateExtendedPrice>-374.89</AlternateExtendedPrice>
            <AlternateSalesTax>0</AlternateSalesTax>
            <AlternateShippingAmount>0</AlternateShippingAmount>
            <AlternateUnitCost>-374.89</AlternateUnitCost>
            <AlternateUnitList>0</AlternateUnitList>
            <AlternateUnitPrice>-374.89</AlternateUnitPrice>
            <CloseProbability>0</CloseProbability>
            <CostModifier></CostModifier>
            <CustomDate01></CustomDate01>
            <CustomDate02></CustomDate02>
            <CustomMemo01></CustomMemo01>
            <CustomMemo02></CustomMemo02>
            <CustomNumber01>0</CustomNumber01>
            <CustomNumber02>0</CustomNumber02>
            <CustomNumber03>0</CustomNumber03>
            <CustomNumber04>0</CustomNumber04>
            <CustomNumber05>0</CustomNumber05>
            <CustomText01></CustomText01>
            <CustomText02></CustomText02>
            <CustomText03></CustomText03>
            <CustomText04></CustomText04>
            <CustomText05></CustomText05>
            <CustomText06></CustomText06>
            <CustomText07></CustomText07>
            <CustomText08></CustomText08>
            <CustomText09></CustomText09>
            <CustomText10></CustomText10>
            <CustomText11></CustomText11>
            <CustomText12></CustomText12>
            <CustomText13></CustomText13>
            <CustomText14></CustomText14>
            <Description>Life Fitness E-Voucher</Description>
            <ExtendedCost>-304.79</ExtendedCost>
            <ExtendedList>0</ExtendedList>
            <ExtendedPrice>-304.79</ExtendedPrice>
            <ExtendedWeight>0</ExtendedWeight>
            <Formula></Formula>
            <ItemType></ItemType>
            <LineAttributes>0</LineAttributes>
            <LineType>1</LineType>
            <Manufacturer></Manufacturer>
            <ManufacturerPartNumber></ManufacturerPartNumber>
            <Notes></Notes>
            <OrderDate>2014-01-08</OrderDate>
            <PONumber></PONumber>
            <PriceModifier></PriceModifier>
            <PriceProfile></PriceProfile>
            <QtyBase>1</QtyBase>
            <QtyGroupMultiplier>1</QtyGroupMultiplier>
            <QtyMultiplier1>1</QtyMultiplier1>
            <QtyMultiplier2>1</QtyMultiplier2>
            <QtyMultiplier3>1</QtyMultiplier3>
            <QtyMultiplier4>1</QtyMultiplier4>
            <QtyTotal>1</QtyTotal>
            <SalesTax></SalesTax>
            <ShippingAmount>0</ShippingAmount>
            <SONumber></SONumber>
            <TaxCode>Y</TaxCode>
            <UnitCost>5.00</UnitCost>
            <UnitList>0</UnitList>
            <UnitOfMeasure></UnitOfMeasure>
            <UnitOfMeasureFactor></UnitOfMeasureFactor>
            <UnitOfPricing></UnitOfPricing>
            <UnitOfPricingFactor></UnitOfPricingFactor>
            <UnitPrice>-304.79</UnitPrice>
            <UnitWeight>0</UnitWeight>
            <Vendor>Shipping</Vendor>
            <VendorPartNumber>Shipping</VendorPartNumber>
            <ItemURL></ItemURL>
        </DocumentItem>
        </DocumentItems>
        </Document>
        </Documents>
</QuoteWerksXML>
EOT;
}
    
    public function export(Core_Order_Models_Order $order){
        header ("content-type: text/xml");
        header("Content-Disposition: attachment; filename=" . $order->getOrderNumber() . ".xml"); 
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        echo $this->buildOrderXML($order);
        exit();
    }
}