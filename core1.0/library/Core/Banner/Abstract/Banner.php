<?php
/**
 * Class Banner_Abstract_Banner
 *
 * @package CO3-Core
 */
abstract class Banner_Abstract_Banner implements Banner_Interface_Banner {

    protected $bannerId;

    public function __set($Mname, $value) {
        $method = 'set' . $Mname;

        if(!method_exists($this, $method)) {
            throw new Exception('Invalid property ' . $method . ': Trying to set a value which doesn\'t exist');
        }

        $this->$method($value);
    }

    public function __get($Mname) {
        $method = 'get' . $Mname;

        if(!method_exists($this, $method)) {
            throw new Exception('Invalid property ' . $method . ': Trying to get a value which doesn\'t exist');
        }

        $this->$method();
    }

    public function getBanners() {
        return $this->bannerId;
    }


}