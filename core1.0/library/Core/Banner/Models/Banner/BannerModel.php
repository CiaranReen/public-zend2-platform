<?php

class Banner_Models_Banner_BannerModel extends Frontend_Models_Abstract_AbstractModel {

    public function getBanners() {
        try {
            $rows = $this->getDbAdapter()->fetchAll("SELECT * FROM `banner`");

            return $rows;
        }

        catch(exception $e) {
            error_log($e->getMessage());

            return false;
        }
    }
}

?>