<?php
/**
 * @package CO3-Core Platform
 *
 * Generates the view for Admin pages
 */

class Banner_Views_View extends View_Abstract_View {

    public function renderPage() {

        $documentRoot = Application_Environment::getClientWebsiteDocumentRoot();

        $templatePath = $documentRoot . '/modules/Banner/Views/' . $this->getTheme() . '/' . $this->getCurrentPage();
        $baseTemplatePath = $documentRoot . '/modules/Admin/Views/' . $this->getBaseTheme() . '/Base';

        $this->getInclude($templatePath, $baseTemplatePath, '/htmlHead.phtml');
        $this->getInclude($templatePath, $baseTemplatePath, '/pageHead.phtml');
        $this->getInclude($templatePath, $baseTemplatePath, '/navigation.phtml');
        $this->getInclude($templatePath, $baseTemplatePath, '/content.phtml');
        $this->getInclude($templatePath, $baseTemplatePath, '/pageFooter.phtml');
        $this->getInclude($templatePath, $baseTemplatePath, '/htmlFooter.phtml');

    }

}
