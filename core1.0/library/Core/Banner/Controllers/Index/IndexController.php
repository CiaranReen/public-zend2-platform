<?php

class Banner_Controllers_Index_IndexController extends Admin_Controllers_Abstract_AbstractController {
    /**
     * @param array $params
     *
     * @return $this
     */

    public function index($params) {

        $this->preDispatch();

        $bannerModel = new Banner_Models_Banner_BannerModel();

        $bannerData = $bannerModel->getBanners();

        /* @var $page Banner_Views_View */
        $page = new Banner_Views_View('Index');

        $page->setTitle('Manage Banners &middot; Admin Panel &middot; CO3 Core');

        $page->setContent('enabled_modules', $this->getEnabledModules());

        $page->setContent('current_module', 'Banner');

        $page->setContent('banners', $bannerModel->getBanners());

        $page->setContent('banners', $bannerData);

        $page->renderPage();

        return $this;

    }

    protected function preDispatch() {

        parent::isLoggedIn();
        return $this;
    }

}

?>