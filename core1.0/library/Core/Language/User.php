<?php
/**
 * Class Language_User
 *
 * @package CO3-Core
 */
class Language_User extends Language_Abstract_Language {

    protected $languageId;

    public function __construct($languageId) {

        $languageModel = new Language_Models_Language_LanguageModel();

        $languageData = $languageModel->loadByLanguageId($languageId);

        $this->setLanguageId($languageId)
             ->setName($languageData['name'])
             ->setShortcode($languageData['iso_shortcode']);

        return $this;
    }

    /**
     *
     * @return $this
     */
    public function getLanguage() {
        $languageModel = new Language_Models_Language_LanguageModel();

        $languageData = $languageModel->loadByLanguageId($this->getLanguageId());

        $this->setName($languageData['name'])
            ->setShortcode($languageData['iso_shortcode']);

        return $this;
    }

    public function saveLanguage($languageData) {
        $languageModel = new Language_Models_Language_LanguageModel();

        $this->setName($languageData['name'])
            ->setShortcode($languageData['iso_shortcode']);

        $languageModel->save($this);

        return $this;
    }

    public function addLanguage($languageData) {
        $languageModel = new Language_Models_Language_LanguageModel();

        $this->setName($languageData['name'])
             ->setShortcode($languageData['iso_shortcode']);

        $languageModel->add($this);

        return $this;
    }
}