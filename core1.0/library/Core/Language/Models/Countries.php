<?php

class Core_Language_Models_Countries
{
    protected $_id;
    protected $_country;
    protected $_top;
    protected $_world;
    protected $_currency;
    protected $_language;
    protected $_iso;
    protected $_iso3;
    protected $_ipcode;
    protected $_ec;
    protected $_bu;
    protected $_paypal;
    
    protected $_mapper;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) 
        {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid country property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid country property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setCountry($country)
    {
        $this->_country = (string) $country;
        return $this;
    }
 
    public function getCountry()
    {
        return $this->_country;
    }
 
    public function setTop($top)
    {
        $this->_top = (string) $top;
        return $this;
    }
 
    public function getTop()
    {
        return $this->_top;
    }
    
    public function setWorld($world)
    {
        $this->_world = (string) $world;
        return $this;
    }
 
    public function getWorld()
    {
        return $this->_world;
    }
    
    public function setCurrency($currency)
    {
        $this->_currency = (string) $currency;
        return $this;
    }
 
    public function getCurrency()
    {
        return $this->_currency;
    }
    
    public function setLanguage($language)
    {
        $this->_language = (string) $language;
        return $this;
    }
    
    public function getLanguage()
    {
        return $this->_language;
    }
 
    public function setIso($iso)
    {
        $this->_iso = (string) $iso;
        return $this;
    }
    
    public function getIso()
    {
        return $this->_iso;
    }
    
    public function setIso3($iso3)
    {
        $this->_iso3 = (string) $iso3;
        return $this;
    }
    
    public function getIso3()
    {
        return $this->_iso3;
    }
    
    public function setIpCode($ipCode)
    {
        $this->_ipcode = (string) $ipCode;
        return $this;
    }
    
    public function getIpCode()
    {
        return $this->_ipcode;
    }
    
    public function setEc($ec)
    {
        $this->_ec = (string) $ec;
        return $this;
    }
    
    public function getEc()
    {
        return $this->_ec;
    }
    
    public function setBu($bu)
    {
        $this->_bu = (string) $bu;
        return $this;
    }
    
    public function getBu()
    {
        return $this->_bu;
    }
    
    public function setPaypal($paypal)
    {
        $this->_paypal = (string) $paypal;
        return $this;
    }
    
    public function getPaypal()
    {
        return $this->_paypal;
    }
    
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Language_Models_CountriesMapper());
        }
        return $this->_mapper;
    }
    
    public function findById($countryId){
        $this->getMapper()->find($countryId, $this);
        return $this;
    }
 
    public function dumpdie($stuff)
    {
        echo '<PRE>';
        print_r($stuff);
        echo '</PRE>';
        die();
    }
}

