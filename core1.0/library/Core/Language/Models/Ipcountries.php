<?php

class Core_Language_Models_Ipcountries
{
    protected $_id;
    protected $_lower;
    protected $_upper;
    protected $_code1;
    protected $_code2;
    protected $_country;
    protected $countryInfo;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) 
        {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid ipcountry property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid ipcountry property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    /**
     * Set the lower value of the ip range
     * 
     * @param int $lower
     * @return \Core_Language_Models_Ipcountries
     */
    public function setLower($lower)
    {
        $this->_lower = (string) $lower;
        return $this;
    }
 
    public function getLower()
    {
        return $this->_lower;
    }
 
    public function setUpper($upper)
    {
        $this->_upper = (string) $upper;
        return $this;
    }
 
    public function getUpper()
    {
        return $this->_upper;
    }
    
    public function setCode1($code1)
    {
        $this->_code1 = (string) $code1;
        return $this;
    }
 
    public function getCode1()
    {
        return $this->_code1;
    }
    
    public function setCode2($code2)
    {
        $this->_code2 = (string) $code2;
        return $this;
    }
 
    public function getCode2()
    {
        return $this->_code2;
    }
    
    public function setCountry($country)
    {
        $this->_country = (string) $country;
        return $this;
    }
    
    public function getCountry()
    {
        return $this->_country;
    }
    
    public function setCountryInfo($countryInfo)
    {
        $this->countryInfo = $countryInfo;
        return $this;
    }
 
    public function getCountryInfo()
    {
        return $this->countryInfo;
    }
 
    public function dumpdie($stuff)
    {
        echo '<PRE>';
        print_r($stuff);
        echo '</PRE>';
        die();
    }
    
    public function getCountryByIp($remoteAddr)
    {
        $ipcountriesMapper = new Core_Language_Models_IpcountriesMapper();
        $ipcountry = $ipcountriesMapper->fetchByIp($remoteAddr);
        
        $this->dumpdie($ipcountry);
    }
}

