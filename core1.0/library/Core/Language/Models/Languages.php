<?php

class Core_Language_Models_Languages
{
    protected $_id;
    protected $_name;
    protected $_isoShortcode;
    protected $_active;
    protected $content;
    protected $ipcountry;
 
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setName($name)
    {
        $this->_name = (string) $name;
        return $this;
    }
 
    public function getName()
    {
        return $this->_name;
    }
 
    public function setIsoShortcode($isoShortcode)
    {
        $this->_isoShortcode = (string) $isoShortcode;
        return $this;
    }
 
    public function getIsoShortcode()
    {
        if(!isset($this->_isoShortcode))
        {
            $this->setIsoShortcode('gb');
        }
        return $this->_isoShortcode;
    }
    
    public function setActive($name)
    {
        $this->_active = (string) $name;
        return $this;
    }
 
    public function getActive()
    {
        return $this->_active;
    }
    
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
 
    public function getContent($identifier = '')
    {
        if(!is_array($this->content))
        {
            $this->setContent();
        }
        
        if($identifier == '')
        {
            $return = $this->content;
        }
        else
        {
            if(isset($this->content[$identifier]))
            {
                $return = $this->content[$identifier];
            }
            else
            {
                $blank = new Core_Content_Models_Content();
                $blank->setId(0);
                $blank->setIdentifier($identifier);
                $blank->setContent('');
                $return = $blank;
            }
        }
        
        return $return;
    }
    
    public function setIpcountry($ipcountry)
    {
        $this->ipcountry = $ipcountry;
        return $this;
    }
 
    public function getIpcountry()
    {
        return $this->ipcountry;
    }
    
    public function saveContent()
    {
        
    }
    
    public function loadByLanguageId($languageId) {
        $row = $this->getDbAdapter()
            ->fetchRow("SELECT * FROM `language` l WHERE l.language_id = ?", array('language_id' => $languageId));

        return $row;
    }

    public function dumpdie($stuff)
    {
        echo '<PRE>';
        print_r($stuff);
        echo '</PRE>';
        die();
    }

    public function getLanguageByShortcode($language) {
        $shortcode = $language->getShortcode();
    }

    public function getAllLanguages()
    {
        $languages = new Core_Language_Models_LanguagesMapper();
        $loadedLangs = $languages->fetchAll();
        return $loadedLangs;
    }
    
    public function getActivatedLanguages()
    {
        $languages = $this->getAllLanguages();
        $unactiveLanguages = array();
        
        foreach($languages as $language)
        {
            if($language->getActive() == 1)
            {
                $unactiveLanguages[] = $language;
            }
        }
        
        return $unactiveLanguages;
    }
    
    public function getUnactivatedLanguages()
    {
        $languages = $this->getAllLanguages();
        $unactiveLanguages = array();
        
        foreach($languages as $language)
        {
            if($language->getActive() == 0)
            {
                $unactiveLanguages[] = $language;
            }
        }
        
        return $unactiveLanguages;
    }
    
    public function getLanguageById($id)
    {
        $languages = new Core_Language_Models_LanguagesMapper();
        $language = $languages->fetchById($id);
        
        return $language;
    }
    
    public function activateLanguage($languageId)
    {
        try
        {
            if(!is_numeric($languageId))
            {
                throw new InvalidArgumentException(
                    'Cannot activate language '.$languageId.' because the correct
                        language id was not provided'
                );
            }
        }
        catch(InvalidArgumentException $e)
        {
            echo $e->getMessage();
        }
        
        $languageMapper = new Core_Language_Models_LanguagesMapper();
        $languageId = intval($languageId);
        $languages = $this->getAllLanguages();
        
        foreach($languages as $language)
        {
            if($language->getId() == $languageId)
            {
                $language->setActive(1);
                $languageMapper->save($language);
            }
        }
        
        return $this;
    }
    
    public function updateContent($updatedContent)
    {
        $content = $this->getContent();
        
        foreach($updatedContent as $identifier => $contentText)
        {
            $contentObject = $content[$identifier];
            
            $contentObject->setContent($contentText);
            $contentObject->saveContent();
        }
        
        $this->setContent($content);
    }
    
    public function getPairs($languages = null){
        if (!isset($languages)) {
            $languages = $this->getAllLanguages();
        }
        $languagePairs = array();
        foreach ($languages as $language) {
            $languagePairs[$language->getId()] = $language->getName();
        }
        return $languagePairs;
    }
}

