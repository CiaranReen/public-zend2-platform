<?php

class Core_Language_Models_LanguagesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable('Core_Language_Models_DbTable_Languages');
        }
        return $this->_dbTable;
    }

    public function save(Core_Language_Models_Languages $language)
    {
        $data = array(
            'name'   => $language->getName(),
            'iso_shortcode' => $language->getIsoShortcode(),
            'active'   => $language->getActive(),
        );

        if (null === ($id = $language->getId()))
        {
            unset($data['language_id']);
            $this->getDbTable()->insert($data);
        }
        else
        {
            $this->getDbTable()->update($data, array('language_id = ?' => $id));
        }
    }

    public function find($id, Core_Language_Models_Languages $language)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result))
        {
            return;
        }
        $row = $result->current();
        $language->setId($row->language_id)
            ->setName($row->name)
            ->setIsoShortcode($row->iso_shortcode)
            ->setActive($row->active);

        return $language;
    }

    public function fetchAll()
    {
        $cache = Zend_Registry::get('Memcache');

        $key = 'all_languages';
        $entries = array();

        if (!$language = $cache->load($key)) {
            //Set up the language
            $resultSet = $this->getDbTable()->fetchAll();
            $cache->save($resultSet, 'all_languages');
            $entries   = $this->constructLanguage($resultSet);
        } else {
            $languages = Zend_Registry::get('Memcache')->load($key);
            $entries   = $this->constructLanguage($languages);
        }

        return $entries;
    }

    public function fetchActiveNotSelected($selectedLangId)
    {
        $select = new Zend_Db_Select($this->getDbTable()->getAdapter());

        $sql = $select->from('languages')
            ->where('active = ?', 1)
            ->where('language_id != ?', $selectedLangId);

        $stmt = $this->getDbTable()->getAdapter()->query($sql);
        $result = $stmt->fetchAll();

        $entries = $this->constructLanguage($result, true);

        return $entries;
    }

    public function fetchById($id)
    {
        $languages = $this->fetchAll();

        foreach($languages as $language)
        {
            if($language->getId() == $id)
            {
                $return = $language;
            }
        }

        return $return;
    }

    protected function constructLanguage($resultSet, $array = false)
    {
        if(!$array)
        {
            if (!empty($resultSet)) {
                foreach($resultSet as $row)
                {
                    $entry = new Core_Language_Models_Languages();

                    $entry->setId($row->language_id)
                        ->setName($row->name)
                        ->setIsoShortcode($row->iso_shortcode)
                        ->setActive($row->active);

                    //Now assign all of that languages content to it
                    $content = new Core_Content_Models_Content();
                    $entry->setContent($content->getLanguageContent($entry));
                    $entries[] = $entry;
                }
                return $entries;
            }
        }
        else
        {
            if (!empty($resultSet)) {
                foreach($resultSet as $row)
                {
                    $entry = new Core_Language_Models_Languages();

                    $entry->setId($row['language_id'])
                        ->setName($row['name'])
                        ->setIsoShortcode($row['iso_shortcode'])
                        ->setActive($row['active']);

                    //Now assign all of that languages content to it
                    $content = new Core_Content_Models_Content();
                    $entry->setContent($content->getLanguageContent($entry));
                    $entries[] = $entry;
                }
                return $entries;
            }
        }
    }

    protected function getRemoteAddress()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    public function setSessionLanguage($isoCode)
    {
        $languageSession = new Zend_Session_Namespace('language');
        $languageSession->chosen = $isoCode;
    }

    public function getSessionLanguage()
    {
        $languageSession = new Zend_Session_Namespace('language');
        return $languageSession;
    }

    public function fetchByIp()
    {
        $languageSession = $this->getSessionLanguage();
        $ipcountry = '';

        /*
         * Elliott - Ive removed the following code from the if statement because it was 
         * breaking my language stuff! :
         * 
         * strpos($languageSession->chosen,'x-debug') && 
         */
//        if(isset($languageSession->chosen) && is_string($languageSession->chosen) && !empty($languageSession->chosen) && $languageSession->chosen != '')
//        {
//            $iso = $languageSession->chosen;
//        }
//        else
//        {
//            //Need to implement logged in language or chosen language here
//            $remoteAddr = $this->getRemoteAddress();
//
//            if($remoteAddr == "127.0.0.1")
//            {
//                //UK
//                $remoteAddr = "213.123.215.205";
//
//                //USA
//                //$remoteAddr = "74.125.227.183";
//
//                //NL
//                //$remoteAddr = "217.196.35.65";
//            }
//
//            $ipcountriesMapper = new Core_Language_Models_IpcountriesMapper();
//
//            $ipcountry = $ipcountriesMapper->fetchByIp($remoteAddr);
//            $iso = $ipcountry->getCountryInfo()->getIso();
//        }

        $ipcountriesMapper = new Core_Language_Models_IpcountriesMapper();

        $ipcountry = $ipcountriesMapper->fetchById(44); //hardcoded to 44 which is the default id for IPs in the UK
        $iso = 'gb'; //hardcoded to override all other isos so that it defaults to english


        $select = new Zend_Db_Select($this->getDbTable()->getAdapter());

        $sql = $select->from('languages')
            ->joinInner('countries', '`countries`.`iso` = `languages`.`iso_shortcode`')
            ->where('iso_shortcode = ?', $iso);

        $stmt = $this->getDbTable()->getAdapter()->query($sql);
        $result = $stmt->fetchAll();

        foreach($result as $row)
        {
            $entry = new Core_Language_Models_Languages();

            $entry->setId($row['language_id'])
                ->setName($row['name'])
                ->setIsoShortcode($row['iso_shortcode'])
                ->setActive($row['active']);

            //Now assign all of that languages content to it
            $content = new Core_Content_Models_Content();
            $entry->setContent($content->getLanguageContent($entry));

            //And assign the ipcountry to it
            if($ipcountry)
            {
                $entry->setIpcountry($ipcountry);
            }
        }

        return $entry;
    }

    public function deactivateLanguage($id)
    {
        $language = $this->fetchById($id);
        $language->setActive(0);
        $this->save($language);

        return $this;
    }
}