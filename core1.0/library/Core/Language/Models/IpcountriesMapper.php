<?php

class Core_Language_Models_IpcountriesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Language_Models_DbTable_Ipcountries');
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Language_Models_Ipcountries $ipcountry)
    {
        $data = array(
            'lower'   => $ipcountry->getLower(),
            'upper'       => $ipcountry->getUpper(),
            'code1'          => $ipcountry->getCode1(),
            'code2'          => $ipcountry->getCode2(),
            'country'          => $ipcountry->getCountry(),
        );
 
        if (null === ($id = $ipcountry->getId())) 
        {
            unset($data['ipcountry_id']);
            $this->getDbTable()->insert($data);
        } 
        else 
        {
            $this->getDbTable()->update($data, array('ipcountry_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Language_Models_Ipcountries $ipcountry)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $ipcountry->setLower($row->lower)
                ->setUpper($row->upper)
                ->setCode1($row->code1)
                ->setCode2($row->code2)
                ->setCountry($row->country);
    }
 
    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $ipcountry = new Core_Language_Models_Ipcountries();
            
            $ipcountry->setLower($row->lower)
                ->setUpper($row->upper)
                ->setCode1($row->code1)
                ->setCode2($row->code2)
                ->setCountry($row->country);
            
            $entries[] = $ipcountry;
        }
        
        return $entries;
    }

    public function fetchById($id)
    {
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->from($this->_dbTable)
            ->joinInner('countries', '`countries`.`iso` = `ipcountries`.`code1`')
            ->where('ipcountry_id = "'.$id.'" ');

        $row = $this->getDbTable()->fetchRow($select);

        $ipcountry = new Core_Language_Models_Ipcountries();
        $country = new Core_Language_Models_Countries();

        $country->setId($row['countryid'])
            ->setCountry($row['country'])
            ->setTop($row['top'])
            ->setWorld($row['world'])
            ->setCurrency($row['currency'])
            ->setLanguage($row['language'])
            ->setIso($row['iso'])
            ->setIso3($row['iso3'])
            ->setIpCode($row['ipcode'])
            ->setEc($row['ec'])
            ->setBu($row['bu'])
            ->setPaypal($row['paypal']);

        $ipcountry->setLower($row['lower'])
            ->setUpper($row['upper'])
            ->setCode1($row['code1'])
            ->setCode2($row['code2'])
            ->setCountry($row['country'])
            ->setCountryInfo($country);

        return $ipcountry;
    }

    
    public function fetchByIp($remoteAddr)
    {
        /*$sql_country = "SELECT c.* FROM ipcountries ip ".
                        "JOIN countries c ON c.iso = ip.code1 " . 
                        "WHERE ip.lower<=inet_aton('" . $remoteAddr . "') ".
                        "AND ip.upper>=inet_aton('" . $remoteAddr . "') ";*/
        $select = new Zend_Db_Select($this->getDbTable()->getAdapter());
        
        $sql = $select->from('ipcountries')
                    ->joinInner('countries', '`countries`.`iso` = `ipcountries`.`code1`')
                    ->where('lower <= inet_aton("'.$remoteAddr.'")')
                    ->where('upper >= inet_aton("'.$remoteAddr.'")');
        
        $stmt = $this->getDbTable()->getAdapter()->query($sql);
        $result = $stmt->fetchAll();
        
        foreach($result as $row) 
        {
            $ipcountry = new Core_Language_Models_Ipcountries();
            $country = new Core_Language_Models_Countries();
            
            $country->setId($row['countryid'])
                    ->setCountry($row['country'])
                    ->setTop($row['top'])
                    ->setWorld($row['world'])
                    ->setCurrency($row['currency'])
                    ->setLanguage($row['language'])
                    ->setIso($row['iso'])
                    ->setIso3($row['iso3'])
                    ->setIpCode($row['ipcode'])
                    ->setEc($row['ec'])
                    ->setBu($row['bu'])
                    ->setPaypal($row['paypal']);
            
            $ipcountry->setLower($row['lower'])
                    ->setUpper($row['upper'])
                    ->setCode1($row['code1'])
                    ->setCode2($row['code2'])
                    ->setCountry($row['country'])
                    ->setCountryInfo($country);
        }
        
        return $ipcountry;
    }
}