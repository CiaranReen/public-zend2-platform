<?php

class Core_Language_Models_CountriesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Language_Models_DbTable_Countries');
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Language_Models_Countries $country)
    {
        $data = array(
            'country'   => $country->getCountry(),
            'top'       => $country->getTop(),
            'world'          => $country->getWorld(),
            'currency'          => $country->getCurrency(),
            'language'          => $country->getLanguage(),
            'iso'          => $country->getIso(),
            'iso3'          => $country->getIso3(),
            'ipcode'          => $country->getIpCode(),
            'ec'          => $country->getEc(),
            'bu'          => $country->getBu(),
            'paypal'          => $country->getPaypal(),
        );
 
        if (null === ($id = $country->getId())) 
        {
            unset($data['country_id']);
            $this->getDbTable()->insert($data);
        } 
        else 
        {
            $this->getDbTable()->update($data, array('country_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Language_Models_Countries $country)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $country->setId($row->countryid)
                ->setCountry($row->country)
                ->setTop($row->top)
                ->setWorld($row->world)
                ->setCurrency($row->currency)
                ->setLanguage($row->language)
                ->setIso($row->iso)
                ->setIso3($row->iso3)
                ->setIpCode($row->ipcode)
                ->setEc($row->ec)
                ->setBu($row->bu)
                ->setPaypal($row->paypal);
        
        return $country;
    }
 
    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll(array(), 'country ASC');
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $country = new Core_Language_Models_Countries();
            
            $country->setId($row->countryid)
                    ->setCountry($row->country)
                    ->setTop($row->top)
                    ->setWorld($row->world)
                    ->setCurrency($row->currency)
                    ->setLanguage($row->language)
                    ->setIso($row->iso)
                    ->setIso3($row->iso3)
                    ->setIpCode($row->ipcode)
                    ->setEc($row->ec)
                    ->setBu($row->bu)
                    ->setPaypal($row->paypal);
            
            $entries[] = $country;
        }
        
        return $entries;
    }
    public function fetchById($id)
    {
        $countries = $this->fetchAll();
        
        foreach($countries as $country)
        {
            if($country->getId() == $id)
            {
                $return = $country;
            }
        }
        
        return $return;
    }
}