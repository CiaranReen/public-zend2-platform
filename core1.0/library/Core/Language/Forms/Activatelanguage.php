<?php

class Core_Language_Forms_Activatelanguage extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * @param Language_Abstract_Language $language
     */
    public function setupAddForm($languages) {
        /* @var $address Language_Abstract_Language */

        // Set properties of the form
        $this->setName('addLanguage')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a language');

        // Name Field
        $name = $this->createElement('select', 'language')
            ->setLabel('Activate Language: ')
            ->setRequired(true);
        
        foreach($languages as $language)
        {
            $name->addMultiOption($language->getId(),$language->getName());
        }
        $this->setOptions(
                array(
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Activate Language', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this ->addElement($name)
            ->addElement($submit);

    }
}