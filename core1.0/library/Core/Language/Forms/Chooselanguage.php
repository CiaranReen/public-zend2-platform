<?php

class Core_Language_Forms_Chooselanguage extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * @param Language_Abstract_Language $language
     */
    public function setupForm($languages) 
    {
        // Set properties of the form
        $this->setName('chooseLanguage')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Choose a language')
            ->setAction('/admin/content/manage');

        // Name Field
        $name = $this->createElement('select', 'language')
            ->setLabel('Choose Language: ')
            ->setRequired(true);
        
        foreach($languages as $language)
        {
            $name->addMultiOption($language->getId(),$language->getName());
        }
        $this->setOptions(
                array(
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Submit Button
        $submit = $this->createElement('submit', 'choosesubmit')->setOptions(array('label' => 'Choose Language', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this ->addElement($name)
            ->addElement($submit);
    }
}