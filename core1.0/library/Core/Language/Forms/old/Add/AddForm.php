<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Rob
 * Date: 23/07/2013
 * Time: 10:30
 * To change this template use File | Settings | File Templates.
 */

class Language_Forms_Add_AddForm extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param Language_Abstract_Language $language
     */
    public function setupAddForm($language) {
        /* @var $address Language_Abstract_Language */

        // Set properties of the form
        $this->setName('addLanguage')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a language');

        // Name Field
        $name = $this->createElement('text', 'name')
            ->setOptions(
                array(
                    'label'    => 'Name',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )
            ->setValue($language->getName());

        // ISO Shortcode Field
        $shortcode = $this->createElement('text', 'iso_shortcode')
            ->setOptions(
                array(
                    'label'    => 'ISO Shortcode',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )
            ->setValue($language->getShortcode());


        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Add', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this ->addElement($name)
            ->addElement($shortcode)
            ->addElement($submit);

    }

}