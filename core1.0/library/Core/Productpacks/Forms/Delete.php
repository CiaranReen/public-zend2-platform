<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_Productpacks_Forms_Delete extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param Usergroup_Abstract_Usergroup $ug
     */
    public function setupDeleteForm($productList, $userList, $pp) {
        /* @var $user Usergroup_Abstract_Usergroup */

        // Set properties of the form
        $this->setName('deleteUsergroup')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Delete Usergroup');

        //Pack ID Field
        $ppId = $this->createElement('hidden', 'id')
                        ->setOptions(
                                array(
                                    'required' => true,
                                )
                        )->setValue($pp->getPackId());

        // Pack Name Field
        $name = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($pp->getPackName());

        // Status Field
        $status = $this->createElement('radio', 'status')
                ->setOptions(
                        array(
                            'label' => 'Status: ',
                            'required' => true,
                            'class' => 'required',
                        )
                )->setMultiOptions(array('Active' => 'Active', 'Inactive' => 'Inactive'))
                ->setValue($pp->getStatus());

        // Products1 Field
        $products = $this->createElement('select', 'products')
                ->setLabel('Products: ')
                ->setRequired(true);

        foreach ($productList as $product) {
            $products->addMultiOption($product->getNameByLanguage(), $product->getNameByLanguage());
                    //->addMultiOption($product->getNameByLanguage(), $product->getNameByLanguage())
                    //->addMultiOption($product->getNameByLanguage(), $product->getNameByLanguage());
        }

        // Name Field
        $users = $this->createElement('select', 'users')
                ->setLabel('Users: ')
                ->setRequired(true);

        foreach ($userList as $user) {
            $users->addMultiOption($user->getForename() . ' ' . $user->getSurname(), $user->getForename() . ' ' . $user->getSurname());
        }

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Confirm & Delete', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($ppId)
                ->addElement($name)
                ->addElement($status)
                ->addElement($products)
                ->addElement($users)
                ->addElement($submit);
    }

}