<?php

class Core_Productpacks_Forms_Add extends Zend_Form {

    public function init() {
        
    }

    public function setUpAddForm($productList, $userList, $packsModel) {
        $this->setName('startPack')
                ->setMethod('post')
                ->setAttrib('class', 'form-signin')
                ->setDescription('Add a pack');

        // Name Field
        $name = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Pack Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                )
                        )->setValue($packsModel->getPackName());
        
        // Name Field
        $price = $this->createElement('text', 'price')
                        ->setOptions(
                                array(
                                    'label' => 'Price: ',
                                    'required' => true,
                                    'class' => 'required',
                                )
                        )->setValue($packsModel->getPrice());

        // Status Field
        $status = $this->createElement('radio', 'status')
                ->setOptions(
                        array(
                            'label' => 'Status: ',
                            'required' => true,
                            'class' => 'required',
                        )
                )->setMultiOptions(array('Active' => 'Active', 'Inactive' => 'Inactive'))
                ->setValue($packsModel->getStatus());

        // Products1 Field
        $products = $this->createElement('multiselect', 'products')
                ->setOptions(array (
                    'label' => 'Products: ',
                    'required' => true,
                    'class' => 'chosen-select',
                ));
        foreach ($productList as $product) {
            $products->addMultiOption($product->getId(), $product->getNameByLanguage());
        }

        $users = $this->createElement('multiselect', 'users')
                ->setOptions(array (
                    'label' => 'Users: ',
                    'required' => true,
                    'class' => 'chosen-select',
                ));
        foreach ($userList as $user) {
            $users->addMultiOption($user->getId(), $user->getForename() . ' ' . $user->getSurname());
        }

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Create', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($name)
                ->addElement($price)
                ->addElement($status)
                ->addElement($products)
                ->addElement($users)
                ->addElement($submit);
    }

}

?>
