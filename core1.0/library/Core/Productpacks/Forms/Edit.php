<?php

class Core_Productpacks_Forms_Edit extends Zend_Form {

    public function init() {
        
    }

    public function setUpEditForm($productList, $userList, $pp) {
        // Set properties of the form
        $this->setName('editPack')
                ->setMethod('post')
                ->setAttrib('class', 'form-signin')
                ->setDescription('Edit current pack');

        // Name Field
        $name = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Pack Name: ',
                                    'required' => true,
                                    'class' => 'required',
                                )
                        )->setValue($pp->getPackName());

        $price = $this->createElement('text', 'price')
                        ->setOptions(
                                array(
                                    'label' => 'Price (£): ',
                                    'required' => true,
                                    'class' => 'required',
                                )
                        )->setValue($pp->getPrice());

        // Status Field
        $status = $this->createElement('radio', 'status')
                ->setOptions(
                        array(
                            'label' => 'Status: ',
                            'required' => true,
                            'class' => 'required',
                        )
                )->setMultiOptions(array('active' => 'Active', 'inactive' => 'Inactive'))
                ->setValue($pp->getStatus());

        // Products Field
        $products = $this->createElement('multiselect', 'products')
                ->setOptions(array(
            'label' => 'Products: ',
            'required' => true,
            'class' => 'chosen-select',
        ));
        foreach ($productList as $product) {
            $products->addMultiOption($product->getId(), $product->getNameByLanguage());
        }

        $users = $this->createElement('multiselect', 'users')
                ->setOptions(array(
            'label' => 'Users: ',
            'required' => true,
            'class' => 'chosen-select',
        ));
        foreach ($userList as $user) {
            $users->addMultiOption($user->user_id, $user->forename . ' ' . $user->surname);
        }

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Update', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($name)
                ->addElement($price)
                ->addElement($status)
                ->addElement($products)
                ->addElement($users)
                ->addElement($submit);
    }

}

?>
