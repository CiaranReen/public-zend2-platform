<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 17/10/13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
class Core_Productpacks_Models_Productpacks extends App_Model_Abstract {

    protected $ppId;
    protected $pack_name;
    protected $price;
    protected $status;
    protected $created;
    protected $products;
    protected $users;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setPackId($ppId) {
        $this->ppId = $ppId;
        return $this;
    }

    public function getPackId() {
        return $this->ppId;
    }

    public function setPackName($name) {
        $this->pack_name = (string) $name;
        return $this;
    }

    public function getPackName() {
        return $this->pack_name;
    }

    public function setPrice($price) {
        $this->price = $price;
        return $this;
    }

    public function getPrice() {
        return $this->price;
    }

    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setCreated($created) {
        $this->created = $created;
        return $this;
    }

    public function getCreated() {
        return $this->created;
    }

    public function getProducts() {
        return $this->products;
    }
    
    public function setProducts($products) {
        $this->products = $products;
    }
    
    public function getUsers() {
        return $this->users;
    }

    public function setUsers($users) {
        $this->users = $users;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Productpacks_Models_ProductpacksMapper());
        }
        return $this->_mapper;
    }

    public function getAllProducts() {
        $ppMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $fetchProducts = $ppMapper->getAllProducts();
        return $fetchProducts;
    }

    public function getAllProductsById($ppId) {
        $fetchProducts = $this->getMapper()->getAllProductsById($ppId);
        return $fetchProducts;
    }

    public function getAllUsersById($ppId) {
        $fetchUsers = $this->getMapper()->getAllUsersById($ppId);
        return $fetchUsers;
    }

    public function getAllUsers() {
        $ppMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $fetchUsers = $ppMapper->getAllUsers();
        return $fetchUsers;
    }

    public function getAllProductPacks() {
        $ppMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $fetchAll = $ppMapper->fetchAll();
        return $fetchAll;
    }

    public function save(Core_Productpacks_Models_Productpacks $pp) {
        $data = array(
            'pack_name' => $pp->getPackName(),
            'price' => $pp->getPrice(),
            'status' => $pp->getStatus(),
        );
        $where = "pp_id = '" . $this->getPackId() . "'";
        $this->getMapper()->getDbTable()->update($data, $where);
    }

    public function saveProductPack($pp) {
        $this->setPackName($pp['name'])
                ->setPrice($pp['price'])
                ->setStatus($pp['status']);
        $this->save($this);
        return $this;
    }

    public function addProductPack($pp) {
        $packMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $this->setPackName($pp['pack_name'])
                ->setPrice($pp['price'])
                ->setStatus($pp['status']);
        $packMapper->add($this);
        return $this;
    }

    public function findById($ppId) {
        $getId = $this->getMapper()->find($ppId, $this);
        return $getId;
    }

    public function delete($ppId) {
        $this->getMapper()->delete($ppId);
        return $this;
    }

    public function loadProductsInPack($packId) {
        $this->getMapper()->loadProductsInPack($packId);
        return $this;
    }

    public function getProductsInPack($packId) {
        $productsInPack = $this->getMapper()->getProductsInPack($packId);
        return $productsInPack;
    }

    public function getUsersInPack($packId) {
        $usersInPack = $this->getMapper()->getUsersInPack($packId);
        return $usersInPack;
    }

}

?>