<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 17/10/13
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
class Core_Productpacks_Models_ProductpacksUsers extends App_Model_Abstract {

    protected $ppId;
    protected $pack_name;
    protected $key;
    protected $url;
    protected $products;
    protected $users;
    protected $status;
    protected $created;
    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setPackId($ppId) {
        $this->ppId = $ppId;
        return $this;
    }

    public function getPackId() {
        return $this->ppId;
    }

    public function setProducts($products) {
        $this->products = $products;
        return $this;
    }

    public function getProducts() {
        return $this->products;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Productpacks_Models_ProductpacksUsersMapper());
        }
        return $this->_mapper;
    }

    public function saveUsersInPack($ppId, $pp) {
        $this->getMapper()->saveUsersInPack($ppId, $pp);
        return $this;
    }

    public function saveUsersInSession($pp) {
        $this->getMapper()->saveUsersInSession($pp);
        return $this;
    }

}

?>