<?php

class Core_Productpacks_Models_ProductpacksMapper {

    protected $_dbTable;
    protected $ppModel;
    protected $ppMapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }

        $this->ppModel = new Core_Productpacks_Models_Productpacks();
        $this->ppMapper = new Core_Productpacks_Models_ProductpacksProductsMapper();
        
        $this->puModel = new Core_Productpacks_Models_ProductpacksUsers();
        $this->puMapper = new Core_Productpacks_Models_ProductpacksUsersMapper();
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Productpacks_Models_DbTable_Productpacks');
        }
        return $this->_dbTable;
    }

    public function getAllProducts() {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('p' => 'products'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function getAllUsers() {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('forename', 'surname'));
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function fetchAll() {
        $resultSet = $this->getDbTable()->fetchAll();
        return $resultSet;
    }
    
    //Get All packs with users in to see if users can access a pack or not
    public function fetchAllWithUsers() {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs'), array('*'))
                ->joinInner(array('p' => 'product_packs_users'), 'pp.pp_id = p.pp_id');
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function find($ppId, Core_Productpacks_Models_Productpacks $ppModel) {
        $result = $this->getDbTable()->find($ppId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $ppModel->setPackId($row->pp_id)
                ->setPackName($row->pack_name)
                ->setPrice($row->price)
                ->setStatus($row->status)
                ->setCreated($row->created);
        //echo '<pre>'; var_dump($ppModel); die();
        return $ppModel;
    }

    public function getAllProductsById($ppId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs_products'), array('pp.product_id', 'p.name'))
                ->joinInner(array('p' => 'products'), 'pp.product_id = p.product_id')
                ->where('pp_id =' . $ppId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function getAllUsersById($ppId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs_users'), array('pp.user_id', 'u.forename', 'u.surname'))
                ->joinInner(array('u' => 'user'), 'pp.user_id = u.user_id')
                ->where('pp_id =' . $ppId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function delete($ppId) {
        $where = "pp_id = '" . $ppId . "'";
        $resultSet = $this->getDbTable()->delete($where);
        return $resultSet;
    }

    /**
     * 
     * @param Core_Productpacks_Models_Productpacks $pp
     * @return Core_Productpacks_Models_ProductpacksMapper
     */
    public function save(Core_Productpacks_Models_Productpacks $pp) {
        $data = array(
            'pp_id' => $pp->getPackId(),
            'pack_name' => $pp->getPackName(),
            'price' => $pp->getPrice(),
            'status' => $pp->getStatus(),
        );


        if (null === ($id = $pp->getPackId())) {
            unset($data['pp_id']);
            $pp->setPackId($this->getDbTable()->insert($data));
            $this->ppMapper->addProductpacksProducts($pp);
            $this->puMapper->addProductpacksUsers($pp);
        } else {
            $this->getDbTable()->update($data, array('pp_id = ?' => $pp->getPackId()));
            $this->ppMapper->updateProductpacksProducts($pp);
            $this->puMapper->updateProductpacksUsers($pp);
        }
        return $this;
    }

    //Load pack based on what product page the user is on
    public function loadPacksByProduct($productId) {
        $active = 'active';
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs'), array('pp.price', 'pp.pack_name', 'b.name'))
                ->joinInner(array('a' => 'product_packs_products'), 'pp.pp_id = a.pp_id')
                ->joinInner(array('b' => 'products'), 'a.product_id = b.product_id')
                ->where('pp.status = ?', $active)
                ->where('a.product_id = ?', $productId);
        $pack = $this->getDbTable()->fetchAll($select);
        return $pack;
    }

    //Load the products in a pack when on the pack page
    public function loadProductsInPack($packId) {
        $isDefault = 'Y';
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs'), array('pp_id', 'pp.price', 'pp.pack_name', 'b.name'))
                ->joinInner(array('a' => 'product_packs_products'), 'pp.pp_id = a.pp_id')
                ->joinInner(array('b' => 'products'), 'a.product_id = b.product_id')
                ->joinInner(array('pi' => 'product_images'), 'b.product_id = pi.product_id')
                ->where('pi.is_default = ?', $isDefault)
                ->where('pp.pp_id = ?', $packId);
        //->group();
        $pack = $this->getDbTable()->fetchAll($select);
        return $pack;
    }

    //Load 
    public function loadAllPacks($productId) {
        //load pack that has that product in it
        $active = 'active';
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs'), array('pp.price', 'pp.pack_name', 'b.name'))
                ->joinInner(array('a' => 'product_packs_products'), 'pp.pp_id = a.pp_id')
                ->joinInner(array('b' => 'products'), 'a.product_id = b.product_id')
                ->where('pp.status = ?', $active)
                ->where('a.product_id = ?', $productId);
        $pack = $this->getDbTable()->fetchAll($select);
        return $pack;
    }

    //Get the products for changeproducts.phtml
    public function getProductsInPack($packId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs_products'), array('product_id'))
                ->where('pp_id = ?', $packId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        //return $resultSet;
        if (count($resultSet) >= 1) {
            foreach ($resultSet as $row) {
                $products[] = $row->product_id;
            }
            //echo '<pre>'; var_dump($products); die();
            return $products;
        } else {
            return false;
        }
    }

    //Get the users for changeusers.phtml
    public function getUsersInPack($packId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pp' => 'product_packs_users'), array('user_id'))
                ->where('pp_id = ?', $packId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        //return $resultSet;
        if (count($resultSet) >= 1) {
            foreach ($resultSet as $row) {
                $users[] = $row->user_id;
            }
            //echo '<pre>'; var_dump($products); die();
            return $users;
        } else {
            return false;
        }
    }

}

?>