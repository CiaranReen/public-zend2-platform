<?php

class Core_Productpacks_Models_ProductpacksProductsMapper {

    protected $_dbTable;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Productpacks_Models_DbTable_ProductpacksProducts');
        }
        return $this->_dbTable;
    }

    public function getAllProducts() {
        $resultSet = $this->getDbTable()->fetchAll();
        return $resultSet;
    }

    public function addProductToDb($pp) {
        $packModel = new Core_Productpacks_Models_DbTable_ProductpacksProducts();
        $packMapper = new Core_Productpacks_Models_ProductpacksMapper();
        $ppId = $packMapper->add($pp);
        $data = array(
            'pp_id' => $ppId,
            'product_id' => $packModel->getProducts(),
        );
        $this->getMapper()->getDbTable()->insert($data);
    }

    /**
     * This method is for actually adding new products to a NEW product pack ya
     * 
     * @param Core_Productpacks_Models_Productpacks $pp
     * @return int Inserted ID
     */
    public function addProductpacksProducts($pp) {
        $products = $pp->getProducts();

        foreach ($products as $product) {
            $data = array(
                'pp_id' => $pp->getPackId(),
                'product_id' => $product->id,
            );
            $this->getDbTable()->insert($data);
        }
    }

    public function updateProductpacksProducts($pp) {
        //All correctly assigned products will be in the array parsed through
        $products = $pp->getProducts();

        //Now get all current products
        $select = new Zend_Db_Select($this->getDbTable()->getAdapter());

        $sql = $select->from('product_packs_products')
                ->where('pp_id = ?', $pp->getPackId());

        $stmt = $this->getDbTable()->getAdapter()->query($sql);
        $productsInPack = $stmt->fetchAll();

        //Now all product id's that shouldn't be in DB and delete them
        foreach ($productsInPack as $productInPack) {
            $delete = 1;
            $insert = 0;
            foreach ($products as $product) {
                if ($productInPack['product_id'] == $product->getId()) {
                    $delete = 0;
                }

                if (!in_array($product->getId(), $productInPack)) {
                    $data = array(
                        'pp_id' => $pp->getPackId(),
                        'product_id' => $product->getId(),
                    );

                    $this->getDbTable()->insert($data);
                }
            }

            if ($delete == 1) {
                $this->getDbTable()->delete(array('pp_id = ?' => $pp->getPackId(), 'product_id = ?' => $productInPack['product_id']));
            }
        }
        return $this;
    }

    public function saveProductsInPack($pp, array $products) {
        $where = "pp_id = '" . $pp->getPackId() . "'";
        $resultSet = $this->getDbTable()->delete($where);
        foreach ($products as $productId) {
            $prodData = array(
                'pp_id' => $pp->getPackId(),
                'product_id' => $productId,
            );
            $resultSet = $this->getDbTable()->insert($prodData);
        }
        return $resultSet;
    }

}

?>