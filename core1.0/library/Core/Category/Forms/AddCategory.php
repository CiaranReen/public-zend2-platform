<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 15/08/2013
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */

class Core_Category_Forms_AddCategory extends Zend_Form {

	public function init() {
            $categoryModel      = new Core_Category_Models_Category();
            $languageModel      = new Core_Language_Models_Languages();
            
            // Set properties of the form
            $this->setName('category')
                    ->setMethod(self::METHOD_POST)
                    ->setDescription('Add New Category')
                    ->setOptions(array('class' => ''));
            
            // Languages
            $languages = $this->createElement('select', 'languages');
            
            $languages->setOptions(
                    array(
                        'label'     => 'Select Language',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
            if($languageModel->getActivatedLanguages()){
                $languages->addMultiOptions($languageModel->getPairs($languageModel->getActivatedLanguages()));
            }else{
                $languages->addMultiOptions($languageModel->getPairs());
            }
            $this->addElement($languages);
           
            
            foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
                $languageId     = $activatedLanguage->getId();
                $language       = $activatedLanguage->getName();
                $isoShortcode   = $activatedLanguage->getIsoShortcode();
                if($isoShortcode === 'gb'){
                    $required = true;
                }else{
                    $required = false;
                }
                $this->addElement(
                    $this->createElement('text', 'name_'.$isoShortcode)
                            ->setOptions(
                            array(
                                     'label'    => 'Category Name [ '.$language.' ]',
                                     'class'    => 'required lang-name',
                                     'id'       => 'name-' . $languageId,
                                     'required' => $required,
                                     'filters'  => array('StringTrim', 'StripTags'),
                            )
                    )
                );
                $this->addElement(
                    $this->createElement('textarea', 'description_'.$isoShortcode)
                            ->setOptions(
                            array(
                                     'label'    => 'Category Description [ '.$language.' ]',
                                     'class'    => 'required field span3 lang-description',
                                     'id'       => 'description-' . $languageId,
                                     'rows'     => 4,
                                     'filters'  => array('StringTrim', 'StripTags'),
                            )
                    )
                );
            }
            
            //categoriess
            $parentCategories = $this->createElement('select', 'parent');
            if($categoryModel->getAllParentCategories()){
                $parentCategories->setOptions(
                        array(
                            'label'     => 'Parent Category',
                            'required'  => true,
                            'filters'   => array('StringTrim', 'StripTags')
                        )
                    );
                $parentCategories->addMultiOptions(array('0' =>'[This is a parent category]'));
                $parentCategories->addMultiOptions($categoryModel->getPairs($categoryModel->getAllParentCategories()));
            }
           
            // Submit Button
            $submit = $this->createElement('submit', 'submit')
                    ->setOptions(
                            array(
                                'label' => 'Save',
                                'class'    => 'btn btn-primary',
                            )
                    );

            // Add controls to the form
            $this->addElement($parentCategories)
                    ->addElement($submit);
	}
    
    public function populate(Core_Category_Models_Category $category){
        $languageModel      = new Core_Language_Models_Languages();
        foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
            $isoShortcode   = $activatedLanguage->getIsoShortcode();
            if($category->getNameContent()){
                $this->getElement('name_'.$isoShortcode)->setValue($category->getNameByLanguage($activatedLanguage));
            }
            if($category->getDescriptionContent()){
                $this->getElement('description_'.$isoShortcode)->setValue($category->getDescriptionByLanguage($activatedLanguage));
            }
        }
        $this->getElement('parent')->setValue($category->getParentId());
        $this->getElement('submit')->setLabel('Update');
    }

    public function setUpDeleteForm(Core_Category_Models_Category $category){
        $this->populate($category);
        $languageModel      = new Core_Language_Models_Languages();
        foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
            $isoShortcode   = $activatedLanguage->getIsoShortcode();
            $this->getElement('name_'.$isoShortcode)->setAttribs(array(
                'disabled' => 'disabled',
            ));
            $this->removeElement('description_'.$isoShortcode);
        }
        $this->getElement('parent')->setAttribs(array(
            'disabled' => 'disabled',
        ));
        $this->getElement('submit')->setLabel('Delete');
    }

}