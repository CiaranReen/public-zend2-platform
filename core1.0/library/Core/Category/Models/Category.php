<?php

class Core_Category_Models_Category extends App_Model_Abstract
{
    protected $_id;
    protected $_name;
    protected $_description;
    protected $_parentId;
    protected $_image;

    protected $_mapper;

    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getNameByLanguage(Core_Language_Models_Languages $language = null){
        if(!$language){
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language       = $languageMapper->fetchByIp();
        }
        $contentMapper  = new Core_Content_Models_ContentMapper();
        $content        = $contentMapper->fetchIndividual($language->getIsoShortcode(), $this->_name);
        if (!is_null($content)) {
            return $content->getContent();
        }
    }

    public function setDescription($description)
    {
        $this->_description = (string) $description;
        return $this;
    }

    public function getDescription()
    {
        return $this->_description;
    }

    public function getDescriptionByLanguage(Core_Language_Models_Languages $language = null){
        if(!$language){
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language       = $languageMapper->fetchByIp();
        }
        $contentMapper  = new Core_Content_Models_ContentMapper();
        $content        = $contentMapper->fetchIndividual($language->getIsoShortcode(), $this->_description);
        return $content->getContent();
    }

    public function setParentId($parentId)
    {
        $this->_parentId = (int) $parentId;
        return $this;
    }

    public function getParentId()
    {
        return $this->_parentId;
    }

    public function setImage($image)
    {
        $this->_image = (string) $image;
        return $this;
    }

    public function getImage()
    {
        return $this->_image;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Category_Models_CategoryMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs category object using category id
     * @param int $categoryId
     * @return \Core_Category_Models_Category
     */
    public function findById($categoryId) {
        $this->getMapper()->find($categoryId, $this);
        return $this;
    }

    /**
     * Gets all categories in database
     * @return array \Core_Category_Models_Category
     */
    public function getAllCategories()
    {
        $allCategories = $this->getMapper()->fetchAll();
        return $allCategories;
    }

    public function fetchAllCategories()
    {
        $allCategories = $this->getMapper()->fetchAllCategories();
        return $allCategories;
    }

    /**
     * Gets all parent categories in database
     * @return array \Core_Category_Models_Category
     */
    public function getAllParentCategories() {
        $allCategories = $this->getAllCategories();
        foreach($allCategories as $category){
            if($category->isParent()){
                $parentCategories[] = $category;
            }
        }
        return $parentCategories;
    }

    /**
     * Gets all parent categories in database
     * @return array \Core_Category_Models_Category
     */
    public function getAllChildCategories() {
        $allCategories = $this->getAllCategories();
        foreach($allCategories as $category){
            if($category->isChild()){
                $childCategories[] = $category;
            }
        }
        return $childCategories;
    }

    /**
     * Gets all child categories that fall under the current object
     * @return array \Core_Category_Models_Category
     */
    public function getChildCategories() {
        $childCategories    = array();
        $allCategories      = $this->getAllCategories();
        foreach($allCategories as $category){
            if($category->getParentId() === $this->getId()){
                $childCategories[] = $category;
            }
        }
        return $childCategories;
    }

    /**
     * Checks if category is a parent category
     * @return boolean
     */
    public function isParent(){
        if($this->getParentId() === 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Checks if the category is a child category
     * @return boolean
     */
    public function isChild(){
        if($this->getParentId() !== 0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Gets all products assigned to category using category mapper
     * @return array \Core_Product_Models_Product
     * @throws Exception
     */
    public function getProducts() {
        $products = $this->getMapper()->fetchCategoryProducts($this->getId());
        if(empty($products)){
            return false;
        }
        return $products;
    }

    public function getAllProductsForHomeCategory() {
        $products = $this->getMapper()->getAllProductsForHomeCategory();
        if(empty($products)){
            return false;
        }
        return $products;
    }

    /**
     * Returns an array of category names paired with their ids
     * @param array $categories
     * @return array
     */
    public function getPairs(array $categories = null){
        if(!$categories){
            $categories = $this->getAllCategories();
        }
        foreach ($categories as $category) {
            if($category->isParent()){
                $categoryPairs[$category->getId()] = $category->getName();
            }
            else{
                $parentCategoryId = $category->getParentId();
                $categoryPairs[$category->getId()] = $this->findById($parentCategoryId)->getName(). ' > ' . $category->getName();
            }
        }
        return $categoryPairs;
    }

    /**
     * Saves this category model
     * @return type
     */
    public function save(){
        return $this->getMapper()->save($this);
    }

    /**
     * Gets all different language translations for this category's name
     * @return \Core_Content_Models_ContentMapper rowset
     */
    public function getNameContent(){
        $contentMapper = new Core_Content_Models_ContentMapper();
        $content = $contentMapper->fetchByIdentifier($this->_name);
        return $content;
    }

    /**
     * Gets all different language translations for this category's description
     * @return \Core_Content_Models_ContentMapper rowset
     */
    public function getDescriptionContent(){
        $contentMapper = new Core_Content_Models_ContentMapper();
        $content = $contentMapper->fetchByIdentifier($this->_description);
        return $content;
    }

}

