<?php

class Core_Category_Models_CategoryMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Category_Models_DbTable_Category';

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Category_Models_Category $category)
    {
        $data = array(
            'name'          => $category->getName(),
            'description'   => $category->getDescription(),
            'parent_id'     => $category->getParentId(),
            'image'         => $category->getImage(),
        );

        if (null === ($id = $category->getId()))
        {
            unset($data['category_id']);
            return $this->getDbTable()->insert($data);
        }
        else
        {
            return $this->getDbTable()->update($data, array('category_id = ?' => $id));
        }
    }

    public function delete($categoryId){
        $where = $this->getDbTable()->getAdapter()->quoteInto('category_id = ?', $categoryId);
        return $this->getDbTable()->delete($where);
    }

    public function find($id, Core_Category_Models_Category $category)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result))
        {
            return;
        }
        $row = $result->current();
        $category->setId($row->category_id)
            ->setName($row->name)
            ->setDescription($row->description)
            ->setParentId($row->parent_id)
            ->setImage($row->image);
        return $category;
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();

        foreach($resultSet as $row)
        {
            $entry = new Core_Category_Models_Category();
            $entry->findById($row->category_id);
            $entries[] = $entry;
        }
        return $entries;
    }

    public function fetchAllCategories()
    {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('c' => 'categories'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    /**
     * Gets all products objects of products assigned to this category
     * @param int $categoryId
     * @return array \Core_Product_Models_Product
     */
    public function fetchCategoryProducts($categoryId){
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->from($this->_dbTable, array())
            ->joinUsing('product_categories', 'category_id')
            ->where('categories.category_id = ?', $categoryId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach($resultSet as $row){
            $productModel = new Core_Product_Models_Product();
            $entry = $productModel->findById($row->product_id);
            if($entry->getActive() === 1){
                $entries[] = $entry;
            }
        }
        if (isset($entries)) {
            return $entries;
        } else {
            return false;
        }
    }

    public function getAllProductsForHomeCategory() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('p' => 'products'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            foreach($resultSet as $row){
                $entries[] = $row;
            }
            return $entries;
        }
    }
}

