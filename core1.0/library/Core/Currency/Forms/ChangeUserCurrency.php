<?php

class Core_Currency_Forms_ChangeUserCurrency extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * 
     * @param type $languageContent
     */
    public function setupForm($currencies, $activeCurrency)
    {
        // Set properties of the form
        $this->setName('changeCurrency')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Change Currency');

        $currencyField = $this->createElement('select', 'currency')
            ->setLabel('Choose Currency: ')
            ->setRequired(true);
        
        foreach($currencies as $currency) {
            $currencyField->addMultiOption($currency->getCurrencyId(),$currency->getCurrency());
            $this->setOptions(
                    array(
                        'class' => 'required',
                        'filters'  => array('StringTrim', 'StripTags'),
                    )
                );
        }
        $currencyField->setValue($activeCurrency);

        // Submit Button
        $submit = $this->createElement('submit', 'submitcurrency')->setOptions(array('label' => 'Choose Currency', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this ->addElement($currencyField)
            ->addElement($submit);
    }
}