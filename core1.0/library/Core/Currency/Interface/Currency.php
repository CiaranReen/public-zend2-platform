<?php
/**
 * Created by JetBrains PhpStorm.
 * Product: Ciaran
 * Date: 05/09/2013
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */

interface Currency_Interface_Currency {
    
    public function getCurrencySymbol();
    
    public function getConversionRate($convFrom, $convTo, $amount);
    
    public function setConversionRate();
    
}