<?php
/**
 * Abstract class for Basket
 */

abstract class Currency_Abstract_Currency extends Frontend_Models_Abstract_AbstractModel implements Currency_Interface_Currency {

    protected static $table = 'country';
    protected $currencyId;
    protected $currencySymbol;
    protected $currency;
    protected $conversionRate;

    public function setCurrencyId($currencyId) {
        $this->currencyId = $currencyId;
        return $this;
    } 
    
    public function getCurrencyId() {
        return $this->currencyId;
    }
    
    public function setCurrencySymbol($currencySymbol) {
        $this->currencySymbol = $currencySymbol;
        return $this;
    }
    
    public function getCurrencySymbol() {
        return $this->currencySymbol;
    }
    
    public function getConversionRate($convFrom, $convTo, $amount) {
        return $this;
    }
    
    public function setConversionRate() {
        return $this;
    }
}