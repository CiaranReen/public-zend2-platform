<?php

class Core_Currency_Models_Currency {

    protected $currencyId;
    protected $currency;
    protected $currencySymbol;
    protected $countryId;
    protected $staticValue;
    protected $shortCode;

    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setCurrencyId($currencyId) {
        $this->currencyId = $currencyId;
        return $this;
    }

    public function getCurrencyId() {
        return $this->currencyId;
    }
    public function setCurrency($currency) {
        $this->currency = $currency;
        return $this;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function setCurrencySymbol($currencySymbol) {
        $this->currencySymbol = $currencySymbol;
        return $this;
    }

    public function getCurrencySymbol() {
        return $this->currencySymbol;
    }

    public function setCountryId($countryId) {
        $this->countryId = $countryId;
        return $this;
    }

    public function getCountryId() {
        return $this->countryId;
    }

    public function setStaticValue($staticValue) {
        $this->staticValue = (float) $staticValue;
        return $this;
    }

    public function getStaticValue() {
        return $this->staticValue;
    }

    public function setShortCode($shortCode) {
        $this->shortCode = (string) $shortCode;
        return $this;
    }

    public function getShortCode() {
        return $this->shortCode;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Currency_Models_CurrencyMapper());
        }
        return $this->_mapper;
    }

    public function load() {
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $getCurrency = $currencyMapper->load();
        return $getCurrency;
    }

    public function findById($currencyId) {
        $this->getMapper()->find($currencyId, $this);
        return $this;
    }

    /**
     * Sets the conversion rate for the currency model
     *
     * @return type
     */
    public function setConversionRate($currencyId) {
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $currencyMode = $currencyMapper->getCurrencyMode();

        if ($currencyMode->currency_mode === '0') {
            switch ($currencyId) {
                case 2:
                    $convFrom = 'gbp';
                    $convTo = 'usd';
                    $amount = 1;
                    break;
                case 3:
                    $convFrom = 'gbp';
                    $convTo = 'eur';
                    $amount = 1;
                    break;
                default:
                    $convFrom = 'gbp';
                    $convTo = 'gbp';
                    $amount = 1;
                    break;
            }
            if ($convFrom != $convTo) {
                $exchangeAmount = $this->getConversionRate($convFrom, $convTo, $amount = 1);
            } else {
                $exchangeAmount = 1.00;
            }
        } else {
            $exchangeAmount = $currencyMapper->getStaticRates($currencyId);
        }
        return $exchangeAmount;
    }

    /**
     * Gets the conversion rate
     *
     * @param type $convFrom
     * @param type $convTo
     * @param type $amount
     * @return type
     */
    public function getConversionRate($convFrom, $convTo, $amount = 1) {
        $get = file_get_contents(("https://www.google.com/finance/converter?a=$amount&from=$convFrom&to=$convTo"));


        $get = explode("<span class=bld>", $get);
        if (isset($get[1])) {
            $get = explode("</span>", $get[1]);
        } else {
            $get = explode("</span>", $get[0]);
        }
        $price = preg_replace("/[^0-9\.]/", null, $get[0]);
        return $price;
    }

}

?>
