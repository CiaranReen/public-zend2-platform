<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 9:14
 * To change this template use File | Settings | File Templates.
 */
class Currency_Models_CurrencyModel extends Currency_Abstract_Currency {
    
    /**
     * constructs the model and loads the currency to ensure that the user's currency is loaded
     */
    /*public function __construct(){
        $this->load();
    }*/
    
    /**
     * Sets the conversion rate for the currency model
     * 
     * @return type
     */
    public function setConversionRate() {
        
        switch ($this->getCurrencyId()) {
            case 2:
                $convFrom = 'gbp';
                $convTo = 'usd';
                $amount = 1;
                break;
            case 3:
                $convFrom = 'gbp';
                $convTo = 'eur';
                $amount = 1;
                break;
            default:
                $convFrom = 'gbp';
                $convTo = 'gbp';
                $amount = 1;
                break;
        }

        $exchangeAmount = $this->getConversionRate($convFrom, $convTo, $amount = 1);

        return $exchangeAmount;
    }

    /**
     * Gets the conversion rate
     * 
     * @param type $convFrom
     * @param type $convTo
     * @param type $amount
     * @return type
     */
    public function getConversionRate($convFrom, $convTo, $amount = 1) {
        $currencyAPIHandle = curl_init();

        curl_setopt($currencyAPIHandle, CURLOPT_URL, 'http://www.google.com/ig/calculator?q=' . urlencode($amount . $convFrom) . '=?' . urlencode($convTo));
        curl_setopt($currencyAPIHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($currencyAPIHandle, CURLOPT_CONNECTTIMEOUT, 0);

        $currencyAPIReturn = curl_exec($currencyAPIHandle);
        $priceData = explode('"', $currencyAPIReturn);
        $priceData = explode(' ', $priceData[3]);
        $price = $priceData[0];

        curl_close($currencyAPIHandle);

        return $price;
    }
    
    /**
     * loads the currency model to default if user is not logged in and sets it to the users
     * currency is user has logged in
     * 
     * @return \Currency_Models_CurrencyModel
     */
    public function load() {
        $userModel = new User_Models_User_UserModel();
        if($userModel->isLoggedIn()) {
             $userId = $userModel->getUserSession()->userId;
             $sql = "SELECT currency_id, symbol FROM currency cr
                    INNER JOIN address a ON a.country_id = cr.country_id
                    INNER JOIN user u ON u.address_id = a.address_id
                    WHERE u.user_id = " . $userId;
             
            $row = $this->getDbAdapter()->fetchRow($sql);
            $this->setCurrencyId($row['currency_id']);
            $this->setCurrencySymbol($row['symbol']);
        }
        else {
            $this->setCurrencyId(1);
            $this->setCurrencySymbol('£');
        }
        return $this;
    }

}

?>