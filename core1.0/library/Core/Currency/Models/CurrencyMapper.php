<?php

class Core_Currency_Models_CurrencyMapper {

    protected $_dbTable;
    const TABLE = 'Core_Currency_Models_DbTable_Currency';

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function findById($cId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('c' => 'currency'), array('*'))
            ->where('c.currency_id = ?', $cId);
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        return $resultSet;
    }

    public function load() {
        $currencyModel = new Core_Currency_Models_Currency();
        $userModel = new Core_User_Models_User();
        if ($userModel->isLoggedIn()) {
            $userId = $userModel->getUserSession()->userId;
            $resultSet = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('u.currency_id', 'c.symbol', 'u.user_id'))
                ->joinInner(array('c' => 'currency'), 'u.currency_id = c.currency_id')
                ->where('u.user_id='.$userId);
            $row = $this->getDbTable()->fetchRow($resultSet);
            $currencyModel->setCurrencyId($row['currency_id']);
            $currencyModel->setCurrency($row['currency']);
            $currencyModel->setCurrencySymbol($row['symbol']);
            $currencyModel->setStaticValue($row['static_value']);
            $currencyModel->setShortCode($row['shortcode']);
            if ($row === null) {
                $currencyModel->setCurrencyId(1);
                $currencyModel->setCurrency('British Pound');
                $currencyModel->setCurrencySymbol('&pound;');
                $currencyModel->setShortCode('gbp');
            }
        } else {
            $currencyModel->setCurrencyId(1);
            $currencyModel->setCurrency('British Pound');
            $currencyModel->setCurrencySymbol('&pound;');
            $currencyModel->setShortCode('gbp');
        }
        return $currencyModel;
    }

    public function find($id, Core_Currency_Models_Currency $currency)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result))
        {
            return;
        }
        $row = $result->current();
        $currency->setCurrencyId($row->currency_id)
            ->setCountryId($row->country_id)
            ->setCurrency($row->currency)
            ->setCurrencySymbol($row->symbol)
            ->setStaticValue($row->static_value)
            ->setShortCode($row->shortcode);

        return $currency;
    }

    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row)
        {
            $option = new Core_Currency_Models_Currency();
            $option ->setCurrencyId($row->currency_id)
                ->setCountryId($row->country_id)
                ->setCurrency($row->currency)
                ->setCurrencySymbol($row->symbol)
                ->setStaticValue($row->static_value)
                ->setShortCode($row->shortcode);

            $entries[] = $option;
        }
        return $entries;
    }

    public function getCurrencyById($currency_id)
    {
        $currency = new Core_Currency_Models_Currency();
        return $this->find($currency_id, $currency);
    }

    public function getCurrencyMode() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('s' => 'settings'), array('s.currency_mode'));
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        return $resultSet;
    }

    public function getStaticRates($currencyId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('c' => 'currency'), array('static_value'))
            ->where('c.currency_id = ?', $currencyId);
        $resultSet = $this->getDbTable()->fetchRow($stmt);
        foreach ($resultSet as $rs) {
            $exchangeAmount = $rs;
        }
        return $exchangeAmount;
    }

    public function getCurrencies() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('c' => 'currency'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function saveValue($value, $cId) {
        $data = array (
            'static_value' => $value,
        );
        $where = "currency_id = '" . $cId . "'";
        $save = $this->getDbTable()->update($data, $where);
        if ($save === 1) {
            return true;
        } else {
            return false;
        }
    }

}
