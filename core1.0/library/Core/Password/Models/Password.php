<?php

class Core_Password_Models_Password extends App_Model_Abstract {

    public function encrypt($password){
        $salt = 'lf-salt';
        $encrypted = hash('sha1', $salt.$password);
        return $encrypted;
    }
    
    public function verify($inputPassword, $password){
        return ($this->encrypt($inputPassword) === $password);
    }
    
}

