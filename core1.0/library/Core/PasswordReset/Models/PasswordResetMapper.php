<?php

class Core_PasswordReset_Models_PasswordResetMapper {

    protected $_dbTable;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_PasswordReset_Models_DbTable_PasswordReset');
        }
        return $this->_dbTable;
    }
    
    public function saveRequest($userId, $url) {
        $data = array (
            'user_id' => $userId,
            'url' => $url,
            'expiration' => new Zend_Db_Expr('NOW() + INTERVAL 30 MINUTE'),
        );
        $save = $this->getDbTable()->insert($data);
        return $save;
    }

    public function getUserIdByRequestId($resetRequest) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('pr' => 'password_reset'), array('pr.user_id'))
            ->where('pr.url = ?', $resetRequest);
        $userId = $this->getDbTable()->fetchRow($stmt);
        foreach ($userId as $uId) {
            $userIds = $uId;
        }
        return $userIds;
    }
    
    public function getUserId($email) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('u.user_id'))
                ->where('u.email = ?', $email);
        $userId = $this->getDbTable()->fetchRow($stmt);
        foreach ($userId as $uId) {
            $userIds = $uId;
        }
        return $userIds;
    }
    
    public function checkExpirationTime($resetRequest) {
        $currentTime = date('Y-m-d h:i:s');
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('pr' => 'password_reset'), array('*'))
                ->where('url = ?', $resetRequest)
                ->where('expiration >= ?', $currentTime);
        $checkRequest = $this->getDbTable()->fetchRow($stmt);
        return $checkRequest;
    }

    public function generateRandomString() {
        $length = 8;
        $randomString = substr(str_shuffle("0123456789"), 0, $length);
        return $randomString;
    }
}