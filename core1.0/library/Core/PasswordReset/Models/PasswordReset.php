<?php

class Core_PasswordReset_Models_PasswordReset {

    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_PasswordReset_Models_PasswordResetMapper());
        }
        return $this->_mapper;
    }

    public function getUserIdByRequestId($resetRequest) {
        $userId = $this->getMapper()->getUserIdByRequestId($resetRequest);
        return $userId;
    }
    
    public function getUserId($email) {
        $userId = $this->getMapper()->getUserId($email);
        return $userId;
    }
    
    public function saveRequest($userId, $url) {
        $save = $this->getMapper()->saveRequest($userId, $url);
        return $save;
    }
    
    public function checkExpirationTime($resetRequest) {
        $check = $this->getMapper()->checkExpirationTime($resetRequest);
        return $check;
    }

    public function saveNewPassword($password, $userId) {
        $savePwd = $this->getMapper()->saveNewPassword($password, $userId);
        return $savePwd;
    }
}

