<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_PasswordReset_Forms_EnterCode extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupEnterCodeForm() {

        // Set properties of the form
        $this->setName('enterCode')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Enter Code');

        // Name Field
        //TODO dynamically generated email for the user on 'label'
        $code = $this->createElement('text', 'code')
                ->setOptions(
                array(
                    'label' => 'Enter the 8 digit code sent to your email, or click the link in the email to reset your password',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Search', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($code)
                ->addElement($submit);
    }

}
