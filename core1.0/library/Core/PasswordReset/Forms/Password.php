<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_PasswordReset_Forms_Password extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupPasswordForm() {

        // Set properties of the form
        $this->setName('resetPassword')
            ->setMethod(self::METHOD_POST)
            ->setAttrib('class', 'form-signin')
            ->setDescription('Reset Password');

        // Forename Field
        $password = $this->createElement('password', 'password')
            ->setOptions(
                array(
                    'label'    => 'New Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        // Surname Field
        $pwdconf = $this->createElement('password', 'pwdconf')
            ->setOptions(
                array(
                    'label'    => 'Re-type Password',
                    'required' => true,
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->addValidator('Identical', false, array('token' => 'password'))
            ->addErrorMessage('Your passwords do not match.');


        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Reset Password', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($password)
            ->addElement($pwdconf)
            ->addElement($submit);
    }

}
