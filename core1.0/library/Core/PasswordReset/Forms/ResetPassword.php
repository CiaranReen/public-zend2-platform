<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_PasswordReset_Forms_ResetPassword extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupResetPasswordForm() {

        // Set properties of the form
        $this->setName('resetPassword')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Reset Password');

        // Name Field
        $query = $this->createElement('text', 'query')
                ->setOptions(
                array(
                    'label' => 'Search by email',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
        );

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Search', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($query)
                ->addElement($submit);
    }

}
