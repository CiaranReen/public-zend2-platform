<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_Usergroup_Forms_Edit extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }
    
    public function setupEditForm($ug) {
        // Set properties of the form
        $this->setName('editUsergroup')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Edit Usergroup');

        // Name Field
        $ugName = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Name:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($ug->getUsergroupName());

        // Business Unit Field
        $businessUnit = $this->createElement('text', 'business_unit')
                        ->setOptions(
                                array(
                                    'label' => 'Business Unit:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($ug->getBusinessUnit());

        // Can they pay by invoice Field
        $invoice = $this->createElement('radio', 'invoice')
                        ->setOptions(
                                array(
                                    'label' => 'Can the user pay via invoice:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setMultiOptions(array('Yes'=>'Yes', 'No'=>'No'))
                        ->setValue($ug->getInvoice());

        // Minimum amount to spend Field
        $invoiceLimit = $this->createElement('text', 'invoice_limit')
                        ->setOptions(
                                array(
                                    'label' => 'If yes, what is the amount they must spend before they can pay via invoice: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setValue($ug->getInvoiceLimit());

        // Invoice Type Field
        $invoiceType = $this->createElement('radio', 'invoice_type')
                        ->setOptions(
                                array(
                                    'label' => 'Invoice Type: ',
                                    'required' => true,
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setMultiOptions(array('Pro Forma'=>'Pro Forma', 'Standard'=>'Standard'))
                        ->setValue($ug->getInvoiceType());

        // Currency Field
        $currency = $this->createElement('radio', 'currency')
                        ->setOptions(
                                array(
                                    'label' => 'What currency do this usergroup see: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setMultiOptions(array('GBP'=>'GBP', 'EUR'=>'EUR'))
                        ->setValue($ug->getCurrency());

        //Currency Invoice Field
        $currencyInvoice = $this->createElement('radio', 'currency_invoice')
                        ->setOptions(
                                array(
                                    'label' => 'What currency do this usergroup get invoiced in:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                         
                        )->setMultiOptions(array('GBP'=>'GBP', 'EUR'=>'EUR'))
                        ->setValue($ug->getCurrencyInvoice());

        //VAT Field
        $vat = $this->createElement('radio', 'vat')
            ->setOptions(
                array(
                    'label' => 'Does this usergroup see VAT?',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )

            )->setMultiOptions(array('1'=>'Yes', '0'=>'No'))
            ->setValue($ug->getVat());

                        

        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Save', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($ugName)
                ->addElement($businessUnit)
                ->addElement($invoice)
                ->addElement($invoiceLimit)
                ->addElement($invoiceType)
                ->addElement($currency)
                ->addElement($currencyInvoice)
                ->addElement($vat)
                ->addElement($submit);
    }
}