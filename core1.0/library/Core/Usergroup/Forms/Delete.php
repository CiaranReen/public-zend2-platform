<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Ciaran
 * Date: 25/09/2013
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
class Core_Usergroup_Forms_Delete extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    /**
     * @param Usergroup_Abstract_Usergroup $ug
     */
    public function setupDeleteForm($ug) {
        /* @var $user Usergroup_Abstract_Usergroup */

        // Set properties of the form
        $this->setName('deleteUsergroup')
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Delete Usergroup');

        //Usergroup ID Field
        $ugId = $this->createElement('hidden', 'id')
                        ->setOptions(
                                array(
                                    'required' => true,
                                )
                        )->setValue($ug->getUsergroupId());

        // Name Field
        $ugName = $this->createElement('text', 'name')
                        ->setOptions(
                                array(
                                    'label' => 'Name:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setAttrib('disabled', 'disabled')
                ->setValue($ug->getUsergroupName());

        // Business Unit Field
        $businessUnit = $this->createElement('text', 'business_unit')
                        ->setOptions(
                                array(
                                    'label' => 'Business Unit:',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setAttrib('disabled', 'disabled')
                ->setValue($ug->getBusinessUnit());

        // Can they pay by invoice Field
        $invoice = $this->createElement('radio', 'invoice')
                ->setOptions(
                        array(
                            'label' => 'Can the user pay via invoice:',
                            'required' => true,
                            'class' => 'required',
                            'filters' => array('StringTrim', 'StripTags'),
                        )
                )->setAttrib('disabled', 'disabled')
                ->setMultiOptions(array('Yes' => 'Yes', 'No' => 'No'))
                ->setValue($ug->getInvoice());

        // Minimum amount to spend Field
        $invoiceLimit = $this->createElement('text', 'invoice_limit')
                        ->setOptions(
                                array(
                                    'label' => 'If yes, what is the amount they must spend before they can pay via invoice: ',
                                    'required' => true,
                                    'class' => 'required',
                                    'filters' => array('StringTrim', 'StripTags'),
                                )
                        )->setAttrib('disabled', 'disabled')
                ->setValue($ug->getInvoiceLimit());

        // Invoice Type Field
        $invoiceType = $this->createElement('radio', 'invoice_type')
                ->setOptions(
                        array(
                            'label' => 'Invoice Type: ',
                            'required' => true,
                            'filters' => array('StringTrim', 'StripTags'),
                        )
                )->setAttrib('disabled', 'disabled')
                ->setMultiOptions(array('Pro Forma' => 'Pro Forma', 'Standard' => 'Standard'))
                ->setValue($ug->getInvoiceType());

        // Currency Field
        $currency = $this->createElement('radio', 'currency')
                ->setOptions(
                        array(
                            'label' => 'What currency do this usergroup see: ',
                            'required' => true,
                            'class' => 'required',
                            'filters' => array('StringTrim', 'StripTags'),
                        )
                )->setAttrib('disabled', 'disabled')
                ->setMultiOptions(array('GBP' => 'GBP', 'EUR' => 'EUR'))
                ->setValue($ug->getCurrency());

        //Currency Invoice Field
        $currencyInvoice = $this->createElement('radio', 'currency_invoice')
                ->setOptions(
                        array(
                            'label' => 'What currency do this usergroup get invoiced in:',
                            'required' => true,
                            'class' => 'required',
                            'filters' => array('StringTrim', 'StripTags'),
                        )
                )->setAttrib('disabled', 'disabled')
                ->setMultiOptions(array('GBP' => 'GBP', 'EUR' => 'EUR'))
                ->setValue($ug->getCurrencyInvoice());

        //Currency Invoice Field
        $vat = $this->createElement('radio', 'vat')
            ->setOptions(
                array(
                    'label' => 'Does this usergroup see VAT?',
                    'required' => true,
                    'class' => 'required',
                    'filters' => array('StringTrim', 'StripTags'),
                )
            )->setAttrib('disabled', 'disabled')
            ->setMultiOptions(array('0' => 'Yes', '1' => 'No'))
            ->setValue($ug->getVat());


        // Submit Button
        $submit = $this->createElement('submit', 'submit')->setOptions(array('label' => 'Delete Usergroup', 'class' => 'btn btn-primary'));

        // Add controls to the form
        $this->addElement($ugId)
                ->addElement($ugName)
                ->addElement($businessUnit)
                ->addElement($invoice)
                ->addElement($invoiceLimit)
                ->addElement($invoiceType)
                ->addElement($currency)
                ->addElement($currencyInvoice)
                ->addElement($vat)
                ->addElement($submit);
    }

}