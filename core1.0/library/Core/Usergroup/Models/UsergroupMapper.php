<?php

class Core_Usergroup_Models_UsergroupMapper {

    protected $_dbTable;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Usergroup_Models_DbTable_Usergroup');
        }
        return $this->_dbTable;
    }

    public function getAllUserGroups($returnType = null) {
        $resultSet = $this->getDbTable()->fetchAll();
        if($returnType === 'object'){
            foreach ($resultSet as $row) {
                $usergroupModel = new Core_Usergroup_Models_Usergroup();
                $usergroups[] = $usergroupModel->findById($row->user_group_id);
            }
            return $usergroups;
        }
        return $resultSet;
    }

    public function add(Core_Usergroup_Models_Usergroup $ug) {
        $ugData = array(
            'user_group_name' => $ug->getUsergroupName(),
            'business_unit' => $ug->getBusinessUnit(),
            'invoice' => $ug->getInvoice(),
            'invoice_limit' => $ug->getInvoiceLimit(),
            'invoice_type' => $ug->getInvoiceType(),
            'currency' => $ug->getCurrency(),
            'currency_invoice' => $ug->getCurrencyInvoice(),
        );

        $where = "user_group_id = '" . $ug->getUsergroupId() . "'";
        $affectedRows = $this->getDbTable()->insert($ugData, $where);

        if ($affectedRows == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $affectedRows;
        }
    }

    public function addUsergroup($ugData) {
        $this->setUsergroupName($ugData->name)
                ->setBusinessUnit($ugData->business_unit)
                ->setInvoice($ugData->invoice)
                ->setInvoiceLimit($ugData->invoice_limit)
                ->setInvoiceType($ugData->invoice_type)
                ->setCurrency($ugData->currency)
                ->setCurrencyInvoice($ugData->currency_invoice)
            ->setVat($ugData->vat);
        $this->add($this);
        return $this;
    }

    public function delete($ugId) {
        $where = "user_group_id = '" . $ugId . "'";
        $delete = $this->getDbTable()->delete($where);
        return $delete;
    }

    public function find($ugId, Core_Usergroup_Models_Usergroup $ugModel) {
        $result = $this->getDbTable()->find($ugId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $ugModel->setUsergroupId($row->user_group_id)
                ->setUsergroupName($row->user_group_name)
                ->setBusinessUnit($row->business_unit)
                ->setInvoice($row->invoice)
                ->setInvoiceLimit($row->invoice_limit)
                ->setInvoiceType($row->invoice_type)
                ->setCurrency($row->currency)
                ->setCurrencyInvoice($row->currency_invoice)
                ->setVat($row->vat);
        return $ugModel;
    }

    public function save(Core_Usergroup_Models_Usergroup $ug) {
        $ugData = array(
            'user_group_name' => $ug->getUsergroupName(),
            'business_unit' => $ug->getBusinessUnit(),
            'invoice' => $ug->getInvoice(),
            'invoice_limit' => $ug->getInvoiceLimit(),
            'invoice_type' => $ug->getInvoiceType(),
            'currency' => $ug->getCurrency(),
            'currency_invoice' => $ug->getCurrencyInvoice(),
            'vat' => $ug->getVat()
        );
        $where = "user_group_id = '" . $ug->getUsergroupId() . "'";

        $resultSet = $this->getDbTable()->update($ugData, $where);

        if ($resultSet == 0) {
            // Query failed; Record was not inserted
            return false;
        } else {
            // Return number of affected rows
            return $resultSet;
        }
    }

    protected function _populateUserData($usergroupData) {
        $ugId = $usergroupData['user_group_id'];
        $populatedUserData = array(
            'user_group_id' => $ugId,
            'user_group_name' => $usergroupData->user_group_name,
            'business_unit' => $usergroupData->business_unit,
            'invoice' => $usergroupData->invoice,
            'invoice_limit' => $usergroupData->invoice_limit,
            'invoice_type' => $usergroupData->invoice_type,
            'currency' => $usergroupData->currency,
            'currency_invoice' => $usergroupData->currency_invoice,
        );
        return $populatedUserData;
    }

    public function getAllCategories() {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('c' => 'categories'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }

    public function saveCategoriesByUsergroup(array $categories) {
        $ugModel = new Core_Usergroup_Models_Usergroup();
        $where = "user_group_id = '" . $ugModel->getUsergroupId() . "'";
        $resultSet = $this->getDbTable()->delete('user_groups_category', $where);
        foreach ($categories as $categoryId) {
            $catData = array(
                'user_group_id' => $ugModel->getUsergroupId(),
                'category_id' => $categoryId,
            );
            $resultSet = $this->getDbTable()->insert('user_groups_category', $catData);
        }
        return $resultSet;
    }

    public function getUsergroupCategories($ugId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('ugc' => 'user_groups_category'), array('category_id'))
                ->where('user_group_id = ' . $ugId);
        $resultSet = $this->getDbTable()->fetchAll($row);
        //return $resultSet;
        if (count($resultSet) >= 1) {
            foreach ($resultSet as $row) {
                $categories[] = $row->category_id;
            }
            return $categories;
        } else {
            return false;
        }
    }

    public function getUsergroupUsers($ugId) {
        $row = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('u' => 'user'), array('*'))
                ->where('user_group_id =' . $ugId . '');
        $resultSet = $this->getDbTable()->fetchAll($row);
        return $resultSet;
    }
    
    //Generate a check if the name exists in the db - 'This name exists already - are you sure you want to continue?' - Ciaran. To do 
    public function isNameUnique($name) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('ug' => 'user_groups'), array('*'))
                ->where('user_group_name = ?', $name);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) >= 1) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function usergroupAvailability($ugId, $cId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('ug' => 'user_groups'), array('*'))
                ->joinInner(array('ugc' => 'user_groups_category'), 'ug.user_group_id = ugc.user_group_id')
                ->where('ugc.user_group_id = ?', $ugId)
                ->where('ugc.category_id = ?', $cId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public function isAssignedToCategories($ugId) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from(array('ug' => 'user_groups'), array('*'))
                ->joinInner(array('ugc' => 'user_groups_category'), 'ug.user_group_id = ugc.user_group_id')
                ->where('ugc.user_group_id = ?', $ugId);
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            return true;
        }
        else {
            return false;
        }
    }

}