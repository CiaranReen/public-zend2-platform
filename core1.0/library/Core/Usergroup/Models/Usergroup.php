<?php

class Core_Usergroup_Models_Usergroup
{
    protected $user_group_id;
    protected $user_group_name;
    protected $business_unit;
    protected $invoice;
    protected $invoice_limit;
    protected $invoice_type;
    protected $currency;
    protected $currency_invoice;
    protected $_vat;
    protected $_mapper;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value)
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods))
            {
                $this->$method($value);
            }
        }
        return $this;
    }

    /**
     * @param $ugId
     * @return $this
     */
    public function setUsergroupId($ugId) {
        $this->user_group_id = $ugId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsergroupId() {
        return $this->user_group_id;
    }

    /**
     * @param $ugName
     * @return $this
     */
    public function setUsergroupName($ugName) {
        $this->user_group_name = $ugName;
        return $this;
    }

    public function getUsergroupName() {
        return $this->user_group_name;
    }

    /**
     * @param $businessUnit
     * @return $this
     */
    public function setBusinessUnit($businessUnit) {
        $this->business_unit = $businessUnit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBusinessUnit() {
        return $this->business_unit;
    }

    /**
     * @param $invoice
     * @return $this
     */
    public function setInvoice($invoice) {
        $this->invoice = $invoice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoice() {
        return $this->invoice;
    }

    /**
     * @param $invoiceLimit
     * @return $this
     */
    public function setInvoiceLimit($invoiceLimit) {
        $this->invoice_limit = $invoiceLimit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceLimit() {
        return $this->invoice_limit;
    }

    /**
     * @param $invoiceType
     * @return $this
     */
    public function setInvoiceType($invoiceType) {
        $this->invoice_type = $invoiceType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvoiceType() {
        return $this->invoice_type;
    }

    /**
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency) {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrency() {
        return $this->currency;
    }

    /**
     * @param $currencyInvoice
     * @return $this
     */
    public function setCurrencyInvoice($currencyInvoice) {
        $this->currency_invoice = $currencyInvoice;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCurrencyInvoice() {
        return $this->currency_invoice;
    }


    /**
     * @param $vat
     * @return mixed
     */
    public function setVat($vat) {
        $this->_vat = $vat;
        return $vat;
    }

    /**
     * @return mixed
     */
    public function getVat() {
        return $this->_vat;
    }

    /**
     * @param $mapper
     * @return $this
     */
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Usergroup_Models_UsergroupMapper());
        }
        return $this->_mapper;
    }

    public function save() {
        return $this->getMapper()->save($this);
    }

    protected function _populateUserData($usergroupData) {
        $populatedUserData = array(
            'user_group_id' => $usergroupData->user_group_id,
            'user_group_name' => $usergroupData->user_group_name,
            'business_unit' => $usergroupData->business_unit,
            'invoice' => $usergroupData->invoice,
            'invoice_limit' => $usergroupData->invoice_limit,
            'invoice_type' => $usergroupData->invoice_type,
            'currency' => $usergroupData->currency,
            'currency_invoice' => $usergroupData->currency_invoice,
            'vat' => $usergroupData->vat
        );
        return $populatedUserData;
    }

    public function getAllCategories() {
        $getCats = $this->getMapper()->getAllCategories();
        return $getCats;
    }

    public function getAllUserGroups($returnType = null) {
        $allUsergroups = $this->getMapper()->getAllUserGroups($returnType);
        return $allUsergroups;
    }

    public function addUsergroup($ugData) {
        $this->setUsergroupName($ugData->name)
            ->setBusinessUnit($ugData->business_unit)
            ->setInvoice($ugData->invoice)
            ->setInvoiceLimit($ugData->invoice_limit)
            ->setInvoiceType($ugData->invoice_type)
            ->setCurrency($ugData->currency)
            ->setCurrencyInvoice($ugData->currency_invoice)
            ->setVat($ugData->vat);
        $this->getMapper()->add($this);
        return $this;
    }

    public function delete($ugId) {
        $this->getMapper()->delete($ugId);
        return $this;
    }

    public function findById($ugId) {
        $this->getMapper()->find($ugId, $this);
        return $this;
    }

    public function findUgId($ugId) {
        $getId = $this->getMapper()->findUgId($ugId);
        return $getId;
    }

    public function getUsergroupUsers($ugId) {
        $getUgUsers = $this->getMapper()->getUsergroupUsers($ugId);
        return $getUgUsers;
    }

    public function getUsergroupCategories($ugId) {
        $getUgCats = $this->getMapper()->getUsergroupCategories($ugId);
        return $getUgCats;
    }

    public function usergroupAvailability($ugId, $cId) {
        $check = $this->getMapper()->usergroupAvailability($ugId, $cId);
        return $check;
    }

    public function isAssignedToCategories($ugId){
        $check = $this->getMapper()->isAssignedToCategories($ugId);
        return $check;
    }



    /**
     * Builds an array for checkbox
     *
     * @return array|boolean
     */
    public function getPairs(array $usergroups = NULL) {
        try {
            if(!$usergroups){
                $usergroups = $this->getAllUserGroups('object');
            }
            $usergroupPairs         = array();

            if(empty($usergroups))
            {
                throw new Exception(' No User Groups Pairs found! ');
            }
            foreach($usergroups as $userGroup) {
                $usergroupPairs[$userGroup->getUsergroupId()] = $userGroup->getUsergroupName();
            }
            return $usergroupPairs;
        }
        catch(Exception $e) {
            echo $e->getMessage();
            error_log($e->getMessage());

            return false;
        }
    }

}

