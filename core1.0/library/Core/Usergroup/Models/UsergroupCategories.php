<?php

class Core_Usergroup_Models_UsergroupCategories
{
    protected $user_group_id;
    protected $user_group_name;
    protected $business_unit;
    protected $invoice;
    protected $invoice_limit;
    protected $invoice_type;
    protected $currency;
    protected $currency_invoice;
    protected $_mapper;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        } 
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setUgId($ugId) {
        $this->user_group_id = $ugId;
        return $this;
    }

    public function setUgName($ugName) {
        $this->user_group_name = $ugName;
        return $this;
    }

    public function setBusinessUnit($businessUnit) {
        $this->business_unit = $businessUnit;
        return $this;
    }

    public function setInvoice($invoice) {
        $this->invoice = $invoice;
        return $this;
    }

    public function setInvoiceLimit($invoiceLimit) {
        $this->invoice_limit = $invoiceLimit;
        return $this;
    }

    public function setInvoiceType($invoiceType) {
        $this->invoice_type = $invoiceType;
        return $this;
    }

    public function setCurrency($currency) {
        $this->currency = $currency;
        return $this;
    }

    public function setCurrencyInvoice($currencyInvoice) {
        $this->currency_invoice = $currencyInvoice;
        return $this;
    }

    public function getUgId() {
        return $this->user_group_id;
    }

    public function getUgName() {
        return $this->user_group_name;
    }

    public function getBusinessUnit() {
        return $this->business_unit;
    }

    public function getInvoice() {
        return $this->invoice;
    }

    public function getInvoiceLimit() {
        return $this->invoice_limit;
    }

    public function getInvoiceType() {
        return $this->invoice_type;
    }

    public function getCurrency() {
        return $this->currency;
    }

    public function getCurrencyInvoice() {
        return $this->currency_invoice;
    }
        
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Usergroup_Models_UsergroupCategoriesMapper());
		}
		return $this->_mapper;
    }
    
    public function saveCategoriesByUsergroup($ugId, $ug) {
        $this->getMapper()->saveCategoriesByUsergroup($ugId, $ug);
        return $this;
    }
    
}

