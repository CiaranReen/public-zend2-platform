<?php

class Core_Usergroup_Models_UsergroupCategoriesMapper {

    protected $_dbTable;

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Usergroup_Models_DbTable_UsergroupCategories');
        }
        return $this->_dbTable;
    }

    public function saveCategoriesByUsergroup($ug, array $categories) {
        $where = "user_group_id = '" . $ug->getUsergroupId() . "'";
        $resultSet = $this->getDbTable()->delete($where);
        foreach ($categories as $categoryId) {
            $catData = array(
                'user_group_id' => $ug->getUsergroupId(),
                'category_id' => $categoryId,
            );
            $resultSet = $this->getDbTable()->insert($catData);
        }
        return $resultSet;
    }
}