<?php

class Core_Colourway_Models_ColourwayMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Colourway_Models_DbTable_Colourway';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Colourway_Models_Colourway $colourway)
    {
        $data = array(
            'name'   => $colourway->getName(),
            'hex' => $colourway->getHex(),
            'code'   => $colourway->getCode(),
        );
 
        if (null === ($id = $colourway->getId())) 
        {
            unset($data['colourway_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('colourway_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Colourway_Models_Colourway $colourway)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $colourway->setId($row->colourway_id)
                  ->setName($row->name)
                  ->setHex($row->hex)
                  ->setCode($row->code);
        return $colourway;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $entry = new Core_Colourway_Models_Colourway();
            $entry->findById($row->colourway_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    public function delete(Core_Colourway_Models_Colourway $colourway){
        echo 'colourway id '.$colourway->getId();
        if($colourway instanceof Core_Colourway_Models_Colourway){
            $where = $this->getDbTable()->getAdapter()->quoteInto('colourway_id = ? ', $colourway->getId());
            return $this->getDbTable()->delete($where);
        }
        else{
            throw new Exception('This is not a valid colourway model');
        }
    }
}

