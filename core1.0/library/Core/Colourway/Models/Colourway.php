<?php

class Core_Colourway_Models_Colourway extends App_Model_Abstract
{
    protected $_id;
    protected $_name;
    protected $_hex;
    protected $_code;
    
    protected $_mapper;
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setName($name)
    {
        $this->_name = (string) $name;
        return $this;
    }
 
    public function getName()
    {
        return $this->_name;
    }
    
    public function setHex($hex)
    {
        $this->_hex = (string) $hex;
        return $this;
    }
 
    public function getHex()
    {
        return $this->_hex;
    }
    
    public function setCode($code)
    {
        $this->_code = (string) $code;
        return $this;
    }
 
    public function getCode()
    {
        return $this->_code;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Colourway_Models_ColourwayMapper());
		}
		return $this->_mapper;
    }
    
    /**
     * Constructs colourway object using colourway id
     * @param int $colourwayId
     * @return \Core_Colourway_Models_Colourway
     */
    public function findById($colourwayId) {
        $this->getMapper()->find($colourwayId, $this);
        return $this;
    }
    
    /**
     * Gets all colourways in database
     * @return array \Core_Colourway_Models_Colourway
     */
    public function getAllColourways() 
    {
        $allColourways = $this->getMapper()->fetchAll();
        return $allColourways;
    }
    
    public function delete(){
        return $this->getMapper()->delete($this);
    }
    
    /**
     * Save Colourway instance to databsae using db mapper
     * @return type
     */
    public function save()
    {
        return $this->getMapper()->save($this);
    }
    
    public function getNameByLanguage(Core_Language_Models_Languages $language = null){
        if(!$language){
            $languageMapper = new Core_Language_Models_LanguagesMapper();
            $language       = $languageMapper->fetchByIp();
        }
        $contentMapper  = new Core_Content_Models_ContentMapper();
        $content        = $contentMapper->fetchIndividual($language->getIsoShortcode(), $this->_name);
        if(!$content){
            return ;
        }
        return $content->getContent();
    }
    
    /**
     * Gets all different language translations for this category's name
     * @return \Core_Content_Models_ContentMapper rowset
     */
    public function getNameContent(){
        $contentMapper = new Core_Content_Models_ContentMapper();
        $content = $contentMapper->fetchByIdentifier($this->_name);
        return $content;
    }
    
}

