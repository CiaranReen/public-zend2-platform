<?php

class Core_Colourway_Models_DbTable_Colourway extends Zend_Db_Table_Abstract
{
    /** Table Name **/
    protected $_name = 'colourways';
    
    protected $_dependentTables = array(
        'Core_Product_Models_ProductImagesMapper',
        'Core_Stock_Models_StockMapper',
    );
}

