<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 17/09/2013
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */

class Colourway_Models_ColourwayModel extends Colourway_Abstract_Colourway {
    
    /**
     * adds new colourway to table
     * @param array $data
     * @return boolean
     */
    public function addColourway(array $data){
        try {
            $colourwayTableFields = array('name', 'hex', 'code');
            foreach ($data as $field => $value) {
                if (in_array($field, $colourwayTableFields)) {
                    $colourwayData[$field] = $value;
                }
            }
            $this->getDbAdapter()->insert( self::$table , $colourwayData );
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }
    
    /**
     * updates colourway with new details
     * @param array $data
     * @return boolean
     */
    public function updateColourway(array $data){
        try {
            $colourwayTableFields = array('name', 'hex', 'code');
            foreach ($data as $field => $value) {
                if (in_array($field, $colourwayTableFields)) {
                    $colourwayData[$field] = $value;
                }
            }
            $where = "colourway_id = '".$this->getColourwayId()."'";
            $affectedRows = $this->getDbAdapter()->update( self::$table , $colourwayData , $where );
            if($affectedRows){
                $this->setName($data['name']);
                $this->setHex($data['hex']);
                $this->setCode($data['code']);
                return true;
            }else{
                return false;
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }
    
    /**
     * deletes colourway from colourways table
     * @return boolean
     */
    public function removeColourway(){
        $sql = 'DELETE FROM ' . self::$table . ' WHERE colourway_id = ' . $this->getColourwayId();
        $this->getDbAdapter()->query($sql);
        return true;
    }
    
    /**
     * gets all colourways in database
     * @return array
     * @throws Exception
     */
    public function getAllColourways() {
        try{
            $sql    = "SELECT * FROM ".self::$table;
            $rows   = $this->getDbAdapter()->fetchAll($sql);
            if(empty($rows)){
                throw new Exception ('No colourways found!');
            }
            return $rows;
        }
        catch (Exception $e){
            error_log($e->getMessage());
        }
    }
    
    /**
     * loads colourway object using colourway id
     * @return boolean|\Colourway_Models_ColourwayModel
     */
    public function load(){
        try{
            $sql = "SELECT * FROM ".self::$table."
                    WHERE colourway_id = '".$this->getColourwayId()."'";
            $row = $this->getDbAdapter()->fetchRow($sql);
            if(!empty($row)){
                $this->setName($row['name']);
                $this->setHex($row['hex']);
                $this->setCode($row['code']);
            }
            return $this;
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    
}