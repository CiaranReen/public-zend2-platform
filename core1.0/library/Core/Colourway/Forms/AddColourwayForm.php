<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Emmanuel
 * Date: 11/11/2013
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */

class Core_Colourway_Forms_AddColourwayForm extends Zend_Form {

	public function init() {
        $languageModel      = new Core_Language_Models_Languages();
        
        // Set properties of the form
        $this->setName('colourway')
                ->setMethod(self::METHOD_POST)
                ->setDescription('Add New Colourway')
                ->setOptions(array('class' => ''));


        // Languages
            $languages = $this->createElement('select', 'languages');
            
            $languages->setOptions(
                    array(
                        'label'     => 'Select Language',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
            if($languageModel->getActivatedLanguages()){
                $languages->addMultiOptions($languageModel->getPairs($languageModel->getActivatedLanguages()));
            }else{
                $languages->addMultiOptions($languageModel->getPairs());
            }
            $this->addElement($languages);
            
            foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
                $languageId     = $activatedLanguage->getId();
                $language       = $activatedLanguage->getName();
                $isoShortcode   = $activatedLanguage->getIsoShortcode();
                if($isoShortcode === 'gb'){
                    $required = true;
                }else{
                    $required = false;
                }
                $this->addElement(
                    $this->createElement('text', 'name_'.$isoShortcode)
                            ->setOptions(
                            array(
                                     'label'    => 'Colourway Name [ '.$language.' ]',
                                     'class'    => 'required lang-name',
                                     'id'       => 'name-' . $languageId,
                                     'required' => $required,
                                     'filters'  => array('StringTrim', 'StripTags'),
                            )
                    )
                );
            }

        // Hex
        $hex = $this->createElement('text', 'hex')
                        ->setOptions(
                        array(
                                 'label'    => 'Hex Value',
                                 'required' => true,
                                 'class'    => 'required hex',
                                 'filters'  => array('StringTrim', 'StripTags'),
                                 'autocomplete' => 'off',
                        )
                );

        // Code
        $code = $this->createElement('text', 'code')
                        ->setOptions(
                        array(
                                 'label'    => 'Code',
                                 'required' => true,
                                 'class'    => 'required',
                                 'filters'  => array('StringTrim', 'StripTags'),
                        )
                );


        // Submit Button
        $submit = $this->createElement('submit', 'submit')
                ->setOptions(
                        array(
                            'label' => 'Save',
                            'class'    => 'btn btn-primary',
                        )
                );

        // Add controls to the form
        $this->addElement($hex)
                ->addElement($code)
                ->addElement($submit);

	}
    
    public function populate(Core_Colourway_Models_Colourway $colourway){
        $this->init();
        $this->setDescription('Edit "'.$colourway->getNameByLanguage().'" Details <a href = "/admin/products/manage-colourways">[new]</a>');
        $languageModel      = new Core_Language_Models_Languages();
        foreach($languageModel->getActivatedLanguages() as $activatedLanguage){
            $isoShortcode   = $activatedLanguage->getIsoShortcode();
            if($colourway->getNameContent()){
                $this->getElement('name_'.$isoShortcode)->setValue($colourway->getNameByLanguage($activatedLanguage));
            }
        }
        $this->getElement('hex')->setValue($colourway->getHex());
        $this->getElement('code')->setValue($colourway->getCode());
        $this->getElement('submit')->setLabel('Update');
    }

}