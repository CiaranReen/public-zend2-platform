<?php
/**
 * Class Application_Bootstrap
 *
 * Bootstraps the CO3 Core platform
 */
Zend_Session::start();
//$_SESSION['basket'] = $productId;


class Application_Bootstrap {

	/**
	 * Bootstrap the application
	 *
	 * @return Application_Bootstrap $this
	 */
	public function bootstrap() {
		// Setup environment
		$this->bootstrapEnvironment();

		//	Setup session
		$this->setupSession();

		return $this;
	}

	/**
	 * Setup app operating environment
	 *
	 * @return Application_Bootstrap $this
	 */
	protected function bootstrapEnvironment() {
		$environment = new Application_Environment();
		$environment->setupEnvironment();

		return $this;
	}

	/**
	 * Setup session
	 *
	 * @return Application_Bootstrap $this
	 */
	protected function setupSession() {
		$session = new Session_Session();

		// Set default Language
		if(!$session->getData('Language')) {
			$session->setData('Language', 'en');
		}

		return $this;
	}

	/**
	 * Run the app by working out which controller and action to dispatch and which params,
	 * if any, need to be passed to that action for this request and then dispatching it
	 *
	 * @return Application_Bootstrap $this
	 */
	public function run() {
		// The REQUEST_URI server variable is used to create a path to the correct controller file.
		$requestUri = $_SERVER['REQUEST_URI'];

		// Check for parameters
		if($this->hasParams($requestUri)) {
			// Get the parameters passed through the URL
			$params = $this->getParams($requestUri);

			// Chop off params (they just complicate things from here on)
			// We'll pass them through to the appropriate action later

			// Get name of first param
			$firstParam = key($params);
			// Trim off everything after the first param
			$requestUri = substr($requestUri, 0, strripos($requestUri, $firstParam));
		} else {
			$params = "";
		}

		// Check for custom routes

		// If request uri doesn't end with a slash, add one
		if(substr($requestUri, strlen($requestUri)-1, 1) != "/") {
			$requestUri .= "/";
		}

		$customRoute = $this->getCustomRoute($requestUri);
		if($customRoute !== false || strlen($customRoute) > 0) {
			// A custom route exists. Use that instead of the actual request uri
			$requestUri = $customRoute;
		}

		// Get the module name
		$moduleName = $this->getModuleName($requestUri);

        // Get the controller name
		$controllerName = $this->getControllerName($requestUri);

		// Get the action name
		$actionName = $this->getActionName($requestUri);

		// Construct the controller class name
		$controllerClassName = $moduleName . '_Controllers_' . $controllerName . '_' . $controllerName . 'Controller';

		// The controller is autoloaded
		// If the controller can't be found, the autoloader will kick us to a 404 page
		$controller = new $controllerClassName();

		if(strlen($actionName) > 0) {
			// Dispatch relevant action with params
			$controller->$actionName($params);
		}

                return $this;
	}

	/**
	 * Checks to see if we are passing any params in the request uri
	 *
	 * @param string $requestUri
	 *
	 * @return bool
	 */
	protected function hasParams($requestUri) {
		$parts = explode('/', $requestUri);

		if(isset($parts[1]) && !empty($parts[1]) && $parts[1] == "admin") {
			// Special case: Admin URLs
			if(isset($parts[5]) && !empty($parts[5])) {
				$hasParams = true;
			} else {
				$hasParams = false;
			}
		} else {
			if(isset($parts[4]) && !empty($parts[4])) {
				$hasParams = true;
			} else {
				$hasParams = false;
			}
		}

		return $hasParams;
	}

	/**
	 * Parse any params from the request uri
	 *
	 * @param string $requestUri
	 *
	 * @return string
	 */
	protected function getParams($requestUri) {
		$parts = explode('/', $requestUri);

		if(isset($parts[1]) && !empty($parts[1]) && $parts[1] == "admin") {
			// Special case: Admin URLs

			// Don't treat these request URIs as standalone modules
			$adminControllers = array("index", "login", "logout", "forgotpassword");
			// This if branch is for passing params to the Admin controllers
			if(isset($parts[2]) && !empty($parts[2]) && in_array($parts[2], $adminControllers)) {
				// Check if there are params
				if(isset($parts[4]) && !empty($parts[4])) {
					$rawParams = array_slice($parts, 4, NULL, true);
					$params = array();
					foreach($rawParams as $key => $value) {
						// Even keys become associative keys in the new params array, odd keys become the values
						if(($key % 2) == 0) {
							$key = $key + 1;
							if(isset($rawParams[$key])) {
								$params[$value] = $rawParams[$key];
							}
						}
					}
					// Given request URI /admin/forgotpassword/resetpassword/id/1/key/b79dc6595519ed4769bae81409f674f04d24c823/,
					// $params now contains: array('id' => 15, 'key' => 'b79dc6595519ed4769bae81409f674f04d24c823')
				} else {
					$params = "";
				}

				return $params;
			}


			if(isset($parts[5]) && !empty($parts[5])) {
				$rawParams = array_slice($parts, 5, NULL, true);
				$params = array();
				foreach($rawParams as $key => $value) {
					// Even keys become associative keys in the new params array, odd keys become the values
					if(($key % 2) != 0) {
						$key = $key + 1;
						if(isset($rawParams[$key])) {
							$params[$value] = $rawParams[$key];
						}
					}
				}
				// Given request URI /admin/forgotpassword/resetpassword/id/1/key/b79dc6595519ed4769bae81409f674f04d24c823/,
				// $params now contains: array('id' => 15, 'key' => 'b79dc6595519ed4769bae81409f674f04d24c823')
			} else {
				$params = "";
			}

		} else {
			if(isset($parts[4]) && !empty($parts[4])) {
				$rawParams = array_slice($parts, 4, NULL, true);
				$params = array();
				foreach($rawParams as $key => $value) {
					// Even keys become associative keys in the new params array, odd keys become the values
					if(($key % 2) == 0) {
						$key = $key + 1;
						if(isset($rawParams[$key])) {
							$params[$value] = $rawParams[$key];
						}
					}
				}
				// Given request URI /frontend/404/index/class/Address_Controllers_Address_AddressController/,
				// $params now contains: array('class' => 'Address_Controllers_Address_AddressController')
			} else {
				$params = "";
			}
		}

		return $params;
	}

	/**
	 * Checks to see if this request uri maps to a custom route and returns that
	 *
	 * @param string $requestUri
	 *
	 * @return bool|string
	 */
	protected function getCustomRoute($requestUri) {

		$moduleName = $this->getModuleName($requestUri);

		// Get module config
		if($this->isModuleEnabled($moduleName)) {
			$moduleConfig = $this->getModuleConfig($moduleName);

			if(isset($moduleConfig['routes']) && count($moduleConfig['routes']) > 0) {
				$customRoute = false;
				foreach($moduleConfig['routes'] as $oldRoute => $newRoute) {
					if($requestUri == $oldRoute) {
						// We've found a custom route. Use this to load the correct controller
						$customRoute = $newRoute;
						break;
					}
				}

				return $customRoute;
			} else {
				// No custom routes defined for this module
				return false;
			}

		} else {
			// Module is disabled
			return false;
		}
	}

	/**
	 * Parse the module name from the request uri
	 *
	 * @param string $requestUri
	 *
	 * @return string
	 */
	protected function getModuleName($requestUri) {

		if($requestUri == "/") {
			// Special case: Frontend
			$moduleName = "Frontend";
		} elseif($requestUri == "/admin" || $requestUri == "/admin/") {
			// Special case: Admin
			$moduleName = "Admin";
		} else {
			// Translates /modulename/controller/action/params into Modulename
			$parts = explode('/', $requestUri);
			$moduleName = ucfirst($parts[1]);

			// Don't treat these request URIs as standalone modules
			$exclusions = array("index", "login", "logout", "forgotpassword");
			// Special case for Admin modules
			if($moduleName == "Admin" && !in_array($parts[2], $exclusions) ) {
				$moduleName = ucfirst($parts[2]);
			}
		}

		return $moduleName;
	}

	/**
	 * Parse the controller name from the request uri
	 *
	 * @param string $requestUri
	 *
	 * @return string
	 */
	protected function getControllerName($requestUri) {

		$parts = explode('/', $requestUri);
		$controllerName = ucfirst($parts[2]);

		return $controllerName;
	}

	/**
	 * Parse the action name from request uri, returning default action if none is specified in url
	 *
	 * @param string $requestUri
	 *
	 * @return string
	 */
	protected function getActionName($requestUri) {

		$actionName = explode('/',$requestUri);
                $request3 = trim($actionName[3]);
		if( isset( $request3 ) && !empty( $request3 ) ) {
			// Given request uri of '/module/controller/action/params', sets action name to 'action'
			$actionName = $request3;
		} else {
			// Set action name to default of 'index'
			$actionName = "index";
		}

		return ucfirst($actionName);
	}

	/**
	 * Determine if module is enabled
	 *
	 * @param string $moduleName
	 *
	 * @return bool
	 */
	protected function isModuleEnabled($moduleName) {
		include('modulesConfig.php');

		if(isset($modulesConfigArray[$moduleName])) {
			return $modulesConfigArray[$moduleName]['enabled'];
		} else {
			// Config for module does not exist, assume it is disabled
			return false;
		}
	}

	/**
	 * Get module config array for module
	 *
	 * @param string $moduleName
	 *
	 * @return array
	 */
	protected function getModuleConfig($moduleName) {
		include('modulesConfig.php');

		return $modulesConfigArray[$moduleName];
	}
}
