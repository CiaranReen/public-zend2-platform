<?php

/**
 * Class Application_Environment
 *
 * Establishes various environment settings
 */

class Core_Application_Environment {

	/* @var $config Zend_Config */
	public $config;
    
    /* @var $config Zend_Config */
	public $coreConfig;

	/* @var $db Zend_Db_Adapter_Mysqli */
	public $db;
    
    /* @var $db Zend_Db_Adapter_Mysqli */
	public $coreDb;

	/**
	 * Setup the environment
	 */
	public function setupEnvironment() {
		// Setup site config
		include('config.php');
		$this->config = new Zend_Config($configArray[$_SERVER['HTTP_HOST']]);
        $sitehost = $_SERVER['HTTP_HOST'];
        $corehost = str_replace('lifefitness', 'core', $sitehost);
        
        //Setup Core config
        include('../co3-core/config.php');
        $this->coreConfig = new Zend_Config($configArray[$corehost]);
        
		// Setup PHP environment
		ini_set('error_reporting',  $this->config->php->error_reporting );
		ini_set('display_errors',   $this->config->php->display_errors  );
        ini_set('error_log',        $this->config->php->error_log       );
		// Setup DB connection
		$this->setDbAdapter();
        
        // Setup Core DB connection
		$this->setCoreDbAdapter();

		return $this;
	}

	/**
	 * Setup DB adapter
	 *
	 * @return $this
	 */
	protected function setDbAdapter() {
		$this->db = new Zend_Db_Adapter_Mysqli(array(
                'dbname'   => $this->config->database->dbname,
                'username' => $this->config->database->username,
                'password' => $this->config->database->password,
                'host'     => $this->config->database->hostname,
                'port'     => $this->config->database->port,
                'charset'  => 'utf8'
       ));
		return $this;
	}

	/**
	 * Get db adapter
	 *
	 * @return Zend_Db_Adapter_Mysqli
	 */
	public function getDbAdapter() {
		if($this->db === NULL) {
			$this->setupEnvironment();
		}
		return $this->db;
	}
    
    /**
	 * Setup DB adapter
	 *
	 * @return $this
	 */
	protected function setCoreDbAdapter() {
		$this->coreDb = new Zend_Db_Adapter_Mysqli(array(
                'dbname'   => $this->coreConfig->database->dbname,
                'username' => $this->coreConfig->database->username,
                'password' => $this->coreConfig->database->password,
                'host'     => $this->coreConfig->database->hostname,
                'port'     => $this->coreConfig->database->port,
                'charset'  => 'utf8'
       ));
		return $this;
	}

	/**
	 * Get core db adapter
	 *
	 * @return Zend_Db_Adapter_Mysqli
	 */
	public function getCoreDbAdapter() {
		if($this->coreDb === NULL) {
			$this->setupEnvironment();
		}
		return $this->coreDb;
	}

	/**
	 * Get the config object
	 *
	 * @return Zend_Config
	 */
	public function getConfig() {
		return $this->config;
	}

	/**
	 * Get the client website document root folder
	 *
	 * @return string
	 */
	public static function getClientWebsiteDocumentRoot() {
		include('hostMap.php');
		if(isset($hostMapArray[$_SERVER['HTTP_HOST']])) {
			return dirname($_SERVER['DOCUMENT_ROOT']) . '/' . $hostMapArray[$_SERVER['HTTP_HOST']];
		} else {
			// Must be loading the core platform
			return dirname($_SERVER['DOCUMENT_ROOT']) . '/co3-core';
		}
	}
    
    /**
	 * Get the client website document root folder
	 *
	 * @return string
	 */
	public static function getCoreDocumentRoot() {
        // Must be loading the core platform
        return dirname($_SERVER['DOCUMENT_ROOT']) . '/co3-core';
	}

	/**
	 * Determine if module is enabled
	 *
	 * @param string $moduleName
	 *
	 * @return bool
	 */
	public static function isModuleEnabled($moduleName) {
		include('modulesConfig.php');

		if(isset($modulesConfigArray[$moduleName])) {
			return $modulesConfigArray[$moduleName]['enabled'];
		} else {
			// Config for module does not exist, assume it is disabled
			return false;
		}
	}

	/**
	 * Get the module config for all modules
	 *
	 * @return array
	 */
	public static function getCompleteModuleConfig() {
		include('modulesConfig.php');

		return $modulesConfigArray;
	}

	/**
	 * @param int $line	Line number of the script debugToErrorLog() was called from, passed by magic constant __LINE__
	 * @param string $file	Absolute filepath of script debugToErrorLog() was called from, passed by mgaic constant __FILE__
	 * @param string $message Debug message
	 * @param string $value	The variable to log
	 * @param string $log_file	Absolute path to the log file. Pass $config->error_log in a non-object-oriented context
	 *
	 * @return bool
	 */
	public static function debugToErrorLog($line, $file, $message, $value, $log_file) {
		$status = false;

		if(file_exists($log_file) && is_writeable($log_file)) {
			$status = error_log(date("Y-m-d H:i:s") . ":LINE:{$line}:{$file}: {$message}: " . print_r($value, true) . "\n", 3, $log_file);
		}

		if($status) {
			// Wrote to log file successfully
			return true;
		} else {
			// Could not write to log file
			return false;
		}
	}


	/**
	 * @param int $line	Line number of the script debugToErrorLog() was called from, passed by magic constant __LINE__
	 * @param string $file	Absolute filepath of script debugToErrorLog() was called from, passed by mgaic constant __FILE__
	 * @param string $message Debug message
	 * @param string $value	The variable to log
	 *
	 * @return bool
	 */
	public static function debugToScreen($line, $file, $message, $value) {
		echo "<pre>" . date("Y-m-d H:i:s") . ":LINE:{$line}:{$file}: {$message}: \n" . print_r($value, true) . "</pre>\n";
	}

	public static function debug($line, $file, $message, $value, $log_file) {
		return self::debugToErrorLog($line, $file, $message, $value, $log_file);
	}
}
