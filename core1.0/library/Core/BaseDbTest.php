<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 24/03/14
 * Time: 14:44
 */

require_once '/var/www/co3-lynx/core/library/Zend/Test/PHPUnit/DatabaseTestCase.php';
require_once '/var/www/co3-lynx/core/application/Bootstrap.php';

abstract class BaseDbTest extends Zend_Test_PHPUnit_DatabaseTestCase
{

    protected $_application;
    static private $pdo = null;
    private $conn = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;

    }

    /**
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(dirname(__FILE__).'/_files/fightscience.xml');
    }

    public function setUp()
    {
        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();
        $this->_mapper = new Core_User_Models_UserMapper();
        $this->_model = new Core_User_Models_User();
    }

    public function appBootstrap()
    {
        $this->_application = new Zend_Application( APPLICATION_ENV, APPLICATION_PATH . "/configs/application.ini" );
        $this->_application->bootstrap();
    }
}