<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 03/03/14
 * Time: 11:55
 */

class Core_History_Models_HistoryMapper {

    protected $_dbTable;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_History_Models_DbTable_History');
        }
        return $this->_dbTable;
    }

    public function saveAction($action, $type, $userId, $name) {
        $data = array (
            'action' => $action,
            'type' => $type,
            'user_id' => $userId,
            'name' => $name,
        );

        $this->getDbTable()->insert($data);
    }

    /**
     * manual statement due to joins with user table
     */
    public function fetchAll() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('h' => 'history'), array ('*'))
            ->joinInner(array('u' => 'user'), 'h.user_id = u.user_id');

        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    /**
     * This is the function for the admin index page
     */
    public function fetchLatestTen() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('h' => 'history'), array ('*'))
            ->joinInner(array('u' => 'user'), 'h.user_id = u.user_id')
            ->order('history_id DESC')
            ->limit(10, 0);

        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }


}