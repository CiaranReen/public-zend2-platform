<?php

class Core_Settings_Models_Settings extends App_Model_Abstract {

    protected $_id;
    protected $_companyName;
    protected $_siteUrl;
    protected $_orderNumberPrefix;
    protected $_orderNumberStartPoint;
    protected $_orderNumberFormat;
    protected $_orderEmailSender;
    protected $_gaUsername;
    protected $_gaPassword;
    protected $_gaProfileId;
    protected $_vpsProtocol;
    protected $_vendor;
    protected $_orderDescription;
    
    protected $_mapper;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    public function setCompanyName($companyName) {
        $this->_companyName = (string) $companyName;
        return $this;
    }

    public function getCompanyName() {
        return $this->_companyName;
    }

    public function setSiteUrl($siteUrl){
        $this->_siteUrl = (string) $siteUrl;
        return $this;
    }

    public function getSiteUrl(){
        return $this->_siteUrl;
    }
    
    public function setOrderNumberPrefix($orderNumberPrefix) {
        $this->_orderNumberPrefix = (string) $orderNumberPrefix;
        return $this;
    }

    public function getOrderNumberPrefix() {
        return $this->_orderNumberPrefix;
    }
    
    public function setOrderNumberStartPoint($orderNumberStartPoint) {
        $this->_orderNumberStartPoint = (int) $orderNumberStartPoint;
        return $this;
    }

    public function getOrderNumberStartPoint() {
        return $this->_orderNumberStartPoint;
    }
    
    public function setOrderNumberFormat($orderNumberFormat) {
        $this->_orderNumberFormat = (string) $orderNumberFormat;
        return $this;
    }

    public function getOrderNumberFormat() {
        return $this->_orderNumberFormat;
    }
    
    public function setOrderEmailSender($orderEmailSender){
        $this->_orderEmailSender = (string) $orderEmailSender;
        return $this;
    }
    
    public function getOrderEmailSender(){
        return $this->_orderEmailSender;
    }

    public function getGAUsername() {
        return $this->_gaUsername;
    }

    public function setGAUsername($gaUsername) {
        $this->_gaUsername = $gaUsername;
        return $this;
    }

    public function getGAPassword() {
        return $this->_gaPassword;
    }

    public function setGAPassword($gaPassword) {
        $this->_gaPassword = $gaPassword;
        return $this;
    }


    public function getGAProfileId() {
        return $this->_gaProfileId;
    }

    public function setGAProfileId($gaProfileId) {
        $this->_gaProfileId = $gaProfileId;
        return $this;
    }
    
    public function getVpsProtocol() {
        return $this->_vpsProtocol;
    }

    public function setVpsProtocol($vpsProtocol) {
        $this->_vpsProtocol = $vpsProtocol;
        return $this;
    }

    public function getVendor() {
        return $this->_vendor;
    }

    public function setVendor($vendor) {
        $this->_vendor = $vendor;
        return $this;
    }

    public function getOrderDescription() {
        return $this->_orderDescription;
    }

    public function setOrderDescription($orderDescription) {
        $this->_orderDescription = $orderDescription;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Settings_Models_SettingsMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs Settings object
     * @return \Core_Settings_Models_Settings
     */
    public function __construct() {
        $row = $this->getMapper()->getSettings();
        $this->setCompanyName($row->company_name)
                ->setSiteUrl($row->site_url)
                ->setOrderNumberPrefix($row->order_number_prefix)
                ->setOrderNumberStartPoint($row->order_number_startpoint)
                ->setOrderNumberFormat($row->order_number_format)
                ->setGAUsername($row->ga_username)
                ->setGAPassword($row->ga_password)
                ->setGAProfileId($row->ga_profile_id)
                ->setVpsProtocol($row->vps_protocol)
                ->setVendor($row->vendor)
                ->setOrderDescription($row->order_description);
        return $this;
    }

    public function getGoogleAnalytics(){
        $totalVisits = 0;
        $totalBounce = 0;
        $email = $this->getGAUsername();
        $password = $this->getGAPassword();
        $profileId = $this->getGAProfileId();
        if(!$email || !$password || !$profileId){
            return ;
        }
        $service = Zend_Gdata_Analytics::AUTH_SERVICE_NAME;
        $client = Zend_Gdata_ClientLogin::getHttpClient($email, $password, $service);
        $analytics = new Zend_Gdata_Analytics($client); 
        
        $accounts = $analytics->getAccountFeed();

        $profileResult = $analytics->getDataFeed($analytics->newAccountQuery()->profiles());

        $profileIds = array();
        /**
         * @var \ZendGData\Analytics\DataEntry $dataBit
         */
         foreach ($profileResult as $dataBit) {
             /**
               * @var \ZendGData\App\Extension\Element $element
               */
//             foreach ($dataBit->getExtensionElements() as $element) {
//                 $attributes = $element->getExtensionAttributes();
//                 echo'<pre>'; var_dump($attributes);
//                 if ($attributes['name']['value']=='ga:profileId') {
//                     $profileIds[] = $attributes['value']['value'];
//                 }
//            }
        }
//        echo '<pre>'; var_dump($profileIds); die();
//        foreach ($accounts as $account) {
//            echo '<pre>'; var_dump($account);
////            die();
//        }
        
        $days = 30;
        $startDate = date('Y-m-d', strtotime('-'.$days.' days'));
        $endDate   = date('Y-m-d', strtotime('today'));
        $query = $analytics->newDataQuery()->setProfileId($profileId)
                        ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_BOUNCES)   
                        ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITS)
                        ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_NEW_VISITS)
                        ->addMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITORS)
                        ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_MEDIUM) 
                        ->addDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_SOURCE) 
                        ->addFilter("ga:browser==Firefox")
                        ->setStartDate($startDate)
                        ->setEndDate($endDate)
                        ->addSort(Zend_Gdata_Analytics_DataQuery::METRIC_VISITS, true) 
                        ->addSort(Zend_Gdata_Analytics_DataQuery::METRIC_BOUNCES, false) 
                        ->setMaxResults(50);
 
        $result = $analytics->getDataFeed($query);
        
        $visits    = array();
        $newVisits = array();
        $visitors  = array();

        $date = '';
        foreach($result as $k => $row) {
            if ($k === 0) {
                $date = date('jS F', strtotime($startDate));
            }
            elseif ($k === $days) {
                $date = date('jS F', strtotime($endDate));
            }
            else {
                $daysLeft = intval($k) - intval($days);
                $date = date('jS F', strtotime(($k - $days).' days'));
            }

            $visits[] = array(
                $date,
                $row->getMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITS)->getValue()
            );
            $newVisits[] = array(
                $date,
                $row->getMetric(Zend_Gdata_Analytics_DataQuery::METRIC_NEW_VISITS)->getValue()
            );
            $visitors[] = array(
                $date,
                $row->getMetric(Zend_Gdata_Analytics_DataQuery::METRIC_VISITORS)->getValue()
            );
        }
//        echo '<pre>'; var_dump($graphNewVisits); die('visits');
        
        
//        foreach($result as $row){
//            echo $row->getDimension(Zend_Gdata_Analytics_DataQuery::DIMENSION_SOURCE)."\n"; // default dimension
//            echo $row->getDimension('ga:medium')."\n"; // dimensions using string instead of constant
//            echo $row->getMetric('ga:visits')."\n"; // metrics only
//            echo $row->getValue('ga:bounces')."\n";  // alternative using getValue
//            
//            $totalVisits += $row->getMetric('ga:visits')->value;
//            $totalBounce += $row->getValue('ga:bounces')->value;
//        }
        $googleAnalytics = array(
            'visits' => $visits,
            'newVisits' => $newVisits,
            'visitors' => $visitors
        );
        
        return (object) ($googleAnalytics);
    }
    
    public static function getSettings(){
        $settingsModel = new Core_Settings_Models_Settings();
        return $settingsModel;
    }

}

