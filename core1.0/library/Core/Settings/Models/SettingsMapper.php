<?php

class Core_Settings_Models_SettingsMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Settings_Models_DbTable_Settings';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Settings_Models_Settings $settings)
    {
        $data = array(
            'company_name'              => $settings->getCompanyName(),
            'site_url'                  => $settings->getSiteUrl(),
            'order_number_prefix'       => $settings->getOrderNumberPrefix(),
            'order_number_startpoint'   => $settings->getOrderNumberStartPoint(),
            'order_number_format'       => $settings->getOrderNumberFormat(),
            'ga_username'               => $settings->getGAUsername(),
            'ga_password'               => $settings->getGAPassword(),
            'ga_profile_id'             => $settings->getGAProfileId(),
            'vps_protocol'              => $settings->getVpsProtocol(),
            'vendor'                    => $settings->getVendor(),
            'order_description'         => $settings->getOrderDescription()
        );
        
        if (null === ($id = $settings->getId())) {
            $settings->setId(1);
        } 
 
        return $this->getDbTable()->update($data, array('setting_id = ?' => $settings->getId()));
    }
    
    public function getSettings()
    {
        $select = $this->getDbTable()->select()->setIntegrityCheck(false)
                ->from('settings');
        $resultSet = $this->getDbTable()->fetchRow($select);
        return $resultSet;
    }
    
}

