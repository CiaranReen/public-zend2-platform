<?php

class Core_Settings_Forms_Rates extends Zend_Form {

    public function init() {
        // Do any pre-form setup here
    }

    public function setupRatesForm($c) {

        // Set properties of the form
        $this->setName('setRates'.$c->currency)
                ->setMethod(self::METHOD_POST)
                ->setAttrib('class', 'form-signin')
                ->setDescription('Set Rates');

        $id = $this->createElement('hidden', $c->currency . $c->currency_id)->setValue($c->currency_id);

        // Rates Field
        $rates = $this->createElement('text', $c->currency)
                ->setOptions(
                array(
                    'Label' => $c->currency,
                    'required' => true,
                    'class' => 'required',
                )
        )->setValue($c->static_value);

        $submit = $this->createElement('submit', $c->currency_id)->setOptions(array('label' => 'Change', 'class' => 'btn btn-primary'));

        $this->addElement($id)
            ->addElement($rates)
            ->addElement($submit);
        return $this;
    }

}