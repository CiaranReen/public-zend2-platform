<?php

class Core_Search_Models_SearchLogMapper {

    protected $_dbTable;

    const TABLE = 'Core_Search_DbTable_SearchLog';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function save(Core_Search_Models_SearchLog $searchLog) {
        $data = array(
            'search_term' => $searchLog->getSearchTerm(),
            'count' => $searchLog->getCount(),
            'date_searched' => $searchLog->getDateSearched()
        );
        

        if (null === ($id = $searchLog->getId())) {
            unset($data['search_log_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('search_log_id = ?' => $id));
        }
    }

    public function find($id, Core_Search_Models_SearchLog $searchLog)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $searchLog->setId($row->search_log_id)
                ->setSearchTerm($row->search_term)
                ->setCount($row->count)
                ->setDateSearched($row->date_searched);
        return $searchLog;
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll(
            $this->getDbTable()->select()->order(
                    array(
                        'count DESC',
                        'date_searched DESC'
                    )
            )
        );
        $entries   = array();
        foreach($resultSet as $row)
        {
            $entry = new Core_Search_Models_SearchLog();
            $entry->findById($row->search_log_id);
            $entries[] = $entry;
        }
        return $entries;
    }

}

