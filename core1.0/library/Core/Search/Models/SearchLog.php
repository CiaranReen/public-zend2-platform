<?php

class Core_Search_Models_SearchLog extends App_Model_Abstract {

    protected $_id;
    protected $_searchTerm;
    protected $_count;
    protected $_dateSearched;
    
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }
    
    public function getSearchTerm() {
        return $this->_searchTerm;
    }

    public function setSearchTerm($searchTerm) {
        $this->_searchTerm = (string) $searchTerm;
        return $this;
    }

    public function getCount() {
        return $this->_count;
    }

    public function setCount($count) {
        $this->_count = (int) $count;
        return $this;
    }

    public function getDateSearched() {
        return $this->_dateSearched;
    }

    public function setDateSearched($dateSearched) {
        $this->_dateSearched = $dateSearched;
        return $this;
    }
    
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Search_Models_SearchLogMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs searchLog object using searchLog id
     * @param int $searchLogId
     * @return \Core_Search_Models_SearchLog
     */
    public function findById($searchLogId) {
        $this->getMapper()->find($searchLogId, $this);
        return $this;
    }

    /**
     * Gets all searchLogs in database
     * @return array \Core_Search_Models_SearchLog
     */
    public function getAllSearchLogs() {
        $allSearchLogs = $this->getMapper()->fetchAll();
        return $allSearchLogs;
    }
    
    public function getTopFiveSearchLogs(){
        $top5 = array();
        $counter = 5;
        $allSearchLogs = $this->getAllSearchLogs();
        if($allSearchLogs){
            foreach($allSearchLogs as $searchLog){
                if($counter != 0){
                    $top5[] = $searchLog;
                    $counter--;
                }
            }
        }
        return $top5;
    }
    
    /*
     * Save The SearchLog
     */
    public function save() {
        $count = 1;
        if($this->getAllSearchLogs()){
            foreach($this->getAllSearchLogs() as $searchLog){
                if($this->getSearchTerm() == $searchLog->getSearchTerm()){
                    $id = $searchLog->getId();
                    $count = $searchLog->getCount() + 1;
                }
            }
        }
        if(isset($id)){
            $this->setId($id);
        }
        $this->setCount($count);
        return $this->getMapper()->save($this);
    }

}

