<?php

class Core_Search_Models_Search extends App_Model_Abstract {
    
    public function getSearchResults($search) {
        $productMapper  = new Core_Product_Models_ProductMapper();
        $select         = $productMapper->getDbTable()
                            ->select()->setIntegrityCheck(false)
                            ->from(array('p' => 'products'), array('*'))
                            ->joinInner(array('s' => 'stock'), 's.product_id = p.product_id')
                            ->joinInner(array('c' => 'colourways'), 'c.colourway_id = s.colourway_id')
                            ->where('p.name LIKE ?', '%'. $search .'%')
                            ->orWhere('p.description LIKE ?', '%'. $search .'%')
                            ->orWhere('p.code LIKE ?', '%'. $search .'%')
                            ->orWhere('s.code LIKE ?', '%'. $search .'%')
                            ->orWhere('c.name LIKE ?', '%'. $search .'%')
                            ->group('p.product_id');
        $rows = $productMapper->getDbTable()->fetchAll($select);
        $entries = array();
        foreach($rows as $row){
            $productModel = new Core_Product_Models_Product();
            $entry = $productModel->findById($row->product_id);
            $entries[] = $entry;
        }
        return $entries;
    }
}
?>
