<?php

class Core_Content_Forms_Editlanguage extends Zend_Form
{
    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * 
     * @param type $languageContent
     */
    public function setupForm($language) 
    {
        // Set properties of the form
        $this->setName('changeLanguage')
            ->setMethod('post')
            ->setDescription('Choose a language')
            ->setAction('/admin/content/manage');
        
        $this->addElement('hidden', 'language', array(
            'value'      => $language->getId()
        ));

        //Add all content to be managed
        
        foreach($language->getContent() as $content)
        {
            $this->addElement('textarea', 'content'.strval($content->getIdentifier()), array(
                'label'      => $content->getIdentifier(),
                'value'      => $content->getContent(),
                'required'   => true,
                'validators' => array(
                    array('validator' => 'StringLength', 'options' => array(0, 20))
                )
            ));
        }
        
        // Submit Button
        $submit = $this->createElement('submit', 'editsubmit')->setOptions(array('label' => 'Save All edited content', 'class' => 'btn btn-primary'));
        $this->addElement($submit);
    }
}