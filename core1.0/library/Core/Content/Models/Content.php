<?php

class Core_Content_Models_Content
{
    protected $_id;
    protected $_identifier;
    protected $_content;
    protected $isoShortcode;
 
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setIdentifier($identifier)
    {
        $this->_identifier = (string) $identifier;
        return $this;
    }
 
    public function getIdentifier()
    {
        return $this->_identifier;
    }
 
    public function setIsoShortcode($isoShortcode)
    {
        $this->isoShortcode = (string) $isoShortcode;
        return $this;
    }
 
    public function getIsoShortcode()
    {
        return $this->isoShortcode;
    }
    
    public function setContent($content)
    {
        $this->_content = $content;
        return $this;
    }
 
    public function getContent()
    {
        return stripslashes(html_entity_decode($this->_content));
    }

    public function getLanguageContent($language)
    {
        $content = new Core_Content_Models_ContentMapper();
        $allcontent = $content->fetchAllByIsoCode($language->getIsoShortcode());
        
        return $allcontent;
    }
    
    public function saveContent()
    {
        $content = new Core_Content_Models_ContentMapper();
        return $content->save($this);
    }
    
}

