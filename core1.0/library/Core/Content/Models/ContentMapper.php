<?php

class Core_Content_Models_ContentMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable('Core_Content_Models_DbTable_Content');
        }
        return $this->_dbTable;
    }

    public function save(Core_Content_Models_Content $content)
    {
        $data = array(
            'identifier'   => $content->getIdentifier(),
            $content->getIsoShortcode()   => $content->getContent(),
        );

        if(null === ($id = $content->getId()))
        {
            unset($data['content_id']);
            return $this->getDbTable()->insert($data);
        }
        else
        {
            return $this->getDbTable()->update($data, array('content_id = ?' => $id));
        }
    }

    public function fetchAllByIsoCode($isoShortcode) {

        $cache = Zend_Registry::get('Memcache');
        $key = 'iso_codes';
        $entries = array();

        if (!$language = $cache->load($key)) {
            $resultSet = $this->getDbTable()->fetchAll();
            $cache->save($resultSet, 'iso_codes');

            //Make sure that the content objects get set up correctly..
            foreach($resultSet as $row) {
                $entry = new Core_Content_Models_Content();

                $entry->setId($row->content_id)
                    ->setIdentifier($row->identifier)
                    ->setIsoShortcode($isoShortcode)
                    ->setContent($row->$isoShortcode);

                $entries[$row->identifier] = $entry;
            }

        } else {
            $iso_codes = Zend_Registry::get('Memcache')->load($key);
            //Make sure that the content objects get set up correctly..
            foreach($iso_codes as $row) {
                $entry = new Core_Content_Models_Content();

                $entry->setId($row->content_id)
                    ->setIdentifier($row->identifier)
                    ->setIsoShortcode($isoShortcode)
                    ->setContent($row->$isoShortcode);

                $entries[$row->identifier] = $entry;
            }
        }

        return $entries;

    }

    public function fetchIndividual($iso, $name)
    {
        if (isset($name)) {
            $content = $this->fetchAllByIsoCode($iso);

            return $content[$name];
        }
    }

    public function fetchByIdentifier($identifier)
    {
        $select = $this->getDbTable()
            ->select()->setIntegrityCheck(false)
            ->where('content.identifier = ?', $identifier);
        $row = $this->getDbTable()->fetchRow($select);
        if (!$row)
        {
            return;
        }
        return $row;
    }
}

