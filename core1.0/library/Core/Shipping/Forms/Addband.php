<?php

class Core_Shipping_Forms_Addband extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    /**
     * Set Up Add Company Form
     */
    public function setupNewBandForm()
    {
        // Set properties of the form
        $this->setName('addBand')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a band');

        //Start Weight
        $startWeight = $this->createElement('text', 'start_weight')
            ->setOptions(
                array(
                    'label'    => 'Start Weight',
                    'required' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        //End Weight
        $endWeight = $this->createElement('text', 'end_weight')
            ->setOptions(
                array(
                    'label'    => 'End Weight',
                    'required' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );

        $submit = $this->createElement('submit', 'Add')->setOptions(array('label' => 'Add Band', 'class' => 'btn btn-primary'));

        
        // Add controls to the form
        $this->addElement($startWeight);
        $this->addElement($endWeight);
        $this->addElement($submit);
    }
}