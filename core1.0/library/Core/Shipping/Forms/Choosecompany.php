<?php

class Core_Shipping_Forms_Choosecompany extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * @param Core_Shipping_Models_CompanyMapper $companies
     */
    public function setupForm($companies) 
    {
        // Set properties of the form
        $this->setName('chooseCompany')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Choose a company')
            ->setAction('/admin/shipping/rates');

        $name = $this->createElement('select', 'company')
                ->setLabel('Choose Company: ')
                ->setRequired(true);
        
        if(count($companies) == 0)
        {
            $name->addMultiOption(0,'No companies exist currently');
            echo '<p><a href="/admin/shipping/addcompany">Why not add a company now?</a></p>';
        }
        else
        {
             // Company Field
            foreach($companies as $company)
            {
                $name->addMultiOption($company->getId(),$company->getCompany());
            }
            
            $this->setOptions(
                array(
                    'class' => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            );
            
//            echo '<p><a href="/admin/shipping/addcompany">Add more companies here</a></p>';

            // Submit Button
            $submit = $this->createElement('submit', 'choosesubmit')->setOptions(array('label' => 'Choose Company', 'class' => 'btn btn-primary'));
            // Add controls to the form
            $this->addElement($name);
            $this->addElement($submit);
        }
        
        
    }
}