<?php

class Core_Shipping_Forms_Deleterate extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    /**
     * Set Up Add Company Form
     */
    public function setupDeleteRateForm($rate, $companyArray, $bandArray) {
        // Set properties of the form
        $this->setName('addRate')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a rate');

        //Company
        $company = $this->createElement('select', 'company')
            ->setLabel('Choose a Company: ')
            ->setOptions(array('disabled' => true))
            ->setRequired(true);
        foreach ($companyArray as $c) {
            $company->addMultiOption($c->company, $c->company);
        }
        $company->setValue($rate->getCompany());

        //Shipping Option
        $name = $this->createElement('text', 'name')
            ->setLabel('Choose a Name (ex. Next Day): ')
            ->setOptions(array('disabled' => true))
            ->setRequired(true)
            ->setValue($rate->getName());

        //Shipping Type
        $band = $this->createElement('select', 'band')
            ->setLabel('Choose a weight band (g): ')
            ->setOptions(array('disabled' => true))
            ->setRequired(true);
        foreach ($bandArray as $bands) {
            $band->addMultiOption($bands->band_id, $bands->start_weight . '-' . $bands->end_weight);
        }
        $band->setValue($rate->getBandId());

        //Price
        $price = $this->createElement('text', 'price')
            ->setLabel('Price: ')
            ->setOptions(array('disabled' => true))
            ->setRequired(true)
            ->setValue($rate->getPrice());

        //Notes
        /*$notes = $this->createElement('textarea', 'notes', array(
            'label'      => 'Add some notes if they are required:',
            'value'      => '',
            'required'   => true,
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 500))
            )
        ));*/

        // Add controls to the form
        $this->addElement($company)
            ->addElement($name)
            ->addElement($band)
            ->addElement($price);

        // Submit Button
        $submit = $this->createElement('submit', 'addratesubmit')->setOptions(array('label' => 'Delete Rate', 'class' => 'btn btn-primary'));
        $this->addElement($submit);
    }
}