<?php

class Core_Shipping_Forms_Deleteband extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    /**
     * Set Up Add Company Form
     */
    public function setupDeleteBandForm($band)
    {
        // Set properties of the form
        $this->setName('addBand')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a band');

        $id = $this->createElement('hidden', 'id')
            ->setValue($band->getId());

        //Start Weight
        $startWeight = $this->createElement('text', 'start_weight')
            ->setOptions(
                array(
                    'label'    => 'Start Weight',
                    'disabled' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($band->getStartWeight());

        //End Weight
        $endWeight = $this->createElement('text', 'end_weight')
            ->setOptions(
                array(
                    'label'    => 'End Weight',
                    'disabled' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($band->getEndWeight());

        $submit = $this->createElement('submit', 'Add')->setOptions(array('label' => 'Delete this band', 'class' => 'btn btn-primary'));

        
        // Add controls to the form
        $this->addElement($id);
        $this->addElement($startWeight);
        $this->addElement($endWeight);
        $this->addElement($submit);
    }
}