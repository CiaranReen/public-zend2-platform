<?php

class Core_Shipping_Forms_Addcompany extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }
    
    /**
     * Set Up Add Company Form
     */
    public function setupForm() 
    {
        // Set properties of the form
        $this->setName('addCompany')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a company')
            ->setAction('/admin/shipping/companies');

        $name = $this->createElement('text', 'name')
            ->setOptions(
            array(
                 'label'    => 'Company Name',
                 'required' => true,
                 'class'    => 'required',
                 'filters'  => array('StringTrim', 'StripTags'),
            )
        );
        
        // Add controls to the form
        $this->addElement($name);
        
        // Submit Button
        $submit = $this->createElement('submit', 'addcompanysubmit')->setOptions(array('label' => 'Add Company', 'class' => 'btn btn-primary'));
        $this->addElement($submit);
    }
}