<?php

class Core_Shipping_Forms_Editband extends Zend_Form
{

    public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }

    /**
     * Set Up Add Company Form
     */
    public function setupEditBandForm($band)
    {
        // Set properties of the form
        $this->setName('addBand')
            ->setMethod('post')
            ->setAttrib('class', 'form-signin')
            ->setDescription('Add a band');

        //Start Weight
        $id = $this->createElement('hidden', 'id')
            ->setValue($band->getId());

        //Start Weight
        $startWeight = $this->createElement('text', 'start_weight')
            ->setOptions(
                array(
                    'label'    => 'Start Weight',
                    'required' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($band->getStartWeight());

        //End Weight
        $endWeight = $this->createElement('text', 'end_weight')
            ->setOptions(
                array(
                    'label'    => 'End Weight',
                    'required' => true,
                    'class'    => 'required',
                    'filters'  => array('StringTrim', 'StripTags'),
                )
            )->setValue($band->getEndWeight());

        $submit = $this->createElement('submit', 'Edit')->setOptions(array('label' => 'Save', 'class' => 'btn btn-primary'));

        
        // Add controls to the form
        $this->addElement($id);
        $this->addElement($startWeight);
        $this->addElement($endWeight);
        $this->addElement($submit);
    }
}