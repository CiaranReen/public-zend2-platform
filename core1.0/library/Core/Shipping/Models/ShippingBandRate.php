<?php

class Core_Shipping_Models_ShippingBandRate {

    protected $_id;
    protected $_bandId;
    protected $_rateId;
    protected $_price;
    protected $_company;
    protected $_name;

    protected $_mapper;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param $bandId
     * @return $this
     */
    public function setBandId($bandId) {
        $this->_bandId = $bandId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBandId() {
        return $this->_bandId;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price) {
        $this->_price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice() {
        return $this->_price;
    }

    /**
     * @param $rateId
     * @return $this
     */
    public function setRateId($rateId) {
        $this->_rateId = $rateId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRateId() {
        return $this->_rateId;
    }

    /**
     * @param $company
     * @return $this
     */
    public function setCompany($company) {
        $this->_company = $company;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany() {
        return $this->_company;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Shipping_Models_ShippingBandRateMapper());
        }
        return $this->_mapper;
    }

    public function findById($bandId) {
        $bandObject = $this->getMapper()->find($bandId, $this);
        return $bandObject;
    }

}

