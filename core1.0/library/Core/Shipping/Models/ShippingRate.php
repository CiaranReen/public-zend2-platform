<?php

class Core_Shipping_Models_ShippingRate
{
    protected $_id;
    protected $_company;
    protected $_type;
    protected $_name;
    protected $_option;
    protected $_country;
    protected $_priceFirstParcel;
    protected $_pricePerParcel;
    protected $_pricePerConsignment;
    protected $_pricePerPallet;
    protected $_freeWeightLimit;
    protected $_consignmentPricePerExtraKg;
    protected $_minimumChargePerParcel;
    protected $_transitTime;
    protected $_invoiceRequired;
    protected $_notes;
    protected $_price;
    protected $_mapper;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping rate property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping rate property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function getId() {
        return $this->_id;
    }
    
    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }
    
    public function getCompany() {
        return $this->_company;
    }

    public function setCompany($company) {
        $this->_company = $company;
        return $this;
    }

    public function getType() {
        return $this->_type;
    }

    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    public function getOption()
    {
        return $this->_option;
    }

    public function setOption($option)
    {
        $this->_option = $option;
        return $this;
    }

    public function getCountry() 
    {
        return $this->_country;
    }

    public function setCountry($country) 
    {
        $this->_country = $country;
        return $this;
    }
    
    public function getPriceFirstParcel() 
    {
        return number_format($this->_priceFirstParcel, 2);
    }

    public function setPriceFirstParcel($priceFirstParcel) 
    {
        $this->_priceFirstParcel = $priceFirstParcel;
        return $this;
    }

    public function getPricePerParcel() 
    {
        return number_format($this->_pricePerParcel, 2);
    }

    public function setPricePerParcel($pricePerParcel) 
    {
        $this->_pricePerParcel = $pricePerParcel;
        return $this;
    }

    public function getPricePerConsignment() 
    {
        return number_format($this->_pricePerConsignment, 2);
    }

    public function setPricePerConsignment($pricePerConsignment) 
    {
        $this->_pricePerConsignment = $pricePerConsignment;
        return $this;
    }

    public function getPricePerPallet() 
    {
        return number_format($this->_pricePerPallet, 2);
    }

    public function setPricePerPallet($pricePerPallet) 
    {
        $this->_pricePerPallet = $pricePerPallet;
        return $this;
    }
    
    public function getFreeWeightLimit()
    {
        return $this->_freeWeightLimit;
    }

    public function setFreeWeightLimit($freeWeightLimit)
    {
        $this->_freeWeightLimit = $freeWeightLimit;
        return $this;
    }

    public function getConsignmentPricePerExtraKg()
    {
        return $this->_consignmentPricePerExtraKg;
    }

    public function setConsignmentPricePerExtraKg($consignmentPricePerExtraKg)
    {
        $this->_consignmentPricePerExtraKg = $consignmentPricePerExtraKg;
        return $this;
    }

    public function getMinimumChargePerParcel()
    {
        return $this->_minimumChargePerParcel;
    }

    public function setMinimumChargePerParcel($minimumChargePerParcel)
    {
        $this->_minimumChargePerParcel = $minimumChargePerParcel;
        return $this;
    }

    public function getTransitTime() 
    {
        return $this->_transitTime;
    }

    public function setTransitTime($transitTime) 
    {
        $this->_transitTime = $transitTime;
        return $this;
    }

    public function getInvoiceRequired() 
    {
        return $this->_invoiceRequired;
    }

    public function setInvoiceRequired($invoiceRequired) 
    {
        $this->_invoiceRequired = $invoiceRequired;
        return $this;
    }

    public function getNotes() 
    {
        return $this->_notes;
    }

    public function setNotes($notes)
    {
        $this->_notes = $notes;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Shipping_Models_ShippingRateMapper());
        }
        return $this->_mapper;
    }

    public function findById($rateId) {
        $rateObject = $this->getMapper()->find($rateId, $this);
        return $rateObject;
    }
}