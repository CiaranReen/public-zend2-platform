<?php

class Core_Shipping_Models_CompanyMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Shipping_Models_DbTable_Companies');
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Shipping_Models_Company $company)
    {
        $data = array(
            'shipping_company_id'   => $company->getId(),
            'company'   => $company->getCompany(),
        );
        
        if(null === ($id = $company->getId())) 
        {
            unset($data['shipping_company_id']);
            $this->getDbTable()->insert($data);
        } 
        else 
        {
            $this->getDbTable()->update($data, array('shipping_company_id = ?' => $id));
        }
    }
    
    public function find($id, Core_Shipping_Models_Company $company)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        
        $row = $result->current();
        $company->setId($row->shipping_company_id)
                  ->setCompany($row->company);
        
        return $company;
    }
 
    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $company = new Core_Shipping_Models_Company();
            $company->setId($row->shipping_company_id);
            $company->setCompany($row->company);
            
            $entries[] = $company;
        }
        
        return $entries;
    }
}

