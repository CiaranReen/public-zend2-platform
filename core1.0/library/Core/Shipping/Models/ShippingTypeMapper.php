<?php

class Core_Shipping_Models_ShippingTypeMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Shipping_Models_DbTable_ShippingTypes');
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Shipping_Models_ShippingType $type)
    {
        $data = array(
            'shipping_type_id'  => $type->getId(),
            'name'              => $type->getName(),
            'description'       => $type->getDescription(),
            'minimum_weight'    => $type->getMinimumWeight(),
            'weight_limit'      => $type->getWeightLimit(),
            'length_limit'      => $type->getLengthLimit(),
            'width_limit'       => $type->getWidthLimit(),
            'depth_limit'       => $type->getDepthLimit(),
        );
        
        if(null === ($id = $type->getId())) 
        {
            unset($data['shipping_type_id']);
            $this->getDbTable()->insert($data);
        } 
        else 
        {
            $this->getDbTable()->update($data, array('shipping_type_id = ?' => $id));
        }
    }
    
    public function delete($id)
    {
        $this->getDbTable()->delete(array('shipping_type_id = ?' => $id));
        
        return $this;
    }
    
    public function find($id, Core_Shipping_Models_ShippingType $type)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        
        $row = $result->current();
        $type   ->setId($row->shipping_type_id)
                ->setName($row->name)
                ->setDescription($row->description)
                ->setMinimumWeight($row->minimum_weight)
                ->setWeightLimit($row->weight_limit)
                ->setLengthLimit($row->length_limit)
                ->setWidthLimit($row->width_limit)
                ->setDepthLimit($row->depth_limit);
        
        return $type;
    }
 
    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $type = new Core_Shipping_Models_ShippingType();
            $type   ->setId($row->shipping_type_id)
                    ->setName($row->name)
                    ->setDescription($row->description)
                    ->setMinimumWeight($row->minimum_weight)
                    ->setWeightLimit($row->weight_limit)
                    ->setLengthLimit($row->length_limit)
                    ->setWidthLimit($row->width_limit)
                    ->setDepthLimit($row->depth_limit);
            
            $entries[] = $type;
        }
        
        return $entries;
    }
}

