<?php

class Core_Shipping_Models_ShippingType
{
    protected $_id;
    protected $_name;
    protected $_description;
    protected $_minimumWeight;
    protected $_weightLimit;
    protected $_lengthLimit;
    protected $_widthLimit;
    protected $_depthLimit;
    
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping type property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping option property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
    
    public function getName() 
    {
        return $this->_name;
    }

    public function setName($name) 
    {
        $this->_name = $name;
        return $this;
    }

    public function getDescription() 
    {
        return $this->_description;
    }

    public function setDescription($description) 
    {
        $this->_description = $description;
        return $this;
    }
    
    public function getMinimumWeight()
    {
        return $this->_minimumWeight;
    }

    public function setMinimumWeight($minimumWeight) 
    {
        $minimumWeight = number_format(floatval($minimumWeight), 2, '.', '');
        $this->_minimumWeight = $minimumWeight;
        return $this;
    }

    public function getWeightLimit()
    {
        return $this->_weightLimit;
    }

    public function setWeightLimit($weightLimit) 
    {
        $weightLimit = number_format(floatval($weightLimit), 2, '.', '');
        $this->_weightLimit = $weightLimit;
        return $this;
    }

    public function getLengthLimit() 
    {
        return $this->_lengthLimit;
    }

    public function setLengthLimit($lengthLimit) 
    {
        $lengthLimit = number_format(floatval($lengthLimit), 2, '.', '');
        $this->_lengthLimit = $lengthLimit;
        return $this;
    }

    public function getWidthLimit() 
    {
        return $this->_widthLimit;
    }

    public function setWidthLimit($widthLimit) 
    {
        $widthLimit = number_format(floatval($widthLimit), 2, '.', '');
        $this->_widthLimit = $widthLimit;
        return $this;
    }

    public function getDepthLimit() 
    {
        return $this->_depthLimit;
    }

    public function setDepthLimit($depthLimit) 
    {
        $depthLimit = number_format(floatval($depthLimit), 2, '.', '');
        $this->_depthLimit = $depthLimit;
        return $this;
    }
    
    public function dumpdie($stuff)
    {
        echo '<PRE>';
        print_r($stuff);
        echo '</PRE>';
        die();
    }
    
    public function saveContent()
    {
        $content = new Core_Shipping_Models_ShippingTypeMapper();
        $content->save($this);
    }
    
}

