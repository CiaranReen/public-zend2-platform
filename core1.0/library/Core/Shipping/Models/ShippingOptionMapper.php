<?php

class Core_Shipping_Models_ShippingOptionMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable('Core_Shipping_Models_DbTable_ShippingOptions');
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Shipping_Models_ShippingOption $option)
    {
        $data = array(
            'shipping_option_id'   => $option->getId(),
            'option'   => $option->getOption(),
        );
        
        if(null === ($id = $option->getId())) 
        {
            unset($data['shipping_option_id']);
            $this->getDbTable()->insert($data);
        } 
        else 
        {
            $this->getDbTable()->update($data, array('shipping_option_id = ?' => $id));
        }
        
        return $this;
    }
    
    public function find($id, Core_Shipping_Models_ShippingOption $option)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        
        $row = $result->current();
        $option->setId($row->shipping_option_id)
                  ->setOption($row->option);
        
        return $option;
    }
 
    public function fetchAll()
    {
        //Set up the language
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $option = new Core_Shipping_Models_ShippingOption();
            $option ->setId($row->shipping_option_id)
                    ->setOption($row->option);
            
            $entries[] = $option;
        }
        
        return $entries;
    }
    
    public function delete($id)
    {
        $this->getDbTable()->delete(array('shipping_option_id = ?' => $id));
        
        return $this;
    }
}

