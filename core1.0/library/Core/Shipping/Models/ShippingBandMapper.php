<?php

class Core_Shipping_Models_ShippingBandMapper {
    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Shipping_Models_DbTable_ShippingBand');
        }
        return $this->_dbTable;
    }

    public function find($bandId, Core_Shipping_Models_ShippingBand $bModel) {
        $result = $this->getDbTable()->find($bandId);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $bModel->setId($row->band_id)
            ->setStartWeight($row->start_weight)
            ->setEndWeight($row->end_weight);
        return $bModel;
    }

    public function save(Core_Shipping_Models_ShippingBand $band) {
        $data = array(
            'start_weight' => $band->getStartWeight(),
            'end_weight' => $band->getEndWeight()
        );

        if (null !== ($id = $band->getId())) {
            $this->getDbTable()->update($data, array('band_id = ?' => $id));
        } else {
            $this->getDbTable()->insert($data);
        }
    }

    public function fetchAll() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sb' => 'shipping_band'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            foreach ($resultSet as $rs) {
                $bands[] = $rs;
            }
            return $bands;
        } else {
            return false;
        }
    }

    public function delete($bandId) {
        $this->getDbTable()->delete(array('band_id = ?' => $bandId));
    }

}

