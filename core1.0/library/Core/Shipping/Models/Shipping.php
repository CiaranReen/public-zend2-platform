<?php

class Core_Shipping_Models_Shipping {
    protected $basket;
    protected $totalWeight;

    public function getBasket() {
        return $this->basket;
    }

    public function setBasket($basket) {
        $this->basket = $basket;
        return $this;
    }

    protected function incrementWeight($value) {
        $this->totalWeight += number_format($value, 2);
    }

    protected function getTotalWeight() {
        return $this->totalWeight;
    }

    protected function setupBasketProduct($productid) {
        $productMapper = new Core_Product_Models_ProductMapper();
        $product = $productMapper->find($productid, new Core_Product_Models_Product());
        $this->incrementWeight($product->getWeight());

        return $product;
    }

    public function calculate() {
        $productMapper = new Core_Product_Models_ProductMapper();
        $shippingTypeMapper = new Core_Shipping_Models_ShippingTypeMapper();
        $shippingRatesMapper = new Core_Shipping_Models_ShippingRateMapper();

        $basket = $this->getBasket();
        $products = array();
        $numberOfProducts = 0;
        $shippingTypes = $shippingTypeMapper->fetchAll();

        foreach($basket as $productid => $item) {
            $products[] = $this->setupBasketProduct($productid);
            $numberOfProducts++;
        }

        /**
         * Now get the correct shipping type
         */
        $usedType = $shippingTypeMapper->find(4, new Core_Shipping_Models_ShippingType());
        $boxes = true;
        $pallet = false;
        $noOfBoxes = 0;

        foreach($shippingTypes as $shippingType) {
            //For each shipping type
            if($this->getTotalWeight() <= $shippingType->getWeightLimit()) {
                //
                if($shippingType->getWeightLimit() <= 5.00) {
                    $boxes = false;
                }
            }
        }

        /**
         * Once we've got the correct shipping type, create the array of
         * prices for all of the options for the relevant shipping type.
         *
         * This will take into account the products weight and dimensions
         */
        /*$rates = $shippingRatesMapper->fetchAllByTypeId($usedType->getId());

        /**
         * Calculate price of delivery for each rate
         */
        /*$returnrates = array();
        if ($rates !== NULL) {
            foreach($rates as $rate) {
                $deliveryPrice = floatval($rate->getPriceFirstParcel());

                if($noOfBoxes > 1) {
                    $deliveryPrice = floatval($deliveryPrice + ($rate->getPricePerParcel() * ($numberOfProducts - 1)));
                }

                $rate->setPrice($deliveryPrice);
                $returnrates[$rate->getId()] = $rate;
            }*/

            /**
             * Return the list of product prices and then show them along with
             * the product options on the checkout on the front end.
             */
            //return $returnrates;
        //}
    }
}

