<?php

class Core_Shipping_Models_ShippingOption
{
    protected $_id;
    protected $_option;
 
    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
 
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping option property');
        }
        $this->$method($value);
    }
 
    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid shipping option property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) 
        {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) 
            {
                $this->$method($value);
            }
        }
        return $this;
    }
    
    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }
 
    public function getId()
    {
        return $this->_id;
    }
 
    public function setOption($option)
    {
        $this->_option = (string) $option;
        return $this;
    }
 
    public function getOption()
    {
        return $this->_option;
    }
    
    public function dumpdie($stuff)
    {
        echo '<PRE>';
        print_r($stuff);
        echo '</PRE>';
        die();
    }
    
    public function saveContent()
    {
        $content = new Core_Shipping_Models_ShippingOptionMapper();
        $content->save($this);
    }
    
}

