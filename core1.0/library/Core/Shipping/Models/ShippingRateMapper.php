<?php

class Core_Shipping_Models_ShippingRateMapper
{
    protected $_dbTable;
    protected $shippingCompanyMapper;
    protected $shippingTypeMapper;
    protected $shippingOptionsMapper;
    protected $countryMapper;

    public function __construct()
    {
        $this->shippingCompanyMapper = new Core_Shipping_Models_CompanyMapper();
        $this->shippingTypeMapper = new Core_Shipping_Models_ShippingTypeMapper();
        $this->shippingOptionsMapper = new Core_Shipping_Models_ShippingOptionMapper();
        $this->countryMapper = new Core_Language_Models_CountriesMapper();
    }

    protected function getCompanyMapper()
    {
        return $this->shippingCompanyMapper;
    }

    protected function getTypeMapper()
    {
        return $this->shippingTypeMapper;
    }

    protected function getOptionsMapper()
    {
        return $this->shippingOptionsMapper;
    }

    protected function getCountryMapper()
    {
        return $this->countryMapper;
    }

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable))
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract)
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable)
        {
            $this->setDbTable('Core_Shipping_Models_DbTable_ShippingRates');
        }
        return $this->_dbTable;
    }

    public function save($id, Core_Shipping_Models_ShippingRate $rate)
    {
        $data = array(
            'company' => $rate->getCompany(),
            'rate_name' => $rate->getName()
        );

        if(null === $id)
        {
            unset($data['rate_id']);
            $this->getDbTable()->insert($data);
        }
        else
        {
            $this->getDbTable()->update($data, array('rate_id = ?' => $id));
        }
    }

    public function find($id, Core_Shipping_Models_ShippingRate $rate) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }

        $row = $result->current();
        $rate->setId($row->rate_id)
            ->setCompany($row->company)
            ->setType($this->getTypeMapper());

        return $rate;
    }

    public function fetchAllByCompanyId($companyid)
    {
        $rates = $this->fetchAll();
        $companyRates = array();

        foreach($rates as $rate)
        {
            if($rate->getCompany()->getId() == $companyid)
            {

            }
        }
    }

    public function fetchAll() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sr' => 'shipping_rate'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            foreach ($resultSet as $rs) {
                $bands[] = $rs;
            }
            return $bands;
        } else {
            return false;
        }
    }

    public function fetchAllDeliveryOptions() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sr' => 'shipping_rate'), array('*'))
            ->joinInner(array('sbr' => 'shipping_band_rate'), 'sr.rate_id=sbr.rate_id')
            ->joinInner(array('sb' => 'shipping_band'), 'sbr.band_id=sb.band_id');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            foreach ($resultSet as $rs) {
                $rates[] = $rs;
            }
            return $rates;
        } else {
            return false;
        }
    }
}

