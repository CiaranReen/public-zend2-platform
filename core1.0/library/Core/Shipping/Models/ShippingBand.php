<?php

class Core_Shipping_Models_ShippingBand {

    protected $_id;
    protected $_startWeight;
    protected $_endWeight;

    protected $_mapper;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param $endWeight
     * @return $this
     */
    public function setEndWeight($endWeight) {
        $this->_endWeight = $endWeight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndWeight() {
        return $this->_endWeight;
    }

    /**
     * @param $startWeight
     * @return $this
     */
    public function setStartWeight($startWeight) {
        $this->_startWeight = $startWeight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartWeight() {
        return $this->_startWeight;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Shipping_Models_ShippingBandMapper());
        }
        return $this->_mapper;
    }

    public function findById($bandId) {
        $bandObject = $this->getMapper()->find($bandId, $this);
        return $bandObject;
    }

}

