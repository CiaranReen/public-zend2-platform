<?php

class Core_Shipping_Models_ShippingBandRateMapper {

    protected $_dbTable;

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Shipping_Models_DbTable_ShippingBandRate');
        }
        return $this->_dbTable;
    }

    public function find($id, Core_Shipping_Models_ShippingBandRate $bModel) {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sbr' => 'shipping_band_rate'), array('*'))
            ->joinInner(array('sr' => 'shipping_rate'), 'sbr.rate_id = sr.rate_id')
            ->where('sbr.id = ?', $id);
        $result = $this->getDbTable()->fetchRow($stmt);
        if (0 == count($result)) {
            return;
        }
        $bModel->setBandId($result->band_id)
            ->setRateId($result->rate_id)
            ->setId($result->id)
            ->setPrice($result->price)
            ->setCompany($result->company)
            ->setName($result->rate_name);
        return $bModel;
    }

    public function save($id, Core_Shipping_Models_ShippingBandRate $br) {
        $data = array(
            'band_id' => $br->getBandId(),
            'rate_id' => $br->getRateId(),
            'price' => $br->getPrice()
        );

        if (null !== $id) {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        } else {
            $this->getDbTable()->insert($data);
        }
    }

    public function fetchAll() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sbr' => 'shipping_band_rate'), array('*'))
            ->joinInner(array('sr' => 'shipping_rate'), 'sbr.rate_id = sr.rate_id')
            ->joinInner(array('sb' => 'shipping_band'), 'sbr.band_id = sb.band_id');
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        if (count($resultSet) > 0) {
            foreach ($resultSet as $rs) {
                $bands[] = $rs;
            }
            return $bands;
        } else {
            return false;
        }
    }

    public function delete($id) {
        $this->getDbTable()->delete(array('id = ?' => $id));
    }

}

