<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 03/03/14
 * Time: 11:55
 */

class Core_Notification_Models_Notification {

    protected $_id;
    protected $_event;
    protected $_url;
    protected $_seen;
    protected $_icon;

    protected $_mapper;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $event
     * @return $this
     */
    public function setEvent($event)
    {
        $this->_event = $event;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->_event;
    }

    /**
     * @param $seen
     * @return $this
     */
    public function setSeen($seen)
    {
        $this->_seen = $seen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSeen()
    {
        return $this->_seen;
    }

    /**
     * @param $icon
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->_icon = $icon;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->_icon;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->_url;
    }



    /**
     * @param $mapper
     * @return $this
     */
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Notification_Models_NotificationMapper());
        }
        return $this->_mapper;
    }


}