<?php
/**
 * Created by PhpStorm.
 * User: ciaran
 * Date: 03/03/14
 * Time: 11:55
 */

class Core_Notification_Models_NotificationMapper {

    protected $_dbTable;

    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable('Core_Notification_Models_DbTable_Notification');
        }
        return $this->_dbTable;
    }

    public function saveAction($action, $type, $userId, $name) {
        $data = array (
            'action' => $action,
            'type' => $type,
            'user_id' => $userId,
            'name' => $name,
        );

        $this->getDbTable()->insert($data);
    }

    /**
     * @return mixed
     */
    public function fetchAll() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('n' => 'notification'), array ('*'))
            ->limit(10, 0)
            ->order('time DESC');

        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    /**
     * @return mixed
     */
    public function fetchAllUnseen() {
        $seen = '0';
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('n' => 'notification'), array ('*'))
            ->where('seen = ?', $seen)
            ->order('id DESC');

        $resultSet = $this->getDbTable()->fetchAll($stmt);
        $count = count($resultSet);
        return $count;
    }

    public function save(Core_Notification_Models_Notification $notification) {
        $data = array (
            'event' => $notification->getEvent(),
            'url' => $notification->getUrl(),
            'time' => new Zend_Db_Expr('Now()'),
            'seen' => '0',
            'icon' => $notification->getIcon()
        );

        $save = $this->getDbTable()->insert($data);
        return $save;
    }

    public function setAllToSeen() {
        $data = array (
            'seen' => '1'
        );

        $save = $this->getDbTable()->update($data);
        return $save;
    }

    public function getTimeDifference($notificationTime) {
        //Let's set the current time
        $currentTime = date('Y-m-d H:i:s');
        $toTime = strtotime($currentTime);

        //And the time the notification was set
        $fromTime = strtotime($notificationTime);

        //Now calc the difference between the two
        $timeDiff = floor(abs($toTime - $fromTime) / 60);

        //Now we need find out whether or not the time difference needs to be in
        //minutes, hours, or days
        if ($timeDiff < 2) {
            $timeDiff = "Just now";
        } elseif ($timeDiff > 2 && $timeDiff < 60) {
            $timeDiff = floor(abs($timeDiff)) . " minutes ago";
        } elseif ($timeDiff > 60 && $timeDiff < 120) {
            $timeDiff = floor(abs($timeDiff / 60)) . " hour ago";
        } elseif ($timeDiff < 1440) {
            $timeDiff = floor(abs($timeDiff / 60)) . " hours ago";
        } elseif ($timeDiff > 1440 && $timeDiff < 2880) {
            $timeDiff = floor(abs($timeDiff / 1440)) . " day ago";
        } elseif ($timeDiff > 2880) {
            $timeDiff = floor(abs($timeDiff / 1440)) . " days ago";
        }

        return $timeDiff;
    }


}