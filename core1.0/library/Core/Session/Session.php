<?php
/**
 * @package CO3-Core Platform
 *
 * Wrapper for PHP SESSION handling
 */

class Session_Session {

	protected $sessionKey;

	/**
	 * Start the session, if one has not already been initiated
	 *
	 * @return \Session_Session
	 */
	public function __construct() {
		if(strlen(session_id()) == 0) {
			session_name('CO3CORE');
			session_start();
		}

		$this->setSessionKey(session_id());

		return $this;
	}

	/**
	 * Set the session identifier
	 *
	 * @param $sessionKey string
	 *
	 * @return $this
	 */
	protected function setSessionKey($sessionKey) {
		$this->sessionKey = $sessionKey;

		return $this;
	}

	/**
	 * Get the session identifier
	 *
	 * @return string
	 */
	public function getSessionKey() {
		return $this->sessionKey;
	}

	/**
	 * Detect if a user is logged in
	 *
	 * @return bool
	 */
	public function isLoggedIn() {
		if($this->getData('user_id') && $this->getData('is_logged_in')) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Login a user
	 *
	 * @param $username
	 * @param $password
	 *
	 * @return array|bool
	 */
	public function processLogin($username, $password) {
		$userModel = new User_Models_User_UserModel();

		$row = $userModel->loadByLoginCredentials($username, $password);

		if($row !== false && count($row) > 0) {
			return $row;
		} else {
			return false;
		}
	}

	/**
	 * Logout the current user
	 *
	 */
	public function processLogout() {
		// Wipe all session data
		$_SESSION = array();
		session_destroy();
		// Regenerate the session id
		session_regenerate_id(true);
	}

	/**
	 * Set data to SESSION
	 *
	 * @param $key string The identifier of the SESSION data to retrieve
	 * @param $data mixed The data to set
	 *
	 * @return $this
	 */
	public function setData($key, $data) {
		$_SESSION[$key] = $data;

		return $this;
	}

	/**
	 * Retrieve data from SESSION
	 *
	 * @param $key string The identifier of the SESSION data to retrieve
	 *
	 * @return bool
	 */
	public function getData($key) {
		if(isset($_SESSION[$key])) {
			return $_SESSION[$key];
		} else {
			return false;
		}
	}

	/**
	 * Save SESSION data to database.
	 *
	 * @return $this
	 */
	public function save() {
		return $this;
	}

	/**
	 * Load SESSION data from database
	 * The idea is to store data in the db rather then in the SESSION[] array
	 *
	 * @return $this
	 */
	public function load() {
		return $this;
	}
}