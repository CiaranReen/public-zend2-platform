<?php

/**
 * Class Core_Basket_Models_Basket
 *
 * Basket Functionality
 */
class Core_Basket_Models_Basket extends App_Model_Abstract {

    protected $_basketItems = array();
    protected $_total;

    public function __construct() {
        $basket = new Zend_Session_Namespace('basket');
        $this->setItems($basket);
    }

    /**
     * Resets the basket to the what is populated in the session
     * Used in the controller after an item has been added or taken away from the basket
     * @return boolean
     */
    public function reset() {
        $this->setItems($this->getSession());
        return true;
    }

    public function setItems(Zend_Session_Namespace $basket) {
        $this->_basketItems = $basket->array;
        return $this;
    }

    public function getItems() {
        return $this->_basketItems;
    }

    public function getSession() {
        $basketSession = new Zend_Session_Namespace('basket');
        return $basketSession;
    }

    public function updateSession() {
        $this->getSession()->array = $this->getItems();
        return true;
    }

    /**
     * adds item to basket
     * @param array $itemData
     * @return boolean
     */
    public function addItem($itemData) {
        $added = true;
        if (!empty($itemData)) 
        {
            $productId = $itemData['product_id'];
            if (isset($this->_basketItems[$productId]) && !empty($this->_basketItems[$productId])) 
            {
                $itemStock = $itemData['stock'];
                foreach ($itemStock as $stockId => $qty) 
                {
                    if ($qty) 
                    {
                        $this->updateItemQty($stockId, $qty);
                    }
                }
            } else {
                unset($itemData['product_id']);
                $this->_basketItems[$productId] = $itemData;
            }
            $this->updateSession();
        } 
        else 
        {
            $added = false;
        }
        return $added;
    }

    /**
     * gets stock in basket
     * @return array
     */
    public function getStock() {
        $basketStock = array();
        $basket = $this->getItems();
        if (!empty($basket)) {
            foreach ($basket as $key => $value) {
                foreach ($value['stock'] as $key2 => $value2) {
                    $basketStock[$key2] = $value2;
                }
            }
        }
        return $basketStock;
    }

    /**
     * removes item from basket
     * @param int $itemStockId
     * @return boolean
     */
    public function removeItem($itemStockId) {
        $basket = $this->getItems();
        foreach ($basket as $key => $value) {
            $isEmpty = true;
            foreach ($value['stock'] as $key2 => $value2) {
                if ($itemStockId == $key2) {
                    unset($this->_basketItems[$key]['stock'][$key2]);
                    $value2 = 0;
                }
                if($value2){
                    $isEmpty = false;
                }
            }
            if ($isEmpty) {
                unset($this->_basketItems[$key]);
            }
        }
       
        $this->updateSession();
        return true;
    }

    /**
     * updates basket quantity
     * @param int $itemStockId
     * @param int $newQty
     * @return boolean
     */
    public function updateItemQty($itemStockId, $newQty) {
        $basket = $this->getItems();
        foreach ($basket as $key => $value) {
            foreach ($value['stock'] as $key2 => $value2) {
                if ($itemStockId == $key2) {
                    $this->_basketItems[$key]['stock'][$key2] = $newQty;
                }
            }
        }
        $this->updateSession();
        return true;
    }

    /**
     * clears basket session
     * @return boolean
     */
    public function emptyBasket() {
        $this->getSession()->unsetAll();
        return true;
    }
    
    /**
     * Checks if basket is empty
     * @return boolean
     */
    public function isEmpty(){
        if(null === $this->_basketItems){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * gets no of items in basket
     * @return int
     */
    public function getNumberOfItems() {
            $numberOfItems = 0;
            $basketStock = $this->getStock();
            if (!empty($basketStock)) {
                foreach ($basketStock as $key => $value) {
                    $numberOfItems += $value;
                }
            }
            return $numberOfItems;
    }
    

    /**
     * returns items total in basket
     * @return float
     */
    public function getItemsTotal() {
        $total = 0;
        $stockModel = new Core_Stock_Models_Stock();
        $basketStock = $this->getStock();
        $basketruleModel = new Core_Basketrule_Models_Basketrule();
        if (!empty($basketStock)) {
            foreach ($basketStock as $key => $value) {
                if ($value) {
                    $stockObject = $stockModel->findById($key);
                    $itemPrice = round($stockObject->getPrice(), 2) ;
                    $itemPriceTotal = $itemPrice * $value;
                    $total += $itemPriceTotal;
                }
            }
        }
        $currentUser = Core_User_Models_User::getCurrent();
        if($currentUser instanceof Core_User_Models_User){
            $totalDiscount = $basketruleModel->getBasketDiscount($this, $currentUser->getUserGroupId());
            $total -= $totalDiscount;
        }
        return $total;
    }

    /**
     * Gets all data to be displayed in basket
     * @return array
     */
    public function getDisplayData() {
        $productModel = new Core_Product_Models_Product();
        $stockModel = new Core_Stock_Models_Stock();
        $colourwayModel = new Core_Colourway_Models_Colourway();
        $basketruleModel = new Core_Basketrule_Models_Basketrule();
        $currencyModel = new Core_Currency_Models_Currency();
        $userModel = new Core_User_Models_User();
        $currencyMapper = new Core_Currency_Models_CurrencyMapper();
        $currency = $currencyModel->load();
        
        $conversion = $currencyModel->setConversionRate($currency->getCurrencyId());
        
        $basketDisplayData = array();
        $basket = $this->getItems();
        if (!empty($basket)) 
        {
            foreach ($basket as $key => $value) 
            {
                foreach ($value['stock'] as $key2 => $value2) 
                {
                    if ($value2) 
                    {
                        $productObject = $productModel->findById($key);
                        $stockObject = $stockModel->findById($key2);
                        $stockObject->setConversion($conversion);
                        $stockObject->convertPrice();
                        $basketDisplayData[$key2]['product_id'] = $key;
                        $basketDisplayData[$key2]['product_name'] = $productObject->getNameByLanguage();
                        $basketDisplayData[$key2]['option'] = $stockObject->getOption();
                        $basketDisplayData[$key2]['product_weight'] = $productObject->getWeight();
                        if ($productObject->hasColourways()) 
                        {
                            $colourwayId = $stockObject->getColourwayId();
                            $colourwayObject = $colourwayModel->findById($colourwayId);
                            $basketDisplayData[$key2]['colourway'] = $colourwayObject->getNameByLanguage();
                            $productColourwayImages = $productObject->getColourwayImagesByColourwayId($colourwayId);
                            $basketDisplayData[$key2]['product_image'] = $productColourwayImages[0]->image_url;
                        } 
                        else 
                        {
                            $basketDisplayData[$key2]['colourway'] = NULL;
                            $basketDisplayData[$key2]['product_image'] = $productObject->getDefaultImage()->image_url;
                        }
                        $currentUser = Core_User_Models_User::getCurrent();
                        $stockDiscount = 0;
                        $totalDiscount = 0;
                        $stockDiscountPerItem = 0;
                        if($currentUser instanceof Core_User_Models_User){
                            $totalDiscount = $basketruleModel->getStockDiscount($stockObject->getId(), $value2, $currentUser->getUserGroupId());
                            $stockDiscountPerItem = $basketruleModel->getStockDiscountPerItem($stockObject->getId(), $value2, $currentUser->getUserGroupId());
                        }
                        $price = (float) $stockObject->getPrice();
                        if($stockDiscountPerItem > 0){
                            $basketDisplayData[$key2]['itemprice'] = $price - $stockDiscountPerItem;
                        }else{
                            $basketDisplayData[$key2]['itemprice'] = $price;
                        }
                        $basketDisplayData[$key2]['qty'] = $value2;
                        
                        $totalPrice = $stockObject->getPrice() * $value2;
                        if($totalDiscount > 0){
                            $basketDisplayData[$key2]['itempricetotal'] = $totalPrice - $totalDiscount;
                        }else{
                            $basketDisplayData[$key2]['itempricetotal'] = $totalPrice;
                        }
                    }
                }
            }
        }
        return $basketDisplayData;
    }

    public function save() {
        
    }

}