<?php

class Core_Emailer_Models_ContactEmail extends App_Model_Abstract {
    
    const COMPANY = 'CE';
    protected $_id;
    protected $_company;
    protected $_name;
    protected $_email;  
    protected $_mapper;

    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getCompany() {
        return $this->_company;
    }

    public function setCompany($company) {
        $this->_company = (string) $company;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function setEmail($email) {
        $this->_email = $email;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Emailer_Models_ContactEmailMapper());
        }
        return $this->_mapper;
    }
    
    public function findById($contactEmailId) {
        $this->getMapper()->find($contactEmailId, $this);
        return $this;
    }
    
    public function getCERecipients(){
        $allRecipients     = $this->getAllRecipients();
        $ceRecipients   = array();
        foreach($allRecipients as $recipient){
            if($recipient->getCompany() === self::COMPANY){
                $ceRecipients[] = $recipient;
            }
        }
        return $ceRecipients;
    }
    
    public function save() {
        return $this->getMapper()->save($this);
    }
    
    public function getAllRecipients() {
        $allRecipients = $this->getMapper()->fetchAll();
        return $allRecipients;
    }
}

