<?php

class Core_Emailer_Models_ContactEmailMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Emailer_Models_DbTable_ContactEmail';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Emailer_Models_ContactEmail $contactEmail)
    {
        $data = array(
            'company' => $contactEmail->getCompany(),
            'name' => $contactEmail->getName(),
            'email' => $contactEmail->getEmail(),
        );

        if (null === ($id = $contactEmail->getId())) {
            unset($data['contact_id']);
            return $this->getDbTable()->insert($data);
        } else {
            return $this->getDbTable()->update($data, array('contact_id = ?' => $id));
        }
    }
    
    public function find($id, Core_Emailer_Models_ContactEmail $contactEmail) {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $contactEmail->setId($row->contact_id)
                ->setCompany($row->company)
                ->setName($row->name)
                ->setEmail($row->email);
        return $contactEmail;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row)
        {
            $entry = new Core_Emailer_Models_ContactEmail();
            $entry->findById($row->contact_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
}

