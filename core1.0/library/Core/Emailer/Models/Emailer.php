<?php

class Core_Emailer_Models_Emailer extends App_Model_Abstract {

     // templates
    const ORDER_CONFIRMATION_TEMPLATE   = "order-confirmation";
    const ORDER_DISPATCH_TEMPLATE       = "order-dispatch";
    const REGISTER_TEMPLATE             = "register";
    const PASSWORD_RESET_TEMPLATE      = "password-reset";
    
    protected $_emailConfig;
    protected $_view;
    protected $_mailer;
    protected $_template;
    protected $_recipient;

    public function setMailer(Zend_Mail $mail){
        $this->_mailer = $mail;
        return $this;
    }
    
    public function getMailer(){
        if (null === $this->_mailer) {
            $this->setMailer(new Zend_Mail());
        }
        return $this->_mailer;
    }
    
    public function setView(Zend_View $view){
        $this->_view = $view;
    }
    
    public function getView(){
        if (null === $this->_view) {
            $this->setView(new Zend_View());
        }
        return $this->_view;
    }
    
    public function setTemplate($template){
        $this->_template = $template;
        return $this;
    }
    
    public function getTemplate(){
        return $this->_template;
    }
    
    public function getEmailConfig() {
        if (null === $this->_emailConfig) {
            $config = Zend_Registry::get('my_config');
            $this->setEmailConfig($config->email);
        }
        return $this->_emailConfig;
    }

    public function setEmailConfig($emailConfig) {
        $this->_emailConfig = $emailConfig;
    }

    public function getRecipient() {
        return $this->_recipient;
    }

    public function setRecipient($recipient) {
        $this->_recipient = $recipient;
    }

    
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Emailer_Models_EmailerMapper());
        }
        return $this->_mapper;
    }
    
    public function getCERecipients(){
        $recipients     = $this->getMapper()->getEmailRecipients();
        $ceRecipients   = array();
        foreach($recipients as $recipient){
            if($recipient->company === self::COMPANY){
                $ceRecipients[] = $recipient;
            }
        }
        return $ceRecipients;
    }

    /**
     * Builds the order email
     * @return string
     * @throws Exception
     */
    public function buildProductsTableFromOrder(Core_Order_Models_Order $order, Core_Language_Models_Languages $language = null){
        if(!$order instanceof Core_Order_Models_Order){
            throw new Exception('Parameter passed is not an instance of the order model');
        }
        $currencyMapper     = new Core_Currency_Models_CurrencyMapper();
        $currency = $currencyMapper->find($order->getCurrencyId(), new Core_Currency_Models_Currency());

        $html       = '<table cellspacing="10" cellpadding="10" border="0" style="border:1px solid #CCC; width: 100%">';
        $html       .= '<thead>
                            <th style="color: #333; background: #e3e3e3;">Product</th>
                            <th style="color: #333; background: #e3e3e3;">Option</th>
                            <th style="color: #333; background: #e3e3e3;">Quantity</th>
                            <th style="color: #333; background: #e3e3e3;">Price</th>
                            <th style="color: #333; background: #e3e3e3;">Total</th>
                        </thead>';
        $html       .= '<tbody>';
        
        foreach($order->getOrderParts() as $orderPart){
            $html .= '<tr>';
            $productModel   = new Core_Product_Models_Product();
            $stockModel     = new Core_Stock_Models_Stock();
            $stockId        = $orderPart->getStockId();
            $stock          = $stockModel->findbyId($stockId);
            $product        = $productModel->findById($stock->getProductId());
            
            $html .= '<td>' . $product->getNameByLanguage() . '</td>';
            $html .= '<td>' . $stock->getOption() . '</td>';
            $html .= '<td>' . $orderPart->getQuantity() . '</td>';
            $html .= '<td>' . $currency->getCurrencySymbol() . number_format($orderPart->getPrice(), 2) . '</td>';
            $html .= '<td>' . $currency->getCurrencySymbol() . number_format($orderPart->getLineTotal(), 2) . '</td>';
            $html .= '</tr>';
        }
        $html .= '</tbody>';
        $html .= '</table>';
        return $html;
    }
    
    public function sendOrderSuccessEmail(Core_Order_Models_Order $order, Core_Language_Models_Languages $language = null)
    {
        if(!$order instanceof Core_Order_Models_Order){
            throw new Exception('Parameter passed is not an instance of the order model');
        }
        
        $settings   = Core_Settings_Models_Settings::getSettings();
        $userModel  = new Core_User_Models_User();
        $usergroupModel = new Core_Usergroup_Models_Usergroup();
        $contactEmailModel = new Core_Emailer_Models_ContactEmail();
        $currencyMapper     = new Core_Currency_Models_CurrencyMapper();

        $emailPath = $this->getEmailConfig()->templatePath;
        $view = $this->getView()->setScriptPath($emailPath);
        $user       = $userModel->findById($order->getUserId());
        $usergroup = $usergroupModel->findById($user->getUserGroupId());
        $currency = $currencyMapper->find($order->getCurrencyId(), new Core_Currency_Models_Currency());

        $view->orderHeading = 'Order Details';
        $view->orderNumber = 'Order Number: ' . $order->getOrderNumber();
        $view->user = 'User: ' .  $user->getForename() . ' ' . $user->getSurname();
        $view->itemsTotal = 'Items Total: ' . $currency->getCurrencySymbol() . number_format($order->getItemsTotal() ,2);
        $view->delivery = 'Delivery: ' . $currency->getCurrencySymbol() . number_format($order->getDeliveryCharge(), 2);
        if ($usergroup->getVat() === '1'){
            $view->VAT = 'VAT: ' . $currency->getCurrencySymbol() . number_format($order->getVatCharge(), 2);
        }
        $view->orderTotal = 'Total: ' . $currency->getCurrencySymbol() . number_format($order->getOrderTotal(), 2);
        $view->productsTable = $this->buildProductsTableFromOrder($order, $language);
        $view->disclaimer = '';
        $html = $view->render(self::ORDER_CONFIRMATION_TEMPLATE . '.tpl');
            
        $ceRecipients = $contactEmailModel->getCERecipients();
        $mail       = $this->getMailer();
        foreach($ceRecipients as $recipient){
            $mail->addTo($recipient->getEmail(), $recipient->getName());
        }
        $mail->setBodyHtml($html);
        //$mail->setBodyText($text);
        $mail->setFrom($settings->getOrderEmailSender(), $settings->getCompanyName() . ' Shop');
        $mail->addTo($user->getEmail(), $user->getForename() . ' ' . $user->getSurname());
        $mail->setSubject($settings->getCompanyName() . ' Order Success');
        if(APPLICATION_ENV == 'live' || APPLICATION_ENV == 'dev'){
            $mail->send();
        }
    }
    
    public function sendOrderDispatchEmail(Core_Order_Models_Order $order, Core_Language_Models_Languages $language = null){
        if(!$order instanceof Core_Order_Models_Order){
            throw new Exception('Parameter passed is not an instance of the order model');
        }
        $settings   = Core_Settings_Models_Settings::getSettings();
        $userModel  = new Core_User_Models_User();
        $addressModel = new Core_Address_Models_Address();
        $currencyMapper     = new Core_Currency_Models_CurrencyMapper();


        $emailPath  = $this->getEmailConfig()->templatePath;
        $view       = $this->getView()->setScriptPath($emailPath);
        $user       = $userModel->findById($order->getUserId());
        $currency = $currencyMapper->find($order->getCurrencyId(), new Core_Currency_Models_Currency());

        $deliveryAddress = $addressModel->findById($order->getDeliveryAddressId());
        $view->userGreeting = 'Hi ' . $user->getForename();
        $view->dispatchOrderText = 'Your item has been dispatched.';
        $view->orderNumber = 'Order Number: ' . $order->getOrderNumber();
        $view->orderTotal = 'Total: ' . $currency->getCurrencySymbol() . number_format($order->getOrderTotal(), 2);
        $view->productsTable = $this->buildProductsTableFromOrder($order, $language);
        $view->deliveryAddress = '<h4>Delivery Address</h4>
                                <p>' . $deliveryAddress->getLineOne() . '</p>' .
                                '<p>' . $deliveryAddress->getLineTwo() . '</p>' .
                                '<p>' . $deliveryAddress->getCity() . '</p>' .
                                '<p>' . $deliveryAddress->getRegion() . '</p>' .
                                '<p>' . $deliveryAddress->getCountry()->getCountry() . '</p>';
        $view->thankYou = 'Thank you';
        $view->trackingText = 'Tracking number ';
        $view->disclaimer = '';
        $html = $view->render(self::ORDER_DISPATCH_TEMPLATE . '.tpl');
        
        $mail       = $this->getMailer();
        $mail->setBodyHtml($html);
        //$mail->setBodyText($text);
        $mail->setFrom($settings->getOrderEmailSender(), $settings->getCompanyName() . ' Shop');
        $mail->addTo($user->getEmail(), $user->getForename() . ' ' . $user->getSurname());
        $mail->setSubject($settings->getCompanyName() . ' Order Dispatched');
        if(APPLICATION_ENV == 'live' || APPLICATION_ENV == 'dev'){
            $mail->send();
        }
    }

    public function passwordResetEmail($email, $code) {
        $settings   = Core_Settings_Models_Settings::getSettings();
        $userMapper = new Core_User_Models_UserMapper();
        $user = $userMapper->findByEmail($email, new Core_User_Models_User());
        $emailPath = $this->getEmailConfig()->templatePath;
        $view = $this->getView()->setScriptPath($emailPath);
        if($user instanceof Core_User_Models_User){
            $view->userGreeting = 'Hi ' . $user->getForename();
            $view->resetPasswordText = 'You have requested for a password reset to your FightScience account. If you have received this in error kindly ignore this email.
            <br/><br/>Your password reset code: <b>' . $code. '</b>';
            $html = $view->render(self::PASSWORD_RESET_TEMPLATE . '.tpl');

            $mail       = $this->getMailer();
            $mail->setBodyHtml($html);
            $mail->setFrom($settings->getOrderEmailSender(), $settings->getCompanyName() . ' Shop');
            $mail->addTo($email);
            $mail->setSubject($settings->getCompanyName() . ' Password Reset Request');
            if(APPLICATION_ENV == 'live' || APPLICATION_ENV == 'dev'){
                $mail->send();
            }
        }
    }
}

