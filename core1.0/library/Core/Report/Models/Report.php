<?php

class Core_Report_Models_Report {

    protected $_mapper;

    protected $_id;
    protected $_name;
    protected $_columns;
    protected $_usergroups;
    protected $_categories;
    protected $_type;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id) {
        $this->_id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param $columns
     * @return $this
     */
    public function setColumns($columns) {
        $this->_columns = $columns;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getColumns() {
        return $this->_columns;
    }

    /**
     * @param $categories
     * @return $this
     */
    public function setCategories($categories) {
        $this->_categories = $categories;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories() {
        return $this->_categories;
    }

    /**
     * @param $usergroups
     * @return $this
     */
    public function setUsergroups($usergroups) {
        $this->_usergroups = $usergroups;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsergroups() {
        return $this->_usergroups;
    }


    /**
     * @param $type
     * @return $this
     */
    public function setType($type) {
        $this->_type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->_type;
    }

    /**
     * @param array $options
     */
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value) {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        $this->$method($value);
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function __get($name) {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid language property');
        }
        return $this->$method();
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options) {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    /**
     * @param $mapper
     * @return $this
     */
    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Report_Models_ReportMapper());
        }
        return $this->_mapper;
    }

    /**
     * @return mixed
     */
    public function save() {
        $save = $this->getMapper()->save($this);
        return $save;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id) {
        $delete = $this->getMapper()->delete($id);
        return $delete;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id) {
        $returnObject = $this->getMapper()->find($id, $this);
        return $returnObject;
    }

    /**
     * @return int|string
     */
    public function loadReport() {
        $lastPage = $_SERVER['HTTP_REFERER'];
        $lastPage = explode('/', $lastPage);
        $lastPage = end($lastPage);
        switch ($lastPage) {
            case "logins":
                return "logins";
                break;
            case "orders":
                return "orders";
                break;
            case "registration":
                return "registration";
                break;
            case "sold":
                return "sold";
                break;
            case "stock":
                return "stock";
                break;
            case "stockchanges":
                return "stockchanges";
                break;
            case "vouchers":
                return "vouchers";
                break;
            case "dispatch":
                return "dispatch";
                break;
            default:
                return 10;
                break;
        }
    }

    /**
     * @param $columns
     * @param $report
     * @param $categories
     * @param $usergroups
     * @param $dateFrom
     * @param $dateTo
     */
    public function build($columns, $report, $categories, $usergroups, $dateFrom, $dateTo) {
        $data = $this->getData($columns, $report, $categories, $usergroups, $dateFrom, $dateTo);
        $this->array2csv($columns, $data);
        die();
    }

    /**
     * @param $columns
     * @param $report
     * @param $categories
     * @param $usergroups
     * @param $dateFrom
     * @param $dateTo
     * @return mixed
     */
    public function getData($columns, $report, $categories, $usergroups, $dateFrom, $dateTo) {
        $data = $this->getMapper()->getData($columns, $report, $categories, $usergroups, $dateFrom, $dateTo);
        return $data;
    }

    /**
     * @param $columns
     * @param $data
     */
    public function array2csv($columns, $data) {
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="data_export_"' . date("Y-m-d") . '".csv"');

        $output = fopen('php://output', 'w+');
        fputcsv($output, array_values($columns), ',', '\'');
        foreach ($data as $dataValue) {
            fputcsv($output, array_values($dataValue), ',', '\'');
        }

    }
}

