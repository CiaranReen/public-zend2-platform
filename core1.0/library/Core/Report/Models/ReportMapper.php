<?php

class Core_Report_Models_ReportMapper {

    protected $_dbTable;

    const TABLE = 'Core_Report_Models_DbTable_Report';

    public function setDbTable($dbTable) {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable() {
        if (null === $this->_dbTable) {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }

    public function getSavedReports() {
        $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
            ->from(array('sr' => 'saved_reports'), array('*'));
        $resultSet = $this->getDbTable()->fetchAll($stmt);
        return $resultSet;
    }

    public function save(Core_Report_Models_Report $report) {
        $columns = serialize($report->getColumns());

        if ($report->getUsergroups() != null) {
            $usergroups = serialize($report->getUsergroups());
        } else {
            $usergroups = null;
        }

        if ($report->getCategories() != null) {
            $categories = serialize($report->getCategories());
        } else {
            $categories = null;
        }

        $data = array (
            'report_name' => $report->getName(),
            'columns' => $columns,
            'usergroups' => $usergroups,
            'categories' => $categories,
            'type' => $report->getType()
        );

        if ($report->getId() === null) {
            $save = $this->getDbTable()->insert($data);
            return $save;
        } else {
            $save = $this->getDbTable()->update($data, 'id = ?', $report->getId());
            return $save;
        }
    }

    public function delete($id) {
        $delete = $this->getDbTable()->delete('id = '. $id);
        return $delete;
    }

    public function find($id, Core_Report_Models_Report $rModel) {
        $result = $this->getDbTable()->find($id);

        if (0 == count($result)) {
            return false;
        }
        $row = $result->current();
        $rModel->setId($row->id)
            ->setName($row->report_name)
            ->setColumns($row->columns)
            ->setUsergroups($row->usergroups)
            ->setCategories($row->categories)
            ->setType($row->type);
        return $rModel;
    }

    public function getData($columns, $report, $categories, $usergroups, $dateFrom, $dateTo) {
        switch ($report) {

            //Logins
            case "logins" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('u' => 'user'), $columns)
                    ->joinInner(array('ug' => 'user_groups'), 'u.user_group_id = ug.user_group_id')
                    ->where('ug.user_group_id IN (?)', $usergroups)
                    ->where('last_login >= ?', $dateFrom)
                    ->where('last_login <= ?', $dateTo);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Orders
            case "orders" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('o' => 'orders'), $columns)
                    ->joinInner(array('u' => 'user'), 'o.user_id = u.user_id')
                    ->joinInner(array('ug' => 'user_groups'), 'u.user_group_id = ug.user_group_id')
                    ->where('ug.user_group_id IN (?)', $usergroups)
                    ->where('o.date_ordered >= ?', $dateFrom)
                    ->where('o.date_ordered <= ?', $dateTo);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Registration
            case "registration" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('u' => 'user'), $columns)
                    ->where('created_at >= ?', $dateFrom)
                    ->where('created_at <= ?', $dateTo);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Sold
            case "sold" :
                $stmt = $this->getDbTable()
                    ->select()->setIntegrityCheck(false)
                    ->from(array('op' => 'order_parts'), array('*', 'count(p.product_id) as count'))
                    ->joinInner(array('s' => 'stock'), 'op.stock_id = s.stock_id')
                    ->joinInner(array('p' => 'products'), 's.product_id = p.product_id')
                    ->joinInner(array('pi' => 'product_images'), 's.product_id = pi.product_id')
                    ->group('p.product_id')
                    ->order('count DESC');
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Stock
            case "stock" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('s' => 'stock'), $columns)
                    ->joinInner(array('p' => 'products'), 'p.product_id = s.product_id')
                    ->joinInner(array('pc' => 'product_categories'), 'pc.product_id = p.product_id')
                    ->joinInner(array('c' => 'categories'), 'c.category_id = pc.category_id')
                    ->where('c.category_id IN (?)', $categories);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Stock Changes
            case "stockchanges" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('s' => 'stock'), array('*'))
                    ->joinInner(array('p' => 'products'), 'p.product_id = s.product_id')
                    ->joinInner(array('pc' => 'product_categories'), 'pc.product_id = p.product_id')
                    ->joinInner(array('c' => 'categories'), 'c.category_id = pc.category_id')
                    ->where('c.category_id IN (?)', $categories);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Vouchers
            case "vouchers" :
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('v' => 'voucher'), $columns)
                    ->where('start >= ?', $dateFrom)
                    ->where('expire <= ?', $dateTo);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;

            //Dispatch
            case "dispatch" :
                $dispatched = 'dispatched';
                $stmt = $this->getDbTable()->select()->setIntegrityCheck(false)
                    ->from(array('o' => 'orders'), $columns)
                    ->where('o.status = ?', $dispatched)
                    ->where('o.date_ordered >= ?', $dateFrom)
                    ->where('o.date_ordered <= ?', $dateTo);
                $resultSet = $this->getDbTable()->fetchAll($stmt);
                $data = array();
                foreach ($resultSet as $key => $result) {
                    foreach ($columns as $column) {
                        $data[$key][$column] = $result->$column;
                    }
                }
                return $data;
                break;
        }
    }

}