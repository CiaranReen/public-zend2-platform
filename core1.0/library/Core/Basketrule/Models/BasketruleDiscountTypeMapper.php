<?php

class Core_Basketrule_Models_BasketruleDiscountTypeMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Basketrule_Models_DbTable_BasketruleDiscountType';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Basketrule_Models_BasketruleDiscountType $basketruleDiscountType)
    {
        $data = array(
            'name'      => $basketruleDiscountType->getName(),
            'shorthand' => $basketruleDiscountType->getShorthand(),
        );
 
        if (null === ($id = $basketruleDiscountType->getId())) 
        {
            unset($data['discount_type_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('discount_type_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Basketrule_Models_BasketruleDiscountType $basketruleDiscountType)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $basketruleDiscountType->setId($row->discount_type_id)
                    ->setName($row->name)
                    ->setShorthand($row->shorthand);
        return $basketruleDiscountType;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Basketrule_Models_BasketruleDiscountType();
            $entry->findById($row->discount_type_id);
            $entries[] = $entry;
        }
        return $entries;
    }
}

