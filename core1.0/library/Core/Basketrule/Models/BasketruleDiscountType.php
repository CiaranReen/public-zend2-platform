<?php
/**
 * BasketruleDiscountType Class
 */

class Core_Basketrule_Models_BasketruleDiscountType extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_shorthand;
    
    protected $_mapper;
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getShorthand() {
        return $this->_shorthand;
    }

    public function setShorthand($shorthand) {
        $this->_shorthand = (string) $shorthand;
        return $this;
    }

    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleDiscountTypeMapper());
		}
		return $this->_mapper;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    
    /**
     * gets all the basket rules
     * @return array|boolean
     */
    public function getAllDiscountTypes(){
        try{
            return $this->getMapper()->fetchAll();
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    public function findById($id){
        return $this->getMapper()->find($id, $this);
    }
    
    public function getPairs(array $discountTypes = null){
        $discountTypePairs = array();
        if(!$discountTypes){
            $discountTypes = $this->getAllDiscountTypes();
        }
        foreach ($discountTypes as $discountType) {
            $discountTypePairs[$discountType->getId()] = $discountType->getName();
        }
        
        return $discountTypePairs;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
}