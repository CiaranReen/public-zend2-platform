<?php

class Core_Basketrule_Models_BasketruleConditionMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Basketrule_Models_DbTable_BasketruleCondition';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Basketrule_Models_BasketruleCondition $basketruleCondition)
    {
        $data = array(
            'name'      => $basketruleCondition->getName(),
            'shorthand' => $basketruleCondition->getShorthand(),
        );
 
        if (null === ($id = $basketruleCondition->getId())) 
        {
            unset($data['condition_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('condition_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Basketrule_Models_BasketruleCondition $basketruleCondition)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $basketruleCondition->setId($row->condition_id)
                    ->setName($row->name)
                    ->setShorthand($row->shorthand);
        return $basketruleCondition;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Basketrule_Models_BasketruleCondition();
            $entry->findById($row->condition_id);
            $entries[] = $entry;
        }
        return $entries;
    }
}

