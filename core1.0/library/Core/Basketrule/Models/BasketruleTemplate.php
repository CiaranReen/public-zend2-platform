<?php
/**
 * Basketrule Templates
 */

class Core_Basketrule_Models_BasketruleTemplate extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_conditionId;
    protected $_quantity;
    protected $_discount;
    protected $_discountTypeId;
    
    protected $_mapper;
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getConditionId() {
        return $this->_conditionId;
    }

    public function setConditionId($conditionId) {
        $this->_conditionId = (int) $conditionId;
        return $this;
    }

    public function getQuantity() {
        return $this->_quantity;
    }

    public function setQuantity($quantity) {
        $this->_quantity = (int) $quantity;
        return $this;
    }

    public function getDiscount() {
        return $this->_discount;
    }

    public function setDiscount($discount) {
        $this->_discount = (float) $discount;
        return $this;
    }

    public function getDiscountTypeId() {
        return $this->_discountTypeId;
    }

    public function setDiscountTypeId($discountTypeId) {
        $this->_discountTypeId = (int) $discountTypeId;
        return $this;
    }

    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleTemplateMapper());
		}
		return $this->_mapper;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    
    /**
     * gets all the basket rules templates
     * @return array|boolean
     */
    public function getAllTemplates(){
        try{
            return $this->getMapper()->fetchAll();
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    public function findById($id){
        return $this->getMapper()->find($id, $this);
    }
    
    public function getPairs(array $templates = null){
        $templatePairs = array();
        if(!$templates){
            $templates = $this->getAllTemplates();
        }
        foreach ($templates as $template) {
            $templatePairs[$template->getId()] = $template->getName();
        }
        return $templatePairs;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
    
}