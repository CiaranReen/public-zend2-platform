<?php

class Core_Basketrule_Models_BasketruleMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Basketrule_Models_DbTable_Basketrule';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Basketrule_Models_Basketrule $basketrule)
    {
        $data = array(
            'name'              => $basketrule->getName(),
            'condition_id'      => $basketrule->getConditionId(),
            'quantity'          => $basketrule->getQuantity(),
            'quantity_max'      => $basketrule->getQuantityMax(),
            'discount'          => $basketrule->getDiscount(),
            'discount_type_id'  => $basketrule->getDiscountTypeId(),
            'start_date'        => $basketrule->getStartDate(),
            'end_date'          => $basketrule->getEndDate(),
            'priority'          => $basketrule->getPriority(),
        );
 
        if (null === ($id = $basketrule->getId())) 
        {
            unset($data['basketrule_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('basketrule_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Basketrule_Models_Basketrule $basketrule)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
        $row = $result->current();
        $basketrule->setId($row->basketrule_id)
                    ->setName($row->name)
                    ->setConditionId($row->condition_id)
                    ->setQuantity($row->quantity)
                    ->setQuantityMax($row->quantity_max)
                    ->setDiscount($row->discount)
                    ->setDiscountTypeId($row->discount_type_id)
                    ->setStartDate($row->start_date)
                    ->setEndDate($row->end_date)
                    ->setPriority($row->priority);
        return $basketrule;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $entry = new Core_Basketrule_Models_Basketrule();
            $entry->findById($row->basketrule_id);
            $entries[] = $entry;
        }
        return $entries;
    }
    
    /**
     * Gets all products objects of products assigned to this basketrule
     * @param int $basketruleId
     * @return array \Core_Product_Models_Product
     */
    public function fetchBasketruleProducts($basketruleId){
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable, array())
                        ->joinUsing('product_categories', 'basketrule_id')
                        ->where('categories.basketrule_id = ?', $basketruleId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        foreach($resultSet as $row){
            $entry = new Core_Product_Models_Product();
            $entries[] = $entry->findById($row->product_id);
        }
        return $entries;
    }
}

