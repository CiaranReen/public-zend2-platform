<?php

class Core_Basketrule_Models_BasketruleProduct extends App_Model_Abstract
{
    protected $_basketruleId;
    protected $_productIds = array();
    
    protected $_mapper;
    
    public function setBasketRuleId($basketRuleId)
    {
        $this->_basketruleId = (int) $basketRuleId;
        return $this;
    }
 
    public function getBasketRuleId()
    {
        return $this->_basketruleId;
    }
 
    public function setProductIds(array $productIds)
    {
        $this->_productIds = $productIds;
        return $this;
    }
 
    public function getProductIds()
    {
        return $this->_productIds;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleProductMapper());
		}
		return $this->_mapper;
    }
    
    public function findByBasketRuleId($basketRuleId){
        $this->getMapper()->find($basketRuleId, $this);
        return $this;
    }
    
    public function save(){
        $this->getMapper()->save($this);
        return $this;
    }
    
    public function getProducts(){
        $products = array();
        if($this->getProductIds()){
            foreach($this->getProductIds() as $productId){
                $productModel = new Core_Product_Models_Product();
                $product = $productModel->findById($productId);
                $products[] = $product;
            }
        }
        return $products;
    }
}