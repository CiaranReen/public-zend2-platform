<?php

class Core_Basketrule_Models_BasketruleProductMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Basketrule_Models_DbTable_BasketruleProduct';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Basketrule_Models_BasketruleProduct $basketruleProduct)
    {
        $this->getDbTable()->delete(array('basketrule_id = ?' => $basketruleProduct->getBasketRuleId()));
        foreach($basketruleProduct->getProductIds() as $productId){
            $data = array( 
                'basketrule_id' => $basketruleProduct->getBasketRuleId(),
                'product_id'    => $productId
            );
            $this->getDbTable()->insert($data); 
        }
    }
 
    public function find($basketRuleId, Core_Basketrule_Models_BasketruleProduct $basketruleProduct)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('basketrule_id = ?', $basketRuleId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        $productIds = array();
        foreach($resultSet as $row){
            $productIds[] = $row->product_id;
        }
        $basketruleProduct->setBasketRuleId($basketRuleId);
        
        $basketruleProduct->setProductIds($productIds);
        return $basketruleProduct;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $basketruleProduct = new Core_Basketrule_Models_BasketruleProduct();
            
            $basketruleProduct->findByBasketRuleId($row->basketrule_id);
            
            $entries[] = $basketruleProduct;
        }
        
        return $entries;
    }
}

