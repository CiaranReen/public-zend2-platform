<?php

class Core_Basketrule_Models_BasketruleUsergroup extends App_Model_Abstract
{
    protected $_basketruleId;
    protected $_usergroupIds = array();
    
    protected $_mapper;
    
    public function setBasketRuleId($basketRuleId)
    {
        $this->_basketruleId = (int) $basketRuleId;
        return $this;
    }
 
    public function getBasketRuleId()
    {
        return $this->_basketruleId;
    }
 
    public function setUsergroupIds(array $usergroupIds)
    {
        $this->_usergroupIds = $usergroupIds;
        return $this;
    }
 
    public function getUsergroupIds()
    {
        return $this->_usergroupIds;
    }
    
    public function setMapper($mapper) {
		$this->_mapper = $mapper;
		return $this;
	}
	
    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleUsergroupMapper());
		}
		return $this->_mapper;
    }
    
    public function findByBasketRuleId($basketRuleId){
        $this->getMapper()->find($basketRuleId, $this);
        return $this;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
    
    public function getUsergroups(){
        $usergroups = array();
        if(!$this->getUsergroupIds()){
            return ;
        }
        else{
            foreach($this->getUsergroupIds() as $usergroupId){
                $usergroupModel = new Core_Usergroup_Models_Usergroup();
                $usergroups[]   = $usergroupModel->findById($usergroupId);
            }
        }
        return $usergroups;
    }
}