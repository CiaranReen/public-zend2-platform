<?php
/**
 * BasketruleCondition Class
 */

class Core_Basketrule_Models_BasketruleCondition extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_shorthand;
    
    protected $_mapper;
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getShorthand() {
        return $this->_shorthand;
    }

    public function setShorthand($shorthand) {
        $this->_shorthand = (string) $shorthand;
        return $this;
    }

    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleConditionMapper());
		}
		return $this->_mapper;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }
    
    /**
     * gets all the basket rules
     * @return array|boolean
     */
    public function getAllConditions(){
        return $this->getMapper()->fetchAll();
    }
    
    public function findById($id){
        return $this->getMapper()->find($id, $this);
    }
    
    public function getPairs(array $conditions = null){
        $conditionPairs = array();
        if(!$conditions){
            $conditions = $this->getAllConditions();
        }
        foreach ($conditions as $condition) {
            $conditionPairs[$condition->getId()] = $condition->getName();
        }
        
        return $conditionPairs;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
}