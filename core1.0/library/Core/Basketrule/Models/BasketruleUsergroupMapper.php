<?php

class Core_Basketrule_Models_BasketruleUsergroupMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Basketrule_Models_DbTable_BasketruleUsergroup';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Basketrule_Models_BasketruleUsergroup $basketruleUsergroup)
    {
        $this->getDbTable()->delete(array('basketrule_id = ?' => $basketruleUsergroup->getBasketRuleId()));
        foreach($basketruleUsergroup->getUsergroupIds() as $usergroupId){
            $data = array( 
                'basketrule_id' => $basketruleUsergroup->getBasketRuleId(),
                'user_group_id'    => $usergroupId
            );
            $this->getDbTable()->insert($data); 
        }
    }
 
    public function find($basketRuleId, Core_Basketrule_Models_BasketruleUsergroup $basketruleUsergroup)
    {
        $select = $this->getDbTable()
                        ->select()->setIntegrityCheck(false)
                        ->from($this->_dbTable)
                        ->where('basketrule_id = ?', $basketRuleId);
        $resultSet = $this->getDbTable()->fetchAll($select);
        $usergroupIds = array();
        foreach($resultSet as $row){
            $usergroupIds[] = $row->user_group_id;
        }
        $basketruleUsergroup->setBasketRuleId($basketRuleId);
        $basketruleUsergroup->setUsergroupIds($usergroupIds);
        return $basketruleUsergroup;
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach($resultSet as $row) 
        {
            $basketruleUsergroup = new Core_Basketrule_Models_BasketruleUsergroup();
            
            $basketruleUsergroup->findByBasketRuleId($row->basketrule_id);
            
            $entries[] = $basketruleUsergroup;
        }
        
        return $entries;
    }
}

