<?php
/**
 * Basketrule Class
 */

class Core_Basketrule_Models_Basketrule extends App_Model_Abstract {

    protected $_id;
    protected $_name;
    protected $_conditionId;
    protected $_quantity;
    protected $_quantityMax;
    protected $_discount;
    protected $_discountTypeId;
    protected $_startDate;
    protected $_endDate;
    protected $_priority;
    
    protected $_mapper;
    protected static $table = 'basket_rules';
    
    public function getId() {
        return $this->_id;
    }

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getName() {
        return $this->_name;
    }

    public function setName($name) {
        $this->_name = (string) $name;
        return $this;
    }

    public function getConditionId() {
        return $this->_conditionId;
    }

    public function setConditionId($conditionId) {
        $this->_conditionId = (int) $conditionId;
        return $this;
    }

    public function getQuantity() {
        return $this->_quantity;
    }

    public function setQuantity($quantity) {
        $this->_quantity = (int) $quantity;
        return $this;
    }

    public function getQuantityMax() {
        return $this->_quantityMax;
    }

    public function setQuantityMax($quantityMax) {
        $this->_quantityMax = (float) $quantityMax;
        return $this;
    }

    public function getDiscount() {
        return $this->_discount;
    }

    public function setDiscount($discount) {
        $this->_discount = (float) $discount;
        return $this;
    }

    public function getDiscountTypeId() {
        return $this->_discountTypeId;
    }

    public function setDiscountTypeId($discountTypeId) {
        $this->_discountTypeId = (int) $discountTypeId;
        return $this;
    }

    public function getStartDate() {
        return $this->_startDate;
    }

    public function setStartDate($startDate) {
        $this->_startDate = $startDate;
        return $this;
    }

    public function getEndDate() {
        return $this->_endDate;
    }

    public function setEndDate($endDate) {
        $this->_endDate = $endDate;
        return $this;
    }

    public function getPriority() {
        return $this->_priority;
    }

    public function setPriority($priority) {
        $this->_priority = (int) $priority;
        return $this;
    }

    public function getMapper() {
		if (null === $this->_mapper) {
			$this->setMapper(new Core_Basketrule_Models_BasketruleMapper());
		}
		return $this->_mapper;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function findById($id){
        return $this->getMapper()->find($id, $this);
    }
    
    /**
     * gets all the basket rules
     * @return array|boolean
     */
    public function getAllRules(){
        return $this->getMapper()->fetchAll();
    }
    
    /**
     * Gets the condition object using this models condition id
     * @return \Core_Basketrule_Models_BasketruleCondition
     */
    public function getCondition(){
        $basketruleConditionModel   = new Core_Basketrule_Models_BasketruleCondition();
        $conditionId                = $this->getConditionId();
        return $basketruleConditionModel->findById($conditionId);
    }
    
    /**
     * Gets the discount type object using this model's discount type id
     * @return \Core_Basketrule_Models_BasketruleDiscountType
     */
    public function getDiscountType(){
        $basketruleDiscountTypeModel    = new Core_Basketrule_Models_BasketruleDiscountType();
        $discountTypeId                 = $this->getDiscountTypeId();
        return $basketruleDiscountTypeModel->findById($discountTypeId);
    }
    
    /**
     * Gets basketrules in pairs of matching ids and names
     * @param array $basketrules
     * @return array
     */
    public function getPairs(array $basketrules = null){
        $basketrulePairs = array();
        if(!$basketrules){
            $basketrules = $this->getAllRules();
        }
        foreach ($basketrules as $basketrule) {
            $basketrulePairs[$basketrule->getId()] = $basketrule->getName();
        }
        return $basketrulePairs;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }
    
    /**
     * checks if the basket rule is still valid / expired
     * @return boolean
     */
    public function isValid(){
        try{
            $today          = strtotime(date('Y-m-d'));
            $startDate      = strtotime($this->getStartDate());
            $endDate        = strtotime($this->getEndDate());
            if($today >= $startDate && $today <= $endDate){
                return true;
            }else{
                return false;
            }
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * gets all valid basket rules
     * @return array|boolean
     */
    public function getAllValidRules(){
        $validRules = array();
        try{
            $allRules      = $this->getAllRules();
            if($allRules){
                foreach($allRules as $rule){
                     if($rule->isValid()){
                         $validRules[] = $rule;
                     }
                }
            }
            return $validRules;
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * gets all applicable products for the basket rule
     * @return array|boolean
     */
    public function getApplicableProducts(){
        $basketruleProductMapper    = new Core_Basketrule_Models_BasketruleProduct();
        $basketruleProduct          = $basketruleProductMapper->findByBasketRuleId($this->getId());
        $productIds = array();
        if($basketruleProduct instanceof Core_Basketrule_Models_BasketruleProduct && $basketruleProduct->getProductIds()){
            $productIds = $basketruleProduct->getProductIds();
        } else {
            $productMapper = new Core_Product_Models_Product();
            $allProducts = $productMapper->getAllProducts();
            foreach($allProducts as $product){
                $productIds[] = $product->id;
            }
        }
        return $productIds;
    }
    
    /**
     * gets all applicable usergroups for the basket rule
     * @return array|boolean
     */
    public function getApplicableUsergroups(){
        $basketruleUsergroupModel = new Core_Basketrule_Models_BasketruleUsergroup();
        $basketruleUsergroup = $basketruleUsergroupModel->findByBasketRuleId($this->getId());
        if(!$basketruleUsergroup instanceof Core_Basketrule_Models_BasketruleUsergroup){
            return ;
        }
        return $basketruleUsergroup->getUsergroupIds();
    }
    
    
    
    /**
     * checks if a basketrule is applicable to a particular product
     * @param int $productId
     * @return boolean
     */
    public function isApplicableToSpecificProduct($productId){
        try{
            $isValid               = false;
            $applicableProducts    = $this->getApplicableProducts();
            if(!$applicableProducts){
                return ;
            }
            foreach($applicableProducts as $productId){
                if($productId == $productId){
                    $isValid = true;
                }
            }
            return $isValid;
        } 
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * checks if a basketrule is applicable to a particular usergroup
     * @param int $userGroupId
     * @return boolean
     */
    public function isApplicableToSpecificUsergroup($usergroupId){
        try{
            $isValid                   = false;
            $applicableUsergroups      = $this->getApplicableUsergroups();
            if(!$applicableUsergroups){
                return ;
            }
            foreach($applicableUsergroups as $usergroupId){
                if($usergroupId == $usergroupId){
                    $isValid = true;
                }
            }
            return $isValid;
        } 
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * gets all valid rules that affect a product
     * @param int $productId
     * @return array|boolean
     */
    public function getAllValidRulesSpecificToProduct($productId){
        try{
            $validRulesSpecificToProduct    = array();
            $validRules                     = $this->getAllValidRules();
            if(!$validRules){
                return ;
            }
            foreach($validRules as $rule){
                if($rule->isApplicableToSpecificProduct($productId)){
                    $validRulesSpecificToProduct[] = $rule;
                }
            }
            return $validRulesSpecificToProduct;
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * gets the rule to applied to the product . sorts all rules using their order of priority 
     * @param int $productId
     * @return int|boolean
     * @throws Exception
     */
    public function getRuleToBeAppliedToProduct($productId){
        try{
            $allValidRulesSpecificToProduct = $this->getAllValidRulesSpecificToProduct($productId);
            if(!$allValidRulesSpecificToProduct){
                return ;
            }
            if(sizeof($allValidRulesSpecificToProduct) == 1){
                $basketRuleId = $allValidRulesSpecificToProduct[0]->getId();
            }
            elseif(sizeof($allValidRulesSpecificToProduct) > 1){
                $basketRuleId = $allValidRulesSpecificToProduct[0]->getId();
                for($i=0; $i < (sizeof($allValidRulesSpecificToProduct)-1); $i++){
                    if($allValidRulesSpecificToProduct[$i+1]->getPriority() < $allValidRulesSpecificToProduct[$i]->getPriority()){
                        $basketRuleId = $allValidRulesSpecificToProduct[$i+1]->getId();
                    }
                }
            }
            return $this->findById($basketRuleId);
        }
        catch(Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $usergroupId
     * @return bool|float
     */
    public function getProductDiscount($productId, $quantity, $usergroupId = NULL){
        try{
            $productModel       = new Core_Product_Models_Product();
            $product            = $productModel->findById($productId);
            $basketrule         = $this->getRuleToBeAppliedToProduct($productId);
            $productDiscount    = 0;
            if(!$basketrule instanceof Core_Basketrule_Models_Basketrule){
                return ;
            }
            if($usergroupId && !$basketrule->isApplicableToSpecificUsergroup($usergroupId)){
                return ;
            }
            $price = number_format($product->getMinPrice(), 2);
            switch ($basketrule->getCondition()->getId()):
                case 1: //greater than condition
                    if($quantity > $basketrule->getQuantity()){
                        switch($basketrule->getDiscountTypeId()){
                            case 1:
                                $percentage = ($basketrule->getDiscount()/100);
                                $productDiscount = ($price * $quantity) * ($basketrule->getDiscount()/100);
                                break;
                            case 2:
                                $productDiscount = $basketrule->getDiscount();
                                break;
                        }
                    }
                    break;
                case 2: //between
                    if($quantity > $basketrule->getQuantity() && $quantity < $basketrule->getQuantityMax()){
                        switch($basketrule->getDiscountTypeId()){
                            case 1:
                                $productDiscount = ($price * $quantity) * ($basketrule->getDiscount()/100);
                                break;
                            case 2:
                                $productDiscount = $basketrule->getDiscount();
                                break;
                        }
                    }
                    break;
                case 3: //equal to condition
                    $multiples = floor($quantity / $basketrule->getQuantity());
                    if($multiples > 0){
                        switch($basketrule->getDiscountTypeId()){
                            case 1:
                                $productDiscount = ($price * $multiples) * ($basketrule->getDiscount()/100);
                                break;
                            case 2:
                                $productDiscount = $basketrule->getDiscount() * $multiples;
                                break;
                        }
                    }
                    break;
            endswitch;
            return round($productDiscount, 2);
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }

    /**
     * @param $stockId
     * @param $quantity
     * @param $usergroupId
     * @return bool|float
     */
    public function getStockDiscount($stockId, $quantity, $usergroupId = NULL){
        try{
            $stockMapper        = new Core_Stock_Models_StockMapper();
            $stock              = $stockMapper->find($stockId, new Core_Stock_Models_Stock);
            $allValidRules      = $this->getAllValidRules();
            if(!$allValidRules || !$stock instanceof Core_Stock_Models_Stock){
                return ;
            }
            return $this->getProductDiscount($stock->getProductId(), $quantity, $usergroupId);
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    public function getStockDiscountPerItem($stockId, $quantity, $usergroupId = NULL){
        $discount = $this->getStockDiscount($stockId, $quantity, $usergroupId);
        return (float) ($discount / $quantity);
    }
    
    /**
     * gets the total basket rule discount of the basket
     * @param array $basket
     * @return float|boolean
     */
    public function getBasketDiscount(Core_Basket_Models_Basket $basket, $usergroupId){
        try{
            $totalDiscount    = 0;
            $stockModel       = new Core_Stock_Models_Stock();
            $allValidRules    = $this->getAllValidRules();
            if(!$allValidRules || !$basket instanceof Core_Basket_Models_Basket){
                return ;
            }
            if($basket->getStock()){
                foreach($basket->getStock() as $stockId => $quantity){
                    $stock = $stockModel->findById($stockId);
                    foreach($allValidRules as $rule){
                        $stockDiscount   = 0;
                        if($rule->isApplicableToSpecificUsergroup($usergroupId) && $rule->isApplicableToSpecificProduct($stock->getProductId())){
                            $stockDiscount   = $rule->getStockDiscount($stockId, $quantity, $usergroupId);
                        }
                        if($stockDiscount){
                            $totalDiscount     += $stockDiscount;
                        }
                    }
                }
                return (float) $totalDiscount;
            }
            else{
                return false;
            }
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
    
    /**
     * deletes a basket rule
     * @param int $basketRuleId
     * @return boolean
     */
    public function deleteRule(){
        try{
            $where = "basket_rule_id = ".$this->getBasketRuleId();
            $this->getDbAdapter()->delete('basket_rule_usergroups', $where);
            $where = "basket_rule_id = ".$this->getBasketRuleId();
            $this->getDbAdapter()->delete('basket_rule_products', $where);
            $where = "basket_rule_id = ".$this->getBasketRuleId();
            $this->getDbAdapter()->delete(self::$table, $where);
            return true;
        }
        catch (Exception $e){
            error_log($e->getMessage());
            return false;
        }
    }
}