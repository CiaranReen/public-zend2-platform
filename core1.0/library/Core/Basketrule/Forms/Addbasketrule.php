<?php
/**
 * User: Emmanuel
 * Date: 16/12/2013
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */

class Core_Basketrule_Forms_AddBasketrule extends Zend_Form {

	public function init() {
        $basketruleTemplateModel        = new Core_Basketrule_Models_BasketruleTemplate();
        $basketruleConditionModel       = new Core_Basketrule_Models_BasketruleCondition();
        $basketruleDiscountTypeModel    = new Core_Basketrule_Models_BasketruleDiscountType();
        $userGroupModel                 = new Core_Usergroup_Models_Usergroup();
        // Set properties of the form
        $this->setName('basketrule')
		->setMethod(self::METHOD_POST)
		->setDescription('Add a basket rule');
        
        $ruleTemplates = $this->createElement('select', 'rule_templates');
        if($basketruleTemplateModel->getPairs())
        {
            $ruleTemplates->setOptions(
                    array(
                        'label'     => 'Select a Template?',
                        'required'  => false,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
            $ruleTemplates->addMultiOptions(array('' => 'No Template Selected'));
            $ruleTemplates->addMultiOptions($basketruleTemplateModel->getPairs());
        }
        
        // Name
		$name = $this->createElement('text', 'name')
				->setOptions(
				array(
					 'label'    => 'Basket Rule Name',
					 'required' => true,
					 'class'    => 'required',
					 'filters'  => array('StringTrim', 'StripTags'),
				)
			);
        // Condition
        $condition = $this->createElement('select', 'condition');
        if($basketruleConditionModel->getPairs())
        {
            $condition->setOptions(
                            array(
                                'label'    => 'Condition',
                                'required' => true,
                            )
                       );
            $condition->addMultiOptions($basketruleConditionModel->getPairs());
        }
        
		// Quantity
		$quantity = $this->createElement('text', 'quantity')
				->setOptions(
				array(
					 'label'    => 'Quantity',
					 'required' => true,
					 'class'    => 'required',
					 'filters'  => array('StringTrim', 'StripTags'),
				)
			);
        // Quantity
		$quantity2 = $this->createElement('text', 'quantity_2')
				->setOptions(
				array(
					 'label'    => 'Quantity [ To ]',
                     'display'  => 'none',
					 'class'    => 'required',
					 'filters'  => array('StringTrim', 'StripTags'),
				)
                
			);
		// Product
		$product = $this->createElement('text', 'product')
				 ->setOptions(
                    array(
                         'label'    => 'Product [ Start typing and click on product to select ]',
                         'class'    => 'product',
                         'filters'  => array('StringTrim', 'StripTags'),
                    )
                );
        
        $discount = $this->createElement('text', 'discount')
                ->setOptions(
                    array(
                        'label'     => 'Discount',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
        $discountType = $this->createElement('select', 'discount_type');
        if($basketruleDiscountTypeModel->getPairs())
        {
            $discountType->setOptions(
                        array(
                            'label'     => 'Discount Type',
                            'required'  => true,
                            'filters'   => array('StringTrim', 'StripTags')
                        )
                    );
            $discountType->addMultiOptions($basketruleDiscountTypeModel->getPairs());
        }
        
        $startDate = $this->createElement('text', 'start_date')
                ->setOptions(
                    array(
                        'id'        => 'start_date',
                        'class'     => 'datepicker',
                        'label'     => 'Start Date',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );
        $endDate = $this->createElement('text', 'end_date')
                ->setOptions(
                    array(
                        'id'        => 'end_date',
                        'class'     => 'datepicker',
                        'label'     => 'End Date',
                        'required'  => true,
                        'filters'   => array('StringTrim', 'StripTags')
                    )
                );

        $userGroups = $this->createElement('multiCheckbox', 'usergroup');
        if($userGroupModel->getPairs())
        {
            $userGroups->setOptions(
                    array(
                        'label'         => 'Usergroups Applicable',
                        'label_class'   => 'checkbox inline',
                        'class'         => 'checkbox inline',
                        'required'      => true,
                        'filters'       => array('StringTrim', 'StripTags')
                    )
                );
            $userGroups->addMultiOptions(array(0 => 'All Usergroups'));
            $userGroups->addMultiOptions($userGroupModel->getPairs());
        }
        
		// Submit Button
		$submit = $this->createElement('submit', 'submit') 
                        ->setOptions(
                            array(
                                'label' => 'save',
                                'class' => 'btn-primary',
                            )
                    );

		// Add controls to the form
		$this   ->addElement($ruleTemplates)
                ->addElement($name)
                ->addElement($condition)
                ->addElement($quantity)
                ->addElement($quantity2)
                ->addElement($product)
                ->addElement($discount)
                ->addElement($discountType)
                ->addElement($startDate)
                ->addElement($endDate)
                ->addElement($userGroups)
                ->addElement($submit);
	}
    
    public function populate(Core_Basketrule_Models_Basketrule $basketrule){
        $basketruleUsergroupModel   = new Core_Basketrule_Models_BasketruleUsergroup();
        $assignedUsergroups         = $basketruleUsergroupModel->findByBasketRuleId($basketrule->getId())->getUsergroupIds();
        $this->getElement('name')->setValue($basketrule->getName());
        $this->getElement('condition')->setValue($basketrule->getConditionId());
        $this->getElement('quantity')->setValue($basketrule->getQuantity());
        $this->getElement('quantity_2')->setValue($basketrule->getQuantityMax());
        $this->getElement('discount')->setValue($basketrule->getDiscount());
        $this->getElement('discount_type')->setValue($basketrule->getDiscountTypeId());
        $startDate = date("d-m-y", strtotime($basketrule->getStartDate()));
        $endDate = date("d-m-y", strtotime($basketrule->getEndDate()));
        $this->getElement('start_date')->setValue($startDate);
        $this->getElement('end_date')->setValue($endDate);
        $this->getElement('usergroup')->setValue($assignedUsergroups);
        $this->getElement('submit')->setLabel('Save Changes');
    }

}