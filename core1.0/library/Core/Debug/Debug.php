<?php
/**
 * Usage:
 * <code>
 * <?php echo Debug_Debug::getInfo($requestUri); ?>
 * </code>
 */

class Debug_Debug extends Application_Bootstrap {

	public static function getInfo($requestUri) {
		$output = "<pre>";
		$output .= "<h2>Debug</h2>\n";
		$output .= "<p>For request <code>{$requestUri}</code></p>";

		$debugApp = new Application_Bootstrap();

		$output .= "<p>Parameters: \n";

		// Check for parameters
		if($debugApp->hasParams($requestUri)) {
			// Get the parameters passed through the URL
			$params = $debugApp->getParams($requestUri);

			// Chop off params (they just complicate things from here on)
			// We'll pass them through to the appropriate action later

			// Get name of first param
			$firstParam = key($params);
			// Trim off everything after the first param
			$requestUri = substr($requestUri, 0, strripos($requestUri, $firstParam));
		} else {
			$output .= "There were no parameters detected";
		}

		$output .= "</p>";

		// Check for custom routes

		// If request uri doesn't end with a slash, add one
		if(substr($requestUri, strlen($requestUri)-1, 1) != "/") {
			$requestUri .= "/";
		}

		$output .= "<p>Custom routes: \n";

		$customRoute = $debugApp->getCustomRoute($requestUri);
		if($customRoute !== false || strlen($customRoute) > 0) {
			// A custom route exists. Use that instead of the actual request uri
			$output .= "<code>{$requestUri}</code> was rewritten to ";
			$requestUri = $customRoute;
			$output .= "<code>{$customRoute}</code>\n";
		} else {
			$output .= "There were no custom routes detected\n";
		}

		$output .= "</p>";

		// Get the module name
		$moduleName = $debugApp->getModuleName($requestUri);
		$output .= "<p>Module name: {$moduleName}</p>";

		// Get the controller name
		$controllerName = $debugApp->getControllerName($requestUri);
		$output .= "<p>Controllers name: {$controllerName}</p>";

		// Get the action name
		$actionName = $debugApp->getActionName($requestUri);
		$output .= "<p>Action name: {$actionName}</p>";

		// Construct the controller class name
		$controllerClassName = $moduleName . '_Controllers_' . $controllerName . '_' . $controllerName . 'Controllers';
		$output .= "<p>Controllers class name: {$controllerClassName}</p>";

		$output .= "</pre>";

		return $output;
	}

}