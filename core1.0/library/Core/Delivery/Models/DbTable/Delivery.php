<?php

class Core_Delivery_Models_DbTable_Delivery extends Zend_Db_Table_Abstract
{
    /** Table Name **/
        protected $_name = 'deliveries';
        
        protected $_dependentTables = array('delivery_items');
    
}

