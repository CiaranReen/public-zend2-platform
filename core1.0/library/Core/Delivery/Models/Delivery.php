<?php

class Core_Delivery_Models_Delivery extends App_Model_Abstract {

    protected $_id;
    protected $_orderId;
    protected $_date;
    protected $_tracking;
    protected $_comments;
    
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getOrderId() {
        return $this->_orderId;
    }

    public function setOrderId($orderId) {
        $this->_orderId = $orderId;
        return $this;
    }

    public function getDate() {
        return $this->_date;
    }

    public function setDate($date) {
        $this->_date = $date;
        return $this;
    }

    public function getTracking() {
        return $this->_tracking;
    }

    public function setTracking($tracking) {
        $this->_tracking = $tracking;
        return $this;
    }

    public function getComments() {
        return $this->_comments;
    }

    public function setComments($comments) {
        $this->_comments = $comments;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Delivery_Models_DeliveryMapper());
        }
        return $this->_mapper;
    }

    /**
     * Constructs Delivery object using Delivery id
     * @param int $DeliveryId
     * @return \Core_Delivery_Models_Delivery
     */
    public function findById($deliveryId) {
        $this->getMapper()->find($deliveryId, $this);
        return $this;
    }

    /**
     * Gets all Deliverys in database
     * @return array \Core_Delivery_Models_Delivery
     */
    public function getAllDeliveries() {
        $allDeliveries = $this->getMapper()->fetchAll();
        return $allDeliveries;
    }
    
    public function save() {
        return $this->getMapper()->save($this);
    }

}

