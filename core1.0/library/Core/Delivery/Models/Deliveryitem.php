<?php

class Core_Delivery_Models_Deliveryitem extends App_Model_Abstract {

    protected $_id;
    protected $_deliveryId;
    protected $_quantity;
    
    protected $_mapper;

    public function setId($id) {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId() {
        return $this->_id;
    }

    public function getDeliveryId() {
        return $this->_deliveryId;
    }

    public function setDeliveryId($deliveryId) {
        $this->_deliveryId = $deliveryId;
    }

    public function getQuantity() {
        return $this->_quantity;
    }

    public function setQuantity($quantity) {
        $this->_quantity = $quantity;
        return $this;
    }

    public function setMapper($mapper) {
        $this->_mapper = $mapper;
        return $this;
    }

    public function getMapper() {
        if (null === $this->_mapper) {
            $this->setMapper(new Core_Delivery_Models_DeliveryitemMapper());
        }
        return $this->_mapper;
    }
    
   
    /**
     * Constructs deliveryitem object using delivery id
     * @param int $deliveryItemId
     * @return \Core_Delivery_Models_Deliveryitem
     */
    public function findById($deliveryItemId) {
        $this->getMapper()->find($deliveryItemId, $this);
        return $this;
    }

    /**
     * Gets all deliveryitems in database
     * @return array \Core_Delivery_Models_Delivery
     */
    public function getAllDeliveryItems() {
        $allDeliveryItems = $this->getMapper()->fetchAll();
        return $allDeliveryItems;
    }
    
    /**
     * Gets all delivery items specific to an delivery id
     * @param type $deliveryId
     * @return type
     */
    public function getDeliveryPartsUsingDeliveryId($deliveryId){
        $allDeliveryItems = $this->getAllDeliveryItems();
        $entries    = array();
        foreach($allDeliveryItems as $deliveryItem){
            if($deliveryItem->getDeliveryId() === $deliveryId){
                $entries[] = $deliveryItem;
            }
        }
        return $entries;
    }
    
    public function save(){
        return $this->getMapper()->save($this);
    }

}

