<?php

class Core_Delivery_Models_DeliveryMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Delivery_Models_DbTable_Delivery';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Delivery_Models_Delivery $delivery)
    {
        $data = array(
            'order_id'          => $delivery->getOrderId(),
            'date'              => $delivery->getDate(),
            'tracking'          => $delivery->getTracking(),
            'comments'          => $delivery->getComments(),
        );
 
        if (null === ($id = $delivery->getId())) 
        {
            unset($data['delivery_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('delivery_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Delivery_Models_Delivery $delivery)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
    
        $row = $result->current();
        $delivery->setId($row->delivery_id)
                ->setOrderId($row->order_id)
                ->setDate($row->date)
                ->setTracking($row->tracking)
                ->setComments($row->comments);
        return $delivery;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Delivery_Models_Delivery();
            $entry->findById($row->delivery_id);
            $entries[] = $entry;
        }
        return $entries; 
    }
    
}

