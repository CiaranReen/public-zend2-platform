<?php

class Core_Delivery_Models_DeliveryitemMapper
{
    protected $_dbTable;
    const TABLE = 'Core_Delivery_Models_DbTable_Deliveryitem';
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) 
        {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) 
        {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) 
        {
            $this->setDbTable(self::TABLE);
        }
        return $this->_dbTable;
    }
 
    public function save(Core_Delivery_Models_Deliveryitem $deliveryItem)
    {
        $data = array(
            'delivery_id'   => $deliveryItem->getDeliveryId(),
            'quantity'      => $deliveryItem->getQuantity(),
        );
 
        if (null === ($id = $deliveryItem->getId())) 
        {
            unset($data['item_id']);
            return $this->getDbTable()->insert($data);
        } 
        else 
        {
            return $this->getDbTable()->update($data, array('item_id = ?' => $id));
        }
    }
 
    public function find($id, Core_Delivery_Models_Deliveryitem $deliveryItem)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) 
        {
            return;
        }
    
        $row = $result->current();
        $deliveryItem->setId($row->item_id)
                ->setDeliveryId($row->delivery_id)
                ->setQuantity($row->quantity);
        return $deliveryItem;
    }
    
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        
        foreach($resultSet as $row) 
        {
            $entry = new Core_Delivery_Models_Deliveryitem();
            $entry->findById($row->item_id);
            $entries[] = $entry;
        }
        return $entries; 
    }
    
}

