<?php

class Plugins_CustomRouter extends Zend_Controller_Plugin_Abstract
{
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
    }

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $front = Zend_Controller_Front::getInstance();
        if (!$front->getDispatcher()->isDispatchable($request)) {
            // Controller does not exist so set to core controller
            $front->setControllerDirectory('../application/core/modules/default-frontend/controllers');
        }

        $module = $request->getModuleName();
        if ($module === 'admin')
        {
            $adminLayoutPath = APPLICATION_PATH . '/core/layouts/scripts/';
            $adminLayout = 'admin';
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayoutPath($adminLayoutPath);
            $layout->setLayout($adminLayout);
        }
    }

    public function dispatchLoopStartup(
        Zend_Controller_Request_Abstract $request)
    {
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
    }

    public function dispatchLoopShutdown()
    {
    }
}