CREATE TABLE `voucher_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `v_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_voucher_user_group_v_id` (`v_id`),
  KEY `fk_voucher_user_group_user_group_id` (`user_group_id`),
  CONSTRAINT `fk_voucher_user_group_user_group_id` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`user_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_voucher_user_group_v_id` FOREIGN KEY (`v_id`) REFERENCES `voucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `voucher_user_group_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`user_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `voucher_user_group_ibfk_2` FOREIGN KEY (`v_id`) REFERENCES `voucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8