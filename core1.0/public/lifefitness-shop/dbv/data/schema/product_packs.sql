CREATE TABLE `product_packs` (
  `pp_id` int(11) NOT NULL AUTO_INCREMENT,
  `pack_name` varchar(255) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `pack_image` varchar(30) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8