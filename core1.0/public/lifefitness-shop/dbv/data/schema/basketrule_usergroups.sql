CREATE TABLE `basketrule_usergroups` (
  `basketrule_usergroup_id` int(10) NOT NULL AUTO_INCREMENT,
  `basketrule_id` int(10) DEFAULT NULL,
  `user_group_id` int(10) NOT NULL,
  PRIMARY KEY (`basketrule_usergroup_id`),
  KEY `FK_basket_rule_usergroups_basket_rules` (`basketrule_id`),
  KEY `FK_basket_rule_usergroups_user_groups` (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='maps the usergroups to the basket rules'