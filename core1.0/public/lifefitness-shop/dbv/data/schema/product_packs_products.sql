CREATE TABLE `product_packs_products` (
  `pp_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `pp_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`pp_product_id`),
  KEY `FK_product_packs_products_product_packs` (`pp_id`),
  KEY `FK_product_packs_products_products` (`product_id`),
  CONSTRAINT `FK_product_packs_products_product_packs` FOREIGN KEY (`pp_id`) REFERENCES `product_packs` (`pp_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_packs_products_ibfk_1` FOREIGN KEY (`pp_id`) REFERENCES `product_packs` (`pp_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1