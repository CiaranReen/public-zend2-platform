CREATE TABLE `colourways` (
  `colourway_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `hex` varchar(10) NOT NULL,
  `code` text NOT NULL,
  PRIMARY KEY (`colourway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1