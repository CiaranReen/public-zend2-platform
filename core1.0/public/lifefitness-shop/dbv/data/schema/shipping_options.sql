CREATE TABLE `shipping_options` (
  `shipping_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(100) NOT NULL,
  PRIMARY KEY (`shipping_option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8