CREATE TABLE `basketrules` (
  `basketrule_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `condition_id` int(10) DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_max` int(10) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `discount_type_id` int(10) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `priority` int(10) DEFAULT NULL,
  PRIMARY KEY (`basketrule_id`),
  KEY `FK_basket_rules_basket_rule_discount_type` (`discount_type_id`),
  KEY `FK_basket_rules_basket_rule_conditions` (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains all information per baset rule and the conditions under which it operates'