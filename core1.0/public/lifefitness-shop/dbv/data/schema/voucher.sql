CREATE TABLE `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voucher_name` varchar(100) NOT NULL,
  `amount` varchar(10) DEFAULT NULL,
  `operand` varchar(1) DEFAULT NULL,
  `code` varchar(100) NOT NULL,
  `status` enum('active','inactive') DEFAULT NULL,
  `start` date DEFAULT NULL,
  `expire` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8