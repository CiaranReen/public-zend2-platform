CREATE TABLE `html_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header` varchar(4000) DEFAULT NULL,
  `footer` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8