CREATE TABLE `shipping_band` (
  `band_id` int(11) NOT NULL AUTO_INCREMENT,
  `start_weight` int(11) NOT NULL,
  `end_weight` int(11) NOT NULL,
  PRIMARY KEY (`band_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8