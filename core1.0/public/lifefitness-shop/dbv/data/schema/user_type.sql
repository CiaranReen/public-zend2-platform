CREATE TABLE `user_type` (
  `user_type_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8