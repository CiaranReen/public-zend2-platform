CREATE TABLE `shipping_companies` (
  `shipping_company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) NOT NULL,
  PRIMARY KEY (`shipping_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8