CREATE TABLE `contact_emails` (
  `contact_id` int(10) NOT NULL AUTO_INCREMENT,
  `company` enum('CE') NOT NULL DEFAULT 'CE',
  `name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8