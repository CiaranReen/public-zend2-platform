CREATE TABLE `region` (
  `region_id` int(10) NOT NULL AUTO_INCREMENT,
  `region` varchar(128) NOT NULL,
  `country_id` int(10) NOT NULL,
  PRIMARY KEY (`region_id`),
  KEY `FK_region_country` (`country_id`),
  CONSTRAINT `FK_region_country` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`),
  CONSTRAINT `region_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8