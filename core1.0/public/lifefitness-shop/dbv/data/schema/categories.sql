CREATE TABLE `categories` (
  `category_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` varchar(500) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1