CREATE TABLE `ipcountries` (
  `ipcountry_id` int(11) NOT NULL AUTO_INCREMENT,
  `lower` bigint(20) NOT NULL DEFAULT '0',
  `upper` bigint(20) NOT NULL DEFAULT '0',
  `code1` varchar(4) NOT NULL DEFAULT '',
  `code2` varchar(4) NOT NULL DEFAULT '',
  `country` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`ipcountry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1