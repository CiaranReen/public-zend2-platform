CREATE TABLE `country` (
  `country_id` int(10) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(128) DEFAULT NULL,
  `country_code_iso2` varchar(2) DEFAULT NULL,
  `country_code_iso3` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8