CREATE TABLE `basketrule_conditions` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `shorthand` varchar(50) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the different conditions for basket rules to map onto the basket_rules table (eg. greater than, less than)'