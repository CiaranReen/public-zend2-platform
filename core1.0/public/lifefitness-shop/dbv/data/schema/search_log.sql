CREATE TABLE `search_log` (
  `search_log_id` int(10) NOT NULL AUTO_INCREMENT,
  `search_term` varchar(200) DEFAULT NULL,
  `count` int(10) NOT NULL,
  `date_searched` datetime NOT NULL,
  PRIMARY KEY (`search_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8