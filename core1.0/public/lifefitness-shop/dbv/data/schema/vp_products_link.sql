CREATE TABLE `vp_products_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `vendor_product_id` (`vendor_product_id`),
  CONSTRAINT `vp_products_link_ibfk_3` FOREIGN KEY (`vendor_product_id`) REFERENCES `vp_products` (`vendor_product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vp_products_link_ibfk_4` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vp_products_link_ibfk_5` FOREIGN KEY (`vendor_product_id`) REFERENCES `vp_products` (`vendor_product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vp_products_link_ibfk_6` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8