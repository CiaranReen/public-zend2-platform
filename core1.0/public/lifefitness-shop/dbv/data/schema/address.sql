CREATE TABLE `address` (
  `address_id` int(10) NOT NULL AUTO_INCREMENT,
  `line_one` varchar(255) DEFAULT NULL,
  `line_two` varchar(255) DEFAULT NULL,
  `city` varchar(150) NOT NULL,
  `region` varchar(200) NOT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `country_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_address_country` (`country_id`),
  CONSTRAINT `address_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`countryid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_address_country_id` FOREIGN KEY (`country_id`) REFERENCES `countries` (`countryid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8