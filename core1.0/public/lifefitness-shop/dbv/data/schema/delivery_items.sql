CREATE TABLE `delivery_items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(10) NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8