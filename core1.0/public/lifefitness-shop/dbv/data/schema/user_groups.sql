CREATE TABLE `user_groups` (
  `user_group_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_group_name` varchar(100) NOT NULL,
  `business_unit` varchar(50) NOT NULL,
  `invoice` enum('Yes','No') NOT NULL,
  `invoice_limit` decimal(10,2) NOT NULL,
  `invoice_type` enum('Pro Forma','Standard') NOT NULL,
  `currency` enum('GBP','EUR') NOT NULL DEFAULT 'GBP',
  `currency_invoice` enum('GBP','EUR') NOT NULL,
  `vat` int(1) NOT NULL,
  PRIMARY KEY (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1