CREATE TABLE `vp_products` (
  `vendor_product_id` int(10) NOT NULL AUTO_INCREMENT,
  `vendor_part_no` varchar(255) NOT NULL,
  `description` varchar(512) NOT NULL,
  `manufacturer_id` int(3) NOT NULL,
  `manufacturer_part_no` varchar(255) NOT NULL,
  `cost` float(10,2) NOT NULL,
  PRIMARY KEY (`vendor_product_id`),
  KEY `manufacturer_id` (`manufacturer_id`),
  CONSTRAINT `vp_products_ibfk_1` FOREIGN KEY (`manufacturer_id`) REFERENCES `vp_manufacturers` (`manufacturer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `vp_products_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `vp_manufacturers` (`manufacturer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1