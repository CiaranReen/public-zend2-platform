CREATE TABLE `languages` (
  `language_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `iso_shortcode` varchar(5) DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8