CREATE TABLE `basketrule_products` (
  `basketrule_product_id` int(10) NOT NULL AUTO_INCREMENT,
  `basketrule_id` int(10) DEFAULT NULL,
  `product_id` int(10) NOT NULL,
  PRIMARY KEY (`basketrule_product_id`),
  KEY `FK__basket_rules` (`basketrule_id`),
  KEY `FK_basket_rule_products_products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='maps products to basket rules applicable'