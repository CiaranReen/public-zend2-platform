CREATE TABLE `shipping_band_rate` (
  `band_id` int(11) NOT NULL,
  `rate_id` int(11) NOT NULL,
  `price` float(10,2) NOT NULL,
  KEY `rate_id` (`rate_id`),
  KEY `band_id` (`band_id`),
  CONSTRAINT `shipping_band_rate_ibfk_1` FOREIGN KEY (`band_id`) REFERENCES `shipping_band` (`band_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `shipping_band_rate_ibfk_2` FOREIGN KEY (`rate_id`) REFERENCES `shipping_rate` (`rate_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8