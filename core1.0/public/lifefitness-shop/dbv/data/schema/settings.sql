CREATE TABLE `settings` (
  `setting_id` int(10) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) NOT NULL DEFAULT '',
  `site_url` varchar(100) NOT NULL,
  `order_number_prefix` varchar(50) NOT NULL DEFAULT '',
  `order_number_startpoint` int(11) NOT NULL,
  `order_number_format` varchar(100) NOT NULL DEFAULT '',
  `order_email_sender` varchar(500) DEFAULT NULL,
  `ga_username` varchar(100) DEFAULT NULL,
  `ga_password` varchar(500) DEFAULT NULL,
  `ga_profile_id` varchar(30) DEFAULT NULL,
  `vps_protocol` varchar(50) DEFAULT NULL,
  `vendor` varchar(100) DEFAULT NULL,
  `order_description` varchar(100) DEFAULT NULL,
  `currency_mode` int(11) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1