CREATE TABLE `order_notes` (
  `order_note_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `note` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`order_note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8