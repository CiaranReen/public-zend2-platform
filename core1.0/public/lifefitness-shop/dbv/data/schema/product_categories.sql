CREATE TABLE `product_categories` (
  `pc_id` int(10) NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  PRIMARY KEY (`pc_id`),
  KEY `FK_product_categories_categories` (`category_id`),
  KEY `FK_product_categories_products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contains a mapping of products to their categories'