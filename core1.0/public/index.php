<?php
$host = $_SERVER['HTTP_HOST'];
$hostarr = explode('.',$host);
$library = ucfirst($hostarr[0]);

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

if(!file_exists(realpath(dirname(__FILE__) . '/../library/'.$library)))
{
    defined('WEBSITE_PATH')
        || define('WEBSITE_PATH', realpath(dirname(__FILE__) . '/../application'));
}
else
{
    defined('WEBSITE_PATH')
        || define('WEBSITE_PATH', realpath(dirname(__FILE__) . '/../library/'.$library));
}



// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));



// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$platform = !empty( $_SERVER[ 'HTTP_HOST' ] ) ? strtolower( $_SERVER[ 'HTTP_HOST' ] ) : getenv( 'APP_PLATFORM' );

if( !$platform )
{ 
    throw new InternalErrorException( 'Application platform not configured' );
}

if(strpos($platform, '.'))
{
    $platformArr = explode('.',$platform);
    array_pop($platformArr);
    $platform = reset($platformArr);
}

define( 'APP_PLATFORM', $platform );

if(file_exists('../application/configs/'.$platform.'.ini'))
{
    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/'.$platform.'.ini'
    );
}
else
{
    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application.ini'
    );
}

/************* Memache ****************/

$frontendOpts = array(
    'caching' => true,
    'lifetime' => 1800,
    'automatic_serialization' => true
);

$backendOpts = array(
    'servers' =>array(
        array(
            'host' => 'localhost',
            'port' => 11211
        )
    ),
    'compression' => false
);

$cache = Zend_Cache::factory('Core', 'Memcached', $frontendOpts, $backendOpts);
Zend_Registry::set('Memcache', $cache);

/************ END OF MEMCACHE ***********/

$application->bootstrap()->run();
