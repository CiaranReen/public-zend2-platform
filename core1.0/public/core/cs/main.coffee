$(document).ready ->
  $(".sidebar > nav > ul > li:has(ul)").addClass "has-sub"
  $(".sidebar > nav > ul > li > a").click ->
    checkElement = $(this).next()
    $(".sidebar li").removeClass "active"
    $(this).closest("li").addClass "active"
    if (checkElement.is("ul")) and (checkElement.is(":visible"))
      $(this).closest("li").removeClass "active"
      checkElement.slideUp "normal"
    if (checkElement.is("ul")) and (not checkElement.is(":visible"))
      $(".sidebar > nav > ul ul:visible").slideUp "normal"
      checkElement.slideDown "normal"
    if checkElement.is("ul")
      false
    else
      true


#Header Notification popup
$ ->
  $(".user-notifications").hide()
  $(".dropdown-toggle").click (e) ->
    $(".user-notifications").slideToggle 400
    $("#user-settings ul li a").toggleClass "active"
    e.stopPropagation()
    return

  $(document).click ->
    if $(".user-notifications").is(":visible")
      $(this).css("opacity", 0).slideDown("slow").animate
        opacity: 1,
        queue: false
        duration: "slow"
  $("#user-settings ul li a").removeClass "active"

#Notification settings popup
$ ->
  $(".notif-settings-popup").hide()
  $(".notification-settings a").click (e) ->
    $(".notif-settings-popup").slideToggle 400
    e.stopPropagation()
    return

  $(document).click ->
    if $(".notif-settings-popup").is(":visible")
      $(this).css("opacity", 0).slideDown("slow").animate
        opacity: 1,
        queue: false
        duration: "slow"


#Panel config popup
$ ->
  $(".panel-config").hide()
  $(".panel-config-toggle").click (e) ->
    $(".panel-config").slideToggle 400
    e.stopPropagation()
    return

  $(document).click ->
    if $(".panel-config").is(":visible")
      $(this).css("opacity", 0).slideDown("slow").animate
        opacity: 1,
        queue: false
        duration: "slow"






