$(document).ready(function(){
    /******************************************************************
    * Checks if an element exists on a page
    ******************************************************************/
    $.fn.doesExist = function(){
        return jQuery(this).length > 0;
    };
    
    /******************************************************************
    * Gets all Product stock using the product id
    * @param {int} id
    * @return {array}
    ******************************************************************/
    $.getProductStock = function(id){
        var stock = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/getproductstock/",
            data: { productid: id},
            async: false,
            success: function(data) {
                stock = $.parseJSON(data);
            }
        });
        return stock;
    };
    
    /******************************************************************
    * Gets all product colourways using the product id
    * @param {int} id
    * @return {array}
    ******************************************************************/
    $.getProductColourways = function(id){
        var colourways  = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/getproductcolourways/",
            data: { productid: id},
            async: false,
            success: function(data) {
                colourways = jQuery.parseJSON(data);
            }
        });
        
        return colourways;
    };
    
    /******************************************************************
    * Gets all product images using the product id and colourway id
    * @param {int} id
    * @param {int} colourwayid
    * @return {array}
    ******************************************************************/
    $.getProductColourwayImages = function(id, colourwayid){
        var colourwayImages  = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/getcolourwayimages/",
            data: { productid: id, colourwayid: colourwayid},
            async: false,
            success: function(data) {
                colourwayImages = jQuery.parseJSON(data);
            }
        });
        
        return colourwayImages;
    };
    
    /******************************************************************
    * Gets all product stock quantity input field ids for validation or any other purpose
    * @param {int} id
    * @return {array}
    ******************************************************************/
    $.getProductQtyInputs = function(id){
        var stockIds        = new Array();
        var qtyInputs       = new Array();
        var productStock    = $.getProductStock(id);
        $.each( productStock, function( key, stock ) {
            var stockId   = stock.id;
            stockIds[key] = stockId;
            qtyInputs[key] = 'qty_'+stockId;
        });
        return qtyInputs;
    };
    
    /******************************************************************
    * Gets all basket items
    * @return {array}
    ******************************************************************/    
    $.getBasket = function(){
        var basket  = new Array();
        $.ajax({
            type: "POST",
            url: "/Ajax/getBasket/",
            async: false,
            success: function(data) {
                basket = $.parseJSON(data);
            }
        });

        return basket;
    };
    
    /******************************************************************
    * Gets all basket item stock ids
    * @return {array}
    ******************************************************************/   
    $.getBasketStockIds = function(){
        var basket      = $.getBasket();
        var stockIds    = new Array();
        var i = 0;
        $.each( basket, function( key, value ) {
            stockIds[i] = key;
            i++;
        });
        return stockIds;
    };

    /******************************************************************
    * Gets all basket quantity inputs for validation
    * @return {array}
    ******************************************************************/   
    $.getBasketQtyInputs = function(){
        var qtyInputs   = new Array();
        var basket      = $.getBasket();
        $.each( basket, function( key, value ) {
            var stockId = key;
            qtyInputs[key] = 'qty_'+stockId;
        });
        return qtyInputs;
    };
    
    /******************************************************************
    * Updates a basket item quantity using the stockid and new quantity entered
    * @param {int} stockId
    * @param {int} newQty
    * @return {bool}
    ******************************************************************/   
    $.updateItemQty = function(stockId, newQty){
        $.ajax({
            type: "POST",
            data: {id: stockId, newQty: newQty},
            url: "/Ajax/updatebasketitem/",
            async: false,
            success: function() {}
        });
        return true;
    };
               
        
});