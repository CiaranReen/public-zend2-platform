// Media Grid global vars
mg_boxMargin = 0;
mg_boxBorder = 0;
mg_imgPadding = 0;
mg_lightbox_mode = "mg_classic_lb";
mg_lb_vert_center = 1;
mg_lb_touchswipe = false;

// Galleria global vars
mg_galleria_fx = 'fadeslide';
mg_galleria_fx_time = 400;
mg_galleria_interval = 3000;

// DOM ready
jQuery(document).ready(function () {

// Create the dropdown base
    jQuery("<select />").appendTo("#topmenu");

// Create default option "Go to..."
    jQuery("<option />", {
        "selected": "selected",
        "value": "",
        "text": "Menu Selection..."
    }).appendTo("#topmenu select");

// Populate dropdown with menu items
    jQuery("#topmenu a").each(function () {
        var el = jQuery(this);
        jQuery("<option />", {
            "value": el.attr("href"),
            "text": el.text()
        }).appendTo("#topmenu select");
    });

// To make dropdown actually work
// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
    jQuery("#topmenu select").change(function () {
        window.location = jQuery(this).find("option:selected").val();
    });

});


// THIS IS THE SLIDER
//jQuery(window).load(function () {
//    jQuery('.slider').flexslider();
//});



// TWITTER FEED
//jQuery(document).ready(function ($) {
//    jQuery('#dc_jqverticalmegamenu_widget-3-item .menu').dcVerticalMegaMenu({
//        rowItems: 1,
//        speed: 'normal',
//        direction: 'right',
//        effect: 'slide'
//    });
//});
//
//jQuery(document).ready(function ($) {
//    jQuery('#dc_jqverticalmegamenu_widget-4-item .menu').dcVerticalMegaMenu({
//        rowItems: 1,
//        speed: 'normal',
//        direction: 'right',
//        effect: 'slide'
//    });
//});
//jQuery(document).ready(function ($) {
//    jQuery('#dc_jqverticalmegamenu_widget-5-item .menu').dcVerticalMegaMenu({
//        rowItems: 1,
//        speed: 'normal',
//        direction: 'right',
//        effect: 'slide'
//    });
//});