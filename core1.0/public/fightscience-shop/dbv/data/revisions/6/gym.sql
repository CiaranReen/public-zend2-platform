CREATE TABLE IF NOT EXISTS `gym` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5;

INSERT INTO `gym` (`id`, `name`) VALUES
(1, 'Leeds'),
(2, 'Staines'),
(3, 'Aldershot'),
(4, 'Guildford');