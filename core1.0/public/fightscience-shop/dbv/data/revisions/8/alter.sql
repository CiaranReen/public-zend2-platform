ALTER TABLE `social_media` ADD `title` VARCHAR(255)  NOT NULL  AFTER `link`;
ALTER TABLE `social_media` ADD `image_name_grey` VARCHAR(55)  NOT NULL  AFTER `title`;
ALTER TABLE `social_media` ADD `image_name_color` VARCHAR(55)  NOT NULL  AFTER `image_name_grey`;
