CREATE TABLE `product_packs_users` (
  `pp_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `pp_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`pp_user_id`),
  KEY `fk_product_packs_users_ibfk_2` (`pp_id`),
  KEY `fk_product_packs_users_ibfk_1` (`user_id`),
  CONSTRAINT `fk_product_packs_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_packs_users_ibfk_2` FOREIGN KEY (`pp_id`) REFERENCES `product_packs` (`pp_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_packs_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_packs_users_ibfk_2` FOREIGN KEY (`pp_id`) REFERENCES `product_packs` (`pp_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8