CREATE TABLE `currency` (
  `currency_id` int(10) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `currency` text NOT NULL,
  `symbol` varchar(20) NOT NULL,
  `static_value` float(10,4) NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1