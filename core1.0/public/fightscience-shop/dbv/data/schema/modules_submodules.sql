CREATE TABLE `modules_submodules` (
  `module_id` int(11) NOT NULL,
  `submodule_id` int(11) NOT NULL,
  PRIMARY KEY (`module_id`,`submodule_id`),
  KEY `FK__submodules` (`submodule_id`),
  CONSTRAINT `FK__modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK__submodules` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`submodule_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_submodules_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_submodules_ibfk_2` FOREIGN KEY (`submodule_id`) REFERENCES `submodules` (`submodule_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8