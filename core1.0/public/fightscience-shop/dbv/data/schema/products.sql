CREATE TABLE `products` (
  `product_id` int(10) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text,
  `code` varchar(255) DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  `pack` int(10) DEFAULT NULL,
  `live` enum('Y','N') NOT NULL,
  `stock_action` enum('show','hide') NOT NULL,
  `pack_size_width` float(10,2) NOT NULL DEFAULT '0.00',
  `pack_size_length` float(10,2) NOT NULL DEFAULT '0.00',
  `pack_size_depth` float(10,2) NOT NULL DEFAULT '0.00',
  `highest` int(1) NOT NULL DEFAULT '0',
  `featured` int(1) NOT NULL DEFAULT '0',
  `new` int(1) NOT NULL DEFAULT '0',
  `dualbrand` int(1) NOT NULL DEFAULT '0',
  `added` datetime NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1