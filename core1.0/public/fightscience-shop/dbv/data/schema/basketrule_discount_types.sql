CREATE TABLE `basketrule_discount_types` (
  `discount_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `shorthand` varchar(50) NOT NULL,
  PRIMARY KEY (`discount_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contains the types of discount (eg percentage - for 25% & currency for £25)'