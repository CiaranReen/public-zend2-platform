CREATE TABLE `poll_products_images` (
  `poll_id` int(3) NOT NULL AUTO_INCREMENT,
  `image1_url` varchar(30) NOT NULL,
  `image2_url` varchar(30) NOT NULL,
  `image3_url` varchar(30) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1