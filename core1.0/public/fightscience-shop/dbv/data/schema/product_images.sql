CREATE TABLE `product_images` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `colourway_id` int(10) NOT NULL,
  `image_url` varchar(300) NOT NULL,
  `order` int(10) NOT NULL,
  `is_default` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `FK_product_colourway_images_colourways` (`colourway_id`),
  KEY `FK_product_colourway_images_products` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='contains all images for the colourways'