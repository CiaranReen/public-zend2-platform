CREATE TABLE `poll_products` (
  `poll_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_1` varchar(30) NOT NULL,
  `product_2` varchar(30) NOT NULL,
  `product_3` varchar(30) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8