CREATE TABLE `product_options` (
  `option_id` int(10) NOT NULL AUTO_INCREMENT,
  `option` varchar(50) NOT NULL,
  `shortcode` varchar(50) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1