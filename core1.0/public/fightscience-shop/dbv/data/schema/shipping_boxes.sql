CREATE TABLE `shipping_boxes` (
  `shipping_box_id` int(11) NOT NULL AUTO_INCREMENT,
  `box_width` float(10,2) DEFAULT '0.00',
  `box_length` float(10,2) DEFAULT '0.00',
  `box_depth` float(10,2) DEFAULT '0.00',
  PRIMARY KEY (`shipping_box_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8