CREATE TABLE `orders` (
  `order_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_number` varchar(100) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `billing_address_id` int(11) NOT NULL,
  `delivery_address_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `items_total` float NOT NULL,
  `delivery_charge` float NOT NULL,
  `vat_charge` float NOT NULL,
  `discount` float DEFAULT NULL,
  `order_total` float NOT NULL,
  `date_ordered` datetime NOT NULL,
  `date_processed` datetime DEFAULT NULL,
  `status` enum('open','awaiting','processed','dispatched') DEFAULT NULL,
  `amount_outstanding` float DEFAULT '0',
  `payment_type` enum('card','invoice') DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1