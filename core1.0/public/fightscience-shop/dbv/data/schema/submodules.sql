CREATE TABLE `submodules` (
  `submodule_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `submodule_path_name` varchar(100) NOT NULL,
  PRIMARY KEY (`submodule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8