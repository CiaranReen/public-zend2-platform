CREATE TABLE `basketrule_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `condition_id` int(10) DEFAULT NULL,
  `quantity` int(10) NOT NULL,
  `discount` float(10,2) NOT NULL,
  `discount_type_id` int(10) NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `FK_basket_rules_basket_rule_discount_type` (`discount_type_id`),
  KEY `FK_basket_rules_basket_rule_conditions` (`condition_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT COMMENT='Contains default basket rule settings'