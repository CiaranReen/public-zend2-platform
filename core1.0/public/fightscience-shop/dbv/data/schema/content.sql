CREATE TABLE `content` (
  `content_id` int(10) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(100) DEFAULT NULL,
  `gb` text,
  `fr` text,
  `es` text,
  `de` text,
  `nl` text,
  `it` text,
  `zh-cn` text,
  `zh-tw` text,
  `pt` text,
  `ru` text,
  `cz` text,
  `dk` text,
  `el` text,
  `hu` text,
  `no` text,
  `pl` text,
  `se` text,
  `tr` text,
  `rs` text,
  `ja` text,
  `ko` text,
  `th` text,
  `bn` text,
  `hi` text,
  `pa` text,
  `gu` text,
  `ar` text,
  `fa` text,
  `he` text,
  `id` text,
  `sq` text,
  `bg` text,
  `hr` text,
  `et` text,
  `fi` text,
  `is` text,
  `lv` text,
  `lt` text,
  `mk` text,
  `ro` text,
  `sk` text,
  `sl` text,
  `uk` text,
  `cy` text,
  `my` text,
  `jv` text,
  `ms` text,
  `vi` text,
  `ur` text,
  `af` text,
  `sw` text,
  PRIMARY KEY (`content_id`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8