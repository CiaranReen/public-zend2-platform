CREATE TABLE `order_parts` (
  `order_part_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `discount` float DEFAULT NULL,
  PRIMARY KEY (`order_part_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1