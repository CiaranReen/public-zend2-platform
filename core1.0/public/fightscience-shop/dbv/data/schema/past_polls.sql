CREATE TABLE `past_polls` (
  `poll_id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `number_of_submissions` int(6) NOT NULL,
  `start` varchar(10) NOT NULL,
  `end` varchar(10) NOT NULL,
  `product1` varchar(30) NOT NULL,
  `product2` varchar(30) NOT NULL,
  `product3` varchar(30) NOT NULL,
  `graph_url` varchar(25) NOT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1