CREATE TABLE `product_availability` (
  `pu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `user_group_id` int(10) NOT NULL,
  PRIMARY KEY (`pu_id`),
  KEY `FK_product_availability_products` (`product_id`),
  KEY `FK_product_availability_user_groups` (`user_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1