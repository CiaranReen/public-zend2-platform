CREATE TABLE `countries` (
  `countryid` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) NOT NULL DEFAULT '',
  `top` int(11) NOT NULL DEFAULT '0',
  `world` varchar(5) NOT NULL DEFAULT '',
  `currency` enum('gbp','eur','usd') NOT NULL DEFAULT 'gbp',
  `language` enum('en','nl','fr','es','it','de') NOT NULL DEFAULT 'en',
  `iso` char(3) NOT NULL DEFAULT '',
  `iso3` char(3) NOT NULL DEFAULT '',
  `ipcode` char(2) NOT NULL DEFAULT '',
  `ec` enum('yes','no') NOT NULL DEFAULT 'yes',
  `bu` varchar(255) NOT NULL,
  `paypal` enum('yes','no') NOT NULL,
  PRIMARY KEY (`countryid`),
  KEY `country` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1