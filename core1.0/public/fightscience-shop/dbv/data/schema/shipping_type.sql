CREATE TABLE `shipping_type` (
  `shipping_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `minimum_weight` float(10,2) NOT NULL,
  `weight_limit` float(10,2) NOT NULL,
  `length_limit` float(10,2) NOT NULL,
  `width_limit` float(10,2) NOT NULL,
  `depth_limit` float(10,2) NOT NULL,
  PRIMARY KEY (`shipping_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8