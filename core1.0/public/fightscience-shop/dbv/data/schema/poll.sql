CREATE TABLE `poll` (
  `poll_id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `start` varchar(10) DEFAULT NULL,
  `end` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`poll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1