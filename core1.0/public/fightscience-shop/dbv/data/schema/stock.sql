CREATE TABLE `stock` (
  `stock_id` int(10) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) NOT NULL,
  `code` varchar(50) NOT NULL,
  `option` varchar(50) NOT NULL,
  `colourway_id` int(10) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `stock_level` int(10) DEFAULT NULL,
  `warning_level` int(10) DEFAULT NULL,
  PRIMARY KEY (`stock_id`),
  KEY `FK__products` (`product_id`),
  KEY `FK_stock_colourways` (`colourway_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1