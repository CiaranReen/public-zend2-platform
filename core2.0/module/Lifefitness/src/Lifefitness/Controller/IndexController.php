<?php
namespace Lifefitness\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Core\Entity\User;

class IndexController extends AbstractActionController
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function setEntityManager (EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function indexAction()
    {

        $categoryRepository = $this->getEntityManager()->getRepository('Core\Entity\Category');
        $categoryRepository->fetchAll();

        $this->flashMessenger()->addMessage('You are now logged in.');

        $history = $categoryRepository->findAll();
        return array(
            'parentCategories' => $parentCategories
        );
    }
}