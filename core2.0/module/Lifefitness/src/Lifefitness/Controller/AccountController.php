<?php
namespace Lifefitness\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;
use Core\Entity\User;

class AccountController extends AbstractActionController
{
    /**
     * @var Doctrine\ORM\EntityManager
     */
    protected $em;

    public function setEntityManager (EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    public function getEntityManager()
    {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        }
        return $this->em;
    }

    public function indexAction()
    {

        $repository = $this->getEntityManager()->getRepository('Core\Entity\History');
        $repository->fetchLatestTen();

        $this->flashMessenger()->addMessage('You are now logged in.');

        $users = $repository->findAll();
//        $address = $this->getEntityManager()->find('Core\Entity\User', 1);
//        $products = $address->getVouchers();
//        foreach ($products as $p) {
//            echo $p->getName();
//        }
//       //var_dump($address->getVouchers()); die();
        return array(
            'users' => $users,
        );
    }
}