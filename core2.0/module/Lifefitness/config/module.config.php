<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Lifefitness\Controller\Index' => 'Lifefitness\Controller\IndexController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'lifefitness' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/[/:controller][/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Lifefitness\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            'account' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/account',
                    'defaults' => array(
                        'controller' => 'Lifefitness\Controller\AccountController',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_map' => array(
            'lifefitness/index/index'   => __DIR__ . '/../view/lifefitness/index/index.phtml',
            'lifefitness/account/index' => __DIR__ . '/../view/lifefitness/account/index.phtml',
            'error/404'                 => __DIR__ . '/../view/error/404.phtml',
            'error/index'               => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);