<?php
namespace LifefitnessTest\Controller;

use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Doctrine\ORM\EntityManager;
use PHPUnit_Framework_TestCase;

use LifefitnessTest\Bootstrap;
use Lifefitness\Controller\IndexController;
use Core\Entity\User;

class IndexControllerTest extends PHPUnit_Framework_TestCase
{
    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new IndexController();
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event      = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);
        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }

    public function testIndexPageCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');

        $result   = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUserCanBeRetrievedFromDatabaseUsingId()
    {
        $entityManager  = $this->controller->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $entityManager->find('Core\Entity\User', 1);
        if (!$user) {
            //$this->fail('not instantialted');
            $newUser = new User();
            $newUser->setId(1);
            $newUser->setForename('Test');
            $newUser->setSurname('Test');
            $newUser->setUsername('Test');
            $newUser->setPassword('Test');
            $entityManager->persist($newUser);
            $entityManager->flush();
            $user = $entityManager->find('Core\Entity\User', 1);
        }
        $this->assertEquals(1, $user->getId());
    }

    public function testCountryObjectCanBeAccessedFromAddressObject()
    {
        $entityManager  = $this->controller->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $address = $entityManager->find('Core\Entity\Address', 1);
        $this->assertInstanceOf('Core\Entity\Country', $address->getCountry());
    }
}