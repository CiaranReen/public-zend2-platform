jQuery(document).ready(function ($) {
    /********************************************************************************** 
     * GENERAL SCRIPT - FOUND ON MOST PAGES & NOT ON ANY ONE PARTICULAR PAGE
     **********************************************************************************/
    $('.more').hide();
    $('#dropdown-languages').hide();
    console.log('here');
    $('.basket-dropdown').hide();
    $('.account-dropdown').hide();
    var toggle = 0;


    /**********************************************************************************
     *Account tabs
     **********************************************************************************/
    $(function(){
      $(".tabbable").tabs();
     });


    /******************************************************************
     * For View More dropdown in header basket 
     ******************************************************************/
//    $('.langFlag.active').hover(function() {
//        if ($(this).hasClass('active')) {
//            $("#dropdown-languages").slideToggle("fast", function() {
//            });
//        } else {
//            $("#dropdown-languages").slideToggle("slow", function() {
//            });
//        }
//
//        return false;
//    });


    /******************************************************************
     * For View More dropdown in header basket 
     ******************************************************************/
    $('.view-more').click(function() {
        if (toggle === 0) {
            toggle = 1;
            $('.view-more').html('View Less &#9650;');
        } else {
            toggle = 0;
            $('.view-more').html('View More &#9660;');
        }
        $(".more").slideToggle("slow", function() {
        });
        return false;
    });

    /******************************************************************
     * Flags dropdown script
     ******************************************************************/
    /* <![CDATA[ */
    var timer;
    var active;
    $(".langFlag.active").hover(function() {
        if (active == 0 || active == null) {
            $("#dropdown-languages").slideToggle('fast');
            active = 1;
            timer = setTimeout(function() {
                active = 0;
                $("#dropdown-languages").each(function() {
                    if ($(this).hasClass('active')) {
                        $(this).slideToggle('fast');
                    } else {
                        $(this).slideToggle('slow');
                    }
                });
            }, 2000);
        }
    });
    $(".languages.langFlag li:not(.active)").hover(function() {
        clearTimeout(timer);
    }, function() {
        timer = setTimeout(function() {
            active = 0;
            $(".langFlag.active").each(function() {
                if ($(this).hasClass('active')) {
                    $(this).slideToggle('fast');
                } else {
                    $(this).slideToggle('slow');
                }
            });
        }, 200);
    });


    $('.langFlag.inactive').on('click', function() {
        $.ajax({
            url: $(this).attr('href'),
            success: function(result) {
                window.location.reload();
            }
        });
    });

    $('#basket-dropdown-icon').click(function() {
        $(".basket-dropdown").slideToggle("slow", function() {
        });
        $(".account-dropdown").hide();
        return false;
    });
    $('.account-dropdown-toggle').click(function() {
        $(".account-dropdown").slideToggle("slow", function() {
        });
        $(".basket-dropdown").hide();
        return false;
    });

    /********************************************************************************** 
     * END OF GENERAL SCRIPT
     **********************************************************************************/

    /********************************************************************************** 
     * CATEGORY PAGE SCRIPT
     **********************************************************************************/
    if ($('.colourway-product-select').doesExist()) {
        /******************************************************************
         * For Category Page Colourways Rollover
         ******************************************************************/
        $('.colourway-product-select').hover(function() {
            var productId = $(this).attr('product-id');
            var colourwayIdString = $(this).attr('id').split('-');
            var colourwayId = colourwayIdString[2];
            var productUrl = "/product/index/id/" + productId + "/col/" + colourwayId;
            var colourwayImages = $.getProductColourwayImages(productId, colourwayId);
            var colourwayImage = '/lifefitness-shop/img/products/' + colourwayImages[0].image_url;
            $(this).parent().parent().find('.product-image').html('<img src="' + colourwayImage + '" />');
            $(this).closest('li').find('a').attr('href', productUrl);
            return false;
        });
    }
    /********************************************************************************** 
     * END OF CATEGORY PAGE SCRIPT
     **********************************************************************************/

    /********************************************************************************** 
     * PRODUCT PAGE SCRIPT - ACTIVATED WHEN ADDTOBASKET BUTTON EXISTS 
     **********************************************************************************/
    if ($('#add-to-basket').doesExist() || $('#add-pack-to-basket').doesExist()) {
        /******************************************************************
         * Shows/hides added product popup if a product has been added to basket
         ******************************************************************/
        if ($("#added-product-popup").hasClass('show')) {
            $("#added-product-popup").show();
            $("#added-product-popup").effect("bounce", 1000);
            $("#added-product-popup").fadeOut(2000);
        }
        /******************************************************************
         * Add to basket tooltip when quantity has not been
         ******************************************************************/
        $('#add-to-basket').attr({title: 'Please enter a quantity'}).tooltip();


        /******************************************************************
         * Get all quantity inputs for validation purposes
         ******************************************************************/
        var qtyInputs = new Array();
        var productId = $('#product-id').val();
        qtyInputs = $.getProductQtyInputs(productId);
        $.each(qtyInputs, function(key, value) {
            $('#' + value).keyup(function() {
                var isEmpty = true;
                var qtyEntered = $(this).val();
                if (qtyEntered != '' && parseInt(qtyEntered) && qtyEntered > 0) {
                    isEmpty = false;
                }
                if (isEmpty == false) {
                    $('#add-to-basket').attr({title: ''});
                    $('#add-to-basket').tooltip('disable');
                } else {
                    $('#add-to-basket').tooltip('enable');
                    $('#add-to-basket').attr({title: 'Please enter a quantity'}).tooltip();
                }
            });
        });
        
        /******************************************************************
         * Generates basket rule discount for the product
         ******************************************************************/
        $('.basketrule-display').hide();
        $('.qty-input').keyup(function() {
            var totalValue = 0;
            $.each(qtyInputs, function(key, qtyInput) {
                totalValue += parseInt($('#' + qtyInput).val());
            });
            $.each(qtyInputs, function(key, qtyInput) {
                var quantity = parseInt($('#' + qtyInput).val());
                var id = $('#' + qtyInput).attr('id');
                var idParts = id.split('_');
                var stockid = idParts[1];
                var ajaxResponse = $.ajax({
                    async: false,
                    type: "POST",
                    data: {stockid: stockid, quantity: quantity},
                    url: "/ajax/get-basketrule-discount-details"
                });
                var discountArray = JSON.parse(ajaxResponse.responseText);
                if(typeof discountArray['stock_discount'] !== 'undefined'){
                    $('.basketrule-display').show();
                    $('#total-cost').text('Total cost: £' + discountArray['total_cost']);
                    $('#total-saving').text('You save: £' + discountArray['total_saving']);
                    $('#cost-per-item').val(discountArray['unit_cost'])
                }
                else{
                    $('.basketrule-display').hide();
                }
            });
            
        });
            
        /******************************************************************
         * Function when add to basket button is clicked. Checks if an quantity input has been entered before allowing the product to be added to basket
         ******************************************************************/

        $('#add-to-basket, #add-pack-to-basket').click(function() {
            var proceed = false;
            $.each(qtyInputs, function(key, qtyInput) {
                var total = 0;
                total += $('#' + qtyInput).val();
                if (parseInt(total) && total > 0) {
                    proceed = true;
                }
            });
            return proceed;
        });


        /******************************************************************
         * Gets all product colourways if there is any and swaps per colourway clicked
         ******************************************************************/
        var productColourways = $.getProductColourways(productId);

        if (productColourways) {
            $.each(productColourways, function(key, colourway) {
                if ($('.colourway-row-' + colourway.id).hasClass('default')) {
                    $('.colourway-row-' + colourway.id).show(); //show quantity input rows
                    $('.colourway-thumbnail-' + colourway.id).show(); //show colourway thumbnails
                    var hex = $('#colourway-select-' + colourway.id).attr('hex').substring(1);
                    var tickClass = (parseInt(hex, 16) > 0xffffff / 2) ? 'black-tick' : 'white-tick';
                    $('#colourway-select-' + colourway.id).addClass(tickClass);
                }
                else {
                    $('.colourway-row-' + colourway.id).hide(); //hide quantity input rows
                    $('.colourway-thumbnail-' + colourway.id).hide(); //hide colourway thumbnails
                }
            });

            $('.colourway-select').on('click', function() {
                $.each(productColourways, function(key, colourway) {
                    $('.colourway-row-' + colourway.id).hide();
                    $('.colourway-thumbnail-' + colourway.id).hide(); //hide colourway thumbnails
                    $('#colourway-select-' + colourway.id).removeClass('black-tick white-tick');
                });
                var colourwayIdString = $(this).attr('id').split('-');
                var colourwayId = colourwayIdString[2];
                $('.colourway-row-' + colourwayId).show(); //show colourway quantity input boxes
                $('.colourway-thumbnail-' + colourwayId).show(); //show colourway thumbnails
                //add colour selected tick
                var hex = $('#colourway-select-' + colourwayId).attr('hex').substring(1);
                var tickClass = (parseInt(hex, 16) > 0xffffff / 2) ? 'black-tick' : 'white-tick';
                $('#colourway-select-' + colourwayId).addClass(tickClass);
                var colourwayImages = $.getProductColourwayImages(productId, colourwayId);
                if (colourwayImages) {
                    var mainProductImage = '/lifefitness-shop/img/products/' + colourwayImages[0].image_url;
                    $('#prod-image-wrapper').html('<img src="' + mainProductImage + '"/>');
                }

            });

            /******************************************************************
             * Swaps main product image with thumbnail image that has been clicked on
             ******************************************************************/
            $('.colourway-thumbnail-select').click(function() {
                $('#prod-image-wrapper').html($(this).html());
            });
        }
    }
    /********************************************************************************** 
     * END OF PRODUCT PAGE SCRIPT 
     **********************************************************************************/


    /********************************************************************************** 
     * BASKET PAGE SCRIPT - ACTIVATED ON BASKET PAGE WHEN GOTOCHECKOUT BUTTON EXISTS 
     **********************************************************************************/
    if ($('#goToCheckout').doesExist()) {
        $('.update').hide();
        var basket = $.getBasket();
        var qtyInputs = new Array();
        qtyInputs = $.getBasketQtyInputs();


        /******************************************************************
         * Shows update link when quanity is changed per input box
         ******************************************************************/
        $.each(qtyInputs, function(key, qtyInput) {
            var isEmpty = true;
            $('#' + qtyInput).keyup(function() {
                $('.update').hide();
                $(this).parent().find('.update').show();
                var qtyEntered = $(this).val();
                if (parseInt(qtyEntered) && qtyEntered > 0) {
                    isEmpty = false;
                }
                if (isEmpty == false) {
                    //alert('working');
                }
            });
        });

        /******************************************************************
         * Updates product quantity in basket
         ******************************************************************/
        $('.update').click(function() {
            var newQty = $(this).parent().find('.qty').val();
            var stockId = $(this).attr('stock-id');
            $.updateItemQty(stockId, newQty);
            location.reload();
            return false;
        });
    }
    /********************************************************************************** 
     * END OF BASKET PAGE SCRIPT 
     **********************************************************************************/



    /********************************************************************************** 
     * ACCOUNT PAGE SCRIPT - ACTIVATED ON ACCOUNT PAGE WHEN ORDEREXPAND BUTTON EXISTS 
     **********************************************************************************/
    if ($('.order-expand').doesExist()) {
        $('.order-hidden').hide();
        $('.order-expand').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var buttonId = $(this).attr('id');
            var parts = buttonId.split('-');
            var orderId = parts[2];
            if ($(this).hasClass('show')) {
                $('.order-part-' + orderId).find('.slide-me').slideUp('slow');
                $(this).html('<i class="fa fa-plus-circle" ></i>');
                $(this).attr('alt', 'Show Order Details');
                $(this).attr('title', 'Show Order Details');
                $(this).removeClass('show');
            }
            else {
                $('html, body').animate({scrollTop: $("#" + orderId).offset().top}, 1000);
                $('.order-part-' + orderId).find('.slide-me').hide();
                $('.order-part-' + orderId).show();
                $('.order-part-' + orderId).find('.slide-me').slideDown('slow');
                $(this).html('<i class="fa fa-minus-circle" ></i>');
                $(this).attr('alt', 'Hide Order Details');
                $(this).attr('title', 'Hide Order Details');
                $(this).addClass('show');
            }
        });
    }
    if ($('#reset-password-btn').doesExist()) {
        $('#reset-password-btn').click(function() {
            if ($('#current_password').val() === '') {
                $('#current_password').attr({title: 'Please enter your current password', 'data-toggle': "popover"}).popover('show').focus();
                return false;
            }
            if ($('#new_password').val() === '') {
                $('#new_password').attr({title: 'Please enter your new password', 'data-toggle': "popover"}).popover('show').focus();
                return false;
            }
            if ($('#new_password').val() === $('#current_password').val()) {
                $('#new_password, #current_password').attr({title: 'Current and new password should not be the same', 'data-toggle': "popover", 'data-original-title': "Current and new password should not be the same"}).popover('show').focus();
                return false;
            }
            if ($('#confirm_password').val() === '') {
                $('#confirm_password').attr({title: 'Please confirm your new password', 'data-toggle': "popover"}).popover('show').focus();
                return false;
            }
            if ($('#new_password').val() !== $('#confirm_password').val()) {
                $('#new_password, #confirm_password').attr({title: 'New and confirm password should match', 'data-toggle': "popover", 'data-original-title': "New and confirm password should match"}).popover('show').focus();
                return false;
            }
        });
    }
    /********************************************************************************** 
     * END OF ACCOUNT PAGE SCRIPT 
     **********************************************************************************/

        // Icon Click Focus
    $('div.icon').click(function(){
        $('input#search').focus();
    });

    // Live Search
    // On Search Submit and Get Results
    function search() {
        var query_value = $('input#search').val();
        $('b#search-string').html(query_value);
        if(query_value !== ''){
            $.ajax({
                type: "POST",
                url: "ajax/vendorproducts",
                data: { query: query_value },
                cache: false,
                success: function(html) {
                    $("ul#results").html(html);
                }
            });
        }return false;
    }

    $("input#search").live("keyup", function(e) {
        // Set Timeout
        clearTimeout($.data(this, 'timer'));

        // Set Search String
        var search_string = $(this).val();

        // Do Search
        if (search_string == '') {
            $("ul#results").fadeOut();
            $('h4#results-text').fadeOut();
        }else{
            $("ul#results").fadeIn();
            $('h4#results-text').fadeIn();
            $(this).data('timer', setTimeout(search, 100));
        };
    });
});