$(document).ready(function() {

    /*$('.editableTableField').editable('http://www.example.com/save.php', { 
     type      : 'textarea',
     cancel    : 'Cancel',
     submit    : 'OK',
     indicator : '<img src="img/indicator.gif">',
     tooltip   : 'Click to edit...'
     });*/


    //Table Highlighting function
    //add class "highlight" when hover over the row  
    $('table tbody tr').hover(function() {
        $(this).addClass('highlight');
    }, function() {
        $(this).removeClass('highlight');
    });

    tableHighlightRow();

    $('.addShippingOption').on('click', function() {
        $('.addOptionForm').css('display', 'block');
    });

    $('.addShippingOptionSubmit').on('click', function() {
        $.ajax({
            url: '/admin/shipping/addoption',
            data: {newOption: $('.newShippingOption').val()},
            success: function(result) {
                window.location.reload();
            }
        });
    });

    $('.deleteShippingOption').on('click', function() {
        var classes = $(this).attr('class');
        var deleteOptionId = classes.replace('deleteShippingOption shippingOption', '');

        $.ajax({
            url: '/admin/shipping/deleteoption',
            data: {deleteOption: deleteOptionId},
            success: function(result) {
                window.location.reload();
            }
        });
    });

    $('.addShippingType').on('click', function() {
        $('.addTypeForm').css('display', 'block');
    });

    $('.addShippingTypeSubmit').on('click', function() {
        $.ajax({
            url: '/admin/shipping/addtype',
            data: {
                name: $('.newShippingTypeName').val(),
                description: $('.newShippingTypeDesc').val(),
                minweight: $('.newShippingTypeMinWeight').val(),
                weightlimit: $('.newShippingTypeWeightLimit').val(),
                lengthlimit: $('.newShippingTypeLengthLimit').val(),
                widthlimit: $('.newShippingTypeWidthLimit').val(),
                depthlimit: $('.newShippingTypeDepthLimit').val()
            },
            success: function(result) {
                window.location.reload();
            }
        });
    });

    $('.deleteShippingType').on('click', function() {
        var classes = $(this).attr('class');
        var deleteTypeId = classes.replace('deleteShippingType shippingType', '');

        $.ajax({
            url: '/admin/shipping/deletetype',
            data: {deleteType: deleteTypeId},
            success: function(result) {
                window.location.reload();
            }
        });
    });

    /** Shipping Rates **/
    $('.addShippingRate').on('click', function() {
        $('.addRateForm').css('display', 'block');
    });

    $('.addShippingRateSubmit').on('click', function() {
        $.ajax({
            url: '/admin/shipping/addrate',
            data: {
                company: $('.newShippingTypeName').val(),
                type: $('.newShippingTypeDesc').val(),
                option: $('.newShippingTypeMinWeight').val(),
                country: $('.newShippingTypeWeightLimit').val(),
                pricefirstparcel: $('.newShippingTypeLengthLimit').val(),
                priceperparcel: $('.newShippingTypeWidthLimit').val(),
                priceperconsignment: $('.newShippingTypeDepthLimit').val(),
                priceperpallet: $('.newShippingTypeDepthLimit').val(),
                transittime: $('.newShippingTypeDepthLimit').val(),
                invoice: $('.newShippingTypeDepthLimit').val(),
                notes: $('.newShippingTypeDepthLimit').val()
            },
            success: function(result) {
                window.location.reload();
            }
        });
    });

    $('.deleteShippingRate').on('click', function() {
        var classes = $(this).attr('class');
        var deleteTypeId = classes.replace('deleteShippingType shippingType', '');

        $.ajax({
            url: '/admin/shipping/deletetype',
            data: {deleteType: deleteTypeId},
            success: function(result) {
                window.location.reload();
            }
        });
    });

    if ($('.order-expand').length > 0) {
        $('.order-hidden').hide();
        $('.order-expand').click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            var buttonId = $(this).attr('id');
            var parts = buttonId.split('-');
            var orderId = parts[2];
            if ($(this).hasClass('show')) {
                $('.order-part-' + orderId).find('.slide-me').slideUp('slow');
                $(this).html('<i class="fa fa-plus-circle" ></i>');
                $(this).attr('alt', 'Show Order Details');
                $(this).attr('title', 'Show Order Details');
                $(this).removeClass('show');
            } 
            else {
                $('html, body').animate({scrollTop: $("#" + orderId).offset().top}, 1000);
                $('.order-part-' + orderId).find('.slide-me').hide();
                $('.order-part-' + orderId).show();
                $('.order-part-' + orderId).find('.slide-me').slideDown('slow');
                $(this).html('<i class="fa fa-minus-circle" ></i>');
                $(this).attr('alt', 'Hide Order Details');
                $(this).attr('title', 'Hide Order Details');
                $(this).addClass('show');
            }
        });
    }

    if ($('.lang-name').length > 0) {
        $('.lang-name, .lang-description').each(function() {
            var idParts = $(this).attr('id').split('-');
            var languageId = idParts[1];
            if (languageId != 1) {
                $('#name-' + languageId + '-label').hide();
                $('#name-' + languageId + '-element').hide();
                $('#description-' + languageId + '-label').hide();
                $('#description-' + languageId + '-element').hide();
            }
        });

        $('#languages').change(function() {
            var chosenId = $(this).val();
            $('.lang-name, .lang-description').each(function() {
                var idParts = $(this).attr('id').split('-');
                var languageId = idParts[1];
                $('#name-' + languageId + '-label').hide();
                $('#name-' + languageId + '-element').hide();
                $('#description-' + languageId + '-label').hide();
                $('#description-' + languageId + '-element').hide();
            });
            $('#name-' + chosenId + '-label').show();
            $('#name-' + chosenId + '-element').show();
            $('#description-' + chosenId + '-label').show();
            $('#description-' + chosenId + '-element').show();
        });
    }
});

function tableHighlightRow()
{
    if (document.getElementById && document.createTextNode)
    {
        var tables = document.getElementsByTagName('table');
        for (var i = 0; i < tables.length; i++)
        {
            if (tables[i].className == 'highlight')
            {
                var trs = tables[i].getElementsByTagName('tr');
                for (var j = 0; j < trs.length; j++)
                {
                    if (trs[j].parentNode.nodeName == 'tbody')
                    {
                        trs[j].onmouseover = function()
                        {
                            $(this).addClass('highlight');
                            return false
                        }
                        trs[j].onmouseout = function()
                        {
                            $(this).removeClass('highlight');
                            return false
                        }
                    }
                }
            }
        }
    }
}

function begin()
{
    //this.append('Click somewhere else to submit');
    alert('ok');
}

$("tr").not(':first').hover(
        function() {
            $(this).css("background", "yellow");
        },
        function() {
            $(this).css("background", "");
        }
);
