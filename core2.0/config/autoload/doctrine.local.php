<?php
$appMod = APP_MOD;

$reader = new Zend\Config\Reader\Ini();
$config = $reader->fromFile(dirname(__DIR__) . '/douwe/config.ini');

return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'      => $config[APP_ENV]['db']['params']['host'],
                    'port'      => $config[APP_ENV]['db']['params']['port'],
                    'user'      => $config[APP_ENV]['db']['params']['username'],
                    'password'  => $config[APP_ENV]['db']['params']['password'],
                    'dbname'    => $config[APP_ENV]['db']['params']['dbname'],
                    'driverOptions' => array('1002' => 'SET NAMES utf8')//UTF-8 Support
                ),
            )
        ),
        'driver' => array(
            'entities' => array (
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../../vendor/core/library/Entity'),
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Core\Entity' => 'entities'
                )
            )
        )
    )
);