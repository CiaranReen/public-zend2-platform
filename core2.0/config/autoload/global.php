<?php
return array(
    'module_layouts' => array(
        ucfirst(APP_MOD)    => 'layout/' . APP_MOD . '.phtml',
        'Admin'             => 'layout/admin.phtml',
        'Application'       => 'layout/default.phtml',
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter'
            => 'Zend\Db\Adapter\AdapterServiceFactory',
        ),
    ),
    // The following are used to configure controller loader
    // Should be compatible with Zend\ServiceManager\Config.
    'controllers' => array(
        // Map of controller "name" to class
        // This should be used if you do not need to inject any dependencies
        // in your controller
        'invokables' => array(
        ),

        // Map of controller "name" to factory for creating controller instance
        // You may provide either the class name of a factory, or a PHP callback.
        'factories' => array(
        ),
    ),

    // The following are used to configure controller plugin loader
    // Should be compatible with Zend\ServiceManager\Config.
    'controller_plugins' => array(
    ),

    // The following are used to configure view helper manager
    // Should be compatible with Zend\ServiceManager\Config.
    'view_helpers' => array(
    ),

    // Configuration for the Router service
    // Can contain any router configuration, but typically will always define
    // the routes for the application. See the router documentation for details
    // on route configuration.
    'router' => array(
        'routes' => array(
        ),
    ),

    // ViewManager configuration
    'view_manager' => array(
        // Base URL path to the application
        'base_path' => '/',

        // Doctype with which to seed the Doctype helper
        'doctype' => 'HTML5', // e.g. HTML5, XHTML1

        // TemplateMapResolver configuration
        // template/path pairs
        'template_map' => array(
        ),

        // TemplatePathStack configuration
        // module/view script path pairs
        'template_path_stack' => array(
        ),

        // Additional strategies to attach
        // These should be class names or service names of View strategy classes
        // that act as ListenerAggregates. They will be attached at priority 100,
        // in the order registered.
        'strategies' => array(
            'ViewJsonStrategy', // register JSON renderer strategy
            'ViewFeedStrategy', // register Feed renderer strategy
        ),
    ),
);