<?php
defined('APP_ENV')
|| define('APP_ENV', (getenv('APP_ENV') ? getenv('APP_ENV') : 'production'));

defined('APP_MOD')
|| define('APP_MOD', (getenv('APP_MOD') ? getenv('APP_MOD') : 'application'));

$modules = array(
    'ZendDeveloperTools',
    'DoctrineModule',
    'DoctrineORMModule',
    'Application',
    'Admin',
    ucfirst(APP_MOD),
);

return array(
    'modules' => $modules,
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array(
            'config/autoload/{,*.}{global,local}.php',
            'config/' . APP_MOD . '/{,*.}{global,local}.php',
        )
    ),
);
