<?php
$reader = new Zend\Config\Reader\Ini();
$config = $reader->fromFile(dirname(__DIR__) . '/' . APP_MOD . '/config.ini');

return array(
    'db' => array(
        'driver'         => 'Pdo',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
        'dsn'      => 'mysql:dbname=' . $config[APP_ENV]['db']['params']['dbname'] . ';host=' . $config[APP_ENV]['db']['params']['host'],
        'username' => $config[APP_ENV]['db']['params']['username'],
        'password' => $config[APP_ENV]['db']['params']['password'],
    ),
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host'      => $config[APP_ENV]['db']['params']['host'],
                    'port'      => $config[APP_ENV]['db']['params']['port'],
                    'user'      => $config[APP_ENV]['db']['params']['username'],
                    'password'  => $config[APP_ENV]['db']['params']['password'],
                    'dbname'    => $config[APP_ENV]['db']['params']['dbname'],
                    'driverOptions' => array('1002' => 'SET NAMES utf8')//UTF-8 Support
                ),
            )
        )
    ),
    // ViewManager configuration
    'view_manager' => array(

        'display_not_found_reason' => true,
        'display_exceptions'       => $config[APP_ENV]['phpSettings']['display_errors'],
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'error/404'               => __DIR__ . '/../../module/' . ucfirst(APP_MOD) . '/view/error/404.phtml',
            'error/index'             => __DIR__ . '/../../module/' . ucfirst(APP_MOD) . '/view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),

    ),
);