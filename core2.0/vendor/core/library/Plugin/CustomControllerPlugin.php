<?php
namespace Core\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\Plugin\Service;

/**
 * CustomControllerPlugin Plugin Class
 */

class CustomControllerPlugin extends AbstractPlugin
{

    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        die();
        $front = Zend_Controller_Front::getInstance();
        if (!$front->getDispatcher()->isDispatchable($request)) {
            // Controller does not exist so set to core controller
            $front->setControllerDirectory('../application/core/modules/default-frontend/controllers');
        }

        $module = $request->getModuleName();
        if ($module === 'admin')
        {
            $adminLayoutPath = APPLICATION_PATH . '/core/layouts/scripts/';
            $adminLayout = 'admin';
            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayoutPath($adminLayoutPath);
            $layout->setLayout($adminLayout);
        }
    }
}