<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting Table
 *
 * @ORM\Entity
 * @ORM\Table(name="setting")
 * @property int $id
 * @property string $companyName
 * @property string $siteUrl
 * @property string $orderNumberPrefix
 * @property int $orderNumberStart
 * @property string $orderNumberFormat
 * @property string $orderEmailSender
 * @property string $gaUsername
 * @property string $gaPassword
 * @property string $gaId
 * @property string $vpsProtocol
 * @property string $vendor
 * @property string $orderDescription
 * @property int $currencyMode
 */

class Setting
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=55)
     */
    protected $companyName;

    /**
     * @ORM\Column(length=150)
     */
    protected $siteUrl;

    /**
     * @ORM\Column(length=55)
     */
    protected $orderNumberPrefix;

    /**
     * @ORM\Column(type="integer")
     */
    protected $orderNumberStart;

    /**
     * @ORM\Column(length=100)
     */
    protected $orderNumberFormat;

    /**
     * @ORM\Column(length=100)
     */
    protected $orderEmailSender;

    /**
     * @ORM\Column(length=30)
     */
    protected $gaUsername;

    /**
     * @ORM\Column(length=30)
     */
    protected $gaPassword;

    /**
     * @ORM\Column(length=20)
     */
    protected $gaId;

    /**
     * @ORM\Column(length=20)
     */
    protected $vpsProtocol;

    /**
     * @ORM\Column(length=55)
     */
    protected $vendor;

    /**
     * @ORM\Column(length=100)
     */
    protected $orderDescription;

    /**
     * @ORM\Column(type="integer")
     */
    protected $currencyMode;

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $currencyMode
     */
    public function setCurrencyMode($currencyMode)
    {
        $this->currencyMode = $currencyMode;
    }

    /**
     * @return mixed
     */
    public function getCurrencyMode()
    {
        return $this->currencyMode;
    }

    /**
     * @param mixed $gaId
     */
    public function setGaId($gaId)
    {
        $this->gaId = $gaId;
    }

    /**
     * @return mixed
     */
    public function getGaId()
    {
        return $this->gaId;
    }

    /**
     * @param mixed $gaPassword
     */
    public function setGaPassword($gaPassword)
    {
        $this->gaPassword = $gaPassword;
    }

    /**
     * @return mixed
     */
    public function getGaPassword()
    {
        return $this->gaPassword;
    }

    /**
     * @param mixed $gaUsername
     */
    public function setGaUsername($gaUsername)
    {
        $this->gaUsername = $gaUsername;
    }

    /**
     * @return mixed
     */
    public function getGaUsername()
    {
        return $this->gaUsername;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $orderDescription
     */
    public function setOrderDescription($orderDescription)
    {
        $this->orderDescription = $orderDescription;
    }

    /**
     * @return mixed
     */
    public function getOrderDescription()
    {
        return $this->orderDescription;
    }

    /**
     * @param mixed $orderEmailSender
     */
    public function setOrderEmailSender($orderEmailSender)
    {
        $this->orderEmailSender = $orderEmailSender;
    }

    /**
     * @return mixed
     */
    public function getOrderEmailSender()
    {
        return $this->orderEmailSender;
    }

    /**
     * @param mixed $orderNumberFormat
     */
    public function setOrderNumberFormat($orderNumberFormat)
    {
        $this->orderNumberFormat = $orderNumberFormat;
    }

    /**
     * @return mixed
     */
    public function getOrderNumberFormat()
    {
        return $this->orderNumberFormat;
    }

    /**
     * @param mixed $orderNumberPrefix
     */
    public function setOrderNumberPrefix($orderNumberPrefix)
    {
        $this->orderNumberPrefix = $orderNumberPrefix;
    }

    /**
     * @return mixed
     */
    public function getOrderNumberPrefix()
    {
        return $this->orderNumberPrefix;
    }

    /**
     * @param mixed $orderNumberStart
     */
    public function setOrderNumberStart($orderNumberStart)
    {
        $this->orderNumberStart = $orderNumberStart;
    }

    /**
     * @return mixed
     */
    public function getOrderNumberStart()
    {
        return $this->orderNumberStart;
    }

    /**
     * @param mixed $siteUrl
     */
    public function setSiteUrl($siteUrl)
    {
        $this->siteUrl = $siteUrl;
    }

    /**
     * @return mixed
     */
    public function getSiteUrl()
    {
        return $this->siteUrl;
    }

    /**
     * @param mixed $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return mixed
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param mixed $vpsProtocol
     */
    public function setVpsProtocol($vpsProtocol)
    {
        $this->vpsProtocol = $vpsProtocol;
    }

    /**
     * @return mixed
     */
    public function getVpsProtocol()
    {
        return $this->vpsProtocol;
    }

}