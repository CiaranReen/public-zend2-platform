<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SearchLog Table
 *
 * @ORM\Entity
 * @ORM\Table(name="searchLog")
 * @property int $id
 * @property string $searchTerm
 * @property int $count
 * @property string $dateSearched
 */

class SearchLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $searchTerm;

    /**
     * @ORM\Column(type="integer")
     */
    protected $count;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateSearched;

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $dateSearched
     */
    public function setDateSearched($dateSearched)
    {
        $this->dateSearched = $dateSearched;
    }

    /**
     * @return mixed
     */
    public function getDateSearched()
    {
        return $this->dateSearched;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $searchTerm
     */
    public function setSearchTerm($searchTerm)
    {
        $this->searchTerm = $searchTerm;
    }

    /**
     * @return mixed
     */
    public function getSearchTerm()
    {
        return $this->searchTerm;
    }

}