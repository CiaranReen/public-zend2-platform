<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductImage Table
 *
 * @ORM\Entity
 * @ORM\Table(name="productImage")
 * @property int $id
 * @property int $productId
 * @property int $colourwayId
 * @property string $imageUrl
 * @property int $order
 * @property int $isDefault
 */

class ProductImage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $productId;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $colourwayId;

    /**
     * @ORM\Column(type="string")
     */
    protected $imageUrl;

    /**
     * @ORM\Column(type="integer")
     */
    protected $order;

    /**
     * @ORM\Column(type="integer")
     */
    protected $isDefault;

    /**
     * @ORM\ManyToOne(targetEntity="Core\Entity\Product", inversedBy="productImages")
     * @ORM\JoinColumn(name="productId", referencedColumnName="id")
     */
    protected $product;

    /**
     * @param mixed $colourwayId
     */
    public function setColourwayId($colourwayId)
    {
        $this->colourwayId = $colourwayId;
    }

    /**
     * @return mixed
     */
    public function getColourwayId()
    {
        return $this->colourwayId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param mixed $isDefault
     */
    public function setIsDefault($isDefault)
    {
        $this->isDefault = $isDefault;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {
        return $this->isDefault;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }


}