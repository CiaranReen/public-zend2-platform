<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Currency Table
 *
 * @ORM\Entity
 * @ORM\Table(name="currency")
 * @property int $id
 * @property int $countryId
 * @property string $name
 * @property string $symbol
 * @property float $staticValue
 * */

class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $countryId;

    /**
     * @ORM\Column(length=30)
     */
    protected $name;

    /**
     * @ORM\Column(length=5)
     */
    protected $symbol;

    /**
     * @ORM\Column(type="float")
     */
    protected $staticValue;

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $staticValue
     */
    public function setStaticValue($staticValue)
    {
        $this->staticValue = $staticValue;
    }

    /**
     * @return mixed
     */
    public function getStaticValue()
    {
        return $this->staticValue;
    }

    /**
     * @param mixed $symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
    }

    /**
     * @return mixed
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

}