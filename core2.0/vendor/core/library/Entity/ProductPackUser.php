<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product Pack User Table
 *
 * @ORM\Entity
 * @ORM\Table(name="productPackUser")
 * @property int $id
 * @property int $productPackId
 * @property int $userId
 */

class ProductPackUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $productPackId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $productPackId
     */
    public function setProductPackId($productPackId)
    {
        $this->productPackId = $productPackId;
    }

    /**
     * @return mixed
     */
    public function getProductPackId()
    {
        return $this->productPackId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

}