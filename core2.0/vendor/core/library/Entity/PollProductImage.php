<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poll Product Image Table
 *
 * @ORM\Entity
 * @ORM\Table(name="pollProductImage")
 * @property int $id
 * @property string $image1
 * @property string $image2
 * @property string $image3
 */

class PollProductImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=30)
     */
    protected $image1;

    /**
     * @ORM\Column(length=30)
     */
    protected $image2;

    /**
     * @ORM\Column(length=30)
     */
    protected $image3;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $image1
     */
    public function setImage1($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return mixed
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * @param mixed $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return mixed
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param mixed $image3
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;
    }

    /**
     * @return mixed
     */
    public function getImage3()
    {
        return $this->image3;
    }



}