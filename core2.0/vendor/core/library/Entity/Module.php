<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module Table
 *
 * @ORM\Entity
 * @ORM\Table(name="module")
 * @property int $id
 * @property string $name
 * @property string $path
 * @property int $adminConfigurable
 * @property string $icon
 * */

class Module
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=30)
     */
    protected $name;

    /**
     * @ORM\Column(length=30)
     */
    protected $path;

    /**
     * @ORM\Column(type="integer")
     */
    protected $adminConfigurable;

    /**
     * @ORM\Column(length=10)
     */
    protected $icon;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Submodule")
     * @ORM\JoinTable(name="moduleSubmodule",
     *      joinColumns={@ORM\JoinColumn(name="moduleId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="submoduleId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $subModules;

    /**
     * @param mixed $adminConfigurable
     */
    public function setAdminConfigurable($adminConfigurable)
    {
        $this->adminConfigurable = $adminConfigurable;
    }

    /**
     * @return mixed
     */
    public function getAdminConfigurable()
    {
        return $this->adminConfigurable;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $subModules
     */
    public function setSubModules($subModules)
    {
        $this->subModules = $subModules;
    }

    /**
     * @return mixed
     */
    public function getSubModules()
    {
        return $this->subModules;
    }

    public function __construct()
    {
        $this->subModules = new Doctrine\Common\Collections\ArrayCollection();
    }
}