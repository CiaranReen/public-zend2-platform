<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserGroup Table
 *
 * @ORM\Entity
 * @ORM\Table(name="userGroup")
 * @property int $id
 * @property string $name
 * @property int $payByInvoice
 * @property float $invoiceLimit
 * @property string $invoiceType
 * @property int $currencyId
 * @property int $vatGroup
 */

class UserGroup
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $payByInvoice;

    /**
     * @ORM\Column(type="float")
     */
    protected $invoiceLimit;

    /**
     * @ORM\Column(type="string")
     */
    protected $invoiceType;

    /**
     * @ORM\Column(type="integer")
     */
    protected $currencyId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $showVat;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Category")
     * @ORM\JoinTable(name="userGroupCategory",
     *      joinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="categoryId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $categories;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Voucher")
     * @ORM\JoinTable(name="voucherUserGroup",
     *      joinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="voucherId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $vouchers;

    /**
     * @param mixed $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return mixed
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $invoiceLimit
     */
    public function setInvoiceLimit($invoiceLimit)
    {
        $this->invoiceLimit = $invoiceLimit;
    }

    /**
     * @return mixed
     */
    public function getInvoiceLimit()
    {
        return $this->invoiceLimit;
    }

    /**
     * @param mixed $invoiceType
     */
    public function setInvoiceType($invoiceType)
    {
        $this->invoiceType = $invoiceType;
    }

    /**
     * @return mixed
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $payByInvoice
     */
    public function setPayByInvoice($payByInvoice)
    {
        $this->payByInvoice = $payByInvoice;
    }

    /**
     * @return mixed
     */
    public function getPayByInvoice()
    {
        return $this->payByInvoice;
    }

    /**
     * @param mixed $showVat
     */
    public function setShowVat($showVat)
    {
        $this->showVat = $showVat;
    }

    /**
     * @return mixed
     */
    public function getShowVat()
    {
        return $this->showVat;
    }

    /**
     * @param mixed $vatGroup
     */
    public function setVatGroup($vatGroup)
    {
        $this->vatGroup = $vatGroup;
    }

    /**
     * @return mixed
     */
    public function getVatGroup()
    {
        return $this->vatGroup;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $vouchers
     */
    public function setVouchers($vouchers)
    {
        $this->vouchers = $vouchers;
    }

    /**
     * @return mixed
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }


    public function __construct()
    {
        $this->categories = new Doctrine\Common\Collections\ArrayCollection();
        $this->vouchers = new Doctrine\Common\Collections\ArrayCollection();
    }
}