<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Social Media Table
 *
 * @ORM\Entity
 * @ORM\Table(name="socialMedia")
 * @property int $id
 * @property string $name
 * @property string $link
 * @property string $title
 * @property string $imageGrey
 * @property string $imageColor
 * @property int $active
 * */

class SocialMedia
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=255)
     */
    protected $name;

    /**
     * @ORM\Column(length=255)
     */
    protected $link;

    /**
     * @ORM\Column(length=255)
     */
    protected $title;

    /**
     * @ORM\Column(length=55)
     */
    protected $imageGrey;

    /**
     * @ORM\Column(length=55)
     */
    protected $imageColor;

    /**
     * @ORM\Column(type="integer")
     */
    protected $active;

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $imageColor
     */
    public function setImageColor($imageColor)
    {
        $this->imageColor = $imageColor;
    }

    /**
     * @return mixed
     */
    public function getImageColor()
    {
        return $this->imageColor;
    }

    /**
     * @param mixed $imageGrey
     */
    public function setImageGrey($imageGrey)
    {
        $this->imageGrey = $imageGrey;
    }

    /**
     * @return mixed
     */
    public function getImageGrey()
    {
        return $this->imageGrey;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

}