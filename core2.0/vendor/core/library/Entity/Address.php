<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address Table
 *
 * @ORM\Entity(repositoryClass="Core\Repository\AddressRepository")
 * @ORM\Table(name="address")
 * @property int $id
 * @property string $lineOne
 * @property string $lineTwo
 * @property string $city
 * @property string $region
 * @property string $postcode
 * @property int $countryId
 * @property int $userId
 * @property int $active
 */

class Address
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $lineOne;

    /**
     * @ORM\Column(type="string")
     */
    protected $lineTwo;

    /**
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @ORM\Column(type="string")
     */
    protected $region;

    /**
     * @ORM\Column(type="string")
     */
    protected $postcode;

    /**
     * @ORM\Column(type="integer")
     */
    protected $countryId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $active;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Country")
     * @ORM\JoinColumn(name="countryId", referencedColumnName="id")
     */
    protected $country;

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
    }

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $lineOne
     */
    public function setLineOne($lineOne)
    {
        $this->lineOne = $lineOne;
    }

    /**
     * @return mixed
     */
    public function getLineOne()
    {
        return $this->lineOne;
    }

    /**
     * @param mixed $lineTwo
     */
    public function setLineTwo($lineTwo)
    {
        $this->lineTwo = $lineTwo;
    }

    /**
     * @return mixed
     */
    public function getLineTwo()
    {
        return $this->lineTwo;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

}