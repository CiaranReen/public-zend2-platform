<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product Table
 *
 * @ORM\Entity
 * @ORM\Table(name="product")
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $code
 * @property float $weight
 * @property int $pack
 * @property int $live
 * @property int $stockAction
 * @property int $highest
 * @property int $featured
 * @property int $new
 * @property int $dualBrand
 * @property string $dateAdded
 * @property int $active
 */

class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @ORM\Column(type="float")
     */
    protected $weight;

    /**
     * @ORM\Column(type="integer")
     */
    protected $pack;

    /**
     * @ORM\Column(type="integer")
     */
    protected $live;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stockAction;

    /**
     * @ORM\Column(type="integer")
     */
    protected $highest;

    /**
     * @ORM\Column(type="integer")
     */
    protected $featured;

    /**
     * @ORM\Column(type="integer")
     */
    protected $new;

    /**
     * @ORM\Column(type="integer")
     */
    protected $dualBrand;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateAdded;

    /**
     * @ORM\Column(type="integer")
     */
    protected $active;

    /**
     * @ORM\OneToMany(targetEntity="Core\Entity\ProductImage", mappedBy="product")
     */
    protected $productImages;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\UserGroup")
     * @ORM\JoinTable(name="productUserGroup",
     *      joinColumns={@ORM\JoinColumn(name="productId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $productUserGroups;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\VendorProduct")
     * @ORM\JoinTable(name="vendorProductProduct",
     *      joinColumns={@ORM\JoinColumn(name="productId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="vendorProductId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $vendorProducts;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\ProductOption")
     * @ORM\JoinColumn(name="productId", referencedColumnName="id")
     */
    protected $productOption;

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $dateAdded
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;
    }

    /**
     * @return mixed
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $dualBrand
     */
    public function setDualBrand($dualBrand)
    {
        $this->dualBrand = $dualBrand;
    }

    /**
     * @return mixed
     */
    public function getDualBrand()
    {
        return $this->dualBrand;
    }

    /**
     * @param mixed $featured
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * @param mixed $highest
     */
    public function setHighest($highest)
    {
        $this->highest = $highest;
    }

    /**
     * @return mixed
     */
    public function getHighest()
    {
        return $this->highest;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $live
     */
    public function setLive($live)
    {
        $this->live = $live;
    }

    /**
     * @return mixed
     */
    public function getLive()
    {
        return $this->live;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $new
     */
    public function setNew($new)
    {
        $this->new = $new;
    }

    /**
     * @return mixed
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * @param mixed $pack
     */
    public function setPack($pack)
    {
        $this->pack = $pack;
    }

    /**
     * @return mixed
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * @param mixed $stockAction
     */
    public function setStockAction($stockAction)
    {
        $this->stockAction = $stockAction;
    }

    /**
     * @return mixed
     */
    public function getStockAction()
    {
        return $this->stockAction;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $productImages
     */
    public function setProductImages($productImages)
    {
        $this->productImages = $productImages;
    }

    /**
     * @return mixed
     */
    public function getProductImages()
    {
        return $this->productImages;
    }

    /**
     * @param mixed $productOption
     */
    public function setProductOption($productOption)
    {
        $this->productOption = $productOption;
    }

    /**
     * @return mixed
     */
    public function getProductOption()
    {
        return $this->productOption;
    }

    /**
     * @param mixed $productUserGroups
     */
    public function setProductUserGroups($productUserGroups)
    {
        $this->productUserGroups = $productUserGroups;
    }

    /**
     * @return mixed
     */
    public function getProductUserGroups()
    {
        return $this->productUserGroups;
    }

    /**
     * @param mixed $vendorProducts
     */
    public function setVendorProducts($vendorProducts)
    {
        $this->vendorProducts = $vendorProducts;
    }

    /**
     * @return mixed
     */
    public function getVendorProducts()
    {
        return $this->vendorProducts;
    }

    public function __construct()
    {
        $this->productImages = new Doctrine\Common\Collections\ArrayCollection();
        $this->productUserGroups = new Doctrine\Common\Collections\ArrayCollection();
    }


}