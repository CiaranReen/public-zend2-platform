<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BasketRuleTemplate Table
 *
 * @ORM\Entity
 * @ORM\Table(name="basketRuleTemplate")
 * @property int $id
 * @property string $name
 * @property int $basketRuleConditionId
 * @property int $quantity
 * @property float $discount
 * @property int $basketRuleDiscountTypeId
 */

class BasketRuleTemplate
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $basketRuleConditionId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\BasketRuleCondition")
     * @ORM\JoinColumn(name="conditionId", referencedColumnName="id")
     */
    protected $basketRuleCondition;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    protected $basketRuleDiscountTypeId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\BasketRuleDiscountType")
     * @ORM\JoinColumn(name="basketRuleDiscountTypeId", referencedColumnName="id")
     */
    protected $basketRuleDiscountType;

    /**
     * @param mixed $basketRuleCondition
     */
    public function setBasketRuleCondition($basketRuleCondition)
    {
        $this->basketRuleCondition = $basketRuleCondition;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleCondition()
    {
        return $this->basketRuleCondition;
    }

    /**
     * @param mixed $basketRuleConditionId
     */
    public function setBasketRuleConditionId($basketRuleConditionId)
    {
        $this->basketRuleConditionId = $basketRuleConditionId;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleConditionId()
    {
        return $this->basketRuleConditionId;
    }

    /**
     * @param mixed $basketRuleDiscountType
     */
    public function setBasketRuleDiscountType($basketRuleDiscountType)
    {
        $this->basketRuleDiscountType = $basketRuleDiscountType;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleDiscountType()
    {
        return $this->basketRuleDiscountType;
    }

    /**
     * @param mixed $basketRuleDiscountTypeId
     */
    public function setBasketRuleDiscountTypeId($basketRuleDiscountTypeId)
    {
        $this->basketRuleDiscountTypeId = $basketRuleDiscountTypeId;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleDiscountTypeId()
    {
        return $this->basketRuleDiscountTypeId;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


}