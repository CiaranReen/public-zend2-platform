<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voucher Table
 *
 * @ORM\Entity
 * @ORM\Table(name="voucher")
 * @property int $id
 * @property string $name
 * @property string $amount
 * @property string $operand
 * @property string $code
 * @property string $status
 * @property string $start
 * @property string $expire
 */
class Voucher
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=100)
     */
    protected $name ;

    /**
     * @ORM\Column(length=10)
     */
    protected $amount;

    /**
     * @ORM\Column(length=1)
     */
    protected $operand;

    /**
     * @ORM\Column(length=55)
     */
    protected $code;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    protected $is_active;

    /**
     * @ORM\Column(type="date")
     */
    protected $start;

    /**
     * @ORM\Column(type="date")
     */
    protected $expire;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Voucher")
     * @ORM\JoinTable(name="voucherUser",
     *      joinColumns={@ORM\JoinColumn(name="userId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="voucherId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $vouchersUsers;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Voucher")
     * @ORM\JoinTable(name="voucherUserGroup",
     *      joinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="voucherId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $voucherUserGroups;

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $expire
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;
    }

    /**
     * @return mixed
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $operand
     */
    public function setOperand($operand)
    {
        $this->operand = $operand;
    }

    /**
     * @return mixed
     */
    public function getOperand()
    {
        return $this->operand;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $voucherUserGroups
     */
    public function setVoucherUserGroups($voucherUserGroups)
    {
        $this->voucherUserGroups = $voucherUserGroups;
    }

    /**
     * @return mixed
     */
    public function getVoucherUserGroups()
    {
        return $this->voucherUserGroups;
    }

    /**
     * @param mixed $vouchersUsers
     */
    public function setVouchersUsers($vouchersUsers)
    {
        $this->vouchersUsers = $vouchersUsers;
    }

    /**
     * @return mixed
     */
    public function getVouchersUsers()
    {
        return $this->vouchersUsers;
    }

}