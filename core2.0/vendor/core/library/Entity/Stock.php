<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stock Table
 *
 * @ORM\Entity
 * @ORM\Table(name="stock")
 * @property int $id
 * @property int $productId
 * @property string $code
 * @property int $optionId
 * @property int $colourwayId
 * @property float $price
 * @property int $stockLevel
 * @property int $stockWarning
 */

class Stock
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $productId;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $optionId;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $colourwayId;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stockLevel;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stockWarning;
}