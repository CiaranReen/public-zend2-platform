<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Poll Product Table
 *
 * @ORM\Entity
 * @ORM\Table(name="pollProduct")
 * @property int $id
 * @property string $product1
 * @property string $product2
 * @property string $product3
 */

class PollProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=30)
     */
    protected $product1;

    /**
     * @ORM\Column(length=30)
     */
    protected $product2;

    /**
     * @ORM\Column(length=30)
     */
    protected $product3;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $product1
     */
    public function setProduct1($product1)
    {
        $this->product1 = $product1;
    }

    /**
     * @return mixed
     */
    public function getProduct1()
    {
        return $this->product1;
    }

    /**
     * @param mixed $product2
     */
    public function setProduct2($product2)
    {
        $this->product2 = $product2;
    }

    /**
     * @return mixed
     */
    public function getProduct2()
    {
        return $this->product2;
    }

    /**
     * @param mixed $product3
     */
    public function setProduct3($product3)
    {
        $this->product3 = $product3;
    }

    /**
     * @return mixed
     */
    public function getProduct3()
    {
        return $this->product3;
    }


}