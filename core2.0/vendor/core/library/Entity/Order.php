<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order Table
 *
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 * @property int $id
 * @property string $orderNumber
 * @property int $userId
 * @property int $billingAddressId
 * @property int $deliveryAddressId
 * @property int $currencyId
 * @property float $itemsTotal
 * @property float $deliveryCharge
 * @property float $vatCharge
 * @property float $discount
 * @property float $orderTotal
 * @property string $dateOrdered
 * @property string $dateProcessed
 * @property string $status
 * @property float $amountOutstanding
 * @property string $paymentType
 */

class Order
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $orderNumber;

    /**
     * @ORM\Column(type="integer")
     */
    protected $userId;


    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\User")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="integer")
     */
    protected $billingAddressId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Address")
     * @ORM\JoinColumn(name="billingAddressId", referencedColumnName="id")
     */
    protected $billingAddress;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $deliveryAddressId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $currencyId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Currency")
     * @ORM\JoinColumn(name="currencyId", referencedColumnName="id")
     */
    protected $currency;

    /**
     * @ORM\Column(type="float")
     */
    protected $itemsTotal;

    /**
     * @ORM\Column(type="float")
     */
    protected $deliveryCharge;

    /**
     * @ORM\Column(type="float")
     */
    protected $vatCharge;

    /**
     * @ORM\Column(type="float")
     */
    protected $discount;

    /**
     * @ORM\Column(type="float")
     */
    protected $orderTotal;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateOrdered;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateProcessed;

    /**
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @ORM\Column(type="float")
     */
    protected $amountOutstanding;

    /**
     * @ORM\Column(type="string")
     */
    protected $paymentType;

    /**
    * @ORM\OneToMany(targetEntity="Core\Entity\OrderNote", mappedBy="order")
    */
    protected $orderNotes;

    /**
     * @ORM\OneToMany(targetEntity="Core\Entity\OrderPart", mappedBy="order")
     */
    protected $orderParts;

    /**
     * @param mixed $amountOutstanding
     */
    public function setAmountOutstanding($amountOutstanding)
    {
        $this->amountOutstanding = $amountOutstanding;
    }

    /**
     * @return mixed
     */
    public function getAmountOutstanding()
    {
        return $this->amountOutstanding;
    }

    /**
     * @param mixed $billingAddressId
     */
    public function setBillingAddressId($billingAddressId)
    {
        $this->billingAddressId = $billingAddressId;
    }

    /**
     * @return mixed
     */
    public function getBillingAddressId()
    {
        return $this->billingAddressId;
    }

    /**
     * @param mixed $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return mixed
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param mixed $dateOrdered
     */
    public function setDateOrdered($dateOrdered)
    {
        $this->dateOrdered = $dateOrdered;
    }

    /**
     * @return mixed
     */
    public function getDateOrdered()
    {
        return $this->dateOrdered;
    }

    /**
     * @param mixed $dateProcessed
     */
    public function setDateProcessed($dateProcessed)
    {
        $this->dateProcessed = $dateProcessed;
    }

    /**
     * @return mixed
     */
    public function getDateProcessed()
    {
        return $this->dateProcessed;
    }

    /**
     * @param mixed $deliveryAddressId
     */
    public function setDeliveryAddressId($deliveryAddressId)
    {
        $this->deliveryAddressId = $deliveryAddressId;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddressId()
    {
        return $this->deliveryAddressId;
    }

    /**
     * @param mixed $deliveryCharge
     */
    public function setDeliveryCharge($deliveryCharge)
    {
        $this->deliveryCharge = $deliveryCharge;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCharge()
    {
        return $this->deliveryCharge;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $itemsTotal
     */
    public function setItemsTotal($itemsTotal)
    {
        $this->itemsTotal = $itemsTotal;
    }

    /**
     * @return mixed
     */
    public function getItemsTotal()
    {
        return $this->itemsTotal;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderTotal
     */
    public function setOrderTotal($orderTotal)
    {
        $this->orderTotal = $orderTotal;
    }

    /**
     * @return mixed
     */
    public function getOrderTotal()
    {
        return $this->orderTotal;
    }

    /**
     * @param mixed $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $vatCharge
     */
    public function setVatCharge($vatCharge)
    {
        $this->vatCharge = $vatCharge;
    }

    /**
     * @return mixed
     */
    public function getVatCharge()
    {
        return $this->vatCharge;
    }

    /**
     * @param mixed $billingAddress
     */
    public function setBillingAddress($billingAddress)
    {
        $this->billingAddress = $billingAddress;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $orderNotes
     */
    public function setOrderNotes($orderNotes)
    {
        $this->orderNotes = $orderNotes;
    }

    /**
     * @return mixed
     */
    public function getOrderNotes()
    {
        return $this->orderNotes;
    }

    /**
     * @param mixed $orderParts
     */
    public function setOrderParts($orderParts)
    {
        $this->orderParts = $orderParts;
    }

    /**
     * @return mixed
     */
    public function getOrderParts()
    {
        return $this->orderParts;
    }

    public function __construct()
    {
        $this->orderParts = new Doctrine\Common\Collections\ArrayCollection();
        $this->orderNotes = new Doctrine\Common\Collections\ArrayCollection();
    }

}