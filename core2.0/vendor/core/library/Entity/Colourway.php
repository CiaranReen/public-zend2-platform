<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Colourway Table
 *
 * @ORM\Entity
 * @ORM\Table(name="colourway")
 * @property int $id
 * @property string $name
 * @property string $hex
 * @property string $code
 */

class Colourway
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $hex;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $hex
     */
    public function setHex($hex)
    {
        $this->hex = $hex;
    }

    /**
     * @return mixed
     */
    public function getHex()
    {
        return $this->hex;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}