<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * History Table
 *
 * @ORM\Entity(repositoryClass="Core\Repository\HistoryRepository")
 * @ORM\Table(name="history")
 * @property int $id
 * @property string $action
 * @property string $type
 * @property int $userId
 * @property string $name
 * */

class History
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=55)
     */
    protected $action;

    /**
     * @ORM\Column(length=55)
     */
    protected $type;

    /**
     * @ORM\Column(type="integer")
     * FOREIGN KEY
     */
    protected $userId;

    /**
     * @ORM\Column(length=55)
     */
    protected $name;

    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

}