<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BasketRule Table
 *
 * @ORM\Entity
 * @ORM\Table(name="basketRule")
 * @property int $id
 * @property string $name
 * @property int $conditionId
 * @property int $quantity
 * @property int $quantityMax
 * @property float $discount
 * @property int $discountTypeId
 * @property string $startDate
 * @property string $endDate
 * @property int $priority
 */

class BasketRule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="integer")
     */
    protected $basketRuleConditionId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\BasketRuleCondition")
     * @ORM\JoinColumn(name="conditionId", referencedColumnName="id")
     */
    protected $basketRuleCondition;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantityMax;

    /**
     * @ORM\Column(type="float")
     */
    protected $discount;

    /**
     * @ORM\Column(type="integer")
     */
    protected $basketRuleDiscountTypeId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\BasketRuleDiscountType")
     * @ORM\JoinColumn(name="basketRuleDiscountTypeId", referencedColumnName="id")
     */
    protected $basketRuleDiscountType;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $startDate;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $endDate;

    /**
     * @ORM\Column(type="integer")
     */
    protected $priority;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\Product")
     * @ORM\JoinTable(name="basketRuleProduct",
     *      joinColumns={@ORM\JoinColumn(name="basketRuleId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="productId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $basketRuleProducts;

    /**
     * @ORM\ManyToMany(targetEntity="Core\Entity\UserGroup")
     * @ORM\JoinTable(name="basketRuleUserGroup",
     *      joinColumns={@ORM\JoinColumn(name="basketRuleId", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="userGroupId", referencedColumnName="id", unique=true)}
     *      )
     **/
    protected $basketRuleUserGroups;

    /**
     * @param mixed $basketRuleCondition
     */
    public function setBasketRuleCondition($basketRuleCondition)
    {
        $this->basketRuleCondition = $basketRuleCondition;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleCondition()
    {
        return $this->basketRuleCondition;
    }

    /**
     * @param mixed $basketRuleConditionId
     */
    public function setBasketRuleConditionId($basketRuleConditionId)
    {
        $this->basketRuleConditionId = $basketRuleConditionId;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleConditionId()
    {
        return $this->basketRuleConditionId;
    }

    /**
     * @param mixed $basketRuleDiscountTypeId
     */
    public function setBasketRuleDiscountTypeId($basketRuleDiscountTypeId)
    {
        $this->basketRuleDiscountTypeId = $basketRuleDiscountTypeId;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleDiscountTypeId()
    {
        return $this->basketRuleDiscountTypeId;
    }

    /**
     * @param mixed $conditionId
     */
    public function setConditionId($conditionId)
    {
        $this->conditionId = $conditionId;
    }

    /**
     * @return mixed
     */
    public function getConditionId()
    {
        return $this->conditionId;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param mixed $discountTypeId
     */
    public function setDiscountTypeId($discountTypeId)
    {
        $this->discountTypeId = $discountTypeId;
    }

    /**
     * @return mixed
     */
    public function getDiscountTypeId()
    {
        return $this->discountTypeId;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantityMax
     */
    public function setQuantityMax($quantityMax)
    {
        $this->quantityMax = $quantityMax;
    }

    /**
     * @return mixed
     */
    public function getQuantityMax()
    {
        return $this->quantityMax;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $basketRuleDiscountType
     */
    public function setBasketRuleDiscountType($basketRuleDiscountType)
    {
        $this->basketRuleDiscountType = $basketRuleDiscountType;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleDiscountType()
    {
        return $this->basketRuleDiscountType;
    }

    /**
     * @param mixed $basketRuleProducts
     */
    public function setBasketRuleProducts($basketRuleProducts)
    {
        $this->basketRuleProducts = $basketRuleProducts;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleProducts()
    {
        return $this->basketRuleProducts;
    }

    /**
     * @param mixed $basketRuleUserGroups
     */
    public function setBasketRuleUserGroups($basketRuleUserGroups)
    {
        $this->basketRuleUserGroups = $basketRuleUserGroups;
    }

    /**
     * @return mixed
     */
    public function getBasketRuleUserGroups()
    {
        return $this->basketRuleUserGroups;
    }


    public function __construct()
    {
        $this->basketRuleProducts = new Doctrine\Common\Collections\ArrayCollection();
        $this->basketRuleUserGroups = new Doctrine\Common\Collections\ArrayCollection();
    }

}