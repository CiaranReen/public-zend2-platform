<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Past Poll Table
 *
 * @ORM\Entity
 * @ORM\Table(name="pastPoll")
 * @property int $id
 * @property string $title
 * @property int $submissions
 * @property date $start
 * @property date $end
 * @property string $product1
 * @property string $product2
 * @property string $product3
 * @property string $graphUrl
 */

class PastPoll
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=25)
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $submissions;

    /**
     * @ORM\Column(type="date")
     */
    protected $start;

    /**
     * @ORM\Column(type="date")
     */
    protected $end;

    /**
     * @ORM\Column(length=30)
     */
    protected $product1;

    /**
     * @ORM\Column(length=30)
     */
    protected $product2;

    /**
     * @ORM\Column(length=30)
     */
    protected $product3;

    /**
     * @ORM\Column(length=30)
     */
    protected $graphUrl;

    /**
     * @param mixed $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param mixed $graphUrl
     */
    public function setGraphUrl($graphUrl)
    {
        $this->graphUrl = $graphUrl;
    }

    /**
     * @return mixed
     */
    public function getGraphUrl()
    {
        return $this->graphUrl;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $product1
     */
    public function setProduct1($product1)
    {
        $this->product1 = $product1;
    }

    /**
     * @return mixed
     */
    public function getProduct1()
    {
        return $this->product1;
    }

    /**
     * @param mixed $product2
     */
    public function setProduct2($product2)
    {
        $this->product2 = $product2;
    }

    /**
     * @return mixed
     */
    public function getProduct2()
    {
        return $this->product2;
    }

    /**
     * @param mixed $product3
     */
    public function setProduct3($product3)
    {
        $this->product3 = $product3;
    }

    /**
     * @return mixed
     */
    public function getProduct3()
    {
        return $this->product3;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $submissions
     */
    public function setSubmissions($submissions)
    {
        $this->submissions = $submissions;
    }

    /**
     * @return mixed
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

}