<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BasketRuleDiscountType Table
 *
 * @ORM\Entity
 * @ORM\Table(name="basketRuleDiscountType")
 * @property int $id
 * @property string $name
 * @property string $shorthand
 */

class BasketRuleDiscountType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $shorthand;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $shorthand
     */
    public function setShorthand($shorthand)
    {
        $this->shorthand = $shorthand;
    }

    /**
     * @return mixed
     */
    public function getShorthand()
    {
        return $this->shorthand;
    }

}