<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country Table
 *
 * @ORM\Entity
 * @ORM\Table(name="country")
 * @property int $id
 * @property string $country
 * @property string $iso
 * @property int $currencyId
 * @property int $languageId
 * */

class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=30)
     */
    protected $country;

    /**
     * @ORM\Column(length=5)
     */
    protected $iso;

    /**
     * @ORM\Column(type="integer")
     */
    protected $currencyId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $languageId;


    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Language")
     * @ORM\JoinColumn(name="languageId", referencedColumnName="id")
     */
    protected $language;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Currency")
     * @ORM\JoinColumn(name="currencyId", referencedColumnName="id")
     */
    protected $currency;

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $currencyId
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
    }

    /**
     * @return mixed
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $iso
     */
    public function setIso($iso)
    {
        $this->iso = $iso;
    }

    /**
     * @return mixed
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * @param mixed $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;
    }

    /**
     * @return mixed
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }



}