<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendor Product Table
 *
 * @ORM\Entity
 * @ORM\Table(name="vendorProduct")
 * @property int $id
 * @property string $vendorPartNo
 * @property string $description
 * @property int $vendorManufacturerId
 * @property string $manufacturerPartNo
 * @property float $cost
 */

class VendorProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=50)
     */
    protected $vendorPartNo;

    /**
     * @ORM\Column(length=512)
     */
    protected $description;

    /**
     * @ORM\Column(type="integer")
     */
    protected $vendorManufacturerId;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\VendorManufacturer")
     * @ORM\JoinColumn(name="vendorManufacturerId", referencedColumnName="id")
     */
    protected $vendorManufacturer;

    /**
     * @ORM\Column(length=50)
     */
    protected $manufacturerPartNo;

    /**
     * @ORM\Column(type="float")
     */
    protected $cost;

    /**
     * @param mixed $cost
     */
    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    /**
     * @return mixed
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $vendorManufacturer
     */
    public function setVendorManufacturer($vendorManufacturer)
    {
        $this->vendorManufacturer = $vendorManufacturer;
    }

    /**
     * @return mixed
     */
    public function getVendorManufacturer()
    {
        return $this->vendorManufacturer;
    }

    /**
     * @param mixed $vendorManufacturerId
     */
    public function setVendorManufacturerId($vendorManufacturerId)
    {
        $this->vendorManufacturerId = $vendorManufacturerId;
    }

    /**
     * @return mixed
     */
    public function getVendorManufacturerId()
    {
        return $this->vendorManufacturerId;
    }

    /**
     * @param mixed $manufacturerPartNo
     */
    public function setManufacturerPartNo($manufacturerPartNo)
    {
        $this->manufacturerPartNo = $manufacturerPartNo;
    }

    /**
     * @return mixed
     */
    public function getManufacturerPartNo()
    {
        return $this->manufacturerPartNo;
    }

    /**
     * @param mixed $vendorPartNo
     */
    public function setVendorPartNo($vendorPartNo)
    {
        $this->vendorPartNo = $vendorPartNo;
    }

    /**
     * @return mixed
     */
    public function getVendorPartNo()
    {
        return $this->vendorPartNo;
    }

}