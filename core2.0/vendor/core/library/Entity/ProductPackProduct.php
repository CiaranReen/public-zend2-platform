<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product Pack Product Table
 *
 * @ORM\Entity
 * @ORM\Table(name="productPackProduct")
 * @property int $id
 * @property int $productPackId
 * @property int $productId
 */

class ProductPackProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $productPackId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $productId;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productPackId
     */
    public function setProductPackId($productPackId)
    {
        $this->productPackId = $productPackId;
    }

    /**
     * @return mixed
     */
    public function getProductPackId()
    {
        return $this->productPackId;
    }

}