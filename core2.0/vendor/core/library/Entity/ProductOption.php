<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductOption Table
 *
 * @ORM\Entity
 * @ORM\Table(name="productOption")
 * @property int $id
 * @property string $option
 * @property string $shortCode
 */

class ProductOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $option;

    /**
     * @ORM\Column(type="integer")
     */
    protected $shortCode;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param mixed $shortCode
     */
    public function setShortCode($shortCode)
    {
        $this->shortCode = $shortCode;
    }

    /**
     * @return mixed
     */
    public function getShortCode()
    {
        return $this->shortCode;
    }

}