<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Submodule Table
 *
 * @ORM\Entity
 * @ORM\Table(name="submodule")
 * @property int $id
 * @property int $name
 * @property int $path
 */

class Submodule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=20)
     */
    protected $name;

    /**
     * @ORM\Column(length=30)
     */
    protected $path;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

}