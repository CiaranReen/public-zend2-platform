<?php
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vendor Manufacturer Table
 *
 * @ORM\Entity
 * @ORM\Table(name="vendorManufacturer")
 * @property int $id
 * @property string $name
 */

class VendorManufacturer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(length=50)
     */
    protected $name;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


}