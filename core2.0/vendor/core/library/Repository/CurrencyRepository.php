<?php
namespace Core\Repository;

use Core\Entity\Currency;
use Core\Repository\User;
use Doctrine\ORM\EntityRepository;

/**
 * Currency Repository
 */

class CurrencyRepository extends EntityRepository
{

    /**
     * @return Currency
     */
    public function load()
    {
        $currencyModel = new Currency();
        $userModel = new User();

        if ($userModel->isLoggedIn()) {
            $qb = $this->_em->createQueryBuilder();
            $userId = $userModel->getUserSession()->userId;
            $qb->select(array('c', 'u'))
                ->from('Core\Entity\Currency', 'c')
                ->innerJoin('Core\Entity\User', 'u', 'WITH', 'u.currencyId = c.currencyId')
                ->where('u.id='.$userId);
            $row = $qb->getQuery()->getResult();

            $currencyModel->setId($row['currency_id']);
            $currencyModel->setName($row['currency']);
            $currencyModel->setSymbol($row['symbol']);
            $currencyModel->setStaticValue($row['static_value']);

            if ($row === null) {
                $currencyModel->setId(1);
                $currencyModel->setName('British Pound');
                $currencyModel->setSymbol('&pound;');
            }
        } else {
            $currencyModel->setId(1);
            $currencyModel->setName('British Pound');
            $currencyModel->setSymbol('&pound;');
        }
        return $currencyModel;
    }

    /**
     * @return array
     */
    public function getCurrencyMode()
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('currencyMode')
            ->from('Core\Entity\Setting', 's');
        return $qb->getQuery()->getResult();
    }

    /**
     * @param $currencyId
     * @return bool
     */
    public function getStaticRates($currencyId)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('staticValue')
            ->from('Core\Entity\Currency', 'c')
            ->where('c.currencyId = ?', $currencyId);

        $result = $qb->getQuery()->getResult();

        if (count($result) > 0) {
            foreach ($result as $r) {
                $exchangeAmount = $r;
            }
            return $exchangeAmount;
        }
        else {
            return false;
        }
    }

    /**
     * Sets the conversion rate for the currency model
     * @param $currencyId
     * @return float|mixed
     */
    public function setConversionRate($currencyId) {
        $currencyMode = $this->getCurrencyMode();

        if ($currencyMode->currency_mode == '0') {
            switch ($currencyId) {
                case 2:
                    $convFrom = 'gbp';
                    $convTo = 'usd';
                    $amount = 1;
                    break;
                case 3:
                    $convFrom = 'gbp';
                    $convTo = 'eur';
                    $amount = 1;
                    break;
                default:
                    $convFrom = 'gbp';
                    $convTo = 'gbp';
                    $amount = 1;
                    break;
            }
            if ($convFrom != $convTo) {
                $exchangeAmount = $this->getConversionRate($convFrom, $convTo, $amount = 1);
            } else {
                $exchangeAmount = 1.00;
            }
        } else {
            $exchangeAmount = $this->getStaticRates($currencyId);
        }
        return $exchangeAmount;
    }

    /**
     * Gets the conversion rate
     * @param $convFrom
     * @param $convTo
     * @param int $amount
     * @return mixed
     */
    public function getConversionRate($convFrom, $convTo, $amount = 1) {
        $get = file_get_contents(("https://www.google.com/finance/converter?a=$amount&from=$convFrom&to=$convTo"));


        $get = explode("<span class=bld>", $get);
        if (isset($get[1])) {
            $get = explode("</span>", $get[1]);
        } else {
            $get = explode("</span>", $get[0]);
        }
        $price = preg_replace("/[^0-9\.]/", null, $get[0]);
        return $price;
    }

}