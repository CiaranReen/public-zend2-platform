<?php
namespace Core\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Setting Repository
 */

class SocialMediaRepository extends EntityRepository
{
    /**
     * @return mixed
     */
    public function getActiveSocialMedia()
    {
        $active = '1';
        $qb = $this->_em->createQueryBuilder();
        $qb->select('sm')
            ->from('Core\Entity\SocialMedia', 'sm')
            ->where('isActive = ?', $active);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     */
    public function getAvailableSocialMedia()
    {
        $active = '0';
        $qb = $this->_em->createQueryBuilder();
        $qb->select('sm')
            ->from('Core\Entity\SocialMedia', 'sm')
            ->where('isActive = ?', $active);

        return $qb->getQuery()->getResult();
    }

}