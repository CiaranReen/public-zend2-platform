<?php
namespace Core\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Voucher Repository
 */

class VoucherRepository extends EntityRepository
{
    public function checkVoucher($voucher)
    {
        $currentDate = date("Y-m-d");
        $qb = $this->_em->createQueryBuilder();

        $qb->select('v')
            ->from('Core\Entity\Voucher', 'v')
            ->where('code = ?', $voucher)
            ->where('start <= ?', $currentDate)
            ->where('expire >= ?', $currentDate);

        return $qb->getQuery()->getResult();
    }

}