<?php
namespace Core\Repository;

use Core\Entity\VendorProduct;
use Doctrine\ORM\EntityRepository;

/**
 * Vendor Product Repository
 */

class VendorProductRepository extends EntityRepository
{

    public function getSearch($string)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('vp')
            ->from('Core\Entity\VendorProduct', 'vp')
            ->where('description LIKE ?', '%' . $string . '%')
            ->orWhere('vendorPartNo LIKE ?', '%' . $string . '%')
            ->orWhere('manufacturerPartNo LIKE ?', '%' . $string . '%');

        return $qb->getQuery()->getResult();
    }
}