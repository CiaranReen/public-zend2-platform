<?php
namespace Core\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * History Repository
 */

class HistoryRepository extends EntityRepository
{
    /**
     * This is the function for the admin index page
     */
    public function fetchLatestTen()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select(array('h', 'u'))
            ->from('Core\Entity\History', 'h')
            ->innerJoin('Core\Entity\User', 'u', 'WITH', 'u.id = h.userId')
            ->orderBy('h.id');

        return $qb->getQuery()->getResult();
    }
}