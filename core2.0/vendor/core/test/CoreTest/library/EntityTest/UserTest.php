<?php
namespace CoreTest\EntityTest;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use PHPUnit_Framework_TestCase;
use Doctrine\ORM\EntityManager;
use Core\Entity\User;

class UserTest extends PHPUnit_Framework_TestCase
{

    public function testUserEntityCanBeRetrievedFromDatabaseUsingId()
    {
        $emMock  = $this->getMock('\Doctrine\ORM\EntityManager',
            array('getRepository', 'getClassMetadata', 'persist', 'flush'), array(), '', false);
        $user = $emMock->find('Core\Entity\User', 1);
        $this->assertEquals(1, $user->getId());

    }

}